require('babel-polyfill');
var $ = require('jquery');

require('./vendors/jquery-ui/jquery-ui.min.js');
require('./vendors/jquery-ui-touch/jquery.ui.touch-punch.min.js');

require('tether/dist/js/tether.js');
require('bootstrap/dist/js/bootstrap.min.js');

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var sanitize = require('angular-sanitize');
var uiMask = require('angular-ui-mask');
var ngMessages = require('angular-messages');
var AppComponent = require('./app.component.js');
var Shared = require('./__shared/shared');
var SignupPages = require('./signup/signuppages');
// var nuskinRegister = require('./nuskin-register/register');
var CommerceComponent = require('./commerce/commerce');
var digitalToolkitComponents = require('./digital-toolkit/digital.toolkit');
var prospectTargetingComponent = require('./prospect-targeting/prospect.targeting');
var Styles = require('./stylesheets/base.scss');
var SlickStyle = require('./stylesheets/vendors/slick/slick.scss');
var SlickThemeStyle = require('./stylesheets/vendors/slick/slick-theme.scss');
var homescreenpage = require('./homescreen/pages/homescreen');
var ngSortable = require('./vendors/ng-sortable/ng-sortable.min.js');

angular.module('myApp', [
        uiRouter,
        Shared.name,
        SignupPages.name,
        // Components.name,
        CommerceComponent.name,
        //nuskinRegister.name,
        digitalToolkitComponents.name,
        homescreenpage.name,
        prospectTargetingComponent.name,
        sanitize,
        uiMask,
        ngMessages,
    ])
    .component('app', AppComponent);
