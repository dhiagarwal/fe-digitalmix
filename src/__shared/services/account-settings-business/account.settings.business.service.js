'use strict';

accountSettingsBusinessService.$inject = ['$http','$q'];

function accountSettingsBusinessService($http,$q) {
	this.$http = $http;

	this.loadDetails = function() {
		var deferred = $q.defer();
		$http.get('../../../src/__shared/services/account-settings-business/account.settings.business.json').then(function(response) {
			deferred.resolve(response.data);
		});
		return deferred.promise;
	};

	this.uploadPhoto = function(files){
		var deferred = $q.defer();
		$http.get('../../../src/__shared/services/account-settings-business/account.settings.business.fileupload.json').then(function(response) {
			deferred.resolve(response.data.data.upload);
		});
		return deferred.promise;
	};

	this.removePhoto = function() {
		var deferred = $q.defer();
		$http.get('../../../src/__shared/services/account-settings-business/account.settings.business.fileupload.json').then(function(response) {
			deferred.resolve(response.data.data.remove);
		});
		return deferred.promise;
	};

};

module.exports = accountSettingsBusinessService;