'use strict';
var angular = require('angular');
var accountSettingsBusinessService = require('./account.settings.business.service');

var accountSettingsBusinessServiceModule = angular.module('accountSettingBusinessServiceModule', [])
                  .service('accountSettingsBusinessService', accountSettingsBusinessService)
                 

module.exports = accountSettingsBusinessServiceModule;
