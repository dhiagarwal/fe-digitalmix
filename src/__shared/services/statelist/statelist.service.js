'use strict';

statelistservice.$inject = ['$http','$q'];

function statelistservice($http,$q) {
  this.$http = $http;

  this.loadDetails = function() {
    var deferred = $q.defer();
    $http.get('../../../src/__shared/services/statelist/statelist.json').then(function(response) {
      deferred.resolve(response.data);
    });
    return deferred.promise;
  }


};

module.exports = statelistservice;
