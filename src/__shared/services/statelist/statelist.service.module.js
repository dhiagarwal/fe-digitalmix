'use strict';
var angular = require('angular');
var statelistservice = require('./statelist.service');

var statelistServiceModule = angular.module('statelistServiceModule', [])
                  .service('statelistservice', statelistservice)
                 

module.exports = statelistServiceModule;