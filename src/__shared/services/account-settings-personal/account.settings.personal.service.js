'use strict';

accountSettingsPersonalService.$inject = ['$http','$q'];

function accountSettingsPersonalService($http,$q) {
  this.$http = $http;

  this.loadDetails = function() {
  	var deferred = $q.defer();
	  $http.get('../../../src/__shared/services/account-settings-personal/account.settings.personal.json').then(function(response) {
	    deferred.resolve(response.data);
	  });
    return deferred.promise;
  };

  	this.uploadPhoto = function(files){
		var deferred = $q.defer();
		$http.get('../../../src/__shared/services/account-settings-personal/account.settings.personal.fileupload.json').then(function(response) {
			deferred.resolve(response.data.data.upload);
		});
		return deferred.promise;
	};

	this.removePhoto = function() {
		var deferred = $q.defer();
		$http.get('../../../src/__shared/services/account-settings-personal/account.settings.personal.fileupload.json').then(function(response) {
			deferred.resolve(response.data.data.remove);
		});
		return deferred.promise;
	};

};

module.exports = accountSettingsPersonalService;