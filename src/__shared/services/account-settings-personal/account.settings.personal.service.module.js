'use strict';
var angular = require('angular');
var accountSettingsPersonalService = require('./account.settings.personal.service');

var accountSettingsPersonalServiceModule = angular.module('accountSettingsPersonalServiceModule', [])
                  .service('accountSettingsPersonalService', accountSettingsPersonalService)
                 

module.exports = accountSettingsPersonalServiceModule;
