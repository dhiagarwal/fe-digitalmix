'use strict';

globalServicesService.$inject = ['$http','$q'];

function globalServicesService($http, $q) {
    this.$http = $http;
    this.deferred = $q.defer();
    
    this.shortenArray = function(sourceArray, targetSize) {
        var sourceArray = sourceArray;
        var targetArray = [];
        for (var key in sourceArray) {
            if(key < targetSize) {
                targetArray.push(sourceArray[key]);
            }
        }
        return targetArray;
    };
};

module.exports = globalServicesService;
