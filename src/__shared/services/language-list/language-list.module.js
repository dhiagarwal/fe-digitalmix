'use strict';
var angular = require('angular');
var languageList = require('./language-list.service');

var languageListModule = angular.module('languageListModule', [])
    .service('languageListService', languageList)

module.exports = languageListModule;