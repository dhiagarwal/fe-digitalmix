'use strict';

languageListService.$inject = ['$http', '$q'];

function languageListService($http, $q) {
    this.$http = $http;

    this.getLanguageList = function () {
        var deferred = $q.defer();
        $http.get('../../../src/__shared/services/language-list/language-list.json').then(function (response) {
            deferred.resolve(response.data);
        });
        return deferred.promise;
    }

};

module.exports = languageListService;