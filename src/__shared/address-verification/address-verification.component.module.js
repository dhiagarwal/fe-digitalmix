'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var addessVerificationComponent = require('./address-verification.component');
var addessVerificationComponentService = require('./address-verification.component.service');
var formUtils = require('../../../src/__shared/form-utils/form-utils');
require('./address-verification.component.scss');

var addessVerificationComponentModule = angular.module('addessVerificationComponentModule', [uiRouter, formUtils.name])
		.component('addessverificationcomponent', addessVerificationComponent)
		.service('addessVerificationComponentService', addessVerificationComponentService)  

module.exports = addessVerificationComponentModule;