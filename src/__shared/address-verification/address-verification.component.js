'use strict';
var controller = require('./address-verification.component.controller');

var addressVerificationComponent = {
	controller: controller,
	controllerAs: 'avCtrl',
    bindings: {
        isModalCall: '@',
		addressData: '=',
		callbackOnSuccess: '@',
		booleanVariableToInvokeModal: '@'
    }
};

module.exports = addressVerificationComponent;