'use strict';
var modalVerificationTemplate = require('./address-verification.component.modal.verification.jade');
function addessVerificationController($scope, addessVerificationComponentService, $uibModal) {
    this.$uibModal = $uibModal;
    var self = this;
    self.isAddressListVisible = false; 

    var promise = addessVerificationComponentService.getJSONData();
    promise.then(function (response) {
        self.addressList = response.modalData.addressList;
    });

    self.modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };

    self.addressVerificationModalController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.model.addressList = self.addressList;

        modalSelf.addressData = self.addressData;
        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.goBackAndEdit = function() {
            var resetBooleanVariable = '$scope.$parent.' + self.booleanVariableToInvokeModal + ' = false;';
            eval(resetBooleanVariable);
            modalSelf.cancel();
        };

        modalSelf.goWithSelectedAddress = function (isValid, selectedObject) {
            if (isValid) {
                modalSelf.useEnteredAddress(selectedObject);
            }
        };

        modalSelf.useEnteredAddress = function (addressDataObj) {
            var resetBooleanVariable = '$scope.$parent.' + self.booleanVariableToInvokeModal + ' = false;';
            eval(resetBooleanVariable);
            modalSelf.finalAddressData = addressDataObj ? addressDataObj : modalSelf.addressData;
            modalSelf.ok();
            var setCallBackMethod = '$scope.$parent.' + self.callbackOnSuccess + '(modalSelf.finalAddressData)';
            eval(setCallBackMethod);
        };
    };

    if (self.isModalCall) {
        self.modalPopupInstance(modalVerificationTemplate, self.addressVerificationModalController, 'app-modal-window address-verification-popup');
    } 

};

module.exports = addessVerificationController;