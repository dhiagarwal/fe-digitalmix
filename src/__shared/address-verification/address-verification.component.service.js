'use strict';

addessVerificationComponentService.$inject = ['$http','$q'];

function addessVerificationComponentService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();

	this.getJSONData = function() {
    	$http.get('../../../../src/__shared/address-verification/address-verification.component.json').then(function(response) {
    		deferred.resolve(response.data);
  		});
    	return deferred.promise;
	};
};

module.exports = addessVerificationComponentService;