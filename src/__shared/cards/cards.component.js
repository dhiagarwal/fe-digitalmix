'use strict';
var template = require('./cards.jade');

var cardsComponent =  {
    bindings: {
      toDoCards : '<',
      show : '=',
      dots: '<',
      arrows: '<',
      cardsCount: '='
    },
    templateUrl: template,
    controller: cardsController,
    controllerAs: 'cards',
};

cardsController.inject = ["$scope", "$timeout", "$window"];

function cardsController($scope, $timeout, $window){

    var self = this;

    this.isShown = false;

    $scope.currentIndex = 0;
    $scope.changedCount = 0;
    this.favourite = this.favourite || false;
    this.dots = this.dots || false;
    this.arrows = this.arrows || false;

    $scope.slickCardsConfig = {
      enabled: true,
      infinite: true,
      adaptiveHeight: false,
      mobileFirst: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: this.arrows,
      dots: this.dots,
      speed: 500,
      method: {
      },
      responsive: [
        {
          breakpoint: 0,
          settings: {
            arrows: this.arrows,
            draggable: true
          }
        }
      ],
      event: {
        afterChange: function (event, slick, currentSlide, nextSlide) {
          $scope.currentIndex = currentSlide; // save current index each time
        },
        init: function (event, slick) {
          slick.slickGoTo($scope.currentIndex); // slide to correct index when init
          $scope.changedCount = self.toDoCards.length;
          $scope.nextCount = self.toDoCards.length;
        }
      }
    };

    $scope.removeSlide = function(event, targetSlide) {
        var slickHeight = angular.element('#slickCard').height();
        var bgCayManHeight = angular.element(event.currentTarget).closest('.bg-cayman').innerHeight();
        angular.element('#slickCard').css({'height': (slickHeight/16) + 'rem'});
        angular.element(event.currentTarget).closest('.bg-cayman').css({'height': (bgCayManHeight/16) + 'rem', 'max-height': (bgCayManHeight/16) + 'rem', 'overflow': 'hidden'});
	      self.toDoCards.splice(self.toDoCards.indexOf(targetSlide), 1);
        angular.element('#slickCard .cards-section').css({'opacity': 0});
        $timeout(function() {
          if(self.toDoCards && self.toDoCards.length > 0) {
        		angular.element("#slickCard").slick("slickRemove").slick("slickAdd");
        		angular.element('#slickCard .cards-section').animate({'opacity': 1});
          }
          if(!self.toDoCards || (self.toDoCards && self.toDoCards.length == 0)) {
			      self.show = true;
          }
      }, 0);
    };

    $scope.hideCard = function() {
      $scope.slickCardsConfig.method.slickNext();
      $scope.nextCount = $scope.nextCount - 1;
      if($scope.nextCount == 0){
        $scope.slickCardsConfig.method.slickGoTo(0);
        $scope.nextCount = self.toDoCards.length;
      }
    }

    $scope.redirectToLink = function(event,item) {
      angular.element(event.currentTarget).parents(".card:first").addClass('card-remove');
      $timeout($scope.removeCard(item), 250);
    }

    $scope.removeCard = function(item) {
      $scope.slickCardsConfig.method.slickRemove($scope.currentIndex);
      angular.element('.card-remove').removeClass('card-remove');
      $scope.changedCount = $scope.changedCount - 1;
      if($scope.changedCount == 0){
        self.show = true;
      } else{
        self.cardsCount = $scope.changedCount;
        angular.element("#slickCard").slick('slickRemove');
        angular.element("#slickCard").slick('slickAdd');
        angular.element("#slickCard").not('.slick-initialized').slick($scope.slickCardsConfig);
      }
      $window.open(item.url, '_blank');
    }

    this.toggle = function () {
      this.isShown = !this.isShown
  	}
}

module.exports= cardsComponent;
