'use strict';
var angular = require('angular');
require('angular-animate');
var cardsComponent = require('./cards.component');
var cardsModule = angular.module('cardsModule', ['ngAnimate','slickCarousel'])
    .component('cardsComponent', cardsComponent);

import Styles from './cards.scss';

module.exports = cardsModule;
