'use strict';
var angular = require('angular');
var phoneVerificationModule = require("../phone-verification/phone.verification.module");
var phoneVerificationController = require("../phone-verification/phone.verification.controller");
var phoneTemplate = require("../phone-verification/phone.verification.jade");

var formValidationModule = angular.module('formValidationModule', []);

formValidationModule.directive('customemail', function() {
    return {
    	restrict: 'A',
    	require: 'ngModel',
    	link: function(scope, elem, attrs, ctrl) {
    		ctrl.$parsers.unshift(function(viewValue) {
    			var emailpattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    			if(scope.ctrl.credentials){
                    scope.ctrl.credentials.isSubmitted = true;
                }
                if(viewValue === "" || viewValue == undefined) {
    				ctrl.$setValidity('cemail', "empty");
    			} else {
    				if(emailpattern.test(viewValue)) {
	    				ctrl.$setValidity('cemail', true);
	    			} else {
	    				ctrl.$setValidity('cemail', false);
    				}
    			}
                return viewValue;
    		});
    	}
    }
});

formValidationModule.directive('customsubmit', function($parse) {
    return {
        restrict: 'A',
        require: 'form',
        link: function(scope, formElement, attributes, formController) {

            var fn = $parse(attributes.customsubmit);

            formElement.bind('submit', function(event) {
                // if form is not valid cancel it.
                if (!formController.$valid) return;

                scope.$apply(function() {
                    fn(scope, { $event: event });
                });
            });
        }
    }
});

formValidationModule.directive('formSubmit', function($parse, $compile, $uibModal) {
    return {
        restrict: 'A',
        require: '^?form',
        link: function(scope, formElement, attributes, formController) {

            var fn = $parse(attributes.formSubmit);
            scope.controllerName = attributes.controller;

            formElement.on('submit', function(event) {
                // if form is not valid cancel it.
                if(scope[scope.controllerName]){
                    scope[scope.controllerName].credentials.isSubmitted = true;
                }
                if (!formController.$valid){
                    scope.$digest();
                    return false;  
                } 
                scope.$apply(function() {
                    fn(scope, {$event:event});
                });
                /*if(scope.ctrl.credentials.isPhoneNumber) {
                    var self = scope;
                    var options = {
                        templateUrl: phoneTemplate,
                        controller: phoneVerificationController,
                        controllerAs: "ctrl",
                        size: 'sm',
                        windowClass: 'phone-verification'
                    };
                    var modalInstance = $uibModal.open(options);
                    modalInstance.result.then(function(){
                        window.setTimeout(function() {
                          fn(self, {$event:event});
                        }, 0);
                    });
                }else{
                    scope.$apply(function() {
                        fn(scope, {$event:event});
                    });
                }*/
            });
        }
    }
});

formValidationModule.directive('customzip', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            elem.bind("keypress", function(event) {
                var keycode = event.which || event.keyCode;
                if (keycode === 101 || keycode === 43) {
                    event.preventDefault();
                }
            });
        }
    }
});

formValidationModule.directive('zipMask', function(){
 return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.bind("keypress keyup touchstart", function (event) { 
                var keyCode = event.which || event.keyCode; // get the keyCode pressed from the event.
                var keyCodeChar = String.fromCharCode(keyCode); //determine the char from the keyCode.

                if (navigator.userAgent.match('Android')) {
                    var zipVal = this.value;
                    var keyCodeCharAndroid = this.value.charCodeAt(zipVal.length-1);
 
                    if((keyCodeCharAndroid >= 65 && keyCodeCharAndroid <= 91) || (keyCodeCharAndroid >= 97 && keyCodeCharAndroid <= 122) || keyCodeCharAndroid == 32) { /* Restrict Alphabets including both lowercase and uppercase and white space */
                        console.log('this.val method call');
                        this.value = zipVal.replace(/\D/g,'');
                        this.value = zipVal.replace(/\s/g,'');
                        return false;
                    }
                    var charSpecial = String.fromCharCode(keyCodeCharAndroid); //Android
                    var specialRegex = /[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i;
                    var isValid = specialRegex.test(charSpecial);
                    if(isValid){
                        this.value = zipVal.replace(/[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i,'');
                        return false;
                    }
                }
                else{
                    if (keyCodeChar) {
                        if((keyCode >= 65 && keyCode <= 91) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32) { /* Restrict Alphabets including both lowercase and uppercase */
                            return false;
                        }
                        return !/^[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i.test(keyCodeChar); /* Restrict Special Characters except '-' */
                    }
                }

            });
            ngModelCtrl.$parsers.unshift(function(viewValue) { 
                viewValue = viewValue.replace(/\D/g,''); //Remove alphabets
                viewValue = viewValue.replace(/[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i,''); //Remove special symbols
                viewValue = viewValue.replace(/\s/g,''); //Remove space

                if(viewValue && viewValue.length >= 10) { /* Restrict on entering more than 10 characters */
                     var updatedValue = viewValue;
                     updatedValue = viewValue.substr(0, 10);
                     element.val(updatedValue);
                }
                else { /* Add '-' when the Zipcode passes 5 characters */
                    var isValidZipcode = viewValue && (viewValue.length != 5 && viewValue.length != 10);
                    var updatedValue = viewValue;
                    if(viewValue && viewValue.length > 5 && viewValue.indexOf('-') < 0) {
                        updatedValue = viewValue.slice(0, 5) + "-" + viewValue.slice(5);
                        viewValue = updatedValue;
                    }
                    element.val(updatedValue);

                    element.focus();
                    var tmppp = element.val();
                    var valLength = tmppp.length;
                    element.val(tmppp);
                   
                    //Resolving the focus shift issue in Android
                    setTimeout((function(element,strLength) {
                        return function() {
                            if(element.setSelectionRange !== undefined) {
                                element.setSelectionRange(strLength, strLength);
                                element.blur();
                            } else {
                                $(element).val(element.val());
 
                            }
                    }}(element,valLength)), 5);
                }
                if(viewValue && viewValue.length == 6) { /* Remove the '-' on delete */
                     var updatedValue = viewValue;
                     if(viewValue.indexOf('-') >= 0) {
                        updatedValue = viewValue.substr(0, viewValue.indexOf('-'));
                        viewValue = updatedValue;
                        element.val(updatedValue);
                     }
                }
                var isZipCodeValid = viewValue && (viewValue.length == 5 || viewValue.length == 10);
                if(isZipCodeValid == "") {
                    isZipCodeValid = false;
                }
                ngModelCtrl.$setValidity('isZipCodeValid', isZipCodeValid);
                return viewValue;
            });
        }
    };

});

formValidationModule.directive('fieldspinner', function($compile,$timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            elem.bind("blur", function(event) {
                elem.addClass('loading-spinner');
                $compile(elem)(scope);
                angular.element(document).find("img#passwordImage").css('display','none');
               $timeout(function(){
                    elem.removeClass('loading-spinner');
                    angular.element(document).find("img#passwordImage").css('display','inline-block');
                },3000);
            });
        }
    }
});

module.exports = formValidationModule;