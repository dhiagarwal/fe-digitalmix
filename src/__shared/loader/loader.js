'use strict';
require('./loader.scss');

var angular =require('angular');

var loaderComponent =require('./loader.component');

var loaderModule = angular.module('loaderModule', [
])
    .component('loader', loaderComponent );

module.exports = loaderModule;
