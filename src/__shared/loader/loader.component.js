'use strict';

var template = require('./loader.jade');

var loaderComponent =  {
    templateUrl: template,
    bindings: {
    	displayLoader: "="
    },
    controller: ['$location', loaderController],
    controllerAs: 'ctrl'
};

function loaderController($location){
    
}

module.exports = loaderComponent;
