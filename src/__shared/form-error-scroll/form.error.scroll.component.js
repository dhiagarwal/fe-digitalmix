'use strict';
var angular = require('angular');

function scrollToError(fieldIdentification){
    angular.element(document.querySelector('body')).animate({
            scrollTop: (fieldIdentification-30)
        }, 1000
    );
}

function ScrollForErrorDirective(){
    return {
        restrict: 'A',
        require: '^form',
        link: function(scope, ele, attr, formCtrl){
            ele.bind('submit', function(){
                var errorField;
                if(formCtrl.$error.required.length){
                   var fieldIdentified = angular.element(document.querySelector('input.ng-invalid')).offset().top;
                    scrollToError(fieldIdentified);
                }

            });
        }
    }
}

module.exports = ScrollForErrorDirective;
