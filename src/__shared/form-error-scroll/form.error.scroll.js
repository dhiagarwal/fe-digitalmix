'use strict';
var angular =require('angular');

var formErrorScroll =require('./form.error.scroll.component');

var formErrorScrollModule = angular.module('formErrorScrollModule', [
])
    .directive('scrollForError', formErrorScroll);

module.exports = formErrorScrollModule;
