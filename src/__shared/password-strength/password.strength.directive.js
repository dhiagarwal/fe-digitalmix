var template =require('./password.strength.jade');
passwordStrengthDirective.$inject = ['$compile'];
function passwordStrengthDirective($compile) { 
	return {
		//transclude: true,
		require: '^passwordPattern',
		restrict: 'E',
	    scope:{
	    	userPassword: '=userPassword'
	    },
	    compile: function(elem, attrs) {
		    return this.link;
		},
		link: function(scope, elem, attrs, ctrl) { 
			var strongRegex = new RegExp("^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[!@#\$%\^&\*]).{8,}$");
	     	scope.$watch('userPassword', function(newVal) {
				scope.strength = isSatisfied(newVal && newVal.length >= 8) &&
				(isSatisfied(newVal && /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(newVal)) +  
	            isSatisfied(newVal && /[a-z]/.test(newVal)) + 
	            isSatisfied(newVal && /[A-Z]/.test(newVal)) + 
	            isSatisfied(newVal && /[0-9]/.test(newVal))); 
	            ctrl.credentials.strength = scope.strength;       
	    	}, true);
	     	
		},
	    templateUrl: template
	}
	function isSatisfied(criteria) { 
        return criteria ? 1 : 0; 
    }
};

module.exports= passwordStrengthDirective;