'use strict';

var angular =require('angular');
var Navbar =require('./navbar/navbar');
var Footnote =require('./footnote/footnote');
var starRating = require('./star-rating/star.rating');
var primaryCarousel = require('./primary-carousel/primary.carousel');
var regionSelection = require('./region-selection/component/region.component.module');
var scrollTop = require('./scroll-to-top/scroll-top');
var birthdayPicker = require('./birthday-picker/birthday.picker');
var sharedCards = require('./cards/cards');
var headerNav = require('./header/headerNav');
var helpSection = require('./help-section/help.section');
var footerNav = require('./footer/footerNav.component.module');
var passwordPattern = require('./password-pattern/password.pattern.module');
var passwordField = require('./password-field/password.field.component.module');
var formValidations = require('./directives/formvalidations.js');
var emailPhone = require('./email-phone/email.phone.module');
var phoneVerification = require('./phone-verification/phone.verification.module');
var pageError = require('./page-level-error/page.level.error.module');
var backToTop = require('./back-to-top/back.to.top');
var scrollForErrors = require('./form-error-scroll/form.error.scroll');
var quickLinks = require('./quick-links/quick.links');
var loader = require('./loader/loader');
var lazyLoader = require('./lazy-loader/lazyloader');
var customModal = require('./custom-modal/custom.modal.module');
var termPolicyModal = require('./term-policy-modal/term.policy.modal.component.module');
var secondaryCarousel = require('./secondary-carousel/secondary.carousel');
var spacerComponent = require('./spacer-component/spacer.component.module');
var disclaimer = require('./disclaimer/disclaimer.component.module');
var sharedModule = angular.module('app.shared', [
  Navbar.name,
  Footnote.name,
  starRating.name,
  primaryCarousel.name,
  regionSelection.name,
  scrollTop.name,
  birthdayPicker.name,
  sharedCards.name,
  starRating.name,
  primaryCarousel.name,
  scrollTop.name,
  birthdayPicker.name,
  helpSection.name,
  footerNav.name,
  headerNav.name,
  helpSection.name,
  passwordPattern.name,
  formValidations.name,
  emailPhone.name,
  phoneVerification.name,
  pageError.name,
  backToTop.name,
  scrollForErrors.name,
  quickLinks.name,
  loader.name,
  lazyLoader.name,
  customModal.name,
  termPolicyModal.name,
  passwordField.name,
  secondaryCarousel.name,
  spacerComponent.name,
  disclaimer.name
]);

module.exports= sharedModule;
