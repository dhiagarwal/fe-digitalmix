var angular =require('angular');
var disclaimerComponent =require('./disclaimer.component');

var disclaimerModule = angular.module('disclaimer', [
])
.component('disclaimer', disclaimerComponent)

import Styles from './disclaimer.component.scss'; 

module.exports= disclaimerModule;