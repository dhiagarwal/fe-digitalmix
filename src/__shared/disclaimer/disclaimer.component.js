var template =require('./disclaimer.component.jade');
var controller =require('./disclaimer.component.controller');

var disclaimerComponent =  {
    templateUrl: template,
    controller: controller,
    controllerAs: 'disclaimerctrl',
    replace: true,
    bindings: {
        custcolor: "@"
    }
};

module.exports= disclaimerComponent;