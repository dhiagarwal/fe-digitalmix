'use strict';

var template = require('./page.level.error.jade');

var pageLevelErrorComponent =  {
    restrict: 'E',
    bindings: {
      errorMessage: '=',
      dangerType: '=',
      successMessage: '=',
      successType: '=',
      isDisplayErrorMessage: '=',
      isShowInfoIconOnError: '=',
      successDescription: '='
    },
    templateUrl: template,
    controller: pageLevelErrorController,
    controllerAs: 'ctrl',
    bindToController: true
};

function pageLevelErrorController(){

    var self = this;
    this.dangerAlert = "danger";
    self.modifyDisplayErrorMessage = function() {
        if(!self.isShowInfoIconOnError) {
            self.isDisplayErrorMessage = !self.isDisplayErrorMessage;
        }
    };
}

module.exports = pageLevelErrorComponent;
