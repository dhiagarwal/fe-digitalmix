'use strict';
var angular = require('angular');
var pageLevelErrorComponent = require('./page.level.error.component');
var pageLevelErrorModule = angular.module('pageLevelErrorModule', [])
    .component('pageLevelErrorComponent', pageLevelErrorComponent);

import Styles from './page.level.error.scss';

module.exports = pageLevelErrorModule;
