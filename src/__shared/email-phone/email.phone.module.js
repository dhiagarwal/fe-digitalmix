var angular =require('angular');
var uiRouter =require('angular-ui-router');
require('angular-messages');
var emailPhoneComponent =require('./email.phone.component');
var formUtils =require('../../__shared/form-utils/form-utils');

var emailPhoneModule = angular.module('emailPhoneModule', [
  uiRouter,
  'ngMessages',
  formUtils.name
])
.component('emailPhone', emailPhoneComponent);

import Styles from './email.phone.component.scss'; 

module.exports= emailPhoneModule;
