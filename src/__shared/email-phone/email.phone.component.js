var template = require('./email.phone.jade');
var controller = require('./email.phone.controller');

var emailPhoneComponent =  {
    bindings: {
    	credentials: '=',
    	title: '@',
    	isEmailOrPhone: '<',
    	isEmail:'<',
    	isPhone:'<',
        isRequired: '<',
        isSubmitted: '<'
    },
    templateUrl : template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports= emailPhoneComponent;
