'use strict';

var template = require('./scroll.top.html');

var starRatinglComponent =  {
    restrict: 'E',
    templateUrl: template,
    bindings: {},
    controller: ['$location', '$anchorScroll', scrollTopController],
    controllerAs: 'ctrl',
    bindToController: true
};

function scrollTopController($location, $anchorScroll){
    this.scrollToTop = function(){
        $anchorScroll();
    };
}

module.exports = starRatinglComponent;
