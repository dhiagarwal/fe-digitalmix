'use strict';
require('./scroll.top.scss');

var angular =require('angular');

var scrollTopComponent =require('./scroll.top.component');

var scrollTopModule = angular.module('scrollTopModule', [
])
    .component('scrolltotop', scrollTopComponent );

module.exports = scrollTopModule;
