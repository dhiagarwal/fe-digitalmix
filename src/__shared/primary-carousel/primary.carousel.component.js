'use strict';

var template = require('./primary.carousel.jade');

var prmyCarouselComponent =  {
    restrict: 'E',
    bindings: {
      imageList : '='  
    },
    templateUrl: template,
    controller: primaryCarouselController,
    controllerAs: 'ctrl',
    bindToController: true

};

function primaryCarouselController(){
    this.slickPrimaryConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots:true,
        speed: 500,
        responsive: [{
            breakpoint: 640,
            settings: {
                arrows: true,
                slidesToShow: 3
            }
        }, {
            breakpoint: 1024,
            settings: {
                arrows: true,
                slidesToShow: 4,
                draggable: false
            }
        }]
    };
}

module.exports= prmyCarouselComponent;
