'use strict';
var angular =require('angular');

var prmyCarouselComponent =require('./primary.carousel.component');

var prmyCarouselModule = angular.module('prmyCarouselModule', [
])
    .component('primaryCarousel', prmyCarouselComponent);

module.exports= prmyCarouselModule;
