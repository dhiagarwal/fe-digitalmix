'use strict'
termPolicyModalController.$inject=["$scope", "$uibModalInstance"];
function termPolicyModalController($scope, $uibModalInstance){
  this.$uibModalInstance = $uibModalInstance;
  var model = this.model = {};
  this.cancel = function() {
    this.$uibModalInstance.dismiss();
  };
};

module.exports = termPolicyModalController;