var template =require('./term.policy.modal.component.jade');
var controller =require('./term.policy.modal.component.controller.js');

var termPolicyModalComponent =  {
	transclude: true,
    bindings:{
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'  
};

module.exports= termPolicyModalComponent;