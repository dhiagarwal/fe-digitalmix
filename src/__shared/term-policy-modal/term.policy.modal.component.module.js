var angular =require('angular');
var termPolicyModalComponent =require('./term.policy.modal.component');

var termPolicyModalModule = angular.module('termPolicyModal', [
])
.component('termPolicyModal', termPolicyModalComponent)

import Styles from './term.policy.modal.component.scss';

module.exports= termPolicyModalModule;