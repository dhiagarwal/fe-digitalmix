var template =require('./footnote.jade');
var controller =require('./footnote.controller');

var footnoteComponent =  {  
    restrict: 'E',
    templateUrl: template,
    controller,
    controllerAs: 'vm',
    bindToController: true  
};

module.exports = footnoteComponent;
