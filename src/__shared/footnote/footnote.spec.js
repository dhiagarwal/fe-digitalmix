// import FootnoteModule from './footnote'
// import FootnoteController from './footnote.controller';
// import FootnoteComponent from './footnote.component';
// import FootnoteTemplate from './footnote.html';

// describe('Footnote', () => {
//   let $rootScope, makeController;

//   beforeEach(window.module(FootnoteModule.name));
//   beforeEach(inject((_$rootScope_) => {
//     $rootScope = _$rootScope_;
//     makeController = () => {
//       return new FootnoteController();
//     };
//   }));

//   describe('Module', () => {
//     // top-level specs: i.e., routes, injection, naming
//   });

//   describe('Controller', () => {
//     // controller specs
//     it('has a name property', () => { // erase if removing this.name from the controller
//       let controller = makeController();
//       expect(controller).to.have.property('name');
//     });
//   });

//   describe('Component', () => {
//       // component/directive specs
//       let component = FootnoteComponent();

//       it('includes the intended template',() => {
//         expect(component.template).to.equal(FootnoteTemplate);
//       });

//       it('uses `controllerAs` syntax', () => {
//         expect(component).to.have.property('controllerAs');
//       });

//       it('invokes the right controller', () => {
//         expect(component.controller).to.equal(FootnoteController);
//       });
//   });
// });
