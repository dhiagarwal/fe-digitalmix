var angular =require('angular');
var footnoteComponent =require('./footnote.component');

var footnoteModule = angular.module('footnoteModule', [])
.component('footnotecomponent', footnoteComponent);

module.exports= footnoteModule;
