'use strict';

headerNavService.$inject = ['$http','$q'];

function headerNavService($http,$q) {
    this.$http = $http;
    this.$q = $q;

    var deferred = $q.defer();
    $http.get('../../../src/__shared/header/headerNav.json').then(function(response) {
        deferred.resolve(response.data);
    });

    this.getHeaderMenuData = function() {
        return deferred.promise;
    }
};
  
headerNavService.prototype.getSearchedHistoryData = function(){
    var deferred = this.$q.defer();
    this.$http.get('/src/digital-toolkit/components/media-center/media.center.component.searchedhistory.json')
      .success(deferred.resolve);

    return deferred.promise;
};

headerNavService.prototype.getQuickSearchData = function(){
    var deferred = this.$q.defer();
    this.$http.get('/src/digital-toolkit/components/media-center/media.center.component.quicksearch.json')
        .success(deferred.resolve);

    return deferred.promise;
};

headerNavService.prototype.getDropdownOptions = function(){
    var deferred = this.$q.defer();
    this.$http.get('../../../src/__shared/header/accountDropdown.json')
        .success(deferred.resolve);

    return deferred.promise;
};

module.exports = headerNavService;
