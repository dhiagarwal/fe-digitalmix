'use strict'

function headerNavController(headerNavService, $timeout, $location, $rootScope,$window){
    this.name = 'headerNav';
    this.isCategoryShown = true;
    this.activeSubCategory = '';
    this.screenHeight = $window.innerHeight;
    this.loggedIn = false;
    this.accType = 'distributor'; // or 'customer'
    this.searchMode = false;
    this.showSearch = false;
    this.accImage = '../../../src/assets/images/css/ic-default-profile-green.svg';
    this.accLevel = '../../../src/assets/images/content/group9-sm.png';
    this.showAccountInfo=false;
    this.dropdownInfo = {}
    this.loginSignupPage=false;
    this.cartItem = 88;
    var self=this;

    this.openMenu = function(){
        this.isCategoryShown = true;
        this.searchMode = false;
        this.showSearch = true;
    };
    this.goToHomePage = function(isModalOpen){
        if(isModalOpen) {
            $('#headerSearch').modal('hide');
            $('#sideMenu').modal('hide');
            $timeout(function(){
                $location.path('/');
            },500);
        }
        else
            $location.path('/');
    };
    this.goToPage = function(pageName){
        console.log("----"+pageName);
        $location.path('/'+pageName);
    }
    this.showSubCategory = function(subCategory){
        if(this.activeSubCategory !== subCategory){
          this.isCategoryShown = false;
          this.activeSubCategory = subCategory;
        } else{
          this.isCategoryShown = true;
          this.activeSubCategory = '';
        }
    };
    this.hideSubCategory = function(event){
        this.showAccountInfo=false;
        if(event.target && (angular.element(event.target).hasClass("menu-dropdown") || angular.element(event.target).hasClass("overlay-dropdown-menu") || angular.element(event.target).hasClass("side-menu-back") ||  angular.element(event.target).hasClass("back-icon"))){
			this.isCategoryShown=true;
			this.activeSubCategory = '';
			this.searchMode = false;
			this.showSearch = true;
		}
    };

    this.showAccountModal = function(){
        if(this.accType=='guest') {
            //redirect to login page
        } else{
          this.isCategoryShown=true;
          this.activeSubCategory = '';
          this.showAccountInfo=!this.showAccountInfo;
        }
    };

    this.clearValue = function () {
        this.headerSearch = null;
    };

    this.submitSearch = function(){
        var search='';
        var minPSV='';
        var maxPSV='';
        if(this.headerSearch!='' || this.minPSV || this.maxPSV)
        {
            $('#headerSearch').modal('hide');
            $('#sideMenu').modal('hide');
            search=(this.headerSearch&&this.headerSearch!='')?'search='+(this.headerSearch.Key?this.headerSearch.Key:this.headerSearch):'';
            minPSV=this.minPSV?(search!=''?'&':'')+'minPSV='+this.minPSV:'';
            maxPSV=this.maxPSV?((search!=''||minPSV!='')?'&':'')+'maxPSV='+this.maxPSV:'';
            $timeout(function(){
                $location.url('/productSearch?'+search+minPSV+maxPSV);
                $window.location.reload();
                searchHandler();
            },500);
        }
    };

    var promise = headerNavService.getHeaderMenuData();
    promise.then(function(data) {
        self.headerMenuData = data;
    });

    var promise1 = headerNavService.getSearchedHistoryData();
    promise1.then(function(data) {
        self.searchedHistoryData = _.take(data, 5);
    });

    var promise2 = headerNavService.getQuickSearchData();
    promise2.then(function(data) {
        self.quickSearchData = data;

	  	//code for typeahead
        self.headerSearch = null;

        // instantiate the bloodhound suggestion engine
        self.searchSettings = new Bloodhound({
            datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.Key); },
	        queryTokenizer: Bloodhound.tokenizers.whitespace,
	        local: self.quickSearchData
        });

        self.searchSettings.initialize();

        // initialize the bloodhound suggestion engine
        self.searchDataset = {
            displayKey: 'Key',
            source: self.searchSettings.ttAdapter()
        };

        self.searchOptions = {
            highlight: true,
            limit: 5,
            minLength: 1
        };
    });

    var accountDropdownPromise = headerNavService.getDropdownOptions();
    accountDropdownPromise.then(function(data) {
        for(var name in data) {
            if(name == self.accType) {
                self.dropdownOptions = data[name];
                self.dropdownInfo.elementsInLastRow = self.dropdownOptions.length % 3;
                for(var i=0; i<self.dropdownInfo.elementsInLastRow; i++) {
                    self.dropdownOptions[self.dropdownOptions.length-i-1].isLastRow =  true;
                }
            }
        }
    });



    var searchHandler = $rootScope.$on('searchHandler', function(ev, args){
        $('#'+args.type).modal();
        self.searchMode = true;
        self.showSearch = true;
        self.headerSearch = $location.search().search?$location.search().search:'';
        self.minPSV = $location.search().minPSV?$location.search().minPSV:'';
        self.maxPSV = $location.search().maxPSV?$location.search().maxPSV:'';
    });

    this.hideModal = function(event){
      if(event.target && (event.target.id=='showAccountInfoId' || event.target.id=='showMyAccountId' || event.target.id=='showAccountInfo')){
         this.showAccountInfo=false;
      } else{
        this.showAccountInfo=true;
      }
    }

    angular.element(window).on('resize', function() {
        self.screenHeight = $window.innerHeight;
        angular.element.find('#sideMenu .side-menu')[0].style.minHeight = self.screenHeight+'px';
        angular.element.find('#headerSearch .header-search')[0].style.minHeight = self.screenHeight+'px';
    });
};

module.exports = headerNavController;
