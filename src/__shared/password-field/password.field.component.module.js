var angular = require('angular');


var passwordFieldComponent = require('./password.field.component');

var passwordFieldModule = angular.module('passwordField', [])
    .component('passwordField', passwordFieldComponent)


import Styles from './password.field.component.scss';

module.exports = passwordFieldModule;