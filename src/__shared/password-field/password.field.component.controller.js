'use strict';
passwordFieldController.$inject = ["$scope"];

function passwordFieldController($scope) {
    var self = this;
    this.name = 'passwordField';
    this.credentials = this.credentials || {};
    this.isRequired = this.isRequired || false;
    this.errorOnSubmit = true;
    this.credentials.passwordImage = {
        "text": "../../../../../src/assets/images/css/ic-password-show.png",
        "mask": "../../../../../src/assets/images/css/ic-password-hide.png"
    };


    this.credentials.enterPasswordState = 'password';

     
    this.changeImage = function() {

        this.credentials.enterPasswordState = this.credentials.enterPasswordState === 'text' ? 'password' : 'text';

         
    };
}

module.exports = passwordFieldController;