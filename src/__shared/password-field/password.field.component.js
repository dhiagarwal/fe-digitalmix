var template =require('./password.field.component.jade');
var controller =require('./password.field.component.controller');

var passwordFieldComponent =  {
	transclude: true,
    bindings:{
    	credentials: '=',
        isRequired: '<',
        isSubmitted: '<'
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'  
};

module.exports= passwordFieldComponent;
