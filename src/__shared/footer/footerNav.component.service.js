'use strict';

footerNavComponentService.$inject = ['$http','$q'];

function footerNavComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../src/__shared/footer/footerNav.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getSocialIconData = function() {
    return deferred.promise;
  }

};

module.exports = footerNavComponentService;