'use strict'

function footerNavController(footerNavComponentService, $location){
    var self=this;
    this.isDistributor=false;
    this.loginSignupPage=false;
    this.showButton=false;
    this.distributorName='DISTRIBUTOR NAME';
    this.distributorEmail='myusername@email.com';
    this.distributorPhone='(123) 456-2234';

    this.socialIcons ={};
    var promise = footerNavComponentService.getSocialIconData();
    promise.then(function(data) {
        self.socialIcons = data;
    });

    self.redirectRegionPage = function(){
        $location.path("/mediaCenter/region")
    }

};
module.exports = footerNavController;
