'use strict';
var angular =require('angular');

var secCarouselComponent =require('./secondary.carousel.component');

var secCarouselModule = angular.module('secCarouselModule', [
])
    .component('secondaryCarousel', secCarouselComponent);

import Styles from './secondary.carousel.scss';

module.exports= secCarouselModule;
