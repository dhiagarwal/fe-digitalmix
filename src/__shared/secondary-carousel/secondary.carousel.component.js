'use strict';

var template = require('./secondary.carousel.jade');

var secCarouselComponent =  {
    bindings: {
      productData : '=',
      slickType: '=',
      slickConfig: '='
    },
    templateUrl: template,
    controller: secondaryCarouselController,
    controllerAs: 'ctrl'

};

function secondaryCarouselController(){
  this.stockStatus = function(value, temp){
    if(temp == value){
      return true;
    }
    else{
      return false;
    }
  };
  this.isNumber = function (n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
  }
}

module.exports= secCarouselComponent;
