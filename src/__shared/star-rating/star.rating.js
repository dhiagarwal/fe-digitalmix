'use strict';
require('./star-rating.scss');

var angular =require('angular');

var starRatingComponent =require('./star.rating.component');

var starRatingModule = angular.module('starRatingModule', [
])
    .component('starRating', starRatingComponent);

module.exports = starRatingModule;
