'use strict';

var template = require('./star.rating.jade');

var starState = {
    starOn: "../../../src/assets/images/content/pdp-productbasics/star-on.svg",
    starOff: "../../../src/assets/images/content/pdp-productbasics/star-off.svg"
};

var starRatinglComponent =  {
    templateUrl: template,
    bindings: {
        ratingValue: '=',
        max: '=',
        starClass: '@',
        starType: '@'
    },
    controller: starRatingController,
    controllerAs: 'ctrl'
};

function starRatingController(){
    var self = this;
    this.ratingValue = this.ratingValue || 1.75;
    this.max = this.max || 5;
    self.ratingValue = self.ratingValue >= self.max ? self.max : Math.round(self.ratingValue);
    this.starType = this.starType || "big";
    this.starClass = "star-" + this.starType;
    this.starRating = this.getStarRatingPosition(this.ratingValue, this.starType);
    self.maxStar = new Array(self.max);
}

starRatingController.prototype.getStarRatingPosition = function(ratingDecimalValue, starType){
  var starTopPosition = 0;
  var starMargin = 0;
  var starWidth = 0;
  var halfStarTopMargin = 0, smallStarFirstPosition = 0;
  var halfStarPosition = -273;
  if(starType == "big"){
    starWidth = 18;
    halfStarPosition = -273;
    halfStarTopMargin = -26;
  } else{
    starWidth = 12;
    halfStarPosition = -242;
    halfStarTopMargin = -64;
    smallStarFirstPosition = -44;
  }
  var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/,'0');
  var floorRatingValue = Math.floor(ratingDecimalValue);
  decimalValue = parseFloat(decimalValue);
  var mutipleValue = 0;
  if(0.26 <= decimalValue && decimalValue <= 0.74){
    starMargin = halfStarTopMargin;
    floorRatingValue++;
  } else if(0.75 <= decimalValue && decimalValue <= 0.99){
    starMargin = smallStarFirstPosition;
    floorRatingValue++;
  } else{
    starMargin = smallStarFirstPosition;
  }
  if(floorRatingValue >= this.max){
    starMargin = smallStarFirstPosition;
    floorRatingValue = this.max;
  }
  floorRatingValue = floorRatingValue*starWidth;
  floorRatingValue = halfStarPosition + floorRatingValue
  return floorRatingValue+"px "+starMargin+"px";
}

module.exports = starRatinglComponent;
