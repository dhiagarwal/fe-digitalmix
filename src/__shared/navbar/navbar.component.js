var template =require('./navbar.jade');
var controller =require('./navbar.controller');

var navbarComponent =  {
  
    restrict: 'E',
    templateUrl: template,
    controller,
    controllerAs: 'vm',
    bindToController: true
  
};

module.exports= navbarComponent;
