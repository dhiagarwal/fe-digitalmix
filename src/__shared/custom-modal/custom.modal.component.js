'use strict';
var controller = require('./custom.modal.controller');

var customModalComponent = {
	controller: controller,
	controllerAs: 'ctrl',
    bindings: {
        isModalCall: '@',
        srcUrl: '@',
		callbackOnSuccess: '@',
		modalHeading: '@'
    }
};

module.exports = customModalComponent;