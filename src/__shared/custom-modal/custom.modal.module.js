'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var customModalComponent = require('./custom.modal.component');

require('./custom.modal.scss');

var customModalModule = angular.module('customModalModule', [uiRouter])
		.component('custommodalcomponent', customModalComponent)

module.exports = customModalModule;