'use strict';
var modalVerificationTemplate = require('./custom.modal.jade');

function customModalController($scope, $uibModal, $sce) {
    this.$uibModal = $uibModal;
    var self = this;

    self.modalPopupInstance = function(modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: ['$uibModalInstance', function($uibModalInstance) {
                var modalSelf = this;
                modalSelf.model = {};

                modalSelf.model.addressList = self.addressList;

                modalSelf.addressData = self.addressData;

                modalSelf.srcUrl = self.srcUrl;

                modalSelf.modalHeading = self.modalHeading;

                modalSelf.ok = function() {
                    $uibModalInstance.close();
                };

                modalSelf.trustUrl = function(url) {
                    return $sce.trustAsResourceUrl(url);
                }

                modalSelf.cancel = function() {
                    $uibModalInstance.dismiss();
                };

                modalSelf.goBackAndEdit = function() {
                    modalSelf.cancel();
                    var resetBooleanVariable = '$scope.$parent.' + self.callbackOnSuccess + '()';
                    eval(resetBooleanVariable);
                };

            }],
            controllerAs: 'ctrl',
            windowClass: windowClass,
            size: 'md'
        });
    };

    self.trustUrl = function(url) {
        return $sce.trustAsResourceUrl(url);
    }

    if (self.isModalCall) {
        self.modalPopupInstance(modalVerificationTemplate, self.modalController, 'custom-modal-popup');
    }

};

module.exports = customModalController;
