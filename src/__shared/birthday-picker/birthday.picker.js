var angular =require('angular');
var birthdayPicker = require('./birthday.picker.component');
var birthdayFactory = require('./birthday.picker.factory');

var birthdayModule = angular.module('birthdayModule', [])
    .component('birthdayPicker', birthdayPicker)
    .factory('birthdayFactory', birthdayFactory);

module.exports= birthdayModule;
