'use strict';
var template =require('./birthday.picker.jade');

var birthdayComponent =  {
    restrict: 'E',
    templateUrl: template,
    bindings:{
        month: '=',
        day: '=',
        year: '=',
        validDateCheck: '&'
    },
    controller: ['birthdayFactory', '$scope', birthdayController],
    controllerAs: 'ctrl',
    bindToController: true
};

function birthdayController(birthdayFactory, $scope){

    this.birthdayFactory = birthdayFactory;
    this.monthsList = birthdayFactory.getMonths();
    this.yearsList = birthdayFactory.getYears();
    this.error = {hasError : false, message : ''};
    $scope.$watch(this.getMonth.bind(this), this.getDaysList.bind(this));

    this.checkDate = function () {
        if(!this.year || !this.month || !this.day) {
          this.error.hasError = true;  
          this.error.message = "Please select the valid date.";
          angular.element(document).find('select').addClass("red-border");
          angular.element(berror).removeClass("hide"); 
          this.validDateCheck();
          return; 
        }
    }
    this.checkFocus = function() {
        this.error.hasError = false;
        this.error.message = '';
        angular.element(document).find('select').removeClass("red-border");
        return;
    }
}

birthdayController.prototype.getMonth = function() {
    return this.month;
}

birthdayController.prototype.getDaysList = function(monthCode){
    this.daysList = this.birthdayFactory.getDaysForMonth(monthCode);
    this.leapYearValidation();
};

birthdayController.prototype.leapYearValidation = function(){

    if(this.year%4 !== 0 && this.month == 2 && this.day == 29){
        this.error.hasError = true;
        this.day = "";
        this.error.message = "Selected birthday is invalid";
        return;
    }

    this.error.hasError = false;
        if(this.year && this.month && this.day){
        this.validDateCheck();
    }



};

module.exports = birthdayComponent;
