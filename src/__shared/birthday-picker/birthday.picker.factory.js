'use strict';

var _ = require('lodash');
var months = [
    {
        code: 1,
        desc: 'JAN'
    },
    {
        code: 2,
        desc: 'FEB'
    },
    {
        code: 3,
        desc: 'MAR'
    },
    {
        code: 4,
        desc: 'APR'
    },
    {
        code: 5,
        desc: 'MAY'
    },
    {
        code: 6,
        desc: 'JUNE'
    },
    {
        code: 7,
        desc: 'JULY'
    },
    {
        code: 8,
        desc: 'AUG'
    },
    {
        code: 9,
        desc: 'SEPT'
    },
    {
        code: 10,
        desc: 'OCT'
    },
    {
        code: 11,
        desc: 'NOV'
    },
    {
        code: 12,
        desc: 'DEC'
    }
];
var days = [
    {code: 1, desc: "1"},
    {code: 2, desc: "2"},
    {code: 3, desc: "3"},
    {code: 4, desc: "4"},
    {code: 5, desc: "5"},
    {code: 6, desc: "6"},
    {code: 7, desc: "7"},
    {code: 8, desc: "8"},
    {code: 9, desc: "9"},
    {code: 10, desc: "10"},
    {code: 11, desc: "11"},
    {code: 12, desc: "12"},
    {code: 13, desc: "13"},
    {code: 14, desc: "14"},
    {code: 15, desc: "15"},
    {code: 16, desc: "16"},
    {code: 17, desc: "17"},
    {code: 18, desc: "18"},
    {code: 19, desc: "19"},
    {code: 20, desc: "20"},
    {code: 21, desc: "21"},
    {code: 22, desc: "22"},
    {code: 23, desc: "23"},
    {code: 24, desc: "24"},
    {code: 25, desc: "25"},
    {code: 26, desc: "26"},
    {code: 27, desc: "27"},
    {code: 28, desc: "28"},
    {code: 29, desc: "29"},
    {code: 30, desc: "30"},
    {code: 31, desc: "31"}
];

var has31days = ['JAN','MAR','MAY','JULY','AUG','OCT','DEC'];
var has30days = ['APR','JUNE','SEPT','NOV'];
var has29days = ['FEB'];

function getDays(dayLength){
    return _.take(days, dayLength)
}

function generateYears(){
    var years = [];
    var currentYear = new Date().getFullYear();
    for(var i = currentYear-18 ; i > (currentYear-100); i--){
        years.push(i);
    }
    return years;
}

function birthdayFactory(){
    return{
        getMonths:function(){
            return months;
        },
        getDaysForMonth: function(monthCode){
            var month = _.result(_.find(months,{code: monthCode}), 'desc');

            if(_.includes(has31days, month)){
                return getDays(31)
            }
            if(_.includes(has30days, month)){
                return getDays(30)
            }
            if(_.includes(has29days, month)){
                return getDays(29)
            }
        },
        getYears: function(){
            return generateYears();
        },
        findItemById: function(id,flag) {
            var returnItem;
            if (flag == "M")
                var baseArray = months;
            if (flag == "D")
                var baseArray = days;
            angular.forEach(baseArray, function(value) {
                if (value.code == id) {
                    returnItem = value.code;
                }
            });
            return returnItem;
        }
    }
}

module.exports = birthdayFactory;