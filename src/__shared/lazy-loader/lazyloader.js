'use strict';
require('./lazyloader.scss');

var angular =require('angular');

var lazyloaderComponent =require('./lazyloader.component');

var lazyloaderModule = angular.module('lazyloaderModule', []).component('lazyloader', lazyloaderComponent );

module.exports = lazyloaderModule;
