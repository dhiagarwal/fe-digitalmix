'use strict';

var template = require('./lazyloader.jade');

var lazyloaderComponent =  {
    templateUrl: template,
    bindings: {
    	displayLoader: "="
    },
    controller: ['$location', lazyloaderController],
    controllerAs: 'ctrl'
};

function lazyloaderController($location){
    
}

module.exports = lazyloaderComponent;
