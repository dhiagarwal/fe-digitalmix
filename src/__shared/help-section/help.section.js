var angular =require('angular');
var helpSectionComponent =require('./help.section.component');

var helpSectionModule = angular.module('helpSectionModule', [])
.component('helpsectioncomponent', helpSectionComponent);

import Styles from './help.section.scss'; 

module.exports= helpSectionModule;
