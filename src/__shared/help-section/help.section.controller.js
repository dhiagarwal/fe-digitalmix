'use strict'

function helpSectionController(){

	this.dataList = [
	{
		"name": "Secure Transactions",
		"data": "Lorem ipsum dolor sit amet, ex eam dictas melius laboramus, id vel mazim qualisque posidonium. Pri an brute temporibus appellantur, qui error fierent et, et mei homero nostrud.",
		"image": "../../../src/assets/images/css/secure-transaction.png"
	},
	{
		"name": "Easy Return Policy",
		"data": " Latine voluptua maluisset pri ex, ea alienum ancillae assueverit pri, ne mea elitr scripserit referrentur. Brute dissentiunt ut sea, quaeque epicurei lobortis qui an.",
		"image": "../../../src/assets/images/css/return.png"
	},
	{
		"name": "Help & Assistance",
		"data": " Ut eum wisi illud omnesque, oporteat accusamus mei in. Ne offendit detraxit repudiandae his. Ex stet ceteros noluisse quo, sit at fierent dissentiet.",
		"image": "../../../src/assets/images/css/help.png"
	}
	]

};

module.exports = helpSectionController;