var template =require('./help.section.jade');
var controller =require('./help.section.controller');

var helpSectionComponent =  {  
    restrict: 'E',
    templateUrl: template,
    controller: controller,
    controllerAs: 'vm',
    bindToController: true  
};

module.exports = helpSectionComponent;
