'use strict';

var template = require('./back.to.top.jade');

var BackToTopComponent =  {
    templateUrl: template,
    controller: ['$window', '$anchorScroll', backToTopController],
    controllerAs: 'ctrl',
    bindToController: true
};

function backToTopController($window, $anchorScroll){
    var offset = 300,
        scroll_top_duration = 700;

    var backToTop = angular.element(document.querySelector('.back-to-top-icon'));

    var self = this;

    angular.element(window).scroll(function(){
        if(angular.element(document.querySelector('.footer-nav')).offset().top){
            var scrollTop = angular.element(document).scrollTop();
            var windowHeight = angular.element(window).height();

            var footerElement = angular.element(document.querySelector('.footer-nav')).offset().top;

            ( angular.element(document).scrollTop() > offset ) ? backToTop.addClass('back-to-top-is-visible') : backToTop.removeClass('back-to-top-is-visible');

            if((scrollTop + windowHeight) > footerElement){
                backToTop.addClass('stick-to-footer');
            }else{
                backToTop.removeClass('stick-to-footer');
            }
        }
    });

    backToTop.on('click', function(event){
        event.preventDefault();
        angular.element('html,body').animate({
                scrollTop: 0
            }, scroll_top_duration
        );
    });
}

module.exports = BackToTopComponent ;
