'use strict';
require('./back.to.top.scss');

var angular =require('angular');

var backToTopComponent =require('./back.to.top.component.js');

var backToTopModule = angular.module('backToTopModule', [
])
    .component('backtotop', backToTopComponent );

module.exports = backToTopModule;
