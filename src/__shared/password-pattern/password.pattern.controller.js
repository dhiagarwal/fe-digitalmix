'use strict'
passwordPatternController.$inject=["$scope", "$sce"];
function passwordPatternController($scope, $sce){
	var self = this;
    this.name = 'passwordPattern';
    this.credentials = this.credentials || {};
    this.isRequired = this.isRequired || false;
    this.errorOnSubmit=true;
    this.credentials.emptyPasswordError=true;
    this.credentials.emptyConfirmPasswordError=true;
    this.credentials.passwordImage = {"text":"../../../../../src/assets/images/css/ic-password-show.png",
                            "mask":"../../../../../src/assets/images/css/ic-password-hide.png"};
    this.credentials.currentPasswordState = 'password';
    this.credentials.confirmPasswordState = 'password';

    this.changeImage = function(flag) {
      if(flag){
        this.credentials.currentPasswordState = this.credentials.currentPasswordState === 'text' ? 'password': 'text';
      } else {
        this.credentials.confirmPasswordState = this.credentials.confirmPasswordState === 'text' ? 'password': 'text';
      }
   	};

  	this.verifyPassword = function(form){
      this.credentials.emptyConfirmPasswordError = !this.credentials.confirmPassword;
  		this.credentials.isInvalidConfirmPassword = this.credentials.userPassword !== this.credentials.confirmPassword;
      this.setFormValidity(form);
  	};

    this.analyzePassword = function(form) {
			this.verifyPassword(form);
      if(!this.isRequired && (this.credentials.userPassword == "undefined" || this.credentials.userPassword == "")) {
        form.$$parentForm.$setValidity("password", true);
        return true;
      }
      var password = this.credentials.userPassword;
      var strength = this.isSatisfied(password && password.length >= 8) &&
        (this.isSatisfied(password && /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(password)) +
              this.isSatisfied(password && /[a-z]/.test(password)) +
              this.isSatisfied(password && /[A-Z]/.test(password)) +
              this.isSatisfied(password && /[0-9]/.test(password)));
      this.credentials.isInvalidPassword = strength < 3;
      this.credentials.emptyPasswordError = !password;
      this.errorOnSubmit=false;
      this.setFormValidity(form);
    };

    this.setFormValidity = function(form) {
      if( this.credentials.isInvalidPassword || this.credentials.isInvalidConfirmPassword ){
            form.$$parentForm.$setValidity("password", false);
        } else {
            form.$$parentForm.$setValidity("password", true);
        }
    };

    this.isSatisfied = function(criteria) {
        return criteria ? 1 : 0;
    };

    if(this.passwordHint) {
      this.passwordHintMessage = $sce.trustAsHtml(this.passwordHint);
    }

};

module.exports = passwordPatternController;
