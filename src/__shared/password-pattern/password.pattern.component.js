var template =require('./password.pattern.jade');
var controller =require('./password.pattern.controller');

var passwordPatternComponent =  {
	transclude: true,
    bindings:{
    	credentials: '=',
        isRequired: '<',
        isSubmitted: '<',
        passwordHint: '='
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'  
};

module.exports= passwordPatternComponent;
