var angular =require('angular');
var passwordStrengthDirective = require('../password-strength/password.strength.directive');
//var passwordStrengthModule =require('../password-strength/password.strength.module');
var passwordPatternComponent =require('./password.pattern.component');

var passwordPatternModule = angular.module('passwordPattern', [
])
.component('passwordPattern', passwordPatternComponent)
.directive('passwordStrength', ['$compile',  function () { return new passwordStrengthDirective() }]);

import Styles from './password.pattern.component.scss';

module.exports= passwordPatternModule;