var template =require('./spacer.component.jade');
var controller =require('./spacer.component.controller');

var spacerComponent =  {
    templateUrl: template,
  controller: controller,
  controllerAs: 'spacerctrl',
  replace: true,
  bindings: {
      custheight: "@",
      custcolor: "@",
      deviceonly: "@"
  }

};

module.exports= spacerComponent;
