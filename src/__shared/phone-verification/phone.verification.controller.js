'use strict'
phoneVerificationController.$inject=["phoneVerificationService","$scope", "$uibModalInstance", "credentials"];
function phoneVerificationController(phoneVerificationService, $scope, $uibModalInstance, credentials){
  var self = this;
  this.$uibModalInstance = $uibModalInstance;
  this.phoneVerificationService = phoneVerificationService;
  this.confirmationCodeInput = "";
  this.invalidData = false;
  this.credentials = credentials;
  this.confirmCode=function () {
    var confirmationCodeValue = this.confirmationCodeInput;
    //Please uncomment this stuff for AEM integration and also provide url name in verifyCredetials function in service file.
    /*        
    this.phoneVerificationService.verifyCredentials({"code": this.confirmationCodeInput}).then((data) {
      self.invalidData = data;
      self.$uibModalInstance.close();
    });
    */
    if(confirmationCodeValue==111)
    {
        this.invalidData = true;
        this.credentials.isNumberVerified = true;

        this.$uibModalInstance.close({
          "credentials": this.credentials
        });
        
        
    }else{
      this.invalidData = true;
    }
}
  this.cancel = function() {
    this.$uibModalInstance.dismiss();
  };
};

module.exports = phoneVerificationController;