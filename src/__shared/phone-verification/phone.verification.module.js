var angular =require('angular');
var phoneVerificationComponent =require('./phone.verification.component');
var phoneVerificationService = require('./phone.verification.service');

var phoneVerificationModule = angular.module('phoneVerification', [
])
.component('phoneVerification', phoneVerificationComponent)
.service('phoneVerificationService', phoneVerificationService);

import Styles from './phone.verification.component.scss';

module.exports= phoneVerificationModule;