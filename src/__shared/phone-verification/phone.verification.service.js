'use strict';

phoneVerificationService.$inject = ['$http','$q'];

function phoneVerificationService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

phoneVerificationService.prototype.verifyCredentials = function(data){
	var deferred = this.$q.defer();
	this.$http.post("", data)
		.success(deferred.resolve(data))
		.error(deferred.reject(data));

		return deferred.promise;
};

module.exports = phoneVerificationService;