var template =require('./phone.verification.jade');
var controller =require('./phone.verification.controller');

var phoneVerificationComponent =  {
	transclude: true,
    bindings:{
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'  
};

module.exports= phoneVerificationComponent;
