'use strict';

var template = require('./quick.links.jade');

var quickLinksComponent =  {
    restrict: 'E',
    bindings: {
      quickLinks : '<',
      config : '<'
    },
    templateUrl: template,
    controller: quickLinksController,
    controllerAs: 'ql',
    bindToController: true
};

function quickLinksController($window){

    var self = this;
    self.breakpoint = function(){
        if($window.innerWidth>=375 && $window.innerWidth<768)
            return "sm";
        else if($window.innerWidth>=768 && $window.innerWidth<1024)
            return "md";
        else if($window.innerWidth>=1024 && $window.innerWidth<1439)
            return "lg";
        else if($window.innerWidth>=1440)
            return "xl";
    }
    
    self.navigate = function(link){
        console.log(link);
    }
}

module.exports= quickLinksComponent;
