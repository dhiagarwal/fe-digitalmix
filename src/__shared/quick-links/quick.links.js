'use strict';
var angular = require('angular');
require('angular-animate');
var quickLinksComponent = require('./quick.links.component');
var quickLinksModule = angular.module('quickLinksModule', ['ngAnimate','slickCarousel'])
    .component('quickLinksComponent', quickLinksComponent);

import Styles from './quick.links.scss';

module.exports = quickLinksModule;
