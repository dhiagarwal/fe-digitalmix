'use strict';
var angular = require('angular');

var formUtilsModule = angular.module('formUtilsModule', []);

formUtilsModule.filter('validMonths', function() {
    return function(months, year) {
        var filtered = [];
        var now = new Date();
        var over18Month = now.getUTCMonth() + 1;
        var over18Year = now.getUTCFullYear() - 18;
        if (year != "") {
            if (year == over18Year) {
                angular.forEach(months, function(month) {
                    if (month.id <= over18Month) {
                        filtered.push(month);
                    }
                });
            } else {
                angular.forEach(months, function(month) {
                    filtered.push(month);
                });
            }
        }
        return filtered;
    };
});
formUtilsModule.filter('daysInMonth', function() {
    return function(days, year, month) {
        var filtered = [];
        angular.forEach(days, function(day) {
            if (month != "") {
                if (month.id == 1 || month.id == 3 || month.id == 5 || month.id == 7 || month.id == 8 || month.id == 10 || month.id == 12) {
                    filtered.push(day);
                } else if ((month.id == 4 || month.id == 6 || month.id == 9 || month.id == 11) && day.id <= 30) {
                    filtered.push(day);
                } else if (month.id == 2) {
                    if (year % 4 == 0 && day.id <= 29) {
                        filtered.push(day);
                    } else if (day.id <= 28) {
                        filtered.push(day);
                    }
                }
            }
        });
        return filtered;
    };
});
formUtilsModule.filter('validDays', function() {
    return function(days, year, month) {
        var filtered = [];
        var now = new Date();
        var over18Day = now.getUTCDate();
        var over18Month = now.getUTCMonth() + 1;
        var over18Year = now.getUTCFullYear() - 18;
        if (year == over18Year && month.id == over18Month) {
            angular.forEach(days, function(day) {
                if (day.id <= over18Day) {
                    filtered.push(day);
                }
            });
        } else {
            angular.forEach(days, function(day) {
                filtered.push(day);
            });
        }
        return filtered;
    };
});

formUtilsModule.filter("ssnCheck", function() {
    return function(value) {
        if (value) {
            if (value.length >= 9) {
                return value.substr(0, 3) + "-" + value.substr(3, 2) + "-" + value.substr(5);
            } else if (value.length >= 5) {
                return value.substr(0, 3) + "-" + value.substr(3, 2) + "-" + value.substr(5);
            } else if (value.length >= 3) {
                return value.substr(0, 3) + "-" + value.substr(3);
            }

        }
        return value;
    };
});

formUtilsModule.directive("moveNextOnMaxlength", function() {
    return {
        restrict: "A",
        link: function($scope, element) {
            element.on("input", function(e) {
                if (element.val().length == element.attr("maxlength")) {
                    var $nextElement = element.closest('div').next().find('input');
                    if ($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
});

formUtilsModule.directive('allowPattern', function() {
    return {
        compile: function (tElement, tAttrs) {
            return function (scope, element, attrs) {
                // handle key events
                element.bind("keypress", function (event) {
                    var keyCode = event.which || event.keyCode; // get the keyCode pressed from the event.
                    var keyCodeChar = String.fromCharCode(keyCode); //determine the char from the keyCode.
                    if (keyCodeChar) {
                        if (tAttrs.allowPattern) {
                            switch (tAttrs.allowPattern) {
                                case 'number-only':
                                    if ((keyCode >= 65 && keyCode <= 91) || (keyCode >= 97 && keyCode <= 122)) { /* Restrict Alphabets */
                                        return false;
                                    }
                                    break;
                                case 'alphabets-only':
                                    if (keyCode >= 48 && keyCode <= 57) { /* Restrict Numbers */
                                        return false;
                                    }
                                    break;
                            }
                        }
                        return !/^[\\\"\'\;\:\>\|\[\]\-_=+~`!@#\$%^<.,?{}/&*\(\)]$/i.test(keyCodeChar); /* Restrict Special Characters */
                    }
                });
            };
        }
    };
});

formUtilsModule.directive('allowData', function () {
    return {
        require: 'ngModel',
        link: function link(scope, elem, attrs, ngModel) {
            ngModel.$parsers.push(function (viewValue) {
                var reg = null;
                switch(attrs.allowData) {
                    case 'numbers-only':
                    reg = /^[^`~!€£¥₹₹@#$%\^&*()_+={}|[\]\\:';"<>?,./A-Za-z/-]*$/;
                    break;
                    case 'alphabets-only':
                    reg = /^[^`~!€£¥₹₹@#$%\^&*()_+={}|[\]\\:';"<>?,./0-9/-]*$/;
                    break;
                    default:
                    reg = /^[^`~!€£¥₹₹@#$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
                    break;
                }
                if (viewValue.match(reg)) {
                    return viewValue;
                }
                var transformedValue = ngModel.$modelValue;
                ngModel.$setViewValue(transformedValue);
                ngModel.$render();
                return transformedValue;
            });
        }
    }
});

formUtilsModule.directive('getDynamicHeight', function($window) {
    return {
        replace: true,
        link: function(scope, ele, attr) {
            ele.css({ 'min-height': ($window.innerHeight / 16) + 'rem' });
        }
    }
});

formUtilsModule.directive('addMinWidth', function($window) {
    return {
        link: function (scope, ele, attr) {
            var viewPortWidth = angular.element($window).width();
            if (viewPortWidth && viewPortWidth < 768) {
                ele.css({ 'min-width': (viewPortWidth / 16) + 'rem' });
            }
        }
    }
});

formUtilsModule.directive('screenWidth', ['$window', function($window) {
    return {
        restrict: 'EA',
        link: function link(scope, element, attrs) {
            scope.width = $window.innerWidth;
            angular.element($window).bind('resize', function() {
                scope.width = $window.innerWidth;
                scope.$digest();
            });
        }
    };
}]);

formUtilsModule.directive('noAlphaChars', function() {
    var number_check = /^\d+$/;

    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(value) {
                var status = true;
                status = status && number_check.test(value);
                ngModel.$setValidity('no-alpha-chars', status);
                return value;
            });
        }
    }
});

formUtilsModule.filter('phone', function() {
    return function(tel) {
        if (!tel) {
            return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
});

formUtilsModule.directive('selectOnClick', function () {
    // Linker function
    return function (scope, element, attrs) {
      element.on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this.setSelectionRange(0, 9999);
      });
    };
});

formUtilsModule.directive('equiHeight', function ($timeout) {
    return function (scope, element, attrs) {
        $timeout(function(){
            var allButtons = angular.element(element).find('.parent-equi-height .equi-height');
            var tempHeight=0;
            angular.forEach(allButtons, function(k, v){
                tempHeight = tempHeight < allButtons[v].offsetHeight ? allButtons[v].offsetHeight : tempHeight;
            });
            angular.element(element).find('.parent-equi-height .equi-height').css('height',tempHeight/16+'rem');
            
        }, 0);
       
    }
});

module.exports = formUtilsModule;