'use strict';
var angular = require('angular');
var scrollToTopWithoutClickService = require('./scroll-to-top-without-click.service');

var scrollToTopWithoutClickModule = angular.module('scrollToTopWithoutClickModule', [])
    .service('scrollToTopWithoutClickService', scrollToTopWithoutClickService);

module.exports = scrollToTopWithoutClickModule;
