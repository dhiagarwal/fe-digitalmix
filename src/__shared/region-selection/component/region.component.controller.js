'use strict'

function regionSelectionController(regionSelectionComponentService, $location){
    var self=this;
    this.regionData = {};


    var promise = regionSelectionComponentService.getRegionData();
    promise.then(function(data) {
        self.regionData = data;

    });

    self.countryClick = function(){
        $location.path("");
    }
    
};
module.exports = regionSelectionController;
