var angular =require('angular');
var regionSelectionComponent =require('./region.component');
var regionSelectionComponentService = require('./region.component.service')

var regionSelectionModule = angular.module('regionModule', [])
.component('regionselectioncomponent', regionSelectionComponent)
.service('regionSelectionComponentService', regionSelectionComponentService)

import Styles from './region.component.scss';

module.exports= regionSelectionModule;