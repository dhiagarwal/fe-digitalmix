'use strict';

regionSelectionComponentService.$inject = ['$http','$q'];

function regionSelectionComponentService($http,$q) {
  this.$http = $http;
  this.$q = $q;
  var deferred = $q.defer();

};

regionSelectionComponentService.prototype.getRegionData = function(){
 		var deferred = this.$q.defer();

 		this.$http.get('../../../../src/__shared/region-selection/component/region.component.json')
 			.success(deferred.resolve);

 			return deferred.promise;
 }

module.exports = regionSelectionComponentService;