	var template =require('./region.component.jade');
var controller =require('./region.component.controller');

var regionSelectionComponent =  {
    templateUrl: template,
    controller: controller,
    controllerAs: 'regionSelector',
    bindToController: true
};

module.exports= regionSelectionComponent;