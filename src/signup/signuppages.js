var angular = require('angular');
var register = require('../signup/pages/register/register.js');
var signupDistributor = require('../signup/pages/signup-distributor/signup.distributor.js');
var signupDistributorStart = require('../signup/pages/signup-distributor-start/signup.distributor.start.js');
var onBoardingPath = require('../signup/pages/welcome-board/welcome.board.viewer.js')
var termsAndConditions = require('../signup/pages/terms-and-conditions/terms.and.conditions.js');
var forgotPassword = require('../signup/pages/forgot-password/forgot.password.js');
var accountSettingsPersonal = require('../signup/pages/account-settings-personal/account.settings.personal.viewer.js');
var accountSettingsBusiness = require('../signup/pages/account-settings-business/account.settings.business.viewer.js');
var accountCreation = require('../signup/pages/account-creation/account.creation.viewer.js');
var accountVerification = require('../signup/pages/account-verification/account.verification.viewer.js');
var accountCompletion = require('../signup/pages/account-completion/account.completion.viewer.js');
var forgotUsername = require('../signup/pages/forgot-username/forgot.username.js');
var findSponsor = require('../signup/pages/find-sponsor/sponsor.js');
var login = require('../signup/pages/login/login.js');
var checkoutAuthentication = require('../signup/pages/checkout-authentication/checkout.authentication.viewer.js');
var reviewSignupDistributor = require('../signup/pages/review-signup-info-viewer/review.signup.info.viewer.js');
var customersignuppostpurchase = require('../signup/pages/customer-signup-post-purchase/customer.signup.post.purchase.js');
var createaccount = require('../signup/pages/create-account/create.account.js');
var accountinfo = require('../signup/pages/account-info/account.info.js');
var friendsFamilySignup = require('../signup/pages/friends-family-signup/friends.family.signup.viewer.js');
var friendsFamilySignupCreatePwd = require('../signup/pages/friends-family-create-password/friends.family.signup.create.password.viewer.js');
var associateSignup = require('../signup/pages/associate-signup/associate.signup.viewer.js');
var associateSignupCreatePwd = require('../signup/pages/associate-signup-create-password/associate.signup.create.password.viewer.js');
var associateLogin = require('../signup/pages/associate-login-viewer/associate.login.viewer.js');
var individualDistributorSetup = require('../signup/pages/individual-distributor-setup/individual.distributor.setup.js');
var reviewInfoDistributor = require('../signup/pages/review-info-distributor/review.info.distributor.js');
var demoModal = require('../signup/pages/demo-modal/demo.modal.viewer.js');
var linkSocialAccount = require('../signup/pages/link-social-account/link.social.account.js')
var distributorInviteTypeViewer = require('../signup/pages/distributor-invite/distributor-invite-type/distributor.invite.type.viewer.js');
var distributorInviteLinkViewer = require('../signup/pages/distributor-invite/distributor-invite-link/distributor.invite.link.viewer.js');
var termsConditionsViewer = require('../signup/pages/terms-conditions/terms.conditions.viewer.js');
var landingPageCustomerViewer = require('../signup/pages/landing-page-customer/landing.page.customer');
var landingPageDistributorViewer = require('../signup/pages/landing-page-distributor/landing.page.distributor');
var alertsNotificationViewer=require('../signup/pages/alerts-notification/alerts.notification.viewer');
var friendsFamilyExpiredSignupViewer = require('../signup/pages/friends-family-expired-signup/friends.family.expired.signup.viewer');
var pwdResetExpiredSignupViewer = require('../signup/pages/pwd-reset-expired-signup/pwd.reset.expired.signup.viewer');
var signupModule = angular.module('app.signuppages', [
    register.name,
    signupDistributor.name,
    signupDistributorStart.name,
    termsAndConditions.name,
    forgotPassword.name,
    onBoardingPath.name,
    accountSettingsPersonal.name,
    accountSettingsBusiness.name,
    customersignuppostpurchase.name,
    login.name,
    checkoutAuthentication.name,
    reviewSignupDistributor.name,
    createaccount.name,
    accountinfo.name,
    accountCreation.name,
    accountVerification.name,
    accountCompletion.name,
    associateSignup.name,
    associateSignupCreatePwd.name,
    forgotUsername.name,
    findSponsor.name,
    friendsFamilySignup.name,
    friendsFamilySignupCreatePwd.name,
    associateLogin.name,
    linkSocialAccount.name,
    individualDistributorSetup.name,
    reviewInfoDistributor.name,
    demoModal.name,
    distributorInviteTypeViewer.name,
    distributorInviteLinkViewer.name,
    termsConditionsViewer.name,
    landingPageCustomerViewer.name,
    landingPageDistributorViewer.name,
    alertsNotificationViewer.name,
    friendsFamilyExpiredSignupViewer.name,
    pwdResetExpiredSignupViewer.name
]);

module.exports = signupModule;
