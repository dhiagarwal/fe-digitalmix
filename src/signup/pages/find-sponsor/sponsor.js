var angular=require('angular');
var uiRouter =require('angular-ui-router');
var sponsorComponent = require('./sponsor.component');
var sponsorComponentModule = require('../../components/find-sponsor/sponsor');
console.log(sponsorComponentModule.name);
var sponsorViewerModule = angular.module('sponsorComponentModule', [
  uiRouter,
  sponsorComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('findSponsor', {
      url: '/findSponsor',
      template: '<sponsorviewer></sponsorviewer>'
    });
})
.component('sponsorviewer', sponsorComponent);

module.exports = sponsorViewerModule;
