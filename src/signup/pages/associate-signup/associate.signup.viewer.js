'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var associateSignupViewerComponent = require('./associate.signup.viewer.component');
var associateSignup = require('../../components/associate-signup/associate.signup.component.module');
var associateSignupModule = angular.module('associateSignupModule', [
  uiRouter,
  associateSignup.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('associatesignup', {
      url: '/associatesignup',
      template: '<associatesignup></associatesignup>'
    });
}])
.component('associatesignupViewer', associateSignupViewerComponent);


module.exports= associateSignupModule;
