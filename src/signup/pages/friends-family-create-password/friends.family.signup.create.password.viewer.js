'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var friendsfamilySignupCreatePwdViewerComponent = require('./friends.family.signup.create.password.viewer.component');
var friendsFamilySignupCreatePwd = require('../../components/friends-family-create-password/friends.family.signup.create.password.component.module');

var friendsFamilySignupCreatePwdModule = angular.module('friendsFamilySignupCreatePwdModule', [
  uiRouter,
  friendsFamilySignupCreatePwd.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('friendsfamilysignupcreatepwd', {
      url: '/friendsfamilysignupcreatepwd',
      template: '<friendsfamilysignupcreatepwd></friendsfamilysignupcreatepwd>'
    });
}])
.component('friendsfamilysignupcreatepwdViewer', friendsfamilySignupCreatePwdViewerComponent);


module.exports= friendsFamilySignupCreatePwdModule;