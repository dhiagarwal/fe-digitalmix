var angular=require('angular');
var uiRouter =require('angular-ui-router');
var loginComponent =require('./login.component');
var signin =require('../../components/signin/signin');

var loginModule = angular.module('login', [
  uiRouter,
  signin.name
])

.config(($stateProvider) => {
  $stateProvider
    .state('login', {
      url: '/login',
      template: '<login></login>'
    });
})
.component('login', loginComponent);


module.exports= loginModule;