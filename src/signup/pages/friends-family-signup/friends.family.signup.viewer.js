'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var friendsFamilySignupViewerComponent = require('./friends.family.signup.viewer.component');
var friendsFamilySignup = require('../../components/friends-family-signup/friends.family.signup.component.module');

var friendsFamilySignupModule = angular.module('friendsFamilySignupModule', [
  uiRouter,
  friendsFamilySignup.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('friendsfamilysignup', {
      url: '/friendsfamilysignup?:accountType',
      template: '<friendsfamilysignup></friendsfamilysignup>'
    });
}])
.component('friendsfamilysignupViewer', friendsFamilySignupViewerComponent);


module.exports= friendsFamilySignupModule;