var angular=require('angular');
var uiRouter =require('angular-ui-router');
var landingPageDistributorComponent = require('./landing.page.distributor.component');
var landingPageDistributorComponentModule = require('../../components/landing-page-distributor/landing.page.distributor');

var landingPageDistributorViewerModule = angular.module('landingPageDistributorComponentModule', [
  uiRouter,
  landingPageDistributorComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('landingPageDistributor', {
      url: '/landingPageDistributor',
      template: '<landingpagedistributorviewer></landingpagedistributorviewer>'
    });
})
.component('landingpagedistributorviewer', landingPageDistributorComponent);

module.exports = landingPageDistributorViewerModule;
