'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var termsAndConditionsViewerComponent = require('./terms.and.conditions.component');
var termsandconditions = require('../../components/terms-and-conditions/termsandconditions');

var termsAndConditionsModule = angular.module('termsAndConditionsModule', [
  uiRouter,
  termsandconditions.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('termsandconditionspage', {
      url: '/termsandconditionspage',
      template: '<termsandconditionspage></termsandconditionspage>'
    });
}])
.component('termsandconditionspage', termsAndConditionsViewerComponent);


module.exports= termsAndConditionsModule;
