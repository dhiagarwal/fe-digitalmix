var angular = require('angular');
var uiRouter = require('angular-ui-router');
var reviewInfoDistributorComponent = require('./review.info.distributor.component');
var reviewInfoDistributorComponentModule = require('../../components/review-info-distributor/review.info.distributor');

var reviewInfoDistributorViewerModule = angular.module('reviewInfoDistributorComponentModule', [
  uiRouter,
  reviewInfoDistributorComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('reviewInfoDistributor', {
      url: '/reviewInfoDistributor',
      template: '<reviewinfodistributorviewer></reviewinfodistributorviewer>'
    });
})
.component('reviewinfodistributorviewer', reviewInfoDistributorComponent);

module.exports = reviewInfoDistributorViewerModule;
