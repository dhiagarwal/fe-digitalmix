'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountSettingsPersonalViewerComponent = require('./account.settings.personal.viewer.component');
var accountsettingsp = require('../../components/account-settings-personal/account.settings.personal.component.module');

var accountSettingsPersonalModule = angular.module('accountSettingsPersonalModule', [
  uiRouter,
  	accountsettingsp.name,
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('accountsettingspersonalpage', {
      url: '/accountsettingspersonalpage?:state',
      template: '<accountsettingspersonalpage></accountsettingspersonalpage>'
    });
}])
.component('accountsettingspersonalpage', accountSettingsPersonalViewerComponent);


module.exports= accountSettingsPersonalModule;
