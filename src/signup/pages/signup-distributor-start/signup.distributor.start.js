var angular=require('angular');
var uiRouter =require('angular-ui-router');
var signupDistributorStartComponent = require('./signup.distributor.start.component');
var distributorsetupComponent = require('../../components/distributorsetup/distributorsetup');
var userProfileHeaderModule = require('../../components/user-profile-header/user.profile.header');

var signupDistributorStartModule = angular.module('signupDistributorStartModule', [
  uiRouter,
  distributorsetupComponent.name,
  userProfileHeaderModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('signupdistributorstart', {
      url: '/signupdistributorstart',
      template: '<signupdistributorstart></signupdistributorstart>'
    });
})
.component('signupdistributorstart',signupDistributorStartComponent);

module.exports = signupDistributorStartModule;