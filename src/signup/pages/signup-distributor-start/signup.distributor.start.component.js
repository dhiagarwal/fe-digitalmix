'use strict';
var template = require('./signup.distributor.start.jade');
var signupDistributorStartComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = signupDistributorStartComponent;