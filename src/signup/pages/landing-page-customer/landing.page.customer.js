var angular=require('angular');
var uiRouter =require('angular-ui-router');
var landingPageCustomerComponent = require('./landing.page.customer.component');
var landingPageCustomerComponentModule = require('../../components/landing-page-customer/landing.page.customer');

var landingPageCustomerViewerModule = angular.module('landingPageCustomerComponentModule', [
  uiRouter,
  landingPageCustomerComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('landingPageCustomer', {
      url: '/landingPageCustomer',
      template: '<landingpagecustomerviewer></landingpagecustomerviewer>'
    });
})
.component('landingpagecustomerviewer', landingPageCustomerComponent);

module.exports = landingPageCustomerViewerModule;
