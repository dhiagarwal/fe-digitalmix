'use strict';
var template = require('./review.signup.info.viewer.jade');
var reviewSignupInfoViewerComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = reviewSignupInfoViewerComponent;