var angular=require('angular');
var uiRouter =require('angular-ui-router');
var reviewSignupInfoViewerComponent = require('./review.signup.info.viewer.component');
var reviewSignupInfoModule = require('../../components/review-signup-info/review.signup.info');
var userProfileHeaderModule = require('../../components/user-profile-header/user.profile.header');

var reviewSignupInfoViewerModule = angular.module('reviewSignupInfoViewerModule', [
  uiRouter,
  reviewSignupInfoModule.name,
  userProfileHeaderModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('reviewsignupinfopage', {
      url: '/reviewsignupinfopage',
      template: '<reviewsignupinfopage></reviewsignupinfopage>'
    });
})
.component('reviewsignupinfopage',reviewSignupInfoViewerComponent);


var base =require('../../../stylesheets/base.scss');

module.exports = reviewSignupInfoViewerModule;