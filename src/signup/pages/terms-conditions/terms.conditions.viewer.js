'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var termsConditionsViewerComponent = require('./terms.conditions.viewer.component');
var termsConditionsComponentModule = require('../../components/terms-conditions/terms.conditions.component.module');

var termsConditionsModule = angular.module('termsConditionsModule', [
  uiRouter,
  termsConditionsComponentModule.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('termsconditions', {
      url: '/termsconditions',
      template: '<termsconditionsviewer></termsconditionsviewer>'
    });
}])
.component('termsconditionsviewer', termsConditionsViewerComponent);

module.exports= termsConditionsModule;