var angular=require('angular');
var uiRouter =require('angular-ui-router');
var onBoardingViewerComponent = require('./welcome.board.viewer.component');
var onBoardingComponentModule = require('../../components/welcome-board/welcome.board.component.module');
console.log(onBoardingComponentModule.name);
var onBoardingViewerModule = angular.module('onBoardingViewerModule', [
  uiRouter,
  onBoardingComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('welcomeOnBoard', {
      url: '/welcomeOnBoard',
      template: '<onboardingviewer></onboardingviewer>'
    });
})
.component('onboardingviewer',onBoardingViewerComponent);

module.exports= onBoardingViewerModule;
