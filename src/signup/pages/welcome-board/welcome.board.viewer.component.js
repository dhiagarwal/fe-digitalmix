'use strict';
var template = require('./welcome.board.viewer.jade');
var onBoardingViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = onBoardingViewerComponent;
