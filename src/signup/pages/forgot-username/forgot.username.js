var angular=require('angular');
var uiRouter =require('angular-ui-router');
var forgotUsernameComponent =require('./forgot.username.component');
var signinforgotusername =require('../../components/signin-forgot-username/signin.forgot.username.component.module');


var forgotUsernameModule = angular.module('forgotusername', [
  uiRouter,
  signinforgotusername.name
])

.config(($stateProvider) => {
  $stateProvider
    .state('forgotusername', {
      url: '/forgotusername',
      template: '<forgotusername></forgotusername>'
    });
})
.component('forgotusername', forgotUsernameComponent);


module.exports= forgotUsernameModule;
