'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountCompletionViewerComponent = require('./account.completion.viewer.component');
var accountCompletion = require('../../components/account-completion/account.completion.component.module');

var accountCompletionModule = angular.module('accountCompletionModule', [
  uiRouter,
  accountCompletion.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('accountCompletion', {
      url: '/accountCompletion',
      template: '<account-completion></account-completion>'
    });
}])
.component('accountCompletionViewer', accountCompletionViewerComponent);


module.exports= accountCompletionModule;
