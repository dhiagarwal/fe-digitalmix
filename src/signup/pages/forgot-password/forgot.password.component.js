'use strict';
var template = require('./forgot.password.jade');
var forgotPasswordComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = forgotPasswordComponent;
