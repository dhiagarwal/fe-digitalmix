'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var forgotPasswordViewerComponent = require('./forgot.password.component');
var forgotpassword = require('../../components/forgotpassword/forgot.password');
// console.log(forgotpassword.name);
var forgotPasswordModule = angular.module('forgotPasswordModule', [
  uiRouter,
  forgotpassword.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('forgotpassword', {
      url: '/forgotpassword',
      template: '<forgotpassword></forgotpassword>'
    });
}])
.component('forgotpassword', forgotPasswordViewerComponent);


module.exports= forgotPasswordModule;
