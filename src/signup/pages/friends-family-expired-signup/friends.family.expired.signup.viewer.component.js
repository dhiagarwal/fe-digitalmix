'use strict';
var template = require('./friends.family.expired.signup.viewer.jade');
var friendsFamilyExpiredSignupViewerComponent = {
	templateUrl: template
};

module.exports = friendsFamilyExpiredSignupViewerComponent;