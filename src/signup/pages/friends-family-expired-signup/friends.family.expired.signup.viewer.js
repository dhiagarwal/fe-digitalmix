var angular = require('angular');
var uiRouter = require('angular-ui-router');
var friendsFamilyExpiredSignupViewerComponent = require('./friends.family.expired.signup.viewer.component');
var friendsFamilyExpiredSignupComponentModule = require('../../../../src/signup/components/friends-family-expired-signup/friends.family.expired.signup.component.module');

var friendsFamilyExpiredSignupComponentViewerModule = angular.module('friendsFamilyExpiredSignupComponentViewerModule', [
  uiRouter,
  friendsFamilyExpiredSignupComponentModule.name
])

.config(['$stateProvider', function($stateProvider) {
  $stateProvider
    .state('expiredSignup', {
      url: '/expiredSignup',
      template: '<friendsfamilyexpiredsignupviewer></friendsfamilyexpiredsignupviewer>'
    });
}])

.component('friendsfamilyexpiredsignupviewer', friendsFamilyExpiredSignupViewerComponent);

module.exports = friendsFamilyExpiredSignupComponentViewerModule;
