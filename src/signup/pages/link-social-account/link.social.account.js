'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var linkSocialAccountViewerComponent = require('./link.social.account.component');
var linkSocialAccount = require('../../components/linksocialaccount/link.social.account');
var linkSocialAccountModule = angular.module('linkSocialAccountModule', [
  uiRouter,
  linkSocialAccount.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('linksocialaccount', {
      url: '/linkSocialAccount',
      template: '<linksocialaccount></linksocialaccount>'
    });
}])
.component('linksocialaccount', linkSocialAccountViewerComponent);


module.exports= linkSocialAccountModule;
