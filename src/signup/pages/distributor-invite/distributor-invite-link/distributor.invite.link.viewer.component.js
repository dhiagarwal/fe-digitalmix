'use strict';

var template = require('./distributor.invite.link.viewer.jade');
var inviteLinkViewerComponent = {
    restrict: 'E',
    templateUrl: template
};

module.exports = inviteLinkViewerComponent;