var angular = require('angular');
var uiRouter = require('angular-ui-router');
var inviteLinkViewerComponent = require('./distributor.invite.link.viewer.component');
var inviteLinkComponentModule = require('../../../components/distributor-invite/distributor-invite-link/distributor.invite.link.component.module');
var inviteLinkViewerModule = angular.module('inviteLinkViewerModule', [
        uiRouter,
        inviteLinkComponentModule.name
    ])
    .config(($stateProvider) => {
        $stateProvider
            .state('distributorInviteLink', {
                url: '/distributorSignup/distributorInviteLink',
                template: '<invitelinkviewer></invitelinkviewer>'
            });
    })
    .component('invitelinkviewer', inviteLinkViewerComponent);

module.exports = inviteLinkViewerModule;