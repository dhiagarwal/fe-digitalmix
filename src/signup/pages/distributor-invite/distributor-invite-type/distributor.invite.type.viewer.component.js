'use strict';

var template = require('./distributor.invite.type.viewer.jade');
var inviteTypeViewerComponent = {
    restrict: 'E',
    templateUrl: template
};

module.exports = inviteTypeViewerComponent;