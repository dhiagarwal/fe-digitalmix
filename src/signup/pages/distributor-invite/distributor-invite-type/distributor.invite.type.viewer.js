var angular = require('angular');
var uiRouter = require('angular-ui-router');
var inviteTypeViewerComponent = require('./distributor.invite.type.viewer.component');
var inviteTypeComponentModule = require('../../../components/distributor-invite/distributor-invite-type/distributor.invite.type.component.module');
var inviteTypeViewerModule = angular.module('inviteTypeViewerModule', [
        uiRouter,
        inviteTypeComponentModule.name
    ])
    .config(($stateProvider) => {
        $stateProvider
            .state('distributorInviteType', {
                url: '/distributorSignup/distributorInviteType',
                template: '<invitetypeviewer></invitetypeviewer>'
            });
    })
    .component('invitetypeviewer', inviteTypeViewerComponent);

module.exports = inviteTypeViewerModule;