'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var demoModalViewerComponent = require('./demo.modal.viewer.component');
var demoModal = require('../../components/demo-modal/demo.modal.component.module');

var demoModalModule = angular.module('demoModalModule', [
  uiRouter,
  demoModal.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('demoModal', {
      url: '/demoModal',
      template: '<demo-modal></demo-modal>'
    });
}])
.component('demoModalViewer', demoModalViewerComponent);


module.exports= demoModalModule;
