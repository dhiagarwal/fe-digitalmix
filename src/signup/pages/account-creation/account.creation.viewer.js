'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountCreationViewerComponent = require('./account.creation.viewer.component');
var accountcreation = require('../../components/account-creation/account.creation.component.module');

var accountCreationModule = angular.module('accountCreationModule', [
  uiRouter,
  accountcreation.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('accountcreation', {
      url: '/accountcreation',
      template: '<accountcreation class="acc-main"></accountcreation>'
    });
}])
.component('accountCreationViewer', accountCreationViewerComponent);


module.exports= accountCreationModule;
