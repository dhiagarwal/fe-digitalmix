'use strict';

var angular=require('angular');
var uiRouter=require('angular-ui-router');
var checkoutAuthenticationViewerComponent=require('./checkout.authentication.viewer.component');
var checkoutauthentication=require('../../components/checkout-authentication/checkout.authentication.component.module');

var checkoutAuthenticationModule=angular.module('checkoutAuthenticationModule',[
    uiRouter,
    checkoutauthentication.name
])
.config(['$stateProvider', function($stateProvider){
    $stateProvider
        .state('checkoutauthenticationpage',{
            url:'/checkoutauthenticationpage',
            template:'<checkoutauthenticationpage></checkoutauthenticationpage>'
        });
}])
.component('checkoutauthenticationpage',checkoutAuthenticationViewerComponent);

module.exports=checkoutAuthenticationModule;