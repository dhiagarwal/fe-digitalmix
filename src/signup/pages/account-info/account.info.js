'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountinfoViewerComponent = require('./account.info.component');
var accountinfo = require('../../components/accountinfo/account.info');
// console.log(forgotpassword.name);
var accountinfoModule = angular.module('accountinfoModule', [
  uiRouter,
  accountinfo.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('accountinfo', {
      url: '/accountInfo',
      template: '<accountinfo></accountinfo>'
    });
}])
.component('accountinfo', accountinfoViewerComponent);


module.exports= accountinfoModule;
