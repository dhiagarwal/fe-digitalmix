'use strict';
var template = require('./account.info.jade');
var accountinfoComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = accountinfoComponent;
