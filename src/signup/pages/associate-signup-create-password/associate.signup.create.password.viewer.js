'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var associateSignupCreatePwdViewerComponent = require('./associate.signup.create.password.viewer.component');
var associateSignupCreatePwd = require('../../components/associate-signup-create-password/associate.signup.create.password.component.module');

var associateSignupCreatePwdModule = angular.module('associateSignupCreatePwdModule', [
  uiRouter,
  associateSignupCreatePwd.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('associatesignupcreatepwd', {
      url: '/associatesignupcreatepwd',
      template: '<associatesignupcreatepwd></associatesignupcreatepwd>'
    });
}])
.component('associatesignupcreatepwdViewer', associateSignupCreatePwdViewerComponent);


module.exports= associateSignupCreatePwdModule;
