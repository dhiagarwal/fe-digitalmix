'use strict';
var template = require('./pwd.reset.expired.signup.viewer.jade');
var pwdResetExpiredSignupViewerComponent = {
	templateUrl: template
};

module.exports = pwdResetExpiredSignupViewerComponent;