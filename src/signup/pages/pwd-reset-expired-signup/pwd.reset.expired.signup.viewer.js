var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pwdResetExpiredSignupViewerComponent = require('./pwd.reset.expired.signup.viewer.component');
var pwdResetExpiredSignupComponentModule = require('../../../../src/signup/components/pwd-reset-expired-signup/pwd.reset.expired.signup.component.module');

var pwdResetExpiredSignupComponentViewerModule = angular.module('pwdResetExpiredSignupComponentViewerModule', [
  uiRouter,
  pwdResetExpiredSignupComponentModule.name
])

.config(['$stateProvider', function($stateProvider) {
  $stateProvider
    .state('pwdExpiredSignup', {
      url: '/pwdExpiredSignup',
      template: '<pwdresetexpiredsignupviewer></pwdresetexpiredsignupviewer>'
    });
}])

.component('pwdresetexpiredsignupviewer', pwdResetExpiredSignupViewerComponent);

module.exports = pwdResetExpiredSignupComponentViewerModule;
