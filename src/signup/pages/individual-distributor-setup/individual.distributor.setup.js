var angular = require('angular');
var uiRouter = require('angular-ui-router');
var individualDistributorSetupComponent = require('./individual.distributor.setup.component');
var individualDistributorSetupComponentModule = require('../../components/individual-distributor-setup/individual.distributor.setup');
console.log(individualDistributorSetupComponentModule.name);
var individualDistributorSetupViewerModule = angular.module('individualDistributorSetupComponentModule', [
  uiRouter,
  individualDistributorSetupComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('individualDistributorSetup', {
      url: '/individualDistributorSetup',
      template: '<individualdistributorsetupviewer></individualdistributorsetupviewer>'
    });
})
.component('individualdistributorsetupviewer', individualDistributorSetupComponent);

module.exports = individualDistributorSetupViewerModule;
