'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var customersignuppostpurchaseViewerComponent = require('./customer.signup.post.purchase.component');
var customersignuppostpurchase = require('../../components/customersignuppostpurchase/customer.signup.post.purchase');
// console.log(forgotpassword.name);
var seemoreseelessComponent = require('../../components/see-more-see-less/see.more.see.less');
var customersignuppostpurchaseModule = angular.module('customersignuppostpurchaseModule', [
  uiRouter,
  customersignuppostpurchase.name,
  seemoreseelessComponent.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('customersignuppostpurchase', {
      url: '/customerSignupPostPurchase',
      template: '<customersignuppostpurchase></customersignuppostpurchase>'
    });
}])
.component('customersignuppostpurchase', customersignuppostpurchaseViewerComponent);


module.exports= customersignuppostpurchaseModule;
