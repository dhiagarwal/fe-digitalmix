'use strict';
var template = require('./customer.signup.post.purchase.jade');
var customersignuppostpurchaseComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = customersignuppostpurchaseComponent;
