var angular=require('angular');
var uiRouter =require('angular-ui-router');
var registerComponent =require('./register.component');
var signup =require('../../components/signup/signup');
var signupPassword =require('../../components/signup-password/signup.password');
// var termsandconditions =require('../../components/terms-and-conditions/termsandconditions');

var registerModule = angular.module('register', [
  uiRouter,
  signup.name,
  signupPassword.name,
])

.config(($stateProvider) => {
  $stateProvider
    .state('register', {
      url: '/signup?:accountType',
      template: '<register></register>'
    });
})
.component('register', registerComponent);


module.exports= registerModule;
