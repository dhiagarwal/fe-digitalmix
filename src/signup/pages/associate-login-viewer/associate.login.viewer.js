var angular=require('angular');
var uiRouter =require('angular-ui-router');
var associateLoginViewerComponent = require('./associate.login.viewer.component');
var associateLoginModule = require('../../components/associate-login/associate.login.component.module');

var associateLoginViewerModule = angular.module('associateLoginViewerModule', [
	uiRouter,
	associateLoginModule.name
	])
.config(($stateProvider) => {
	$stateProvider
	.state('associateloginpage', {
		url: '/associateloginpage',
		template: '<associateloginpage></associateloginpage>'
	});
})
.component('associateloginpage',associateLoginViewerComponent);

var base =require('../../../stylesheets/base.scss');

module.exports = associateLoginViewerModule;