'use strict';
var template = require('./associate.login.viewer.jade');
var associateLoginViewerComponent = {
  templateUrl: template
};

module.exports = associateLoginViewerComponent;