var angular=require('angular');
var uiRouter =require('angular-ui-router');
var signupDistributorComponent = require('./signup.distributor.component');
var signupAccSetupModule = require('../../components/signup-acc-setup/signup.acc.setup');
var userProfileHeaderModule = require('../../components/user-profile-header/user.profile.header');

var signupDistributorModule = angular.module('signupDistributorModule', [
  uiRouter,
  signupAccSetupModule.name,
  userProfileHeaderModule.name,
])
.config(($stateProvider) => {
  $stateProvider
    .state('signupdistributor', {
      url: '/signupdistributor',
      template: '<signupdistributor></signupdistributor>'
    });
})
.component('signupdistributor',signupDistributorComponent);


var base =require('../../../stylesheets/base.scss');

module.exports = signupDistributorModule;