'use strict';
var template = require('./signup.distributor.jade');
var signupDistributorComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = signupDistributorComponent;