'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var createaccountViewerComponent = require('./create.account.component');
var createaccount = require('../../components/createaccount/create.account');
// console.log(forgotpassword.name);
var createaccountModule = angular.module('createaccountModule', [
  uiRouter,
  createaccount.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('createaccount', {
      url: '/createAccount',
      template: '<createaccount></createaccount>'
    });
}])
.component('createaccount', createaccountViewerComponent);


module.exports= createaccountModule;
