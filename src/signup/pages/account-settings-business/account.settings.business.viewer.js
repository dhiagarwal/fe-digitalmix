'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountSettingsBusinessViewerComponent = require('./account.settings.business.viewer.component');
var accountsettingsbusiness = require('../../components/account-settings-business/account.settings.business.component.module');

var accountSettingsBusinesslModule = angular.module('accountSettingsBusinessModule', [
  uiRouter,
  accountsettingsbusiness.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('accountsettingsbusinesspage', {
      url: '/accountsettingsbusinesspage',
      template: '<accountsettingsbusinesspage></accountsettingsbusinesspage>'
    });
}])
.component('accountsettingsbusinesspage', accountSettingsBusinessViewerComponent);


module.exports= accountSettingsBusinesslModule;
