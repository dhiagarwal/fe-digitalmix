'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountVerificationViewerComponent = require('./account.verification.viewer.component');
var accountVerification = require('../../components/account-verification/account.verification.component.module');

var accountVerificationModule = angular.module('accountVerificationModule', [
  uiRouter,
  accountVerification.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('accountVerification', {
      url: '/accountVerification',
      template: '<account-verification class="acc-main"></account-verification>'
    });
}])
.component('accountVerificationViewer', accountVerificationViewerComponent);


module.exports= accountVerificationModule;
