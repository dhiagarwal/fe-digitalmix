'use strict';

var angular=require('angular');
var uiRouter=require('angular-ui-router');
var alertsNotificationViewerComponent=require('./alerts.notification.viewer.component');
var alertsNotification=require('../../components/alerts-notification/alerts.notification.component.module');

var alertsNotificationModule=angular.module('alertsNotificationModule',[
    uiRouter,
    alertsNotification.name
])
.config(['$stateProvider', function($stateProvider){
    $stateProvider
        .state('alertsNotification',{
            url:'/alertsNotification',
            template:'<alertsnotification></alertsnotification>'
        });
}])
.component('alertsnotification',alertsNotificationViewerComponent);

module.exports=alertsNotificationModule;