'use strict';
var template = require('./associate.signup.component.jade');
var controller = require('./associate.signup.component.controller');

var associateSignupComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'assosignup',
};

module.exports = associateSignupComponent;