'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var associateSignupComponent = require('./associate.signup.component');
var socialmedia =require('../social-media/social.signup');
var mandatoryFieldsPage =require('../signup-acc-setup/signup.acc.setup');

var associateSignupComponentModule = angular.module('associateSignupComponentModule', [
					uiRouter,
					socialmedia.name,
					mandatoryFieldsPage.name
					])
                  .component('associatesignup', associateSignupComponent)
                  
import Styles from './associate.signup.component.scss';

module.exports = associateSignupComponentModule;