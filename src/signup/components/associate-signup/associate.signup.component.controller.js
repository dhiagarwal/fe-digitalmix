'use strict';
function associateSignupController() {
    var self=this;
    this.credentials={};
    this.isEmail=true;
    this.isRequired=true;
    this.isEmailSubmitted =false;
    this.nonDistributorStoreFront = false;
    // adding this flag for logic of rendering placeholder image when profile pic is not present
    // in AEM get actual data from back end
    this.profilepresent = false;
    this.assoSignupContinue=function() {
        var baseUrl=window.location.origin;
        window.location.href=baseUrl+'/#/associatesignupcreatepwd';
    };
};

module.exports = associateSignupController;