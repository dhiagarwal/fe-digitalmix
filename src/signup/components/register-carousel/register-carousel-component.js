'use strict';
var template = require('./register-carousel.html');

// as we are using component, return statement is expected to be an Object
var registerCarouselComponent = {
    restrict: 'E',
    templateUrl : template,
    controller: registerController,
    controllerAs: 'ctrl',
    bindToController: true
};

function registerController(){
    this.slides = [
        {
            image: '../../../assets/images/1.png',
            id:1
        },
        {
            image: '../../../assets/images/2.png',
            id:2
        },
        {
            image: '../../../assets/images/3.png',
            id:3
        }
    ];
    
}

module.exports = registerCarouselComponent;