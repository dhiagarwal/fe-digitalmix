'use strict';

require('./register-carousel.scss');

var angular = require('angular');
require('angular-ui-bootstrap');
require('angular-touch');
//require('./angular-carousel');


var registerCarousel = require('./register-carousel-component');

var registerCarouselModule = angular.module('register-module', [
    'ui.bootstrap',
    'ngTouch'
    //'angular-carousel'
]);

registerCarouselModule.component('registerCarousel', registerCarousel);

module.exports = registerCarouselModule;
