var template =require('./signup.jade');
var controller =require('./signup.controller');

var signUpComponent =  {
  //return {
    restrict: 'E',
    scope: {},
    templateUrl : template,
    controller,
    controllerAs: 'vm',
    bindToController: true
  //};
};

module.exports = signUpComponent;
