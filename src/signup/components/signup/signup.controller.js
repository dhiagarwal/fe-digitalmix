
function signUpController(signupService, $filter) {
    this.result = {};
    var self = this;
    this.service = signupService;
    this.name='signup';
    this.profileimage = {"img":"../../../../src/assets/images/content/distributor-avatar.png"}
    this.submitted=false;
    this.showSocialSignup = false;
    this.countryInput='Spain';
    this.languageInput='Spanish';
    self.communication = {};
    this.submitForm = function(formvalid) {
      this.clickedsubmit=true;
      if (formvalid) {
        this.showSocialSignup = true;
        return formvalid;
      }
      return false;
    }
  var lpromise = signupService.getLanguageList();
  lpromise.then(function (data) {
    self.countriesList = data.countries;
    self.communication.countryPref = self.countriesList[0].code;
    self.loadRegionLanguages(self.communication.countryPref);
  });

  self.loadRegionLanguages = function(countryCode) {
    var contryList = $filter('filter')(self.countriesList, {code: countryCode})[0];
    self.languagesList = contryList && contryList.languageList ? contryList.languageList : null;
     self.communication.languagepref = self.languagesList ? self.languagesList[0].id : null;
  
  }
}
module.exports = signUpController;
