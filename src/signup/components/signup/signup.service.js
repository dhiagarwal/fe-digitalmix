'use strict';
signupService.$inject = ['$http','$q'];
function signupService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

signupService.prototype.getLanguageList = function(){
    var deferred = this.$q.defer();
    this.$http.get("../../../../src/signup/components/signup/signup.json")
        .success(deferred.resolve);
    return deferred.promise;
};

module.exports = signupService;

