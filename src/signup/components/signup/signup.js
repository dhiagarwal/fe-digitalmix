var angular =require('angular');
var uiRouter =require('angular-ui-router');
var socialmedia =require('../social-media/social.signup');
var signupComponent =require('./signup.component');
var signupService =require('./signup.service');

var signupModule = angular.module('signup', [
  uiRouter,
  socialmedia.name
])
.component('signup', signupComponent)
.service('signupService', signupService);

console.log(signupComponent);
console.log(signupModule);

var base =require('../../../stylesheets/base.scss');
var Styles =require('./signup.scss'); 

module.exports= signupModule;
