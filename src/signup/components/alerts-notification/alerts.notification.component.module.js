'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var alertsNotificationComponent = require('./alerts.notification.component');


var alertsNotificationComponentModule = angular.module('alertsNotificationComponentModule', [uiRouter])
    .component('alertsnotificationcomponent', alertsNotificationComponent)


import Styles from './alerts.notification.component.scss';

module.exports = alertsNotificationComponentModule;