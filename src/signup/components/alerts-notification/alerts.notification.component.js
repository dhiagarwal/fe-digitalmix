'use strict';

var template = require('./alerts.notification.component.jade');
var controller = require('./alerts.notification.component.controller');

var alertsNotificationComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = alertsNotificationComponent;