'use strict';
function createaccountController($location) {
  var self = this;
  var vmp = this;
  this.credentials = {};
  this.isEmail = true;
  this.required= true;
  this.submit = false;
  this.navigatetoaccountinfo = function(form){
    this.submit = true;
    if(form.customLoginForm.$valid){
      $location.path('/accountInfo');
    }
  }
}
module.exports = createaccountController;
