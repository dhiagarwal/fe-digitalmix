var template =require('./create.account.jade');
var controller =require('./create.account.controller');

var createaccountComponent =  {
    templateUrl : template,
    controller: controller,
    controllerAs: 'createCtrl',
};

module.exports = createaccountComponent;
