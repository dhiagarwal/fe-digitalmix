var angular =require('angular');
var uiRouter =require('angular-ui-router');
var createaccountComponent =require('./create.account.component');

var createaccountModule = angular.module('createaccount', [
  uiRouter
])
.component('createaccountcomponent', createaccountComponent)
var base =require('../../../stylesheets/base.scss');
var Styles =require('./create.account.scss');

module.exports= createaccountModule;
