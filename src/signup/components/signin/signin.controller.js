'use strict';

var usernameVerificationModalTemplate = require('./signin.modal.username.jade');

function signinController(signinService, $location, $uibModal) {
    var self = this;
    self.result = {};
    self.service = signinService;
    self.clickedsignin = false;
    self.passwordempty = false;
    self.isEmail = true;
    self.credentials = {};
    self.isEmailAddressValidated = false;

    self.submitlogin = function () {
        self.clickedsignin = true;
        if (self.username == null || self.username == "") {
            self.usernameempty = true;
        }
        if (self.password == "" || self.password == null) {
            self.passwordempty = true;
        }
        if (self.usernameempty == false && self.passwordempty == false && self.match == true) {
            return true;
        }

        return false;

    }
    self.redirectFb = function () {
        // function to integrate social media api
    }
    self.redirectsignup = function () {
        $location.url("/signup")
    }
    self.redirectforgotpassword = function () {
        $location.url("/forgotpassword");
    }
    self.redirectforgotusername = function () {
        $location.url("/forgotusername");
    }
    self.enterpassword = function (value) {
        self.clickedsignin = false;
        self.passwordempty = false;
    }

    var modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };

    self.usernameVerificationController = function ($uibModalInstance) {
        var modalSelf = this;
        var hiddenEmailLength = 17;

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.phone = "1-800-487-1000";
        var email = self.logindetails.customLoginForm.userEmailField.$modelValue ? self.logindetails.customLoginForm.userEmailField.$modelValue : null;
        if (email) {
            var privacyEmail = email.substr(0, email.indexOf('@'));
            var tailEmail = email.substr(email.indexOf('@'));
            modalSelf.emailAddress = privacyEmail.substr(0, 2);
            for (var i = 2; i < ((privacyEmail.length <= hiddenEmailLength) ? privacyEmail.length : hiddenEmailLength); i++) {
                modalSelf.emailAddress += "*";
            }
            modalSelf.emailAddress += email.substr(email.indexOf('@'));
        }

        modalSelf.resendEmailFromPopup = function () {
            modalSelf.ok();
        };
    };

    self.resendEmail = function () {
        if (self.logindetails.customLoginForm.userEmailField.$modelValue) {
            modalPopupInstance(usernameVerificationModalTemplate, self.usernameVerificationController, 'app-modal-window email-username-popup');
        }
    };
}

module.exports = signinController;
