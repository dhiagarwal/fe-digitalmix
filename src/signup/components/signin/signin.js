var angular =require('angular');
var uiRouter =require('angular-ui-router');
var signinComponent =require('./signin.component');
var signinService =require('./signin.service');

var signinModule = angular.module('signin', [
  uiRouter
])
.component('signin', signinComponent)
.service('signinService', signinService);
var base =require('../../../stylesheets/base.scss');
var Styles =require('./signin.scss');

module.exports= signinModule;