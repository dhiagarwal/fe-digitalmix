var template = require('./signin.jade');
var controller = require('./signin.controller');

var signinComponent =  {
  //return {
    restrict: 'E',
    scope: {},
    templateUrl : template,
    controller:controller,
    controllerAs: 'signinCtrl',
    bindToController: true
  //};
};

module.exports= signinComponent;
