var angular =require('angular');
var uiRouter =require('angular-ui-router');
var customersignuppostpurchaseComponent =require('./customer.signup.post.purchase.component');

var customersignuppostpurchaseModule = angular.module('customersignuppostpurchase', [
  uiRouter
])
.component('customersignuppostpurchasecomponent', customersignuppostpurchaseComponent)
var base =require('../../../stylesheets/base.scss');
var Styles =require('./customer.signup.post.purchase.scss');

module.exports= customersignuppostpurchaseModule;
