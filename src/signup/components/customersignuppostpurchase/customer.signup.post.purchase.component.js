var template =require('./customer.signup.post.purchase.jade');
var controller =require('./customer.signup.post.purchase.controller');

var customersignuppostpurchaseComponent =  {
    templateUrl : template,
    controller: controller,
    controllerAs: 'customerCtrl',
};

module.exports = customersignuppostpurchaseComponent;
