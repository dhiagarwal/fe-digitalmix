function reviewSignupInfoController(reviewSignupInfoService, $window, birthdayFactory, statelistservice) {

    var model = this.model = {};
    var self = this;

    // all base objects for each section
    model.mandatoryInfo = {};
    model.address = {};
    model.payment = {};
    model.preferences = {};
    model.familyInfo = {};
    model.sponsor = {};
    model.associateList = [];
    model.stateList = {};
    model.hasError = { err: false, message: '' };
    model.mandatoryInfoOriginal = {};
    model.addressOriginal = {};
    model.paymentOriginal = {};
    model.preferencesOriginal = {};
    model.familyInfoOriginal = {};
    model.sponsorOriginal = {};
    model.associateInfoOriginal = {};
    model.isFamilyAdded = false;
    model.isAssociateAdded = false;

    model.showMandatoryInfoForm = false;
    model.showAddressForm = false;
    model.showPaymentForm = false;
    model.showPreferenceForm = false;
    model.showFamilyInfoForm = false;
    model.showAssociateForm = false;


    // skip section flags
    model.isAddressSkipped = false;
    model.isPaymentSkipped = false;
    model.isPreferencesSkipped = false;
    model.isFamilyMembersSkipped = false;
    model.isBusinessAssociateSkipped = false;
    model.isSponsorSkipped = false;
    model.payment.directpolicy = false;


    // for password image
    model.inputType = 'tel';
    model.currentState = 'masked';


    model.years = [];
    var yearValue = new Date();
    for (var i = (yearValue.getFullYear() - 18); i > (yearValue.getFullYear() - 100); i--) {
        model.years.push(i);
    }
    model.year = "";
    model.month = "";
    model.day = "";
    model.birthdate = "";



    var promise = reviewSignupInfoService.getDistributorInfo();
    var statepromise = statelistservice.loadDetails();
    statepromise.then(function(data) {
        model.stateList = data;
    });

    promise.then(function(data) {
        // self.distributorInfo = data;
        model.mandatoryInfo = data.mandatoryInfo;
        model.address = data.address;
        model.payment = data.payment;
        model.preferences = data.preferences;
        model.sponsor = data.sponsor;
        model.familyMemberList = data.familyMemberList;
        model.associateInfo = data.associateInfo;
        model.directPolicyContent = data.directPolicyInfo.directPolicyContent;
        angular.extend(model.addressOriginal, model.address);
        angular.extend(model.paymentOriginal, model.payment);
        angular.extend(model.preferencesOriginal, model.preferences);
        angular.extend(model.sponsorOriginal, model.sponsor);


        // to select email by default
        model.familyInfo.contactByEmail = true;
        model.associateInfo.contactByEmail = true;
        if (model.mandatoryInfo.birthdate) {
            var bdatestring = model.mandatoryInfo.birthdate.split("-");
            model.year = parseInt(bdatestring[0]);
            model.month = birthdayFactory.findItemById(parseInt(bdatestring[1]), "M");
            model.day = birthdayFactory.findItemById(parseInt(bdatestring[2]), "D");
        }

        model.mandatoryInfo.mobilePhoneInputDisplay = "";
        if (model.mandatoryInfo.mobilePhoneInput) {
            model.mandatoryInfo.mobilePhoneInputDisplay = "(" + model.mandatoryInfo.mobilePhoneInput.substr(0, 3) + ") " + model.mandatoryInfo.mobilePhoneInput.substr(3, 3) + "-" + model.mandatoryInfo.mobilePhoneInput.substr(6);
        }
        model.mandatoryInfo.ssnInputDisplay = "";
        if (model.mandatoryInfo.ssnInput) {
            model.mandatoryInfo.ssnInputDisplay = "**** - ** - " + model.mandatoryInfo.ssnInput.substr(6);
        }


    });


    model.updateDate = function(input) {
        if (input == "year") {
            model.month = "";
            model.day = "";
        } else if (input == "month") {
            model.day = "";
        }
        if (model.year && model.month && model.day) {
            model.mandatoryInfo.birthdate = new Date(model.year, model.month.id, model.day.id);
        }
    };

    model.selectPreference = function(preferenceType, flag) {
        if (flag == "F") {
            if (preferenceType == 'email') {
                model.familyInfo.contactByEmail = true;
                model.familyInfo.contactByPhone = false;
            } else {
                model.familyInfo.contactByPhone = true;
                model.familyInfo.contactByEmail = false;
            }
        } else if (flag == "A") {
            if (preferenceType == 'email') {
                model.associateInfo.contactByEmail = true;
                model.associateInfo.contactByPhone = false;
            } else {
                model.associateInfo.contactByPhone = true;
                model.associateInfo.contactByEmail = false;
            }
        }
    }

    model.updateMember = function() {
            model.isFamilyAdded = true;
            self.addNewMember.$submitted = true;
            if (self.addNewMember.$invalid)
                return false;
            var familyMember = {
                "firstname": self.model.familyInfo.firstname,
                "lastname": self.model.familyInfo.lastname,
                "accountstatus": "Account Pending",
                "contactpreference": {
                    "phone": (self.model.familyInfo.contactpreference != undefined) ? self.model.familyInfo.contactpreference.phone : "",
                    "email": (self.model.familyInfo.contactpreference != undefined) ? self.model.familyInfo.contactpreference.email : ""
                }
            }
            model.familyMemberList.push(familyMember);
            model.familyInfo = {};
            model.familyInfo.contactByEmail = true;
            model.familyInfo.contactByPhone = false;
            model.isMemberPresent = true; //show save button
            model.showMemberForm = false; // hide form and show add button
            model.showFamilyInfoForm = false;



        }
        // if (vm.model.associateInfo.length > 0) {
    model.isAssociateAdded = false;
    model.showAddButton = true;
    // }


    model.updateAssociate = function() {
        model.isAssociateAdded = true;
        self.addNewAssociate.$submitted = true;
        if (self.addNewAssociate.$invalid)
            return false;
        var associateMember = {
            "firstname": self.model.associateInfo.firstname,
            "lastname": self.model.associateInfo.lastname,
            "accountstatus": "Account Pending",
            "contactpreference": {
                "phone": (self.model.associateInfo.contactpreference != undefined) ? self.model.associateInfo.contactpreference.phone : "",
                "email": (self.model.associateInfo.contactpreference != undefined) ? self.model.associateInfo.contactpreference.email : ""
            }
        }
        model.associateInfo.push(associateMember);
        // model.associateInfo = associateList;
        model.isAssociateAdded = true; //show save button
        model.showAssociateForm = false;
        model.showAddButton = false;
        model.restrictAddButton = true;
        model.showAssociateForm = false;


    }


    model.submitPayment = function(form) {
        // save the information
        // post call here
        if (form.$valid) {
            angular.extend(model.paymentOriginal, model.payment);
            model.showPaymentForm = false;
        }
    }

    model.cancelPayment = function() {
        angular.extend(model.payment, model.paymentOriginal);
        model.showPaymentForm = false;
    }

    model.submitAddress = function(form) {
        // save address
        if (form.$valid) {
            angular.extend(model.addressOriginal, model.address);
            //post call here
            model.showAddressForm = false;
        }
    }

    model.cancelAddress = function() {
        // cancel address
        angular.extend(model.address, model.addressOriginal);
        model.showAddressForm = false;
    }

    model.submitPref = function() {
        // save pref
        model.isCompletePreferences = true;
        angular.extend(model.preferencesOriginal, model.preferences);
        //post call here
        model.showPreferenceForm = false;

    }

    model.cancelPref = function() {
        // cancel address
        angular.extend(model.preferences, model.preferencesOriginal);
        model.showPreferenceForm = false;
    }

    model.submitFamilyInfo = function() {
        // post call here

        angular.extend(model.familyInfoOriginal, model.familyInfo);
        model.showFamilyInfoForm = false;
    }

    model.cancelFamilyInfo = function(form) {
        form.$setPristine();    
        form.$setUntouched(); 
        angular.extend(model.familyInfo, model.familyInfoOriginal);
        model.isFamilyAdded = true;
        model.showFamilyInfoForm = false;
    }
    model.submitAssociateInfo = function() {
        angular.extend(model.associateInfoOriginal, model.associateInfo);
        model.showAssociateForm = false;
    }
    model.cancelAssociateInfo = function(form) {
        form.$setPristine();    
        form.$setUntouched(); 
        model.isAssociateAdded = false;
        angular.extend(model.associateInfo, model.associateInfoOriginal);
        model.showAssociateForm = false;
    }
    model.moveNext = function() {
        $window.location.href = "/#/termsandconditionspage";
    }

    model.submitMandatoryInfo = function(form) {
        if (form.$valid) {
            if (!model.year || !model.month || !model.day) {
                model.hasError.err = true;
                model.hasError.message = 'Please enter a valid Date';
                return
            }
            if (model.mandatoryInfo.mobilePhoneInput) {
                model.mandatoryInfo.mobilePhoneInputDisplay = "(" + model.mandatoryInfo.mobilePhoneInput.substr(0, 3) + ") " + model.mandatoryInfo.mobilePhoneInput.substr(3, 3) + "-" + model.mandatoryInfo.mobilePhoneInput.substr(6);
            }

            if (model.mandatoryInfo.ssnInput) {
                model.mandatoryInfo.ssnInputDisplay = "**** - ** - " + model.mandatoryInfo.ssnInput.substr(6);
            }

            model.hasError.err = false;
            if (model.year && model.month && model.day) {
                model.mandatoryInfo.birthdate = new Date(model.year, model.month, model.day);
            }

            // process date object to store in string format
            var monthtemp = model.mandatoryInfo.birthdate.getMonth();
            var datetemp = model.mandatoryInfo.birthdate.getDate();
            var yeartemp = model.mandatoryInfo.birthdate.getFullYear();
            if (monthtemp < 10) {
                monthtemp = '0' + monthtemp;
            };
            if (datetemp < 10) {
                datetemp = '0' + datetemp;
            };
            model.mandatoryInfo.birthdate = (yeartemp + '-' + monthtemp + '-' + datetemp);
            if (self.mandatoryInfoForm.$invalid) {
                return false;
            }
            reviewSignupInfoService.setDistributorInfo(model.mandatoryInfo);
            model.optionalForm = true;


            angular.extend(model.mandatoryInfoOriginal, model.mandatoryInfo);
            //post call here
            model.showMandatoryInfoForm = false;
        }

        model.cancelMandatoryInfo = function() {
            angular.extend(model.mandatoryInfo, model.mandatoryInfoOriginal);
            model.showMandatoryInfoForm = false;
        }

        model.inputType = 'tel';
        model.currentState = 'masked';

        model.changeImage = function() {
            model.currentState = this.currentState == 'masked' ? 'unmasked' : 'masked';
            model.inputType = this.inputType == 'tel' ? 'password' : 'tel';  
        };
        model.checkForError = function() {
            if (!model.year || !model.month || !model.day) {
                model.hasError = { err: true, message: 'Invalid Date' };
                return;
            }
            model.hasError.err = false;
        }
    }
}


module.exports = reviewSignupInfoController;