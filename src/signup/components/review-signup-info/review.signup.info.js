var angular = require('angular');
var uiRouter = require('angular-ui-router');
var formUtils = require('../../../__shared/form-utils/form-utils'); 
var stateListServiceModule = require('../../../__shared/services/statelist/statelist.service.module');


var reviewSignupInfoComponent = require('./review.signup.info.component');
var reviewSignupInfoService = require('./review.signup.info.service');


var reviewsignupAccSetupModule = angular.module('reviewsignupAccSetupModule', [uiRouter, formUtils.name, stateListServiceModule.name])
    .component('reviewsignupinfo', reviewSignupInfoComponent)
    .service('reviewSignupInfoService', reviewSignupInfoService)
    

var base = require('../../../stylesheets/base.scss');
var Styles = require('./review.signup.info.scss');

module.exports = reviewsignupAccSetupModule;