'use strict';

reviewSignupInfoService.$inject = ['$http', '$q'];

function reviewSignupInfoService($http, $q) {
    this.$http = $http;
    var deferred = $q.defer();
    var distributorInfoPayload = {};
    $http.get('../../../../src/signup/components/review-signup-info/review.signup.info.component.json').then(function(response) {
        deferred.resolve(response.data);
    });

    this.getDistributorInfo = function() {
        return deferred.promise;
    }

    this.setDistributorInfo = function(mandatoryInfo) {
    	distributorInfoPayload = mandatoryInfo;
    }
}



module.exports = reviewSignupInfoService;