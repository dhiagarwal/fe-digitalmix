var template =require('./review.signup.info.jade');
var controller =require('./review.signup.info.controller');

var reviewSignupInfoComponent =  {
  //return {
    restrict: 'E',
    templateUrl : template,
    controller,
    controllerAs: 'vm',
    bindToController: true
  //};
};

module.exports = reviewSignupInfoComponent;
