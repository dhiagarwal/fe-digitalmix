  'use strict';

  function userProfileHeaderController(userProfileHeaderService) {

      this.userData = {};
      var model = this.model = {};
      var self = this;
    
    
    var promise = userProfileHeaderService.getUserProfileInfo();
    promise.then(function(data) {

      self.userData = data;

    });
    
  }

  module.exports = userProfileHeaderController;
