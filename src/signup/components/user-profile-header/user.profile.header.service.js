'use strict';

userProfileHeaderService.$inject = ['$http','$q'];

function userProfileHeaderService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/signup/components/user-profile-header/user.profile.header.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getUserProfileInfo = function() {
    return deferred.promise;
  }

};


module.exports = userProfileHeaderService;
