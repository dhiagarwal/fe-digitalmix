'use strict';
var template = require('./user.profile.header.jade');
var controller = require('./user.profile.header.controller');

var userProfileHeaderComponent = {
  //return {
  restrict: 'E',
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'userProfileCtrl',
  bindToController: true
  //};
};

module.exports = userProfileHeaderComponent;
