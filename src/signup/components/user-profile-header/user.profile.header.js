'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var userProfileHeaderComponent = require('./user.profile.header.component');

var userProfileHeaderService = require('./user.profile.header.service')

var userProfileHeaderModule = angular.module('userProfileHeaderModule', [uiRouter])
                  .component('userprofileheader', userProfileHeaderComponent)
                  .service('userProfileHeaderService', userProfileHeaderService)


var base =require('../../../stylesheets/base.scss');
var Styles =require('./user.profile.header.scss');

module.exports = userProfileHeaderModule;
