var template =require('./review.info.distributor.jade');
var controller =require('./review.info.distributor.controller');

var reviewInfoDistributorComponent =  {
    templateUrl : template,
    controller,
    controllerAs: 'ctrl'
};

module.exports = reviewInfoDistributorComponent;
