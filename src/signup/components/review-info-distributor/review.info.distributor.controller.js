'use strict';

function reviewInfoDistributorController(reviewInfoDistributorService, $location, birthdayFactory) {

  var self = this;
  var model = this.model = {};
  this.showAdd = true;
  this.currentSel = "email";
  this.avatarUrl = "../../../../src/assets/images/content/avatar-profile.jpg";
  this.avatarName = "Sarah Juschka";
  this.avatarLocation = "United States";
  model.hasError = { err: false, message: '' };

  model.mandatoryInfo = {};
  model.payment = {};

  model.showMandatoryInfoForm = false;
  model.showPaymentForm = false;
  model.isCompletePayment = false;

  var getData = reviewInfoDistributorService.getSearchData();
  getData.then(function(data){
      model.mandatoryInfo = data.mandatoryInfo;
      model.payment = data.payment;
      console.log(model.payment);
      model.mandatoryInfo.ssnInputDisplay = "";
      if (model.mandatoryInfo.birthdate) {
          var bdatestring = model.mandatoryInfo.birthdate.split("-");
          model.year = parseInt(bdatestring[0]);
          model.month = birthdayFactory.findItemById(parseInt(bdatestring[1]), "M");
          model.day = birthdayFactory.findItemById(parseInt(bdatestring[2]), "D");
      }
      if (model.mandatoryInfo.ssnInput) {
          model.mandatoryInfo.ssnInputDisplay = "**** - ** - " + model.mandatoryInfo.ssnInput.substr(6);
      }
  });

  model.cancelMandatoryInfo = function() {
      model.showMandatoryInfoForm = false;
  }

  model.submitMandatoryInfo = function() {

      if (!model.year || !model.month || !model.day) {
          model.hasError.err = true;
          model.hasError.message = 'Please enter a valid Date';
          return
      }

      if (model.mandatoryInfo.ssnInput) {
          model.mandatoryInfo.ssnInputDisplay = "**** - ** - " + model.mandatoryInfo.ssnInput.substr(6);
      }

      model.hasError.err = false;
      if (model.year && model.month && model.day) {
          model.mandatoryInfo.birthdate = new Date(model.year, model.month, model.day);
      }

      // process date object to store in string format
      var monthtemp = model.mandatoryInfo.birthdate.getMonth();
      var datetemp = model.mandatoryInfo.birthdate.getDate();
      var yeartemp = model.mandatoryInfo.birthdate.getFullYear();
      if (monthtemp < 10) {
          monthtemp = '0' + monthtemp;
      };
      if (datetemp < 10) {
          datetemp = '0' + datetemp;
      };
      model.mandatoryInfo.birthdate = (yeartemp + '-' + monthtemp + '-' + datetemp);
      if (self.mandatoryInfoForm.$invalid) {
          return false;
      }

      reviewInfoDistributorService.setDistributorInfo(model.mandatoryInfo);
      model.optionalForm = true;

      //post call here
      model.showMandatoryInfoForm = false;
  }

  model.cancelPayment = function() {
      model.showPaymentForm = false;
  }

  model.submitPayment = function() {
      // save the information
      model.showPaymentForm = false;
  }


}
module.exports = reviewInfoDistributorController;
