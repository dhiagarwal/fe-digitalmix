var angular =require('angular');
var uiRouter =require('angular-ui-router');

var reviewInfoDistributorComponent =require('./review.info.distributor.component');
var reviewInfoDistributorService =require('./review.info.distributor.service');

var reviewInfoDistributorModule = angular.module('reviewInfoDistributorModule', [uiRouter])
.component('reviewInfoDistributorComponent', reviewInfoDistributorComponent)
.service('reviewInfoDistributorService', reviewInfoDistributorService);

var Styles = require('./review.info.distributor.scss');

module.exports = reviewInfoDistributorModule;
