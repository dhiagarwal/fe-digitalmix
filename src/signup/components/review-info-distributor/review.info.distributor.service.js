'use strict';

reviewInfoDistributorService.$inject = ['$http','$q'];

function reviewInfoDistributorService($http, $q) {
  this.$http = $http;

  var deferred = $q.defer();
  var distributorInfoPayload = {};
  $http.get('../../../../src/signup/components/review-info-distributor/review.info.distributor.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getSearchData = function() {
    return deferred.promise;
  }
  
  this.setDistributorInfo = function(mandatoryInfo) {
    distributorInfoPayload = mandatoryInfo;
  }
}

module.exports = reviewInfoDistributorService;
