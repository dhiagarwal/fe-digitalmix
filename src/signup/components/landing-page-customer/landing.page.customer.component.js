var template =require('./landing.page.customer.jade');
var controller =require('./landing.page.customer.controller');

var landingPageCustomerComponent =  {
    templateUrl : template,
    controller,
    controllerAs: 'ctrl'
};

module.exports = landingPageCustomerComponent;
