'use strict';

landingPageCustomerService.$inject = ['$http','$q'];

function landingPageCustomerService($http, $q) {
  this.$http = $http;
  this.$q = $q;
}

landingPageCustomerService.prototype.getFeaturedCategoriesData = function() {
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/signup/components/landing-page-customer/landing.page.customer.json').then(function(response) {
		deferred.resolve(response.data);
	});
	return deferred.promise;
};

landingPageCustomerService.prototype.getCarouselData = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/signup/components/landing-page-customer/landing.page.customer.carousel.json').then(function(response) {
		deferred.resolve(response.data);
	});
	return deferred.promise;
};

module.exports = landingPageCustomerService;
