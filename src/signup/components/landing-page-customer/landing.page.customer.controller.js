'use strict';

function landingPageCustomerController(landingPageCustomerService, $timeout) {
    var self = this;
    this.carouselData = {};
    self.learnMoreImage = "../../../../src/assets/images/content/success-story.png";
    self.videoURL = "../../../../src/assets/videos/Testimonial-Video.mp4";
    self.slickVideoConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        speed: 500,
        event: {
            beforeChange: function(event, slick, currentSlide, nextSlide) {
                var video = angular.element.find('#video-' + currentSlide);
                if (video[0]) {
                    video[0].pause();
                    video[0].controls = false;
                    self.videoPlaying = false;
                }
            },
            afterChange: function(event, slick, currentSlide, nextSlide) {}
        }
    };

    var getData = landingPageCustomerService.getFeaturedCategoriesData();
    getData.then(function(data) {
        self.featuredCategoriesData = data;
    });

    var getCarouselData = landingPageCustomerService.getCarouselData();
    getCarouselData.then(function(data) {
        self.carouselData = data;
    });

     window.setTimeout(function(){
        var maxHeight = Math.max.apply(null, $("div.slick-slide .success-story").map(function ()
        {
            return $(this).height();
        }).get());

        $("div.slick-slide .success-story").each(function(){
            if ($(this).height() < maxHeight){
                $(this)
                .css("height",maxHeight+"px");
        }
    });
    
    },2000);

    self.togglePlay = function(index) {
         window.open('../../../../src/assets/videos/Testimonial-Video.mp4', '_blank','location=yes,height=500,width=600,scrollbars=no,status=yes')
        if (index > -1) {
            var video = angular.element.find('#video-' + index);
            console.log("Index");
        } else {
            var video = angular.element.find('#video-category-directory');
            console.log("No Index");
        }
        self.videoPlaying = true;
        if (video[0].paused) {
            $timeout(function() { video[0].play(); }, 10)
            video[0].controls = true;
            self.videoPlaying = true;
        } else {
            $timeout(function() { video[0].pause(); }, 10)
            video[0].controls = false;
            self.videoPlaying = false;
        }
    }

    self.videoEnd = function(index) {
        if (index > -1) {
            var video = angular.element.find('#video-' + index);
            console.log("Index");
        } else {
            var video = angular.element.find('#video-category-directory');
            console.log("No Index");
        }
        if (video[0].ended) {
        video[0].controls = false;
        self.videoPlaying = false;
        }
    };


}

module.exports = landingPageCustomerController;
