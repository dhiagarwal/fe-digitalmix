var angular = require('angular');
var uiRouter = require('angular-ui-router');
var landingPageCustomerComponent = require('./landing.page.customer.component');
var landingPageCustomerService = require('./landing.page.customer.service');
var landingPageCustomerModule = angular.module('landingPageCustomer', [uiRouter])
.component('landingPageCustomerComponent', landingPageCustomerComponent)
.service('landingPageCustomerService', landingPageCustomerService);

var styles = require('./landing.page.customer.scss');

module.exports = landingPageCustomerModule;
