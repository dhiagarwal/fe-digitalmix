var angular =require( 'angular');
var signup =require('./signup/signup');
var socialmedia =require('./social-media/social.signup');
var alertsnotification = require('./alerts-notification/alerts.notification.component');

var singupModule = angular.module('app.signup', [
	socialmedia.name,
	signup.name,
	alertsnotification.name
]);

module.exports= singupModule;
