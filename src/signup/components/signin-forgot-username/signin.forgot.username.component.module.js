var angular =require('angular');
var uiRouter =require('angular-ui-router');
var signinForgotUsernameComponent =require('./signin.forgot.username.component');
var signinForgotUsernameService =require('./signin.forgot.username.component.service');

var signinForgotUsernameModule = angular.module('signinforgotusername', [
  uiRouter
])
.component('signinforgotusername', signinForgotUsernameComponent)
.service('signinForgotUsernameService', signinForgotUsernameService);

var base =require('../../../stylesheets/base.scss');
var Styles =require('./signin.forgot.username.component.scss'); 

module.exports= signinForgotUsernameModule;
