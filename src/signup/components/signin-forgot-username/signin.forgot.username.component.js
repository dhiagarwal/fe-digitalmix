'use strict';
var template =require('./signin.forgot.username.component.jade');
var controller =require('./signin.forgot.username.component.controller');

var signinForgotUsernameComponent = {
  restrict: 'E',
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true,
  bindings:{
    sourceUrl:'='
  }
};
  

module.exports = signinForgotUsernameComponent;
