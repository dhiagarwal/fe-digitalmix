function signinForgotUsernameController(signinForgotUsernameService, $timeout, $scope,$location) {
  this.result = {};
    this.service = signinForgotUsernameService;
    this.name='signinforgotusername';
    var self = this;
    var tabs = ["tab1", "tab2", "tab3", "tab4"];
    var patterns = {
        email: /^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))/,
        phone: /^[1-9]{1}[0-9]{9}$/
    };
    self.currentTab = tabs[0];
    self.clickedSearch = false;
    self.emailrequired=false;
    self.phonerequired=false;
    self.totalDisplayed = 8;
    self.loadMoreData = false;
    self.searchResultStatus = false;
    self.searchTotalResultsCount = 0;
    self.searchedText = "";
    self.searchresultempty = false;
    self.clickedselect = false;
    self.credentials = {};
    self.isEmail = true;
    self.isPhone = true;
    self.email = false;
    self.phone = false;

    self.stopChars = function($event) {
        var keycode = $event.which || $event.keyCode;
        if(keycode === 101 || keycode === 43) {
          $event.preventDefault();
        }
    }


    self.onClickTab = function (tab) {
        var formVal = "";
        self.currentTab = tab;
        self.clickedSearch = false;
        self.searchResultStatus = false;
        if(tab == tabs[0]){
            self.distributorId = "";
            self.emailrequired=false;
            self.phonerequired=false;
        } else  if(tab == tabs[1]){
            self.emailId = "";
            self.emailrequired=true;
            self.phonerequired=false;
        } else  if(tab == tabs[2]){
            self.phoneNum = "";
            self.phonerequired=true;
            self.emailrequired=false;
        }
    }

    self.submitForm = function(formvalid, tab) {
        var searchText = "";
        self.clickedSearch = true;
        if (formvalid) {
        if(tab == tabs[0]){
            searchText = self.distributorId;
        } else  if(tab == tabs[1]){
            searchText = self.emailId;
        } else  if(tab == tabs[2]){
            searchText = self.phoneNum;
        }
        self.searchedText = searchText;
        self.searchResultStatus = true;
        var setSearchData = signinForgotUsernameService.getSearchData();
        setSearchData.then(function(data) {
            self.searchData = data;
            self.searchTotalResultsCount = self.searchData.length;
            if(self.searchTotalResultsCount == 0){
                self.searchresultempty =true;
                self.searchResultStatus = false;
            }
        });
        return formvalid;
        }
        return false;
    }

    self.loadMore = function () {
        if(self.searchResultStatus){
            if(self.searchData.length > self.totalDisplayed){
                self.loadMoreData = true;
                $timeout(function(){self.loaderFunction();}, 500);
            }
        }
    };

    self.loaderFunction = function () {
        self.totalDisplayed += 8;
        self.loadMoreData = false;
    };

    self.onclickselect = function(value, tab) {
        self.clickedselect = true;
        patterns.phone.test(value)? self.phone = true : self.email = true;
        if(self.email== true){
            self.userdetail = value.slice(0,2)+value.replace(/[a-z0-9]/g,'*').slice(2,value.indexOf('@'))+value.slice(value.indexOf('@'))
        }
        if(self.phone== true){
            self.userdetail= value.replace(/[0-9]/g,'*').slice(0,3) + "-"+ value.replace(/[0-9]/g,'*').slice(3,6) +"-"+value.slice(6,10);
        }
    }
    self.redirectlogin = function() {
      $location.url("/login");
    }
}
module.exports = signinForgotUsernameController;
