'use strict';


signinForgotUsernameService.$inject = ['$http','$q'];
function signinForgotUsernameService($http , $q) {
    
    var deferred = $q.defer();
    $http.get('../../../../src/signup/components/signin-forgot-username/signin.forgot.username.component.json').then(function(response) {
    deferred.resolve(response.data);
    });

    this.getSearchData = function() {
        return deferred.promise;
    }
}

module.exports= signinForgotUsernameService;
