'use strict';

const bp = {
  'sm': 375,
  'md': 768,
  'lg': 1024,
  'xl': 1440
};

function onBoardingComponentController(onBoardingComponentService, $timeout) {

  var self = this;

  self.videoPlaying = false;
  self.videoBannerShow = true;
  self.cardDismissShow = false;

  var videoControl = angular.element.find('#video-1');
  videoControl[0].controls = false;

  self.onVideoEnd = function($event) {
    self.closeVideo("close");
  };

  var getStaticVal = onBoardingComponentService.getPageStaticComponent();

  var getCardVal = onBoardingComponentService.getCardComponent();

  getStaticVal.then(function(data) {
      self.staticVal = data;
  });

  getCardVal.then(function(data) {
      self.toDoCards = data;
      self.cardsCount = self.toDoCards.length;
  });

  self.closeVideo = function(accessFrom) {
    if(accessFrom == "close"){
      self.videoBannerShow = false;
    } else{
      self.videoPlaying = false;
      self.videoBannerShow = true;
      videoControl[0].controls=false;
    }
  };

  self.togglePlay = function() {
      var video = angular.element.find('#video-1');
      if (video[0].paused) {
          video[0].play();
          video[0].controls=true;
          self.videoPlaying = true;
      } else {
          $timeout(function(){video[0].pause();},10);
          video[0].controls=false;
          self.videoPlaying = false;
      }
  };
};

module.exports = onBoardingComponentController;
