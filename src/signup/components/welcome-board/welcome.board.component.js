'use strict';
var template = require('./welcome.board.component.jade');
var controller = require('./welcome.board.component.controller');

var onBoardingComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'onBoard'
};

module.exports = onBoardingComponent;
