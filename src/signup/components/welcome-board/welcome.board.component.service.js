'use strict';

onBoardingComponentService.$inject = ['$http','$q'];

function onBoardingComponentService($http,$q) {
  this.$http = $http;

  var staticComponents = $q.defer();
  var cardComponents = $q.defer();
  $http.get('../../../../src/signup/components/welcome-board/welcome.board.static.component.json').then(function(response) {
    staticComponents.resolve(response.data);
  });

  this.getPageStaticComponent = function() {
    return staticComponents.promise;
  }

  $http.get('../../../../src/signup/components/welcome-board/card.component.json').then(function(response) {
    cardComponents.resolve(response.data);
  });

  this.getCardComponent = function() {
    return cardComponents.promise;
  }

};

module.exports = onBoardingComponentService;
