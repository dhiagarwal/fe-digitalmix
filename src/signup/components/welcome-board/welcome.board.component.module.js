var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('angular-ui-event');
require('angular-animate');

var cardsComponent = require('../../../../src/__shared/cards/cards.component');
var onBoardingComponent = require('./welcome.board.component');
var onBoardingComponentService = require('./welcome.board.component.service');

var onBoardingComponentModule = angular.module('onBoardingComponentModule', [uiRouter, 'ui.event', 'ngAnimate'])
.component('onboardingcomponent', onBoardingComponent)
.component('cardscomponent', cardsComponent)
.service('onBoardingComponentService', onBoardingComponentService);

import Styles from './welcome.board.component.scss';

module.exports = onBoardingComponentModule;
