'use strict';
var template = require('./friends.family.signup.create.password.component.jade');
var controller = require('./friends.family.signup.create.password.component.controller');

var friendsFamilySignupCreatePasswordComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'familysignupcreatepwd',
};

module.exports = friendsFamilySignupCreatePasswordComponent;