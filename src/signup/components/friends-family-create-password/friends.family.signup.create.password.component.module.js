'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var friendsFamilySignupCreatePwdComponent = require('./friends.family.signup.create.password.component');

var friendsFamilySignupCreatePasswordComponentModule = angular.module('friendsFamilySignupCreatePasswordComponentModule', [
					uiRouter,
					])
                  .component('friendsfamilysignupcreatepwd', friendsFamilySignupCreatePwdComponent)
                  
import Styles from './friends.family.signup.create.password.component.scss';

module.exports = friendsFamilySignupCreatePasswordComponentModule;