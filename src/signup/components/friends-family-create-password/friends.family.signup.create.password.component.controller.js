'use strict';

function friendsFamilySignupCreatePasswordController() {
    var self = this;

    this.lastName='';
    this.firstName='';
    this.isPhone=true;
    this.title='Mobile Phone Number';
    this.credentials= {};
    this.isRequired=true;
    this.accountregistrationpolicy=false;
    self.checkBox = false;
    self.submitButtonClicked = false;
    this.submitFamilyNamePwd=function () {
        self.submitButtonClicked=true;
        if(this.accountregistrationpolicy){
            self.checkBox = false;
            window.location.href = window.location.origin + '/#/signupdistributor';
        }
        else{
            self.checkBox = true;
        }
    }
    this.checkboxShowHide = function() {
        if (self.submitButtonClicked) {
            if (this.accountregistrationpolicy) {
                self.checkBox = false;
            } else {
                self.checkBox = true;
            }
        }
    };
};

module.exports = friendsFamilySignupCreatePasswordController;