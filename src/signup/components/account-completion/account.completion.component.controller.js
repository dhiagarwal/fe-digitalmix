'use strict';
accountCompletionController.$inject=['$location','accountCompletionComponentService'];
function accountCompletionController($location, accountCompletionComponentService) {
    var self = this;

    this.credentials = {
        userDetailsField: "",
        userEmailField: "",
        userPhoneField: "",
        userPassword: "",
        confirmPassword: "",
        isInvalidPassword: false,
        isInvalidData: false,
        isInvalidConfirmPassword: false,
        isSubmitted: false,
        emptyPasswordError: false,
        emptyConfirmPasswordError: false,
        emptyEmailError: true
    };
    this.isEmail = true;
    this.required = true;

    this.saveUserCredentials = function() {
        // Please uncomment this code for integration with AEM.
        /*
        var userName = this.credentials.userDetailField || this.credentials.userEmailField || this.credentials.userPhoneField;
        var promise = accountCompletionComponentService.saveUserCredentials({"userName": userName, "userPassword": this.credentials.userPassword});
        promise.then(function (data) {
            $location.path("/signup")
        }, function(data){

        });*/
    };


    this.cancel = function() {
        this.credentials = {
            userDetailsField: "",
            userPhoneField: "",
            userEmailField: "",
            userPassword: "",
            confirmPassword: "",
            isInvalidPassword: false,
            isInvalidData: false,
            isInvalidConfirmPassword: false,
            isSubmitted: false,
            emptyPasswordError: false,
            emptyConfirmPasswordError: false,
            emptyEmailError: true
        };
    };

};

module.exports = accountCompletionController;
