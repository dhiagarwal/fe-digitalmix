'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountCompletionComponent = require('./account.completion.component');
var accountCompletionComponentService = require('./account.completion.component.service');
var passwordPattern = require('../../../__shared/password-pattern/password.pattern.module');
var emailPhone = require('../../../__shared/email-phone/email.phone.module');

var accountCompletionComponentModule = angular.module('accountCompletionComponentModule', [
					uiRouter
					])
                  .component('accountCompletion', accountCompletionComponent)
                  .service('accountCompletionComponentService', accountCompletionComponentService);

module.exports = accountCompletionComponentModule;
