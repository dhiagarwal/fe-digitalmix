'use strict';

accountCompletionComponentService.$inject = ['$http','$q'];

function accountCompletionComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

accountCompletionComponentService.prototype.saveUserCredentials = function(data){
	var deferred = this.$q.defer();
	this.$http.post("", data)
		.then(function(data){
			deferred.resolve(data);
		}, function(error){
			deferred.reject(error);
		});

	return deferred.promise;
};

module.exports = accountCompletionComponentService;