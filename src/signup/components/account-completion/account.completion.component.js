'use strict';
var accountCompletionTemplate = require('./account.completion.component.jade');
var accountCompletionController = require('./account.completion.component.controller');

var accountCompletionComponent = {
  bindings:{},
  templateUrl: accountCompletionTemplate,
  controller: accountCompletionController,
  controllerAs: 'ctrl'
};

module.exports = accountCompletionComponent;
