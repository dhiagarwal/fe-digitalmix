var template = require('./social.signup.jade');
var controller = require('./social.signup.controller');

var socialSignUpComponent =  {
    templateUrl : template,
    controller:controller,
    controllerAs: 'vmp'
};

module.exports= socialSignUpComponent;
