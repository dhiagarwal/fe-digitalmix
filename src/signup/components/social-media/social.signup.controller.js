'use strict';

function socialSignUpController(socialSignupService, $scope, $filter,$stateParams) {
    var vmp = this;

    this.result = {};
    this.service = socialSignupService;
    this.name = 'social.signup';
    this.isSubmitted = false;
    this.socialLogin = function() {
        // function to integrate social media api
    }
    this.maskFormat = '';
    this.maskOptions = {
        allowInvalidValue: true,
        clearOnBlur: false
    }

    var patterns = {
        email: /^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))/,
        phone: /^[1-9]{1}[0-9]{9}$/
    };

    vmp.onChange = onChange;
    this.confirmCode=function (confirmationCodeValue) {
    console.log("check");
    if(confirmationCodeValue==111)
    {
        console.log("yesss");
    }
}
    function onChange(form, modelValue) {
        this.isSubmitted = false;

        if(!form.userDetailField.$modelValue){
            this.maskFormat = '';
        }
        if(form.userDetailField.$modelValue && form.userDetailField.$modelValue.length == 1){
            if(!isNaN(modelValue)){
                this.maskFormat = '(999) 999-9999';
            }
        }

        this.isSubmitted = false;
        this.invalidError = false;
        this.emptyError = false;

       var isPhone;
        var value = this.userDetailField;
        if (!value) {
            return;
        }
        value = value.replace(/[\-()]/g, '');
        var isPhoneNumber = patterns.phone.test(value);
        if(isPhoneNumber && value.length == 10) {
            return false;
        }
        var match = patterns.email.test(value) || patterns.phone.test(value);
        if(value.length > 3 && !isNaN(value)) {
            isPhone = true;
        }

        if(isPhone){
            var telephoneValue = value.replace(/[^0-9]/g, '').slice(0,10);
            var city, remainingNumber;

            switch (telephoneValue.length) {
                case 1:
                case 2:
                case 3:
                    city = telephoneValue;


                default:
                    city = "(" + telephoneValue.slice(0, 3) + ")";
                    remainingNumber = telephoneValue.slice(3);
            }

            if(remainingNumber){
                if(remainingNumber.length>3){
                    remainingNumber = remainingNumber.slice(0, 3) + "-" + remainingNumber.slice(3,7);
                }
                else{
                    remainingNumber = remainingNumber;
                }

                this.userDetailField =  ( city  + remainingNumber).trim();
            }
            else{
                return "(" + city;
            }
        }
            
    }

    this.getStartedShow = function(form) {
        this.isSubmitted = true;
        var value = this.userDetailField;
        if(!this.userDetailField){this.emptyError = true;return}
        value = value.replace(/[\-()]/g, '');
        if(patterns.phone.test(value)){
            $("#phoneConfirmation").modal();
        }
        else{
            if(patterns.email.test(value)){
                console.log('email is proper');
            }
            else{
                this.invalidError = true;
            }
        }

    }
}
module.exports = socialSignUpController;
