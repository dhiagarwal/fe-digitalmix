var angular =require('angular');
var uiRouter =require('angular-ui-router');
require('angular-messages');
var socialSignupComponent =require('./social.signup.component');
var socialSignupService =require('./social.signup.service');

var socialSignupModule = angular.module('socialSignup', [
  uiRouter,
    'ngMessages'
])
.component('socialmedia', socialSignupComponent)
.service('socialSignupService', socialSignupService);
var base =require('../../../stylesheets/base.scss');
var Styles =require('./social.signup.scss');

module.exports= socialSignupModule;
