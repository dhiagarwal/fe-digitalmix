'use strict';

function linksocialaccountController(linksocialaccountService,$window) {
    var self = this;
    this.isLinked = false;
    this.linkingCompleted = false;
    this.userData = {};
    this.credentials = {}
    this.isEmail = true;
    this.required = true;
    this.clickedsignin = false;
    this.usernameempty = false;
    this.passwordempty = false;
    this.redirectToLogin = function(){
          $window.location.href = "/#/login";
    };

    this.getUsername = function(){
      if(self.isLinked){
        self.username =  self.userData.email;
      }
      else{
        self.username = this.credentials.userDetailsField || this.credentials.userEmailField || this.credentials.userPhoneField;
      }
      this.usernameempty = false;
    };
    this.submitlogin = function () {
        self.getUsername();
        this.clickedsignin = true;

        if (this.username == null || this.username == "") {
            this.usernameempty = true;
        }
        if (this.password == "" || this.password == null) {
            this.passwordempty = true;
        }
        if (this.usernameempty == false && this.passwordempty == false) {
            return true;
        }
        return false;
    }
    this.change = function(){
      self.isLinked = !self.isLinked;
    }
    this.redirectsignup = function () {
        $window.location.href = "/#/signup";

    }
    this.redirectforgotpassword = function () {
        $window.location.href = "/#/forgotpassword";
    }
    this.enterpassword = function (value) {
        this.clickedsignin = false;
        this.passwordempty = false;
    }
    this.saveUserCredentials = function () {
    };

    var promise = linksocialaccountService.getUserData();
    promise.then(function (data) {
        self.userData = data;
    });
}
module.exports = linksocialaccountController;
