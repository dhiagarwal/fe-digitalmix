'use strict';

linkSocialAccountComponentService.$inject = ['$http',"$q"];

function linkSocialAccountComponentService($http,$q) {
  this.$http = $http;
  var deferred = $q.defer();
  var distributorInfoPayload = {};
  $http.get('../../../../src/signup/components/linksocialaccount/link.social.account.component.json').then(function(response) {
      deferred.resolve(response.data);
  });

  this.getUserData = function() {
    return deferred.promise;
  }

};

module.exports = linkSocialAccountComponentService;
