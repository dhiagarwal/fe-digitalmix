var angular =require('angular');
var uiRouter =require('angular-ui-router');
var linksocialaccountComponent =require('./link.social.account.component');
var linksocialaccountService =require('./link.social.account.service');

var linksocialaccountModule = angular.module('linkSocialAccount', [
  uiRouter
])
.component('linksocialaccountcomponent', linksocialaccountComponent)
.service('linksocialaccountService', linksocialaccountService);
var Styles =require('./link.social.account.scss');

module.exports= linksocialaccountModule;
