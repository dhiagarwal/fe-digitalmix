var template =require('./link.social.account.jade');
var controller =require('./link.social.account.controller');

var linksocialaccountComponent =  {
    templateUrl : template,
    controller: controller,
    controllerAs: 'linksocialaccountctrl',
};

module.exports = linksocialaccountComponent;
