'use strict';

function sponsorController(sponsorService, $timeout) {

  var self = this;
  var tabs = ["tab1", "tab2", "tab3", "tab4"];
  this.currentTab = tabs[3];
  this.clickedSearch = false;
  this.totalDisplayed = 8;
  this.loadMoreData = false;
  this.showSearchResult = false;
  this.showNoSearchResult = false;
  this.searchResultStatus = false;
  this.searchTotalResultsCount = 0;
  this.required = true;
  this.credentials = {};
  this.imageUrl = "../../../../src/assets/images/css/lazy-loader.gif";
  this.searchedText = "";
  this.onClickTab = function (tab) {
      var formVal = "";
      this.currentTab = tab;
      this.clickedSearch = false;
      this.showSearchResult = false;
      this.showNoSearchResult = false;
      this.searchResultStatus = false;
      if(tab == tabs[0]){
        this.distributorId = "";
        this.formTab1.$setPristine();
        this.formTab1.$setUntouched();
      } else  if(tab == tabs[1]){
        this.credentials.isSubmitted = false;
        this.credentials.userEmailField = "";
        this.formTab2.$setPristine();
        this.formTab2.$setUntouched();
      } else  if(tab == tabs[2]){
        this.credentials.isSubmitted = false;
        this.credentials.userPhoneField = "";
        this.formTab3.$setPristine();
        this.formTab3.$setUntouched();
      } else{
        this.firstName = "";
        this.lastName = "";
        this.formTab4.$setPristine();
        this.formTab4.$setUntouched();
      }
  }

  this.submitForm = function(formvalid, tab) {
    var searchText = "";
    this.clickedSearch = true;
    if (formvalid) {
      if(tab == tabs[0]){
        searchText = this.distributorId;
      } else  if(tab == tabs[1]){
        searchText = this.credentials.userEmailField;
      } else  if(tab == tabs[2]){
        searchText = this.credentials.userPhoneField;
      } else{
        searchText = this.firstName + " " + this.lastName;
      }
      this.searchedText = searchText;
      this.searchResultStatus = true;
      this.showSearchResult = true;
      var setSearchData = sponsorService.getSearchData();
      setSearchData.then(function(data) {
        self.searchData = data;
        self.searchTotalResultsCount = self.searchData.length;
        if(self.searchData.length == 0){
          self.showSearchResult = false;
          self.showNoSearchResult = true;
        }
      });
      return formvalid;
    }
    return false;
  }

  self.loadMore = function () {
    if(self.showSearchResult){
      if(self.searchData.length > self.totalDisplayed){
        self.loadMoreData = true;
        $timeout(function(){self.loaderFunction();}, 500);
      }
    }
  };

  self.loaderFunction = function () {
    self.totalDisplayed += 8;
    self.loadMoreData = false;
  };

}
module.exports = sponsorController;
