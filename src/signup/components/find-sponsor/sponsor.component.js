var template =require('./sponsor.jade');
var controller =require('./sponsor.controller');

var sponsorComponent =  {
    restrict: 'E',
    scope: {},
    templateUrl : template,
    controller,
    controllerAs: 'sponsor',
    bindToController: true
};

module.exports = sponsorComponent;
