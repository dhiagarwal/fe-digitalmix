'use strict';

sponsorService.$inject = ['$http','$q'];

function sponsorService($http, $q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/signup/components/find-sponsor/sponsor.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getSearchData = function() {
    return deferred.promise;
  }
}



module.exports = sponsorService;
