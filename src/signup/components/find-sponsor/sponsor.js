var angular =require('angular');
var uiRouter =require('angular-ui-router');
var sponsorComponent =require('./sponsor.component');
var sponsorService =require('./sponsor.service');
var sponsorModule = angular.module('sponsor', [uiRouter])
.component('sponsorComponent', sponsorComponent)
.service('sponsorService', sponsorService)
.directive('scrolly', function (){
  return {
      restrict: 'A',
      link: function (scope, element, attrs, $document) {
          var raw = element[0];
          var $window = angular.element(window);
          $window.bind('scroll', function () {
              var scrollPos = document.body.scrollTop + document.documentElement.clientHeight;
              var elemBottom = element[0].offsetTop + element.height();
              if (scrollPos >= elemBottom) {
                scope.$apply(attrs.scrolly);
              }
          });
      }
  };
});

var Styles =require('./sponsor.scss');

module.exports = sponsorModule;
