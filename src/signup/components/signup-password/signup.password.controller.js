  'use strict';

  function signupPasswordController(signupPasswordService) {
    var signup = this.signup = {};
    signup.profileimage = {"img":"../../../../src/assets/images/content/distributor-avatar.png"};
    signup.passwordimage = {"text":"../../../../../src/assets/images/css/ic-password-show.png",
                            "mask":"../../../../../src/assets/images/css/ic-password-hide.png"};
    signup.currentState = 'text';
      signup.changeImage = function() {
      signup.currentState = this.currentState == 'text' ? 'password': 'text';

      };

    signup.checkHint = function(){
      if(signup.userpassword === signup.userhint){
        alert("Password and hint cannot be same!");
        signup.userhint = " ";
      }

    };

  }

  module.exports = signupPasswordController;
