'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var signupPasswordComponent = require('./signup.password.component');
var signupPasswordService = require('./signup.password.service');

var signupPasswordModule = angular.module('signupPassword', [uiRouter])
                  .component('signuppassword', signupPasswordComponent)
                  .service('signupPasswordService', signupPasswordService)
var base =require('../../../stylesheets/base.scss');
var Styles =require('./signup.password.scss');

module.exports = signupPasswordModule;
