'use strict';
var template = require('./signup.password.jade');
var controller = require('./signup.password.controller');

var signupPasswordComponent = {
  //return {
  restrict: 'E',
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
  //};
};

module.exports = signupPasswordComponent;
