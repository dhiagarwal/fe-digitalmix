'use strict';
var accountVerificationTemplate = require('./account.verification.component.jade');
var accountVerificationController = require('./account.verification.component.controller');

var accountVerificationComponent = {
  bindings:{},
  templateUrl: accountVerificationTemplate,
  controller: accountVerificationController,
  controllerAs: 'ctrl'
};

module.exports = accountVerificationComponent;
