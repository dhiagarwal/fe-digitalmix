'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountVerificationComponent = require('./account.verification.component');
var accountVerificationComponentService = require('./account.verification.component.service');
var passwordPattern = require('../../../__shared/password-pattern/password.pattern.module');
var accountVerificationComponentModule = angular.module('accountVerificationComponentModule', [
					uiRouter
					])
                  .component('accountVerification', accountVerificationComponent)
                  .service('accountVerificationComponentService', accountVerificationComponentService);

module.exports = accountVerificationComponentModule;
