'use strict';

accountVerificationComponentService.$inject = ['$http','$q'];

function accountVerificationComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

accountVerificationComponentService.prototype.getEmailId = function(){
	
	var deferred = this.$q.defer();
	this.$http.get("../../../../src/signup/components/account-verification/account.verification.json")
		.then(function(obj) {
			deferred.resolve(obj.data)
		}, function(data){
			deferred.reject(data);
		});

	return deferred.promise;
};

module.exports = accountVerificationComponentService;