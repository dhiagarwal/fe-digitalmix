'use strict';
accountVerificationController.$inject=['$location','accountVerificationComponentService'];
function accountVerificationController($location, accountVerificationComponentService) {
    var self = this;
    this.isInvalidData = false;
    this.securityCode = "";
    this.emptyError = false;
    this.emailCode = "";
    this.isOpen = false;
    var dropdown = angular.element(".info-dropdown");

    this.getEmailId = function(){
        accountVerificationComponentService.getEmailId().then(function(data){
            self.emailCode = self.maskEmail(data.emailId);

        }, function(data){
            console.log('error');
        });
    };

    this.navigateUser = function() {
        $location.path("/accountCompletion");
    };

    this.maskEmail = function(email) {
        var emailId = email.split('@');
        var mainEmail = emailId[0];
        var splCharacter = '';
        for(var i =0;i < mainEmail.length -2; i++) {
            splCharacter +="*";
        }
        mainEmail = mainEmail.replace(mainEmail.substr(2, mainEmail.length), splCharacter) + '@' + emailId[1];
        return mainEmail;
    };
    this.cancel = function(){
        this.securityCode = "";
        this.isInvalidData = this.emptyError= false;
    };

    this.togglePopUp = function(e){
        e.stopPropagation();
        this.isOpen = !this.isOpen;
        dropdown.toggleClass("open");
    };
    angular.element('.acc-main').off('click').on('click', function() {
        if(self.isOpen){
            self.isOpen = false;
            dropdown.removeClass("open");
        }
    });
    this.getEmailId();
};

module.exports = accountVerificationController;
