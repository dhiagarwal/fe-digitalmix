'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountSettingspComponent = require('./account.settings.personal.component');
var accountSettingsPersonalServiceModule = require('../../../__shared/services/account-settings-personal/account.settings.personal.service.module');
var stateListServiceModule = require('../../../__shared/services/statelist/statelist.service.module');
var languageListServieModule = require('../../../__shared/services/language-list/language-list.module');
var formUtils = require('../../../__shared/form-utils/form-utils'); 

var accountSettingspComponentModule = angular.module('accountSettingspComponentModule', [
					uiRouter,
					accountSettingsPersonalServiceModule.name,
					stateListServiceModule.name,
					languageListServieModule.name,
					formUtils.name
					])
                  .component('accountsettingsp', accountSettingspComponent)
              
                  
import Styles from './account.settings.personal.component.scss';

module.exports = accountSettingspComponentModule;
