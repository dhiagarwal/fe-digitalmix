'use strict';
var template = require('./account.settings.personal.component.jade');
var controller = require('./account.settings.personal.component.controller');

var accountSettingspComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'vm',
};

module.exports = accountSettingspComponent;
