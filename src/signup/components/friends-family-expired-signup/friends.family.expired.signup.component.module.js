'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var friendsFamilyExpiredSignupComponent = require('./friends.family.expired.signup.component');
var friendsFamilyExpiredSignupComponentService = require('./friends.family.expired.signup.component.service');

require('./friends.family.expired.signup.component.scss');

var friendsFamilyExpiredSignupComponentModule = angular.module('friendsFamilyExpiredSignupComponentModule', [uiRouter])
		.component('friendsfamilyexpiredsignupcomponent', friendsFamilyExpiredSignupComponent)
		.service('friendsFamilyExpiredSignupComponentService', friendsFamilyExpiredSignupComponentService);

module.exports = friendsFamilyExpiredSignupComponentModule;