'use strict';
var template = require('./friends.family.expired.signup.component.jade');
var controller = require('./friends.family.expired.signup.component.controller');

var friendsFamilyExpiredSignupComponent = {
	templateUrl: template,
	controller: controller,
	controllerAs: 'ctrl'
};

module.exports = friendsFamilyExpiredSignupComponent;