'use strict';

friendsFamilyExpiredSignupController.inject = ["friendsFamilyExpiredSignupComponentService"];

function friendsFamilyExpiredSignupController(friendsFamilyExpiredSignupComponentService) {
    var self = this;
    self.model = {};

    var promise = friendsFamilyExpiredSignupComponentService.loadData();
    promise.then(function(response) {
        self.model.data = response.data;
    });

};

module.exports  = friendsFamilyExpiredSignupController;