'use strict';

friendsFamilyExpiredSignupComponentService.$inject = ['$http','$q'];

function friendsFamilyExpiredSignupComponentService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	
    this.loadData = function() {
        $http.get('../../../../src/signup/components/friends-family-expired-signup/friends.family.expired.signup.component.json').then(function(response) {
    	    deferred.resolve(response.data);
  	    });
        return deferred.promise;
    };
};

module.exports = friendsFamilyExpiredSignupComponentService;