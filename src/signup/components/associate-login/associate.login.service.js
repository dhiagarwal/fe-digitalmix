'use strict';

associateLoginService.$inject = ['$http', '$q'];

function associateLoginService($http, $q) {
    this.$http = $http;
    var deferred = $q.defer();
    var associateLoginPayload = {};
    $http.get('../../../../src/signup/components/associate-login/associate.login.component.json').then(function(response) {
        deferred.resolve(response.data);
    });

    this.getAssociateList = function() {
        return deferred.promise;
    }
}



module.exports = associateLoginService;