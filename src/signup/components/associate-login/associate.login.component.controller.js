'use strict';

function associateLoginController(associateLoginService, $filter){
	var model = this.model = {};

	var model = this.model = {};
	var self = this;

	self.model.selectedAssociate = null;

	var promise = associateLoginService.getAssociateList();

	self.model.associateListCapAboveMd = 3;
	self.model.associateListCapbelowSm = 2;
	self.model.formEmailValue = 0;
	self.model.formMobileValue = 1;
	self.credentials = {};

	promise.then(function(response){
		self.model.dataList = response.data[0];
		angular.forEach(self.model.dataList.associateList, function(value, key){
			if (!value.contact) {
				value.contact = self.model.formEmailValue;
			}
		});
		self.model.currentAssociateListAboveMd = self.model.dataList.associateList.slice(0, self.model.associateListCapAboveMd);
		self.model.currentAssociateListBelowSm = self.model.dataList.associateList.slice(0, self.model.associateListCapbelowSm);
	});

	self.sendInvite = function(isValid, selectedAssociateObject, emailModel) {
		if (isValid) {
			var objInAssociateList = $filter('filter')(self.model.dataList.associateList, {id: selectedAssociateObject.id})[0];
			if (objInAssociateList) {
				angular.forEach(objInAssociateList.inviteStatus, function(value, key) {
					value.status = false;
					if (value.id == 1) {
						value.status = true;
					}
				});
				angular.forEach(self.model.dataList.associateList, function(value, key) {
					if (value.id == objInAssociateList.id) {
						value = objInAssociateList;
					}
				});
			}
			angular.element('#invite-popup').modal('hide');
		}
	};

	self.goToUrl = function(url) {
		if (url) {
			window.location.href = url;
		}
	};

	self.openPopup = function(associate) {
		self.credentials.isSubmitted = false;
		self.model.selectedAssociate = angular.copy(associate);
		self.credentials.userDetailsField = angular.copy(self.model.selectedAssociate.email);
	};

	self.loadMore = function() {
		self.isLoadMore = true;
		self.model.currentAssociateListAboveMd = self.model.currentAssociateListBelowSm = self.model.dataList.associateList;
	};
}

module.exports = associateLoginController;