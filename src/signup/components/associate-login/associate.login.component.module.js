var angular = require('angular');
var uiRouter = require('angular-ui-router');
var formUtils = require('../../../__shared/form-utils/form-utils'); 
var stateListServiceModule = require('../../../__shared/services/statelist/statelist.service.module');
var emailPhoneValidator = require('../../../__shared/email-phone/email.phone.module');

var associateLoginComponent = require('./associate.login.component');
var associateLoginService = require('./associate.login.service');


var associateLoginModule = angular.module('associateLoginModule', [uiRouter, formUtils.name, stateListServiceModule.name, emailPhoneValidator.name])
    .component('associatelogin', associateLoginComponent)
    .service('associateLoginService', associateLoginService)
    

var base = require('../../../stylesheets/base.scss');
var Styles = require('./associate.login.component.scss');

module.exports = associateLoginModule;