var template =require('./associate.login.component.home.jade');
var controller =require('./associate.login.component.controller');

var associateLoginComponent=  {
	templateUrl : template,
	controller,
	controllerAs: 'ctrl'
};

module.exports = associateLoginComponent;