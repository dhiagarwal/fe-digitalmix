var angular =require('angular');
var uiRouter =require('angular-ui-router');

var individualDistributorSetupComponent =require('./individual.distributor.setup.component');

var individualDistributorSetupModule = angular.module('individualDistributorSetupModule', [uiRouter])
.component('individualDistributorSetupComponent', individualDistributorSetupComponent);

var Styles = require('./individual.distributor.setup.scss');

module.exports = individualDistributorSetupModule;
