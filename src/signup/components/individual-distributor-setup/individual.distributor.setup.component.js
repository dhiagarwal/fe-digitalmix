var template =require('./individual.distributor.setup.jade');
var controller =require('./individual.distributor.setup.controller');

var individualDistributorSetupComponent =  {
    templateUrl : template,
    controller,
    controllerAs: 'ctrl'
};

module.exports = individualDistributorSetupComponent;
