'use strict';

function individualDistributorSetupController() {

  var self = this;
  var model = self.model = {};
  model.mandatoryInfo = {};
  model.hasError = { err: false, message: '' };
  model.years = [];
  var yearValue = new Date();
  for (var i = (yearValue.getFullYear() - 18); i > (yearValue.getFullYear() - 100); i--) {
      model.years.push(i);
  }
  model.year = "";
  model.month="";
  model.day = "";
  model.ssnInput = "";
  model.birthdate = "";
  model.inputType = 'tel';
  model.currentState = 'masked';
  model.passwordimage = {"text":"../../../../../src/assets/images/css/ic-password-show.png",
                          "mask":"../../../../../src/assets/images/css/ic-password-hide.png"};
  this.customerConvertDistributorPath = "customerConvertDistributor";
  model.changeImage = function() {
      model.currentState = this.currentState == 'masked' ? 'unmasked' : 'masked';
      model.inputType = this.inputType == 'tel' ? 'password' : 'tel';  
  };
  model.submitMandatoryInfo = function() {
      model.hasError.err = false;
      if ((model.year == "" && model.month == "" && model.day == "") || !self.mandatoryInfoForm.ssnInput.$valid) {
          model.hasError.err = true;
          model.hasError.message = 'Please enter a valid Date';
          if (model.year !== "" && model.month !== "" && model.day !== ""){
            model.hasError.err = false;
          } else{
            self.mandatoryInfoForm.ssnInput.$touched = true;
          }
          return;
      } else{
        model.hasError.err = false;
        if (model.year && model.month && model.day) {
            model.mandatoryInfo.birthdate = new Date(model.year, model.month, model.day);
        }

        var monthtemp = model.mandatoryInfo.birthdate.getMonth();
        var datetemp = model.mandatoryInfo.birthdate.getDate();
        var yeartemp = model.mandatoryInfo.birthdate.getFullYear();
        if (monthtemp < 10) {
            monthtemp = '0' + monthtemp;
        };
        if (datetemp < 10) {
            datetemp = '0' + datetemp;
        };
        model.mandatoryInfo.birthdate = (yeartemp + '-' + monthtemp + '-' + datetemp);
      }
  }

}
module.exports = individualDistributorSetupController;
