'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var associateSignupCreatePwdComponent = require('./associate.signup.create.password.component');

var associateSignupCreatePasswordComponentModule = angular.module('associateSignupCreatePasswordComponentModule', [
					uiRouter,
					])
                  .component('associatesignupcreatepwd', associateSignupCreatePwdComponent)
                  
import Styles from './associate.signup.create.password.component.scss';

module.exports = associateSignupCreatePasswordComponentModule;