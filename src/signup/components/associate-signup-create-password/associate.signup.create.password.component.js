'use strict';
var template = require('./associate.signup.create.password.component.jade');
var controller = require('./associate.signup.create.password.component.controller');

var associateSignupCreatePasswordComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'assosignupcreatepwd',
};

module.exports = associateSignupCreatePasswordComponent;