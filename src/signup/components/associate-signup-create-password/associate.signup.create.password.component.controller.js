'use strict';

function associateSignupCreatePasswordController() {
    var self = this;

    this.lastName='';
    this.firstName='';
    this.isRequired=true;
    // adding this flag to render the placeholder image if profile picture is not present 
    this.profilepresent = false; 

    this.credentials= {};
    this.submitNamePwd=function () {
        window.location.href = window.location.origin + '/#/signupdistributor';
    }

    this.passwordHint = "<p>Password must meet three of the four requirements below:</p><ul><li>Password contains a lower case letter</li><li>Password contains an upper case letter</li><li>Password contains a number</li><li>Password contains a special character</li></ul>";
   
};

module.exports = associateSignupCreatePasswordController;