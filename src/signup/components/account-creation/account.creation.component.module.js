'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var accountCreationComponent = require('./account.creation.component');
var accountCreationComponentService = require('./account.creation.component.service');

var accountCreationComponentModule = angular.module('accountCreationComponentModule', [
					uiRouter
					])
                  .component('accountcreation', accountCreationComponent)
                  .service('accountCreationComponentService', accountCreationComponentService)
                  
import Styles from './account.creation.component.scss';

module.exports = accountCreationComponentModule;
