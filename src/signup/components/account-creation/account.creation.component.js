'use strict';
var accountCreationTemplate = require('./account.creation.component.jade');
var accountCreationController = require('./account.creation.component.controller');

var accountCreationComponent = {
  //bindings:{},
  templateUrl: accountCreationTemplate,
  controller: accountCreationController,
  controllerAs: 'ctrl'
};

module.exports = accountCreationComponent;
