'use strict';

accountCreationComponentService.$inject = ['$http','$q'];

function accountCreationComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

accountCreationComponentService.prototype.validateCustomerId = function(data){	
	var deferred = this.$q.defer();
	this.$http.post("", data)
		.then(function(data) {
			deferred.resolve(data);
		}, function(error){
			deferred.reject(error);
		});

	return deferred.promise;
};

module.exports = accountCreationComponentService;