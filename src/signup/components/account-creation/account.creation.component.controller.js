'use strict';
accountCreationController.$inject=['$location','accountCreationComponentService'];
function accountCreationController($location, accountCreationComponentService) {
    var self = this;
    this.isInvalidData = false;
    this.emptyError = false;
    this.customerId = "";
    this.isOpen = false;
    var dropdown = angular.element(".info-dropdown");
    
    this.validateCustomerId = function() {
        this.emptyError = !this.customerId;
        
        if(!this.emptyError) {
            this.isInvalidData = !this.isInvalidData;
            if(!this.isInvalidData){
                $location.path("/accountVerification");
            }
        }
        //Remove above code and uncomment the below code for AEM Integration.
        /*
        this.emptyError = !this.customerId;
        if(!this.emptyError){
            var promise = accountCreationComponentService.validateCustomerId({"customerId": this.customerId});
            promise.then(function (data) {
                self.isInvalidData = data;
                if(self.isInvalidData){
                    $location.path("/accountVerification");
                }
            }, function(data){
                
            });
        }*/
    };

    this.checkCustomerId = function() {
        this.emptyError = !this.customerId;
        this.isInvalidData = false;
    };
    
    this.cancel = function(){
        this.customerId = "";
        this.isInvalidData = false;
        this.emptyError = false;
    };

    this.togglePopUp = function(e){
        e.stopPropagation();
        this.isOpen = !this.isOpen;
        dropdown.toggleClass("open");
    };

    angular.element('.acc-main').off('click').on('click', function() {
        if(self.isOpen){
            self.isOpen = false;
            dropdown.removeClass("open");
        }
    });

};

module.exports = accountCreationController;