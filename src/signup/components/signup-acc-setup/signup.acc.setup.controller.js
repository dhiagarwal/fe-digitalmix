function signupAccSetupController(signupAccSetupService, $window) {

    var model = this.model = {};
    model.mandatoryInfo = model.address = model.payment = model.preferences = model.familyInfo = model.associateInfo = {}
    model.isCompleteAddress = false;
    model.isCompletePayment = false;
    model.isCompletePreferences = false;
    model.isCompleteAddFamily = false;
    model.isCompleteAddAssociate = false;
    model.isMemberPresent = false;
    model.isAssociateAdded = false;
    model.showMemberForm = false;
    model.showAssociateForm = false;
    model.familyMemberList = [];
    model.associateList = [];
    model.familyInfo.contactByEmail = true;
    model.familyInfo.contactByPhone = false;
    model.associateInfo.contactByEmail = true;
    model.associateInfo.contactByPhone = false;
    model.restrictAddButton = false;
    model.addressSubmitted = false;
    model.hasError = { err: false, message: '' };
    model.familyInfo.contactpreference = {};
    model.associateInfo.contactpreference = {};
    model.emailValidation = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    var self = this;
    model.passwordimage = {
        "text": "../../../src/assets/images/css/ic-password-show.png",
        "mask": "../../../src/assets/images/css/ic-password-hide.png"
    };

    // for family email component
    model.familyInfo.emailValueObj = {};

    // for associate email component
    model.associateInfo.emailValueObj = {};



    self.distributorInfo = {};
    model.year = "";
    model.month = "";
    model.day = "";
    model.birthdate = "";

    var promise = signupAccSetupService.getDistributorInfo();

    // get service is not required for user info as user will never come here second time. It will always be fresh information
    promise.then(function(data) {

        self.distributorInfo = data;
        model.directPolicyContent = self.distributorInfo.directPolicyInfo.directPolicyContent;
        model.moreInfoText = self.distributorInfo.moreInfoText;
        model.stateList = self.distributorInfo.stateList;
    });

    model.selectPreference = function(preferenceType, flag) {
        if (flag == "F") {
            if (preferenceType == 'email') {
                model.familyInfo.contactByEmail = true;
                model.familyInfo.contactByPhone = false;
            } else {
                model.familyInfo.contactByPhone = true;
                model.familyInfo.contactByEmail = false;
            }
        } else if (flag == "A") {
            if (preferenceType == 'email') {
                model.associateInfo.contactByEmail = true;
                model.associateInfo.contactByPhone = false;
            } else {
                model.associateInfo.contactByPhone = true;
                model.associateInfo.contactByEmail = false;
            }
        }
    }

    model.updateMember = function() {
        self.addNewMember.$submitted = true;
        if (self.addNewMember.$invalid)
            return false;
        var familyMember = {
            "firstname": self.model.familyInfo.firstname,
            "lastname": self.model.familyInfo.lastname,
            "accountstatus": "Account Pending",
            "contactpreference": {
                "phone": (self.model.familyInfo.contactpreference != undefined) ? model.familyInfo.emailValueObj.userPhoneField : "",
                "email": (self.model.familyInfo.contactpreference != undefined) ? model.familyInfo.emailValueObj.userEmailField : ""
            }
        }
        model.familyMemberList.push(familyMember);
        model.familyInfo = {};
        model.familyInfo.contactByEmail = true;
        model.familyInfo.contactByPhone = false;
        model.isMemberPresent = true; //show save button
        model.showMemberForm = false; // hide form and show add button


    }

    model.isAssociateAdded = false;
    model.showAddButton = true;
    model.updateAssociate = function() {
        self.addNewAssociate.$submitted = true;
        if (self.addNewAssociate.$invalid)
            return false;
        var associateMember = {
            "firstname": self.model.associateInfo.firstname,
            "lastname": self.model.associateInfo.lastname,
            "accountstatus": "Account Pending",
            "contactpreference": {
                "phone": (self.model.associateInfo.contactpreference != undefined) ? self.model.associateInfo.emailValueObj.userPhoneField : "",
                "email": (self.model.associateInfo.contactpreference != undefined) ? self.model.associateInfo.emailValueObj.userEmailField : ""
            }
        }
        model.associateList.push(associateMember);
        model.isAssociateAdded = true; //show save button
        model.showAssociateForm = false;
        model.showAddButton = false;
        model.restrictAddButton = true;


    }


    model.submitPayment = function() {
        // save the information
        model.isCompletePayment = true;
        console.log("Payment =" + model.payment);
    }

    model.submitAddress = function(form) {
        model.addressSubmitted = true;
        if (form.$valid) {
            model.addressToggle = 'collapse';
            model.isCompleteAddress = true;
            return
        }
        model.addressToggle = '';
        model.isCompleteAddress = false;
    }

    model.submitPref = function() {
        // save pref
        console.log("preferences =" + model.preferences);
        model.isCompletePreferences = true;

    }

    model.submitFamilyInfo = function() {
        console.log("Family Members =" + model.familyMemberList);
        model.isCompleteAddFamily = true;
    }

    model.submitAssociateInfo = function() {
        console.log("Associates =" + model.familyMemberList);
        model.isCompleteAddAssociate = true;
    }

    model.moveNext = function() {
        $window.location.href = "/#/reviewsignupinfopage";
    }

    model.submitMandatoryInfo = function() {
        //model.checkForError();

        if (!model.year || !model.month || !model.day) {
            model.hasError.err = true;
            model.hasError.message = 'Please select the valid date';
            angular.element(berror).addClass("hide");
            return
        }
        model.hasError.err = false;

        if (model.year && model.month && model.day) {
            model.mandatoryInfo.birthdate = new Date(model.year, model.month, model.day);
        }

        // process date object to store in string format
        var monthtemp = model.mandatoryInfo.birthdate.getMonth();
        var datetemp = model.mandatoryInfo.birthdate.getDate();
        var yeartemp = model.mandatoryInfo.birthdate.getFullYear();
        if (monthtemp < 10) {
            monthtemp = '0' + monthtemp;
        };
        if (datetemp < 10) {
            datetemp = '0' + datetemp;
        };
        model.mandatoryInfo.birthdate = (yeartemp + '-' + monthtemp + '-' + datetemp);
        if (self.form.$invalid) {
            return false;
        }
        signupAccSetupService.setDistributorInfo(model.mandatoryInfo);
        model.optionalForm = true;
    }

    model.inputType = 'tel';
    model.currentState = 'masked';

    model.changeImage = function() {
        model.currentState = this.currentState == 'masked' ? 'unmasked' : 'masked';
        model.inputType = this.inputType == 'tel' ? 'password' : 'tel';  
    };

    model.checkForError = function() {
        if (!model.year || !model.month || !model.day) {
            model.hasError = { err: true, message: 'Invalid Date' };
            return;
        }
        model.hasError.err = false;
    }

    model.validDate  =   function() {    
        model.hasError.err  =  false;
    } 


    model.checkLength = function(flag, form) {
        if (flag == "r") {
            if (model.payment.routingNum) {
                if (model.payment.routingNum.toString().length != 9) {
                    model.invalidRoutingLength = true;
                    form.routingnum.$error.invalidRoutingLength = true;
                } else {
                    model.invalidRoutingLength = false;
                    form.routingnum.$error.invalidRoutingLength = false;
                }
            }
        }
        if (flag == "b") {
            if (model.payment.accNum) {
                if (model.payment.accNum.length != 34) {
                    model.invalidAccountLength = true;
                    form.accnum.$error.invalidAccountLength = true;
                } else {
                    model.invalidAccountLength = false;
                    form.accnum.$error.invalidAccountLength = false;
                }
            }
        }
    }

    model.skipFamilySection = function() {
        model.showMemberForm = false;
    }

    model.resetForm = function(form) {

        switch (form.$name) {
            case "optionalAddress":
                {
                    model.address = {};
                    model.isCompleteAddress = false;
                    model.addressSubmitted = false;
                    break;
                }

            case "paymentForm":
                {
                    model.payment = {};
                    model.isCompletePayment = false;
                    break;
                }
            case "preferencesForm":
                {
                    model.preferences = {};
                    break;

                }
        }    
        form.$setPristine();    
        form.$setUntouched();      
    }

}


module.exports = signupAccSetupController;