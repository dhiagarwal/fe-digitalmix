var template =require('./signup.acc.setup.jade');
var controller =require('./signup.acc.setup.controller');

var signupAccSetupComponent =  {
  //return {
    restrict: 'E',
    scope: {},
    templateUrl : template,
    controller,
    controllerAs: 'signupAccSetupCtrl',
    bindToController: true
  //};
};

module.exports = signupAccSetupComponent;
