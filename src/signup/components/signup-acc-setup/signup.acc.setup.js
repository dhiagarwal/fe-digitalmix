var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-messages')
var formUtils = require('../../../__shared/form-utils/form-utils'); 

var signupAccSetupComponent = require('./signup.acc.setup.component');
var signupAccSetupService = require('./signup.acc.setup.service');

var signupAccSetupModule = angular.module('signupAccSetupModule', [
    uiRouter,
    'ngMessages',
    formUtils.name])
	    .component('signupaccsetup', signupAccSetupComponent)
	    .service('signupAccSetupService', signupAccSetupService);

var base = require('../../../stylesheets/base.scss');
var Styles = require('./signup.acc.setup.scss');

module.exports = signupAccSetupModule;
