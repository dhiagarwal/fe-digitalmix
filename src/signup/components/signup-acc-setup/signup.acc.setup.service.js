'use strict';

signupAccSetupService.$inject = ['$http', '$q'];

function signupAccSetupService($http, $q) {
    this.$http = $http;
    var deferred = $q.defer();
    var distributorInfoPayload = {};
    $http.get('../../../../src/signup/components/signup-acc-setup/signup.acc.setup.component.json').then(function(response) {
        deferred.resolve(response.data);
    });

    this.getDistributorInfo = function() {
        return deferred.promise;
    }
    this.setDistributorInfo = function(mandatoryInfoData) {
        distributorInfoPayload = mandatoryInfoData;
    }
}



module.exports = signupAccSetupService;