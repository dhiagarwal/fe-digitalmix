'use strict';

function termsConditionsController($scope,termsConditionsCarouselComponentService, $uibModal) {
    var self = this;
    this.$uibModal = $uibModal;
    this.beforeYouMayContinue = "Before you may continue with your application, please review the terms and conditions of the Distributor Agreement and Policies and Procedures (\"Policies\") located below. The Policies describe your rights and obligations as an independent Nu Skin distributor."
    this.needToAgreeText = "You'll need to agree to the Distributor Agreement in order to sell Nu Skin Products and earn comissions.";
    this.definitionTitle = "A. Definitions.";
    this.definitionOne = "Defined terms are set forth below or may be separately defined in any of the agreements. The meaning of capitalized terms not found in this document is set forth in the Policies and Procedures.";
    this.definitionTwo = "“Bonuses” means the compensation paid to Distributors based on the volume of Nu Skin Products sold by a Distributor, Downline Organization, and breakaway executives as set forth in the Sales Compensation Plan.";
    this.definitionThree = "“Business Portfolio” means the non-commissionable, not-for-profit kit and is the only “Business Portfolio” means the non-commissionable, not-for-profit kit and is the only“Business Portfolio” means the non-commissionable, not-for-profit kit and is the only...";
    self.checkBoxes = false;
    this.policyCheckbox = false;
    this.arbitrationPolicyCheckbox = false;
    self.acceptButtonClicked = false;
    this.srcUrl="";
    this.showModal=false;
    this.knowMore = function(linkType) {
        if(linkType == 'compensationSummary'){
            self.modalHeading = "Distributor Compensation Summary";
            self.modalURL = "src/assets/distearnings.html";
        } else {
            self.modalHeading = "Product Return Policy";
            self.modalURL = "src/assets/nuskin_refund_policy.html";
        }
        this.showModal = true;
    };
    this.hideCustomModal = function(){
        this.showModal = false;
    };
    this.acceptTermsConditions = function() {
        self.acceptButtonClicked = true;
        if (this.policyCheckbox && this.arbitrationPolicyCheckbox) {
            self.checkBoxes = false;
        } else {
            self.checkBoxes = true;
        }
    };
    this.checkboxShowHide = function() {
        if (self.acceptButtonClicked) {
            if (this.policyCheckbox && this.arbitrationPolicyCheckbox) {
                self.checkBoxes = false;
            } else {
                self.checkBoxes = true;
            }
        }
    };

    this.skipForNowShow = function() {
        $("#skipForNowTermsConditions").modal();
    };
    this.remindLater = function() {
        var baseUrl = window.location.origin;
        window.location.href = baseUrl + '/#/';
    }
    this.slickConfig = {
        enabled: true,
        infinite: false,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        speed: 500,
        responsive: [{
            breakpoint: 320,
            settings: {
                slidesToShow: 1,
            }
        }, {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
            }
        }, {
            breakpoint: 1023,
            settings: {
                slidesToShow: 3,
                draggable: false,
            }
        }, {
            breakpoint: 1440,
            settings: {
                slidesToShow: 3,
                draggable: false,
            }
        }]
    };
    var promise = termsConditionsCarouselComponentService.getTileData("../../../src/signup/components/terms-conditions/terms.conditions.component.json");

    promise.then(function(data) {
        self.title = Object.keys(data)[0];
        self.filteredData = _.take(data.data, 10);
        self.tileData = data;
    });
};

module.exports = termsConditionsController;
