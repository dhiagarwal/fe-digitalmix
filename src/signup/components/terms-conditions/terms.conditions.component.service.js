'use strict';

termsConditionsCarouselComponentService.$inject = ['$http','$q'];

function termsConditionsCarouselComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

termsConditionsCarouselComponentService.prototype.getTileData = function(Url){
	var deferred = this.$q.defer();
	this.$http.get(Url)
		.success(deferred.resolve);
		return deferred.promise;
};

module.exports = termsConditionsCarouselComponentService;