'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('slick-carousel');
require('angular-slick-carousel');
require('angular-animate');

var termsConditionsComponent = require('./terms.conditions.component');
var termsConditionsCarouselComponentService = require('./terms.conditions.component.service');
var customModal = require('../../../__shared/custom-modal/custom.modal.module');

var termsConditionsComponentModule = angular.module('termsConditionsComponentModule', [uiRouter,'ngAnimate','slickCarousel',customModal.name])
                  .component('termsconditionscomponent', termsConditionsComponent)
				  .service('termsConditionsCarouselComponentService', termsConditionsCarouselComponentService)
                  
import Styles from './terms.conditions.component.scss';

module.exports = termsConditionsComponentModule;