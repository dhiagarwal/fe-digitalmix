'use strict';
var template = require('./terms.conditions.component.jade');
var controller = require('./terms.conditions.component.controller');

var termsConditionsComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'termsconditionsctrl'
};

module.exports = termsConditionsComponent;