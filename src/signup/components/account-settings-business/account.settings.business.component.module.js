'use strict';
require('../../../stylesheets/base.scss');
require('./account.settings.business.component.scss');

var angular = require('angular');
require('slick-carousel');
require('angular-slick-carousel');
require('../../../../src/signup/components/terms-conditions/terms.conditions.component.scss');

var uiRouter = require('angular-ui-router');
var accountSettingsBusinessComponent = require('./account.settings.business.component');
var accountSettingsBusinessServiceModule = require('../../../__shared/services/account-settings-business/account.settings.business.service.module');
var languageListServiceModule = require('../../../__shared/services/language-list/language-list.module');
var formUtils = require('../../../__shared/form-utils/form-utils'); 
var sharedUtils = require('../../../__shared/shared');
var addressVerification = require('../../../__shared/address-verification/address-verification.component.module');

var accountSettingsBusinessComponentModule = angular.module('accountSettingsBusinessComponentModule', [
	uiRouter,
	accountSettingsBusinessServiceModule.name,
	'slickCarousel',
	sharedUtils.name,
	languageListServiceModule.name,
	formUtils.name,
	addressVerification.name
	])
.component('accountsettingsbusiness', accountSettingsBusinessComponent)

module.exports = accountSettingsBusinessComponentModule;