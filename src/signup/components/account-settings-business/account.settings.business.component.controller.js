'use strict';

//var termsModalTemplate = require('./account.settings.business.modal-terms.jade');
var termsConditionsModal = require('../../../../src/signup/components/terms-conditions/terms.conditions.modal.component.jade');
var skipForNowModal = require('../../../../src/signup/components/terms-conditions/terms.conditions.skip.modal.component.jade');
var confirmationModalTemplate = require('./account.settings.business.modal-confirmation.jade');
var addAssociateModalTemplate = require('./account.settings.business.modal-add-associate.jade');
var fileUploadModalTemplate = require('./account.settings.business.modal-file-upload.jade');
var directDepositModalTemplate = require('./account.settings.business.modal.direct.deposit.jade');

accountSettingsBusinessController.$inject = ['$window', 'accountSettingsBusinessService', 'statelistservice', '$filter', '$uibModal', 'languageListService', '$location'];

function accountSettingsBusinessController($window, accountSettingsBusinessService, statelistservice, $filter, $uibModal, languageListService, $location) {

    var self = this;

    var model = self.model = {};
    model.isBusinessSelected = true;
    model.isPersonalSelected = false;

    this.$uibModal = $uibModal;

    // overview properties
    model.showContactForm = false;
    model.showAddressForm = false;
    model.showBankingForm = false;
    model.accountOriginal = model.contactOriginal = model.bankingOriginal = model.userdataOriginal = model.associatesListOriginal = {};
    model.account = {};
    model.contact = {};
    model.address = {};
    model.addressList = {};
    model.associatesList = {};
    model.banking = {};
    model.userdata = {};
    model.directPolicyInfo = {};
    model.termsConditionsData={};
    model.sameAddressCheckbox = true;
    model.emailInvalid = false;
    model.emailRequired = false;
    model.isPreferredNamePresent = false;
    model.invalidRoutingLength = false;
    model.invalidAccountLength = false;
    self.associatesLimitAboveMd = 3;
    self.associatesLimitBelowMd = 4;
    self.associatesLimitForIndividualDistributor = 1;
    self.isSaveForAddressClicked = false;
    self.isEnteredAddressCannotBeIdentified = true;
    self.showCustomModal = false;
    // service call
    var promise = accountSettingsBusinessService.loadDetails();
    promise.then(function (data) {

        //overview processing
        model.accountOriginal = data.account;
        model.contactOriginal = data.contact;
        model.addressList = data.address;
        model.associatesList = data.associatesList ? data.associatesList : null;
        model.bankingOriginal = data.banking;
        model.userdataOriginal = data.userdata;
        model.businessImageList = data.imageDataList;
        model.leftTabsList = data.leftTabsList;
        model.directPolicyInfo = data.directPolicyInfo;
        model.termsConditionsData = data;
        angular.extend(model.account, model.accountOriginal);
        angular.extend(model.contact, model.contactOriginal);
        angular.extend(model.banking, model.bankingOriginal);
        angular.extend(model.userdata, model.userdataOriginal);
        model.associatesListOriginal = angular.copy(model.associatesList);
        // mobile phone number 
        if (model.contact.mobilePhoneInput && model.contact.mobilePhoneInput.length == 10) {
            model.contact.mobilePhoneInputDisplay = "(" + model.contact.mobilePhoneInput.substr(0, 3) + ")" + model.contact.mobilePhoneInput.substr(3, 3) + "-" + model.contact.mobilePhoneInput.substr(6);
        }

        if (model.banking.routingNumber && model.banking.routingNumber.length == 9) {
            model.banking.routingNumberDisplay = "*".repeat(5) + model.banking.routingNumber.substr(model.banking.routingNumber.length - 4);
        }

        if (model.banking.accountNumber && model.banking.accountNumber.length == 34) {
            model.banking.accountNumberDisplay = "*".repeat(30) + model.banking.accountNumber.substr(model.banking.accountNumber.length - 4);
        }

        model.accountType = model.getActiveBusinessAccount(model.account);
        self.checkBusinessAccountType();
    });

    if(model.userdata.preferredname != "" && model.userdata.preferredname != null && model.userdata.preferredname != 'undefined') {
        model.isPreferredNamePresent  = true;
    } else {
        model.isPreferredNamePresent  = false;
    }

    var spromise = statelistservice.loadDetails();
    spromise.then(function (data) {
        self.model.stateList = data;
    });

    model.loadTab = function (currentTab) {
        model.currentTab = currentTab;
    }

    model.saveContact = function (isValid) {
        if (isValid) {
            model.contact.email = self.credentials.userEmailField;
            if (model.contact.mobilePhoneInput && model.contact.mobilePhoneInput.length == 10) {
                model.contact.mobilePhoneInputDisplay = "(" + model.contact.mobilePhoneInput.substr(0, 3) + ")" + model.contact.mobilePhoneInput.substr(3, 3) + "-" + model.contact.mobilePhoneInput.substr(6);
            }
            angular.extend(model.contactOriginal, model.contact);
            model.showContactForm = false;
        }
    };

    self.checkBusinessAccountType = function () {
        if (model.accountType.accountType == 'individual' && model.associatesList) {
            var associatesList = []; angular.copy(model.associatesList[0]);
            associatesList.push(angular.copy(model.associatesList[0]));
            model.associatesList = associatesList;
        }
        else if (model.accountType.accountType != 'individual' && model.associatesList) {
            model.associatesList = angular.copy(model.associatesListOriginal);
        }
    };

    self.manageSameAddressCheckbox = function (isCancelPressed) {
        isCancelPressed = isCancelPressed ? isCancelPressed : false;
        model.sameAddressCheckbox = isCancelPressed ? !model.sameAddressCheckbox : model.sameAddressCheckbox;
        switch (model.sameAddressCheckbox) {
            case true:
                angular.extend(model.address, model.addressList.homeAddress);
                break;
            case false:
                if (model.enterAddress && model.enterAddress.length) {
                    angular.extend(model.address, model.enterAddress);
                    angular.extend(model.addressList.businessAddress, model.enterAddress);
                }
                else {
                    angular.extend(model.address, model.addressList.businessAddress);
                }
                break;
        }
    };

    self.setForm = function(formName) {
        self.currentForm = formName;
    };

    model.cancelContact = function () {
        angular.extend(model.contact, model.contactOriginal);
        self.credentials.userEmailField = model.contact.email;
        model.showContactForm = false;
    }

    self.checkIfValidAddress = function (isValid, isSameAddressCheckbox) {
        self.isSaveButtonClicked = true;
        if(!isValid && isValid == undefined) {
            isValid = self.currentForm ? self.currentForm.$valid : false;
        }
        var checkIfIsValid = isSameAddressCheckbox ? true : isValid ? true : false;
        if (checkIfIsValid && !isSameAddressCheckbox) {
            self.isSaveForAddressClicked = true;
            if (self.isEnteredAddressCannotBeIdentified) {
                self.addressJsonDataForModal = {
                    "addressLine1": model.enterAddress.streetAddress,
                    "addressLine2": model.enterAddress.streetAddress2,
                    "city": model.enterAddress.city,
                    "state": model.enterAddress.state,
                    "zip": model.enterAddress.zipcode
                };
            }
        }
        else if (isSameAddressCheckbox) {
            model.saveAddress();
        }
    };

    model.saveAddress = function (addressData) {
        if(addressData) {
            model.enterAddress = {
                "streetAddress": addressData.addressLine1,
                "streetAddress2": addressData.addressLine2,
                "city": addressData.city,
                "state": addressData.state,
                "zipcode": addressData.zip,
            }
        }
        model.address = model.enterAddress;
        if (!model.sameAddressCheckbox) {
            angular.extend(model.addressList.businessAddress, model.enterAddress);
        }
        model.showAddressForm = false;
    }

    model.cancelAddress = function () {
        if (model.sameAddressCheckbox) {
            var isCancelPressed = true;
            self.manageSameAddressCheckbox(isCancelPressed);
        }
        else {
            self.manageSameAddressCheckbox();
        }
        model.showAddressForm = false;
    }

    model.saveBanking = function (isValid) {
        if (isValid) {
            if (model.banking.routingNumber && model.banking.routingNumber.length == 9) {
                model.banking.routingNumberDisplay = "******" + model.banking.routingNumber.substr(model.banking.routingNumber.length - 4);
            }

            if (model.banking.accountNumber && model.banking.accountNumber.length == 34) {
                model.banking.accountNumberDisplay = "******" + model.banking.accountNumber.substr(model.banking.accountNumber.length - 4);
            }
            angular.extend(model.bankingOriginal, model.banking);
            model.showBankingForm = false;
        }
    };

    self.directDepositCheckbox = function () {
        self.isDirectDepositCheckboxTouched = true;
    };

    model.cancelBanking = function () {
        angular.extend(model.banking, model.bankingOriginal);
        model.showBankingForm = self.isDirectDepositCheckboxTouched = self.bankingSubmitPressed = false;
    };

    model.saveAccEdit = function () {
        if(model.userdata.preferredname != "" && model.userdata.preferredname != null && model.userdata.preferredname != 'undefined') {
            model.isPreferredNamePresent  = true;
        } else {
            model.isPreferredNamePresent  = false;
        }
        angular.extend(model.userdataOriginal, model.userdata);
        model.cancelAccEdit();
    }

    model.cancelAccEdit = function () {
        angular.extend(model.userdata, model.userdataOriginal);
    }

    model.getActiveBusinessAccount = function (dataModel) {
        dataModel.activeAccount = $filter('filter')(dataModel.accountTypeList, {
            isActive: true
        })[0];
        return dataModel.activeAccount;
    };
    var modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };
    model.skipForNowController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };
         modalSelf.skipForNowClose=function(){
            modalSelf.ok();
            modalPopupInstance(termsConditionsModal, model.termsConditionsController, 'app-modal-window terms-conditions-modal-body');
        };
        modalSelf.remindLater=function() {
            $uibModalInstance.dismiss();
            var baseUrl=window.location.origin;
            window.location.href=baseUrl+'/#/';
        }
    };
    model.termsConditionsController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};
        modalSelf.modalData= model.termsConditionsData;
        modalSelf.beforeYouMayContinue= "Before you may continue with your application, please review the terms and conditions of the Distributor Agreement and Policies and Procedures (\"Policies\") located below. The Policies describe your rights and obligations as an independent Nu Skin distributor."
        modalSelf.needToAgreeText="You'll need to agree to the Distributor Agreement in order to sell Nu Skin Products and earn comissions.";
        modalSelf.definitionTitle="A. Definitions.";
        modalSelf.definitionOne="Defined terms are set forth below or may be separately defined in any of the agreements. The meaning of capitalized terms not found in this document is set forth in the Policies and Procedures.";
        modalSelf.definitionTwo="“Bonuses” means the compensation paid to Distributors based on the volume of Nu Skin Products sold by a Distributor, Downline Organization, and breakaway executives as set forth in the Sales Compensation Plan.";
        modalSelf.definitionThree="“Business Portfolio” means the non-commissionable, not-for-profit kit and is the only “Business Portfolio” means the non-commissionable, not-for-profit kit and is the only“Business Portfolio” means the non-commissionable, not-for-profit kit and is the only...";
        modalSelf.checkBoxes=false;
        modalSelf.policyCheckbox=false;
        modalSelf.arbitrationPolicyCheckbox=false;
        modalSelf.acceptButtonClicked=false;

        modalSelf.knowMore = function(linkType) {
            modalSelf.cancel();
            if(linkType == 'compensationSummary'){
                self.modalHeading = "Distributor Compensation Summary";
                self.modalURL = "src/assets/distearnings.html";
            } else {
                self.modalHeading = "Product Return Policy";
                self.modalURL = "src/assets/nuskin_refund_policy.html";
            }
            self.showCustomModal = true;
        };
        modalSelf.acceptTermsConditions=function(){
            modalSelf.acceptButtonClicked=true;
            if(modalSelf.policyCheckbox && modalSelf.arbitrationPolicyCheckbox){
                modalSelf.checkBoxes=false;
            }
            else{
                modalSelf.checkBoxes=true;
            }
        };
        modalSelf.checkboxShowHide=function(){
            if(modalSelf.acceptButtonClicked){
                if(modalSelf.policyCheckbox && modalSelf.arbitrationPolicyCheckbox){
                    modalSelf.checkBoxes=false;
                }
                else{
                    modalSelf.checkBoxes=true;
                }
            }
        };
        
       
        modalSelf.cancelTermsConditions=function(){
            modalSelf.cancel();
        };
        
        modalSelf.slickPrimaryConfig = {
            enabled: true,
            infinite: false,
            adaptiveHeight: false,
            mobileFirst: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            speed: 500,
            responsive: [
            {
              breakpoint: 320,
              settings: {
                slidesToShow: 1,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 1023,
              settings: {
                slidesToShow: 3,
                draggable: false,
              }
            },
            {
              breakpoint: 1440,
              settings: {
                slidesToShow: 3,
                draggable: false,
              }
            }
          ]
        };

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };
        modalSelf.skipForNowShow = function () {
            modalSelf.ok();
            modalPopupInstance(skipForNowModal, model.skipForNowController, 'app-modal-window skip-now-modal-body');
        };
    };
  
    model.confirmationController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.confirmAndClose = function () {
            self.showBusinessDistributorForm = false;
            modalSelf.ok();
            angular.forEach(model.account.accountTypeList, function (value, key) {
                switch (value.accountType) {
                    case 'individual':
                        value.isActive = false;
                        break;
                    case 'business':
                        value.isActive = true;
                        break;
                }
            });
            model.account.activeAccount = $filter('filter')(model.account.accountTypeList, { isActive: true })[0];
            angular.extend(model.account, model.accountOriginal);
            model.account.taxID = model.businessDistributor.taxId1 + model.businessDistributor.taxId2;
            model.accountType = model.getActiveBusinessAccount(model.account);
            self.checkBusinessAccountType();
        };
    };

    self.addAssociateController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};
        modalSelf.credentials = {};

        modalSelf.emailValue = 0;
        modalSelf.mobilePhoneValue = 1;

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.executeInvite = function () {
            // To Be Done By AEM people
        };

        var inviteStatus = [{
            "id": 101,
            "status": false,
            "name": "notSent",
            "label": "Send Invite"
        },
            {
                "id": 102,
                "status": true,
                "name": "resend",
                "label": "Resend Invite"
            }, {
                "id": 103,
                "status": false,
                "name": "loggedIn"
            }];

        modalSelf.sendInvite = function (isValid) {
            if (isValid) {
                modalSelf.model.email = modalSelf.credentials.userEmailField;
                model.associatesList = model.associatesList ? model.associatesList : [];
                model.associatesList.push({
                    'id': (model.associatesList.length - 1),
                    'firstname': modalSelf.model.firstname,
                    'lastname': modalSelf.model.lastname,
                    'email': modalSelf.model.email ? modalSelf.model.email : null,
                    "mobilephone": modalSelf.model.mobilephone ? modalSelf.model.mobilephone : null,
                    "inviteStatus": inviteStatus
                });
                angular.extend(model.associatesListOriginal, model.associatesList);
                modalSelf.ok();
            }
        };
    };

    self.fileUploadController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.sizeLimit = 2; // 2MB is the size limit
        modalSelf.model = {
            "profileImage": model.userdata && model.userdata.profileImage ? model.userdata.profileImage : null,
            "accountType": model.userdata && model.userdata.accountType ? $filter('filter')(model.userdata.accountType, { status: true })[0] : null,
            "firstname": model.userdata.firstname,
            "lastname": model.userdata.lastname,
            "preferredname": model.userdata.preferredname ? model.userdata.preferredname : null
        };

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.uploadFiles = function (files) {
            var arrayOfFiles;
            if (files instanceof Array) {
                modalSelf.Files = files;
                arrayOfFiles = files;
            } else {
                arrayOfFiles = new Array();
                arrayOfFiles.push(files);
                modalSelf.Files = arrayOfFiles;
            }

            modalSelf.sizeLimitError = modalSelf.files && modalSelf.files.size && (modalSelf.files.size / 1048576) > modalSelf.sizeLimit ? true : false;

            if (!modalSelf.sizeLimitError) {
                var fileUploadPromise = accountSettingsBusinessService.uploadPhoto(modalSelf.files);
                fileUploadPromise.then(function (response) {
                    if (response.success) {
                        modalSelf.uploadRemoveErrorResponse = modalSelf.uploadRemoveSuccessResponse = null;
                        modalSelf.fileUploadSuccessResponse = response.success ? response.success : { "status": "ok", "image": "../../../../src/assets/images/content/avatar-profile.jpg" };
                        modalSelf.model.profileImage = response.success.image;
                    }
                }, function (response) {
                    if (response.error) {
                        modalSelf.uploadRemoveErrorResponse = null;
                        modalSelf.fileUploadErrorResponse = response.error ? response.error : { "message": "Upload failed" };
                    }
                });
            }
        };

        modalSelf.removeProfilePhoto = function () {
            var uploadRemovePromise = accountSettingsBusinessService.removePhoto();
            uploadRemovePromise.then(function (response) {
                modalSelf.fileUploadErrorResponse = modalSelf.fileUploadSuccessResponse = null;
                modalSelf.uploadRemoveSuccessResponse = response.success ? response.success : { "status": "ok", "placeholderImage": "../../../../src/assets/images/css/ic-profile-copy-2.png" };
                modalSelf.model.profileImage = null;
            }, function (response) {
                modalSelf.fileUploadErrorResponse = null;
                modalSelf.uploadRemoveErrorResponse = response.error ? response.error : { "message": "Remove failed" };
            });
        };

        modalSelf.saveFileUpload = function (isValid) {
            if (isValid) {
                model.userdata.firstname = modalSelf.model.firstname;
                model.userdata.lastname = modalSelf.model.lastname;
                model.userdata.preferredname = modalSelf.model.preferredname ? modalSelf.model.preferredname : null;
                model.userdata.profileImage = modalSelf.fileUploadSuccessResponse
                    ? modalSelf.fileUploadSuccessResponse.image
                    : modalSelf.uploadRemoveSuccessResponse
                        ? null
                        : model.userdata.profileImage ? model.userdata.profileImage : null;
                modalSelf.ok();
                model.saveAccEdit();
            }
        };

    };

    self.directDepositController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.directPolicyInfo = model.directPolicyInfo.directPolicyContent;
    };

    self.directDepositPopup = function () {
        modalPopupInstance(directDepositModalTemplate, self.directDepositController, 'app-modal-window direct-deposit-popup business-section-popup');
    };

    self.editProfile = function () {
        modalPopupInstance(fileUploadModalTemplate, self.fileUploadController, 'app-modal-window file-upload-popup business-section-popup');
    };

    model.agreeToTerms = function (isValid) {
        if (isValid) {
            modalPopupInstance(termsConditionsModal, model.termsConditionsController, 'app-modal-window terms-conditions-modal-body');
        }
    };

    self.addAssociate = function (associateObj) {
        model.selectedAssociate = associateObj;
        modalPopupInstance(addAssociateModalTemplate, self.addAssociateController, 'app-modal-window add-associate-popup business-section-popup');
    };

    self.removeAssociate = function (associateObj, domAssociateId) {
        angular.forEach(model.associatesList, function (value, key) {
            if (associateObj == value) {
                model.associatesList.splice(key, 1);
            }
        });
    };

    self.saveEditAssociates = function () {
        angular.copy(model.associatesList, model.associatesListOriginal);
    };

    self.cancelEditAssociate = function () {
        angular.copy(model.associatesListOriginal, model.associatesList);
        self.checkBusinessAccountType();
    };

    var languagePromise = languageListService.getLanguageList();
    languagePromise.then(function (response) {
        self.model.languageList = response;
        model.initContactPreference();
    });

    model.inlineEditForAddress = function () {
        model.showAddressForm = true;
        model.showCheckBox = true;
    }

    model.loadTab = function (currentTab) {

        model.currentTab = currentTab;
    }

    model.initContactPreference = function () {
        model.contactPreferenceSetup = {};
        model.contactPreferenceSetup.emailValue = 0;
        model.contactPreferenceSetup.textValue = 1;
        model.contactPreferenceSetup.mailValue = 2;

        var isLoop = true,
            selectedLanguageKey = null;
        if (self.model.languageList) {
            angular.forEach(self.model.languageList, function (value, key) {
                if (isLoop && value.code == "en") {
                    selectedLanguageKey = key;
                    isLoop = false;
                }
            });
        }
        self.selectedLanguageKey = selectedLanguageKey;
        model.contactPreference = {
            "radio": model.contactPreferenceSetup.emailValue,
            "language": model.languageList[selectedLanguageKey],
            "checkboxDnd": true
        };

        model.contactPreferenceCopy = {};
        angular.extend(model.contactPreferenceCopy, model.contactPreference);
    };

    self.saveCommunicationPreference = function (isValid) {
        if (isValid) {
            self.isCommunicationReadOnly = true;
            angular.extend(model.contactPreferenceCopy, model.contactPreference);
        }
    };

    model.cancelCommunicationPreference = function () {
        angular.extend(model.contactPreference, model.contactPreferenceCopy);
    };

    model.checkLength = function(flag) {
        if(flag == "r") {
            if(model.banking.routingNumber) {
               if(model.banking.routingNumber.length != 9) {
                    model.invalidRoutingLength = true;
                } else {
                    model.invalidRoutingLength = false;
                } 
            } 
        } 
        if(flag == "b") {
            if(model.banking.accountNumber) {
               if(model.banking.accountNumber.length != 34) {
                    model.invalidAccountLength = true;
                } else {
                    model.invalidAccountLength = false;
                } 
            } 
        }
    }
    

    self.addressVerificationModal = function () {
        console.log('modal call');
    };

    model.loadPersonalPage = function () {
        $location.path('/accountsettingspersonalpage');
    }

    model.showTermsConditionsModal = function(){
        self.showCustomModal = false;
        modalPopupInstance(termsConditionsModal, model.termsConditionsController, 'app-modal-window terms-conditions-modal-body');
    }

    self.goToUrl = function (urlPath) {
        if (urlPath) {
            $location.path(urlPath);
        }
    };
};

module.exports = accountSettingsBusinessController;