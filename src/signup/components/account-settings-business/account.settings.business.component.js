'use strict';
var template = require('./account.settings.business.component.jade');
var controller = require('./account.settings.business.component.controller');

var accountSettingsBusinessComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'vmbusiness',
};

module.exports = accountSettingsBusinessComponent;
