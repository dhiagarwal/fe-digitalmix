var angular = require('angular');
var uiRouter = require('angular-ui-router');
var slickCarousel = require('slick-carousel');
var angularSlick =  require('angular-slick-carousel');
var landingPageDistributorComponent = require('./landing.page.distributor.component');
var landingPageDistributorService = require('./landing.page.distributor.service');
var landingPageDistributorModule = angular.module('landingPageDistributor', [uiRouter,'slickCarousel'])
.component('landingPageDistributorComponent', landingPageDistributorComponent)
.service('landingPageDistributorService', landingPageDistributorService);

var styles = require('./landing.page.distributor.scss');

module.exports = landingPageDistributorModule;
