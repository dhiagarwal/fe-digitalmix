'use strict';

landingPageDistributorService.$inject = ['$http','$q'];

function landingPageDistributorService($http, $q) {
  this.$http = $http;
  this.$q = $q;
}

landingPageDistributorService.prototype.getFeaturedCategoriesData = function() {
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/signup/components/landing-page-distributor/landing.page.distributor.json').then(function(response) {
		deferred.resolve(response.data);
	});
	return deferred.promise;
};

landingPageDistributorService.prototype.getCarouselData = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/signup/components/landing-page-distributor/landing.page.distributor.carousel.json').then(function(response) {
		deferred.resolve(response.data);
	});
	return deferred.promise;
};

module.exports = landingPageDistributorService;
