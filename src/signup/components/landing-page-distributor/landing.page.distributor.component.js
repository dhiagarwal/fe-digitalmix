var template =require('./landing.page.distributor.jade');
var controller =require('./landing.page.distributor.controller');

var landingPageDistributorComponent =  {
    templateUrl : template,
    controller,
    controllerAs: 'ctrl'
};

module.exports = landingPageDistributorComponent;
