'use strict';
function friendsFamilySignupController($stateParams) {
    var self=this;
    this.credentials={};
    this.isEmail=true;
    this.isRequired=true;
    this.familySignupContinue=function() {
        var baseUrl=window.location.origin;
        window.location.href=baseUrl+'/#/friendsfamilysignupcreatepwd';
    }
};

module.exports = friendsFamilySignupController;