'use strict';
var template = require('./friends.family.signup.component.jade');
var controller = require('./friends.family.signup.component.controller');

var friendsFamilySignupComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'familysignup',
};

module.exports = friendsFamilySignupComponent;