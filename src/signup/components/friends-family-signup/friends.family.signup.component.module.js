'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var friendsFamilySignupComponent = require('./friends.family.signup.component');
var socialmedia =require('../social-media/social.signup');

var friendsFamilySignupComponentModule = angular.module('friendsFamilySignupComponentModule', [
					uiRouter,
					socialmedia.name
					])
                  .component('friendsfamilysignup', friendsFamilySignupComponent)
                  
import Styles from './friends.family.signup.component.scss';

module.exports = friendsFamilySignupComponentModule;