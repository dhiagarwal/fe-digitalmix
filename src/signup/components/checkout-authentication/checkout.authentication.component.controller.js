'use strict';
function checkoutAuthenticationController() {
    var self=this;
    self.newToNuskin=true;
    self.credentials={};
    self.isEmail=true;
    self.required=true;
    self.isSubmitted = false;
    self.credentials={};
    self.isEmailOrPhone=true;

    self.successHeading = "Lorem ipsum dolor";
    self.successSubheading = "Lorem ipsum dolor sit amet, ex eam dictas melius laboramus";
    self.showModal = false;
    self.modalURL = "";
    self.modalHeading = "";

    self.signUpShow = function() {
       $("#sign-up-pre-checkout").modal('show');
       self.isSubmitted = false;
       self.showModal = false;
    };
    self.continuePreCheckoutSignup = function(){
        self.isSubmitted= true;
    };
    self.openCustomPopup = function(linkType){
        if(linkType == 'termsOfUse'){
            self.modalHeading = "Terms of Use";
            self.modalURL = "src/assets/distearnings.html";
        } else {
            self.modalHeading = "Privacy Policy";
            self.modalURL = "src/assets/nuskin_refund_policy.html";
        }
        $("#sign-up-pre-checkout").modal('hide');
        self.showModal = true;
    };
}

module.exports=checkoutAuthenticationController;