'use strict';

var angular= require('angular');
var uiRouter=require('angular-ui-router');
var checkoutAuthenticationComponent=require('./checkout.authentication.component');

var checkoutAuthenticationComponentModule=angular.module('checkoutAuthenticationComponentModule',[
    uiRouter
])
.component('checkoutauthentication',checkoutAuthenticationComponent)

import Styles from './checkout.authentication.component.scss';

module.exports=checkoutAuthenticationComponentModule;
