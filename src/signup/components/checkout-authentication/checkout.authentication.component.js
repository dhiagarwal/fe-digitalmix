'use strict';
var template= require('./checkout.authentication.component.jade');
var controller= require('./checkout.authentication.component.controller');

var checkoutAuthenticationComponent={
    templateUrl:template,
    controller:controller,
    controllerAs: 'checkoutAuth',
    bindToController: true
};

module.exports=checkoutAuthenticationComponent;