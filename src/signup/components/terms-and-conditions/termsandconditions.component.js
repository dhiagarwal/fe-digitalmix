var template =require('./termsandconditions.jade');
var controller =require('./termsandconditions.controller');

var termsAndConditionsComponent =  {
    restrict: 'E',
    templateUrl : template,
    controller: controller,
    controllerAs: 'ctrl',
    bindToController: true
};

module.exports = termsAndConditionsComponent;
