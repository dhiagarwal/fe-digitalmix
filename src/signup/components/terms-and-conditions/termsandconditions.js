'use strict';

require('../../../stylesheets/base.scss');
require('./termsandconditions.scss');

var angular=require('angular');
require('slick-carousel');
require('angular-slick-carousel');

var termsAndConditionsComponent=require('./termsandconditions.component');
var termsandconditionService = require('./termsandconditions.service');

var sharedUtils = require('../../../__shared/shared');

var termsAndConditionsModule = angular.module('termsandconditions', ['slickCarousel', sharedUtils.name])
.component('termsandconditions', termsAndConditionsComponent)
    .service('termsandconditionService', termsandconditionService);


module.exports= termsAndConditionsModule;
