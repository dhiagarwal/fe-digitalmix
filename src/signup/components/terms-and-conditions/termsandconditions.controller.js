'use strict';

function termsAndConditionsController(termsandconditionService, $window) {
    this.result = {};
    // this.carouselDemoCtrl($scope);
    // function carouselDemoCtrl($scope) {
    //   $scope.uri = "../../../assets";
    //   $scope.images = [0, 1, 2, 3];
    // }

    termsandconditionService.getCarouselData().then(this.setCarouselData.bind(this));
    termsandconditionService.getDistributorAgreementData().then(
        this.setDistributorAgreementData.bind(this)
    );

    this.navigateBack = function(){
      $window.location.href ='/#/reviewsignupinfopage';
    }

}

termsAndConditionsController.prototype.setCarouselData = function(imagesData) {
    this.images = imagesData;
};

termsAndConditionsController.prototype.setDistributorAgreementData = function(distributorData) {
    this.distributorData = distributorData;
    this.distributorReview = this.distributorData.distributorReview.reviewContent;
    this.distributorAgreement = this.distributorData.distributorAgreement.distributorAgreementContent;
    this.internationalDistributor = this.distributorData.internationalDistributor.internationalDistributorContent;
    this.termsAndConditions = this.distributorData.termsAndConditions.termsAndConditionsContent;
};
module.exports = termsAndConditionsController;