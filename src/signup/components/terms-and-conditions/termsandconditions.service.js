'use strict';

function termsAndConditionsService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

termsAndConditionsService.prototype.getCarouselData = function(){
    
    var deferred = this.$q.defer();
    
    this.$http.get("../../../../src/signup/components/terms-and-conditions/termsandconditions.json")
        .success(deferred.resolve);
    
    return deferred.promise;
};

termsAndConditionsService.prototype.getDistributorAgreementData = function(){
    
    var deferred = this.$q.defer();
    
    this.$http.get("../../../../src/signup/components/terms-and-conditions/termsandconditionscontent.json")
        .success(deferred.resolve);
    
    return deferred.promise;
};


module.exports = ['$http', '$q', termsAndConditionsService];