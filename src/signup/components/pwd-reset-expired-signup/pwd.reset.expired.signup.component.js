'use strict';
var template = require('./pwd.reset.expired.signup.component.jade');
var controller = require('./pwd.reset.expired.signup.component.controller');

var pwdResetExpiredSignupComponent = {
	templateUrl: template,
	controller: controller,
	controllerAs: 'ctrl'
};

module.exports = pwdResetExpiredSignupComponent;