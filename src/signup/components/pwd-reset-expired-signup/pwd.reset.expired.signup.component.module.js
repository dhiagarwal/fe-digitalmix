'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pwdResetExpiredSignupComponent = require('./pwd.reset.expired.signup.component');

require('./pwd.reset.expired.signup.component.scss');

var pwdResetExpiredSignupComponentModule = angular.module('pwdResetExpiredSignupComponentModule', [uiRouter])
		.component('pwdresetexpiredsignupcomponent', pwdResetExpiredSignupComponent);

module.exports = pwdResetExpiredSignupComponentModule;