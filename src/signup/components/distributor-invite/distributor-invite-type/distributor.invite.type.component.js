'use strict';

var template = require('./distributor.invite.type.component.jade');
var controller = require('./distributor.invite.type.component.controller');

var inviteTypeComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = inviteTypeComponent;