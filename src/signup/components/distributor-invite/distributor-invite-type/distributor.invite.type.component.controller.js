'use strict';


inviteTypeController.$inject = ['$window'];

function inviteTypeController(window) {
    var self = this;

    var model = this.model = {};
    model.distributorInvite = [];
    model.distributorInvite.inviteCustomer = model.distributorInvite.inviteDistributor = {};
    self.model.isCustomerSelected = false;
    self.model.isDistributorSelected = false;
    self.model.showErrorSelection = false;

    this.errorMessage = "Please make a selection to continue";

    self.selectCustomer = function() {
        model.distributorInvite.inviteCustomer.isCustomerInvited = self.model.isCustomerSelected;
        self.model.isCustomerSelected = true;
        self.model.isDistributorSelected = false;
    };

    self.selectDistributor = function() {
        model.distributorInvite.inviteDistributor.isDistributorInvited = self.model.isDistributorSelected;
        self.model.isCustomerSelected = false;
        self.model.isDistributorSelected = true;
    };

    self.gotoInvitePage = function(pageName) {
        var distributorInviteType = {
            "isCustomerSelected": self.model.isCustomerSelected,
            "customerType": (self.model.isCustomerSelected) ? self.model.customerType : "",
            "isDistributorSelected": self.model.isDistributorSelected

        };
        model.distributorInvite.push(distributorInviteType);

        if (self.model.isCustomerSelected || self.model.isDistributorSelected) {
            self.model.showErrorSelection = false;
            window.location.href = pageName;
        } else {
            self.model.showErrorSelection = true;
        }
    };

    self.gotoPage = function(pageName) {
        window.location.href = pageName; 
    };

};

module.exports = inviteTypeController;