'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var inviteTypeComponent = require('./distributor.invite.type.component');


var inviteTypeComponentModule = angular.module('inviteTypeComponentModule', [uiRouter])
    .component('invitetypecomponent', inviteTypeComponent)

import Styles from '../distributor.invite.component.scss';

module.exports = inviteTypeComponentModule;