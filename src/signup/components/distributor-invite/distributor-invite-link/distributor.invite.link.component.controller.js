'use strict';


inviteLinkController.$inject = ['$window','$timeout'];

function inviteLinkController($window,$timeout) {
    var self = this;
    var model = this.model = {};
    self.copyValue = "COPY LINK";
    self.blurField = false;
    self.goToHome = function() {
        $window.location.href = "/#/";
    };

    self.gotoPage = function(pageName) {
        window.location.href = pageName;
    };

    self.copyClip = function(id) {
	    self.isCopyClicked = true;
	    var clipboard =  new Clipboard('.copy-btn');

		clipboard.on('success', function(e) {
		  e.clearSelection();
		});

	    self.copyValue = "COPIED";
	    self.blurField = true;
	    
	    $timeout(function(){
	    	self.copyValue = "COPY LINK";
	    	self.blurField = false;
	    },2000);
  	};

  	self.fireBlur = function(){
  		return false;
  	};
};

module.exports = inviteLinkController;