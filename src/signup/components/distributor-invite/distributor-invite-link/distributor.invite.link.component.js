'use strict';

var template = require('./distributor.invite.link.component.jade');
var controller = require('./distributor.invite.link.component.controller');

var inviteLinkComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = inviteLinkComponent;