'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var inviteLinkComponent = require('./distributor.invite.link.component');


var inviteLinkComponentModule = angular.module('inviteLinkComponentModule', [uiRouter])
    .component('invitelinkcomponent', inviteLinkComponent)


import Styles from '../distributor.invite.component.scss';

module.exports = inviteLinkComponentModule;