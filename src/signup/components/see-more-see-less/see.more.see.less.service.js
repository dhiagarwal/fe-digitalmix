'use strict';

seemoreseelessComponentService.$inject = ['$http',"$q"];

function seemoreseelessComponentService($http,$q) {
  this.$http = $http;
  var deferred = $q.defer();
  var distributorInfoPayload = {};
  $http.get('../../../../src/signup/components/seemoreseeless/see.more.see.less.component.json').then(function(response) {
      deferred.resolve(response.data);
  });

  this.getProductData = function() {
    return deferred.promise;
  }

};

module.exports = seemoreseelessComponentService;
