var angular =require('angular');
var uiRouter =require('angular-ui-router');
var seemoreseelessComponent =require('./see.more.see.less.component');
var seemoreseelessService =require('./see.more.see.less.service');

var seemoreseelessModule = angular.module('seemoreseeless', [uiRouter])
  .component('seeComponent', seemoreseelessComponent)
  .service('seemoreseelessService', seemoreseelessService);
var base =require('../../../stylesheets/base.scss');
var Styles =require('./see.more.see.less.scss');

module.exports= seemoreseelessModule;
