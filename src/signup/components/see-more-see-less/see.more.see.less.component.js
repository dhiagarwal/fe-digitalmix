'use strict';

var template =require('./see.more.see.less.jade');
var controller =require('./see.more.see.less.controller');

var seemoreseelessComponent =  {
    restrict: 'E',
    scope:{},
    templateUrl : template,
    controller: controller,
    controllerAs: 'seeCtrl',
    bindToController: true
};

module.exports = seemoreseelessComponent;
