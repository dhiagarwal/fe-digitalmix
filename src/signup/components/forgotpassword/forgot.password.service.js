'use strict';

forgotPasswordComponentService.$inject = ['$http',"$q"];

function forgotPasswordComponentService($http,$q) {
  this.$http = $http;
  var deferred = $q.defer();
  var distributorInfoPayload = {};
  $http.get('../../../../src/signup/components/forgotpassword/forgotpassword.component.json').then(function(response) {
      deferred.resolve(response.data);
  });

  this.getProductData = function() {
    return deferred.promise;
  }

};

module.exports = forgotPasswordComponentService;
