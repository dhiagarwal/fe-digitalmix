'use strict';
function forgotPasswordController(forgotPasswordService,$location) {
var self = this;
this.required= true;
this.thankYouHeaderSection = false;
this.clickedSearch=false;
this.credentials = {};
this.isEmail = true;
this.submit = false;
/*this.forgotUsernamePage = function(){
  $location.url('/forgotusername');
}*/
    this.redirectlogin = function () {
        $location.url("/login");
    };

this.changecontent = function (current, next) {
this.changeSection(current, next);
};
this.submitForm = function(form) {
  this.submit = true;
  if(form.customLoginForm.$valid){
    this.changecontent('forgotusernamefirstpage','resetinformationforemail');
  }
}
this.section = {
        forgotusernamefirstpage: true,
        resetinformationforemail: false,
    };
this.changeSection = function (current, next) {
    this.section[current] = false;
    this.section[next] = true;

    if (next === 'resetinformationforemail') {
      this.thankYouHeaderSection = true;
    } else {
      this.thankYouHeaderSection = false;
    }
};
this.maskEmailId = function(value){
    var temp = value.split('@');
    value = temp[0].substr(0,2) + temp[0].substr(2).replace(/./g,'*') + '@' + temp[1];
    return value;
};
var promise = forgotPasswordService.getProductData();
promise.then(function (data) {
    self.productData = data;
});
}
module.exports = forgotPasswordController;
