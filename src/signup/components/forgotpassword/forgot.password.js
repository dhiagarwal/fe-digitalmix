var angular =require('angular');
var uiRouter =require('angular-ui-router');
var forgotPasswordComponent =require('./forgot.password.component');
var forgotPasswordService =require('./forgot.password.service');

var forgotPasswordModule = angular.module('forgotPassword', [
  uiRouter
])
.component('forgotpasswordcomponent', forgotPasswordComponent)
.service('forgotPasswordService', forgotPasswordService);
var base =require('../../../stylesheets/base.scss');
var Styles =require('./forgot.password.scss');

module.exports= forgotPasswordModule;
