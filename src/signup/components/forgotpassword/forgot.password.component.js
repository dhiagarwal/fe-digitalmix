var template =require('./forgot.password.jade');
var controller =require('./forgot.password.controller');

var forgotPasswordComponent =  {
    restrict: 'E',
    templateUrl : template,
    controller: controller,
    controllerAs: 'forgotpasswordctrl',
    bindToController: true
};

module.exports = forgotPasswordComponent;
