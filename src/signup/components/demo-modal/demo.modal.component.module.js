'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var demoModalComponent = require('./demo.modal.component');
var emailPhone = require('../../../__shared/email-phone/email.phone.module');

var demoModalComponentModule = angular.module('demoModalComponentModule', [
					uiRouter
					])
                  .component('demoModal', demoModalComponent);

module.exports = demoModalComponentModule;
