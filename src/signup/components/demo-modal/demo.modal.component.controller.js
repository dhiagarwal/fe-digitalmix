'use strict';
var phoneVerificationController = require("../../../__shared/phone-verification/phone.verification.controller");
var phoneTemplate = require("../../../__shared/phone-verification/phone.verification.jade");

demoModalController.$inject=['$location', '$uibModal'];
function demoModalController($location, $uibModal) {
    var self = this;
    
    this.credentials = {
        userDetailsField: "",
        userEmailField: "",
        userPhoneField: "",        
        isSubmitted: false,
        emptyEmailError: true,
        isNumberVerified: false
    };
    this.isEmailOrPhone = false;
    this.isEmail = false;
    this.isPhone = true;
    this.required = true;

    this.saveUserCredentials = function() {

        var options = {
            templateUrl: phoneTemplate,
            controller: phoneVerificationController,
            controllerAs: "ctrl",
            size: 'sm',
            windowClass: 'phone-verification',
            resolve : {
                credentials: function() {
                    return self.credentials;
                }
            }
        };
        var modalInstance = $uibModal.open(options);
        modalInstance.result.then(function(credentials) {
            console.log(credentials);
            self.credentials = credentials;

        });
        
    };


    this.cancel = function() {
        this.credentials = {
            userDetailsField: "",
            userPhoneField: "",
            userEmailField: "",
            isSubmitted: false,
            emptyEmailError: true
        };
    };

};

module.exports = demoModalController;