'use strict';
var demoModalTemplate = require('./demo.modal.component.jade');
var demoModalController = require('./demo.modal.component.controller');

var demoModalComponent = {
  bindings:{},
  templateUrl: demoModalTemplate,
  controller: demoModalController,
  controllerAs: 'ctrl'
};

module.exports = demoModalComponent;
