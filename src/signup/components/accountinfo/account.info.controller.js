'use strict';
function accountinfoController($location) {
  var self = this;
  self.lastName='';
  self.firstName='';
  self.credentials= {};
  self.passwordinfo={};
  self.isChecked = false;
  this.isRequired=true;
  this.navigateTo = function(){
    $location.url('/reviewInfoDistributor');
  }
}
module.exports = accountinfoController;
