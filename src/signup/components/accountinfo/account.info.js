var angular =require('angular');
var uiRouter =require('angular-ui-router');
var accountinfoComponent =require('./account.info.component');

var accountinfoModule = angular.module('accountinfo', [
  uiRouter
])
.component('accountinfocomponent', accountinfoComponent)
var base =require('../../../stylesheets/base.scss');
var Styles =require('./account.info.scss');

module.exports= accountinfoModule;
