var template =require('./account.info.jade');
var controller =require('./account.info.controller');

var accountinfoComponent =  {
    templateUrl : template,
    controller: controller,
    controllerAs: 'accountInfoCtrl',
};

module.exports = accountinfoComponent;
