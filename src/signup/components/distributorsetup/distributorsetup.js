var angular =require('angular');
var uiRouter =require('angular-ui-router');
var distributorsetupComponent =require('./distributorsetup.component');
var distributorsetupService =require('./distributorsetup.service');

var distributorsetupModule = angular.module('distributorsetup', [
  uiRouter
])
.config(($stateProvider) => {
  $stateProvider
    .state('distributorsetup', {
      url: '/distributorsetup',
      template: '<distributorsetup></distributorsetup>'
    });
})
.component('distributorsetup', distributorsetupComponent)
.service('distributorsetupService', distributorsetupService);
var base =require('../../../stylesheets/base.scss');
var Styles =require('./distributorsetup.scss');

module.exports= distributorsetupModule;
