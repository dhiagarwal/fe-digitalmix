var template = require('./distributorsetup.jade');
var controller = require('./distributorsetup.controller');

var distributorsetupComponent =  {
  //return {
    restrict: 'E',
    scope: {},
    templateUrl : template,
    controller,
    controllerAs: 'vm',
    bindToController: true
  //};
};

module.exports = distributorsetupComponent;
