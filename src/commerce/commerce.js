var angular = require('angular');
var PdpSocialViewer = require('./pages/pdp-social/pdp.social.viewer');
var PdpUpsellViewer = require('./pages/pdp-upsell/pdp.upsell.viewer');
var PdpUpsellKitViewer = require('./pages/pdp-upsellkit/pdp.upsellkit.viewer');
var PdpCrosssellViewer = require('./pages/pdp-crosssell/pdp.crosssell.viewer');
var PdpProductBasicsViewer = require('./pages/pdp-productbasics/pdp.productbasics.viewer');
var PdpShoppingCartViewer = require('./pages/pdp-shoppingcart/pdp.shoppingcart.viewer');
var PdpTestimonialViewer = require('./pages/pdp-testimonial/pdp.testimonial.viewer');
var PdpShoppingBagViewer = require('./pages/shopping-cart/shopping.cart.page');
var CheckoutAsGuestShippingAddressViewer = require('./pages/checkout-guest/shipping-address/shipping.address.viewer');
var CheckoutAsGuestDeliveryOptionsViewer = require('./pages/checkout-guest/delivery-options/delivery.options.viewer');
var CheckoutAsGuestPaymentMethodViewer = require('./pages/checkout-guest/payment-method/payment.method.viewer');
var CheckoutAsGuestOrderConfirmationViewer = require('./pages/checkout-guest/order-confirmation/order.confirmation.viewer');
var CheckoutAsGuestOrderReviewViewer = require('./pages/checkout-guest/order-review/order.review.viewer');
var CheckoutAsDistributorShippingAddressViewer = require('./pages/checkout-distributor/shipping-address/shipping.address.viewer');
var CheckoutAsDistributorDeliveryOptionsViewer = require('./pages/checkout-distributor/delivery-options/delivery.options.viewer');
var CheckoutAsDistributorPaymentMethod = require('./pages/checkout-distributor/payment-method/payment.method.viewer');
var CheckoutAsDistributorOrderReviewViewer = require('./pages/checkout-distributor/order-review/order.review.viewer');
var ProductSearchViewer = require('./pages/product-search/product.search.viewer');
var PdpProductInfoIngredientsViewer = require('./pages/pdp-product-info/pdp.productinfo.viewer');
var PdpCategoryDirectoryViewer = require('./pages/pdp-category-directory/pdp.category.directory.viewer');
var PdpCategoryStandardViewer = require('./pages/pdp-category-standard/pdp.category.standard.viewer');
var RecurringOrderDetailsViewer = require ('./pages/recurring-order-details/recurring.order.details.viewer.js')
var OrderDetailViewer = require('./pages/order-management/order.detail.viewer');
var MyStoreViewer = require('./pages/my-store/my.store.viewer');
var MyStoreMessagesViewer = require('./pages/my-store-header/my.store.header.viewer');
var TenProducts = require ('./pages/ten-products/ten.products.viewer');
var MyPromotions = require ('./pages/promotions/promotions.viewer');
var DistributorAuthenticated = require ('./pages/distributor-authenticated-suggestions/distributor.authenticated.viewer');
var RecentlyVieweditems = require ('./pages/recently-vieweditems/recently.vieweditems');
var FeaturedCategories = require ('./pages/featured-categories/featured.categories.viewer');
var WelcomeMessage = require ('./pages/welcome-message/welcome.message.viewer.js');
var featuredVideoViewer = require('./pages/featured-video/featured.video.viewer');
var MyStoreFront = require ('./pages/my-storefront/my.storefront.viewer');
var pitchLandingPageViewer = require ('./pages/pitch-landing-page/pitch.landing.page.viewer');
var commerceModule = angular.module('app.commerce', [
  PdpSocialViewer.name,
  PdpUpsellViewer.name,
  PdpUpsellKitViewer.name,
  PdpProductBasicsViewer.name,
  PdpCrosssellViewer.name,
  PdpShoppingCartViewer.name,
  PdpTestimonialViewer.name,
  PdpShoppingBagViewer.name,
  CheckoutAsGuestShippingAddressViewer.name,
  CheckoutAsGuestDeliveryOptionsViewer.name,
  CheckoutAsGuestPaymentMethodViewer.name,
  CheckoutAsGuestOrderConfirmationViewer.name,
  CheckoutAsGuestOrderReviewViewer.name,
  CheckoutAsDistributorShippingAddressViewer.name,
  CheckoutAsDistributorDeliveryOptionsViewer.name,
  CheckoutAsDistributorPaymentMethod.name,
  CheckoutAsDistributorOrderReviewViewer.name,
  ProductSearchViewer.name,
  PdpProductInfoIngredientsViewer.name,
  PdpCategoryDirectoryViewer.name,
  PdpCategoryStandardViewer.name,
  RecurringOrderDetailsViewer.name,
  OrderDetailViewer.name,
  MyStoreViewer.name,
  MyStoreMessagesViewer.name,
  TenProducts.name,
  MyPromotions.name,
  DistributorAuthenticated.name,
  RecentlyVieweditems.name,
  WelcomeMessage.name,
  FeaturedCategories.name,
  featuredVideoViewer.name,
  MyStoreFront.name,
  pitchLandingPageViewer.name
]);

module.exports = commerceModule;
