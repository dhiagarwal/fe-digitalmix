'use strict';
var template = require('./pdp.category.standard.jade');
var controller = require('./pdp.category.standard.controller');

var pdpCategoryStandardComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = pdpCategoryStandardComponent;
