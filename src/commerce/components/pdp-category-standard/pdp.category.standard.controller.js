'use strict';

function pdpCategoryStandardController(pdpCategoryStandardService) {
  var self = this;

  this.maxRating = 5;
  this.ratingCount = 3;
  this.ratingTotal = 13;
  this.productPrice = "$25";
  this.psv = "340";

  this.pageHeadline = "You Glow Girl";
  this.pageShortDescription = "Don't let dull skin get you down. Put your best face forward with our cleansers, moisturizers and scrubs to help your skin be the brightest.";
  this.pageCategory = "Personal Care";
  this.pageSubCategory = "Face Care";
  this.bannerUrl = '../../../../src/assets/images/content/category-standard-banner.png';
  this.categoryLeftUrl = '../../../../src/assets/images/content/content-banner-1.png';
  this.categoryRightUrl = '../../../../src/assets/images/content/content-banner-2.png';
  this.categoryHighlightUrl = '../../../../src/assets/images/content/true-face-banner.png';
  this.categoryHighlightDesc = 'Description of this feature product a little description about why the product is amazing, what it does and why it works.';

  var getData = pdpCategoryStandardService.getCategoryStandardData();
  getData.then(function(data){
      self.categoryData = data;
  });
};

module.exports = pdpCategoryStandardController;
