var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-ui-bootstrap');
var pdpCategoryStandardComponent = require('./pdp.category.standard.component');
var pdpCategoryStandardService = require('./pdp.category.standard.service');

var pdpCategoryStandardModule = angular.module('pdpCategoryStandardModule', [uiRouter, 'ui.bootstrap'])
.component('pdpcategorystandard', pdpCategoryStandardComponent)
.service('pdpCategoryStandardService', pdpCategoryStandardService);

import Styles from './pdp.category.standard.scss';

module.exports = pdpCategoryStandardModule;
