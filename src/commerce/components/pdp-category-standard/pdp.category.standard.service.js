'use strict';

pdpCategoryStandardService.$inject = ['$http','$q'];

function pdpCategoryStandardService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-category-standard/pdp.category.standard.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getCategoryStandardData = function() {
    return deferred.promise;
  }

};

module.exports = pdpCategoryStandardService;
