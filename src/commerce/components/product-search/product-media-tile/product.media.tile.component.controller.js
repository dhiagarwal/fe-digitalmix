'use strict';

function productMediaTileComponentController() {
	var self = this;
	self.isLogoVisible = true;
	self.isHeartVisible = true;
	self.isDeleted = false;
	self.isFavorited = false;
	self.videoPlaying = false;
	
	this.toggleFavorite = function () {
        self.isFavorited = !self.isFavorited;
        self.listItem.isFavorite = !self.listItem.isFavorite;
	};

	this.toggleDel = function(){
		self.isDeleted = !self.isDeleted;
	};
	self.video = function(productId){
        var videoElements = angular.element.find('#video-'+productId);
        videoElements[0].currentTime = 0;
		self.videoPlaying = false;
	};
    self.togglePlay = function(productId) {
        var video = angular.element.find('#video-'+productId);
        self.videoDuration = video.duration;
        if (video[0].paused) {
            video[0].play();
			if (video[0].mozRequestFullScreen) {
				video[0].mozRequestFullScreen();
			} else if (video[0].webkitRequestFullScreen) {
				video[0].webkitRequestFullScreen();
			} else if (video[0].requestFullScreen){
				video[0].requestFullScreen();
			} else if(video[0].msRequestFullscreen){
				video[0].msRequestFullscreen();
			}

            video[0].controls = true;
            this.videoPlaying = true;
        } else {
            $timeout(function() { video[0].pause(); }, 10)
            video[0].controls = false;
            this.videoPlaying = false;
        }
    };
};

module.exports = productMediaTileComponentController;
