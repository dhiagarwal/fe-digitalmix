'use strict';
var template = require('./product.media.tile.component.jade');
var controller = require('./product.media.tile.component.controller');

var productMediaTileComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      listItem: "=",
      extraClass: "@",
      ifDeleteVisible: "=",
      ifFavUndoVisible: "=",
      //Video and other tab specific bindings
      ifDurationVisible: '=',
      ifShareIcnVisible: '=',
      ifDownloadIcnVisible: '=',
      ifTypeVisible: '=',
      currentActiveTab: '=',
      isListView: '='
  }
};

module.exports = productMediaTileComponent;
