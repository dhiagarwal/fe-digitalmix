'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var productMediaTileComponent = require('./product.media.tile.component');


var productMediaTileComponentModule = angular.module('productMediaTileComponentModule', [uiRouter])
                  .component('productmediatilecomponent',productMediaTileComponent)
                 
                  
import Styles from './product.media.tile.component.scss';                      

module.exports = productMediaTileComponentModule;
