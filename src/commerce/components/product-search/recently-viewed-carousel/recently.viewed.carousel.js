'use strict';
var template = require('./recently.viewed.carousel.jade');
var controller = require('./recently.viewed.carousel.controller');


var recentlyViewedCarouselComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl',
    bindToController: true
};

module.exports = recentlyViewedCarouselComponent;
