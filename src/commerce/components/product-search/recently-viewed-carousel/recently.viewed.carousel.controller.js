'use strict';

function recentlyViewedCarouselController(recentlyViewedCarouselService) {

    this.viewData = {};
    var self = this;

    var promise = recentlyViewedCarouselService.getData();
    promise.then(function (data) {
        self.viewData = data;
        self.rating = data.rating;
    });
    
    self.slickSecondaryConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        focusonselect: true,
        speed: 500,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    draggable: false,
                }
            },
            {
                breakpoint: 1432,
                settings: {
                    slidesToShow: 5,
                    draggable: false,
                }
            }
        ]
    };



    this.toggleFavorite = function (data) {
        data.isFavorite = !data.isFavorite;
    };
    

};

module.exports = recentlyViewedCarouselController;