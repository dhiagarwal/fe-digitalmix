'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var slickCarousel = require('slick-carousel');
var angularSlick =  require('angular-slick-carousel');
var recentlyViewedCarouselComponent = require('./recently.viewed.carousel');
var recentlyViewedCarouselService = require('./recently.viewed.carousel.service');


var recentlyViewedCarouselModule = angular.module('recentlyViewedCarouselModule', [uiRouter,'slickCarousel'])
                  .component('recentlyviewedcarouselcomponent', recentlyViewedCarouselComponent)
                  .service('recentlyViewedCarouselService', recentlyViewedCarouselService)

import Styles from './recently.viewed.carousel.scss';

module.exports = recentlyViewedCarouselModule;
