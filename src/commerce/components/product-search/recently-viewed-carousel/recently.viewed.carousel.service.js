'use strict';

recentlyViewedCarouselService.$inject = ['$http','$q'];

function recentlyViewedCarouselService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};


 recentlyViewedCarouselService.prototype.getData = function(){
 		var deferred = this.$q.defer();

 		this.$http.get('../../../../src/commerce/components/product-search/recently-viewed-carousel/recently.viewed.carousel.json')
 			.success(deferred.resolve);

 			return deferred.promise;
 }

module.exports = recentlyViewedCarouselService;