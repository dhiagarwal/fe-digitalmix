'use strict';
var template = require('./product.search.banner.component.jade');
var controller = require('./product.search.banner.component.controller');


var productSearchBannerComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      categoryName: "@",
      uploadIcon: "=",
      bannerUrl: "=",
      content: "@"
  }
};

module.exports = productSearchBannerComponent;
