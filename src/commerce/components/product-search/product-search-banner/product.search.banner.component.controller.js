'use strict';

function productSearchBannerComponentController($location,$rootScope) {

	var self = this;
	this.zeroResult = false;
	this.closeMatchFound = false;
	this.resultNum = 100;

	var urlParam = $location.search();


	if (urlParam.search || urlParam.minPSV || urlParam.maxPSV) {

		this.minPSV = urlParam.minPSV;
		this.maxPSV = urlParam.maxPSV;
		this.searchInput = urlParam.search;

		if (urlParam.search == 'abcd') {
			self.zeroResult = true;
			self.resultNum = 0;
		}
		else if(urlParam.search == "drk"){
			self.closeMatchFound = true;
			this.searchInput = "Age Loc";
		}
		else
		{
			self.zeroResult = false;
			self.closeMatchFound = false;
		}
	}
	else{
		this.minPSV = '';
		this.maxPSV = '';
		this.searchInput = '';
	}


    this.searchClicked = function(){
    	if(screen.width < 1024){
    		$rootScope.$broadcast('searchHandler', {type:'sideMenu'});
    	}
        else{
        	$rootScope.$broadcast('searchHandler', {type:'headerSearch'});
        }
    }
};


module.exports = productSearchBannerComponentController;
