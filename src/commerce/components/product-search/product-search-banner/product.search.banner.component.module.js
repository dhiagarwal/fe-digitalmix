'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var productSearchBannerComponent = require('./product.search.banner.component');


var productSearchBannerComponentModule = angular.module('productSearchBannerComponentModule', [uiRouter])
                  .component('productsearchbannercomponent',productSearchBannerComponent)
                  
                  
                  
import Styles from './product.search.banner.component.scss';                      

module.exports = productSearchBannerComponentModule;
