'use strict';

function productTileComponentController($location) {
	var self = this;

	self.isFavorited = false;
	self.starNumber = 5;
	this.rating = 3;

	this.starOff = "../../../../src/assets/images/css/star-off.svg";
	this.starOn = "../../../../src/assets/images/css/star-on.svg";


	this.toggleFavorite = function () {
        self.isFavorited = !self.isFavorited;
        self.listItem.isFavorite = !self.listItem.isFavorite;
	};
    
    this.redirect =function(product){
        $location.url('/pdpProductBasics');
    }

	this.getNumber = function() {
	    var a = new Array();
	    var i = 0;
	    while (i < this.starNumber) {
	        a[i] = i;
	        i++;
	    }
	    return a;
	};

	this.starFunction = function(index) {
	    if (index <= this.rating) {
	        return this.starOn;
	    } else {
	        return this.starOff;
	    }
	};


  this.stockStatus = function(value){
    var temp = self.listItem.stockStatus;
    if(temp == value){
      return true;
    }
    else{
      return false;
    }
  };

};

module.exports = productTileComponentController;
