'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var productTileComponent = require('./product.tile.component');
var pdpShoppingCartComponentModule = require('../../pdp-shoppingcart/pdp.shoppingcart.component.module');


var productTileComponentModule = angular.module('productTileComponentModule', [uiRouter, pdpShoppingCartComponentModule.name])
                  .component('producttilecomponent',productTileComponent)
                 
                  
import Styles from './product.tile.component.scss';                      

module.exports = productTileComponentModule;
// 