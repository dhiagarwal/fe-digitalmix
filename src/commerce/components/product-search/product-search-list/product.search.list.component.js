'use strict';
var template = require('./product.search.list.component.jade');
var controller = require('./product.search.list.component.controller');


var productSearchListComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
};

module.exports = productSearchListComponent;
