'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

// var angularSlider = require('angularjs-slider'); //required for slider
require('angular-ui-bootstrap');

var productSearchListComponent = require('./product.search.list.component');
var productSearchComponentService = require('./product.search.list.component.service')
var productMediaCategoryComponentModule = require('../product-media-category/product.media.category.component.module');
//var mediaCategoryComponentModule = require('../../../../../src/digital-toolkit/components/media-center/media-category-list/media.category.component.module');
// var productSearchListComponentService = require('./product.search.list.component.service');

var productSearchListComponentModule = angular.module('productSearchListComponentModule', 
				  [uiRouter,'ui.bootstrap',productMediaCategoryComponentModule.name])
                  .component('productsearchlistcomponent',productSearchListComponent)
                  .service('productSearchComponentService', productSearchComponentService)



import Styles from './product.search.list.component.scss';

module.exports = productSearchListComponentModule;
