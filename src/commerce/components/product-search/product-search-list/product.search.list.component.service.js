'use strict';

productSearchComponentService.$inject = ['$http','$q'];

function productSearchComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};


 productSearchComponentService.prototype.getProductData = function(){
 		var deferred = this.$q.defer();

 		this.$http.get('../../../../src/commerce/components/product-search/product.search.json')
 			.success(deferred.resolve);

 			return deferred.promise;
 }

 productSearchComponentService.prototype.getFilterData = function(){
 		var deferred = this.$q.defer();

 		this.$http.get('../../../../src/commerce/components/product-search/product-search-list/product.search.filter.json')
 			.success(deferred.resolve);

 			return deferred.promise;
 }

module.exports = productSearchComponentService;