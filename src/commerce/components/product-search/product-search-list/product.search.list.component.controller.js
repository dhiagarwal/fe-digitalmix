function productSearchListComponentController(productSearchComponentService, $timeout, $location) {

    this.noData = false;
    this.productData = {};
    this.showFilter = false;
    this.isMobileMode = false;
    this.viewType = 1;
    var self = this;
    var searchString = $location.search();
    if (searchString.search == 'abcd') {
        self.noData = true;
    } else {
        self.noData = false;
    }
    // self.limit = '';

    // self.minRangeSlider = {
    //     options: {
    //         floor: 0,
    //         ceil: 1000,
    //         hideLimitLabels: true,
    //         step: 1
    //     }
    // };

    //filter range json
    // {
    //  "filter_category" : "range",
    //  "filter_type": "LOREM RANGE",
    //  "minValue": "0",
    //  "maxValue": "1000",
    //  "gap_range":"50",
    //  "filter_options":[]
    // }



    this.setLimit = function(filter) {
        filter.limit = null;
        if (filter.filter_options && filter.filter_options.length > 8) {
            filter.limit = 5;
        } else {
            filter.limit = filter.filter_options.length;
        }

    }

    var promise = productSearchComponentService.getProductData();
    promise.then(function(data) {
        self.productData = data;

    });

    var promise = productSearchComponentService.getFilterData();
    promise.then(function(data) {
        self.filterData = data;
    });




    var settings = {
        selectors: {
            tile: '.producttile',
            list: '#list-product',
            filter: '#filter-product'
        },
        class: {
            filterOpen: {
                    tile: 'col-sm-6 col-md-6 col-lg-4 col-xl-3',
                    list: 'col-sm-12 col-md-8 col-lg-8 col-xl-9'
                },
                filterClose: {
                    tile: 'col-sm-6 col-md-4 col-lg-3 col-xl-2',
                    list: 'col-sm-12 col-md-12 col-lg-12 col-xl-12'
                }
        }
    }

    //callFilter function will open the filter panel and adjust the bootstrap classes accordingly

    this.callFilter = function() {
        if (screen.width < 768) {

            self.isMobileMode = !self.isMobileMode;
        }


        var tileGroup = document.querySelectorAll(settings.selectors.tile);
        if (self.showFilter == false) {

            angular.element(settings.selectors.filter).addClass('is-open');

            for (var i = 0; i < tileGroup.length; i++) {
                tileGroup[i].className = tileGroup[i].className.replace(settings.class.filterClose.tile, settings.class.filterOpen.tile);
            }
            document.querySelector(settings.selectors.list).className = document.querySelector(settings.selectors.list).className.replace(settings.class.filterClose.list, settings.class.filterOpen.list)

        } else {
            angular.element(settings.selectors.filter).removeClass('is-open');

            for (var i = 0; i < tileGroup.length; i++) {
                tileGroup[i].className = tileGroup[i].className.replace(settings.class.filterOpen.tile, settings.class.filterClose.tile);
            }
            document.querySelector(settings.selectors.list).className = document.querySelector(settings.selectors.list).className.replace(settings.class.filterOpen.list, settings.class.filterClose.list)

        }

        self.showFilter = !self.showFilter;

    }

    //resetFilter function will reset the filter values
    this.resetFilter = function() {
        for (var i = 0; i < self.filterData.data.length; i++) {
            var currentFilter = self.filterData.data[i];
            if (currentFilter.filter_type == "SORT BY") {
                currentFilter.selectedFilterType = 'Price';
                currentFilter.isFilterSelected = false;
                currentFilter.showFilterOptions = false;
                currentFilter.isDropdownOpen = false;
            }
            // else if(currentFilter.filter_category === "range"){
            //   currentFilter.selectedFilterType = currentFilter.minValue + " LOREM to " + currentFilter.maxValue + " LOREM";
            //   currentFilter.isFilterSelected= false;
            //   currentFilter.minValue = self.minRangeSlider.options.floor;
            //   currentFilter.maxValue = self.minRangeSlider.options.ceil;
            //   currentFilter.showFilterOptions = false;
            //   currentFilter.isDropdownOpen = false;
            // }
            else {
                for (var j = 0; j < self.filterData.data[i].filter_options.length; j++) {
                    self.filterData.data[i].filter_options[j].currentSelected = false;
                }

                currentFilter.selectedFilterType = '';

                if (currentFilter.showFilterOptions) {
                    currentFilter.showFilterOptions = !currentFilter.showFilterOptions;
                    currentFilter.isDropdownOpen = !currentFilter.isDropdownOpen;
                }

            }
        }
        for (var i = 0; i < self.filterData.toggleData.length; i++) {
            var currentFilter = self.filterData.toggleData[i];
            currentFilter.isSelected = false;
        }
    }

    this.selectOption = function(currentOption, filterName) {
        for (var i = 0; i < self.filterData.data.length; i++) {

            var options = self.filterData.data[i].filter_options;

            for (var j = 0; j < options.length; j++) {

                if (self.filterData.data[i].filter_options[j].currentSelected && (self.filterData.data[i].filter_type == filterName)) {

                    self.filterData.data[i].filter_options[j].currentSelected = false;

                }

                if (options[j].option_value === currentOption) {
                    self.filterData.data[i].isFilterSelected = true;
                    self.filterData.data[i].selectedFilterType = currentOption;
                    self.filterData.data[i].filter_options[j].currentSelected = true;
                }
            }
        }

    }

    this.change = function(state, filter, $event) {
        // Prevent bubbling to showItem.
        // On recent browsers, only $event.stopPropagation() is needed
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
        $event.cancelBubble = true;
        $event.returnValue = false;
        if (state) {
            filter.showFilterOptions = !filter.showFilterOptions;
            filter.isDropdownOpen = !filter.isDropdownOpen;
        }

        // if(filter.filter_category == 'range'){
        //   filter.selectedFilterType = filter.minValue + " LOREM to " + filter.maxValue + " LOREM";
        //   self.refreshSlider();
        // }
    }

    // self.refreshSlider = function () {
    //      $timeout(function () {
    //          $scope.$broadcast('rzSliderForceRender');
    //      });
    //  };

    //hideCurrentFilter function will open and close the option panel
    this.hideCurrentFilter = function(filterName) {
        for (var i = 0; i < self.filterData.data.length; i++) {

            var currentFilter = self.filterData.data[i].filter_type;

            if (currentFilter == filterName) {
                self.filterData.data[i].isFilterSelected = !self.filterData.data[i].isFilterSelected;
                self.filterData.data[i].isOptionSelected = true;
            }

        }

    }

    this.setViewType = function(_type) {
        if (_type == 1) {
            self.viewType = 1;
        } else {
            self.viewType = 0;
        }
    }



};


module.exports = productSearchListComponentController;