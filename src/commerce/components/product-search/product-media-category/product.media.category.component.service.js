'use strict';

productMediaCategoryComponentService.$inject = ['$http','$q'];

function productMediaCategoryComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

productMediaCategoryComponentService.prototype.getTileData = function(Url){
	var deferred = this.$q.defer();
	
	this.$http.get(Url)
		.success(deferred.resolve);

		return deferred.promise;
};
 productMediaCategoryComponentService.prototype.getFilterData = function(){
 		var deferred = this.$q.defer();

 		this.$http.get('../../../../src/digital-toolkit/components/media-center/media-category-list/media.category.filter.component.json')
 			.success(deferred.resolve);

 			return deferred.promise;
 }

module.exports = productMediaCategoryComponentService;