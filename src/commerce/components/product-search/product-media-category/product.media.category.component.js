'use strict';
var template = require('./product.media.category.component.jade');
var controller = require('./product.media.category.component.controller');

var productMediaCatagoryComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      sourceUrl:'=',
      ifDeleteVisible:'=',
      ifFavUndoVisible:'=',
      //Tab Content Specific bindings
      ifDurationVisible: '=',
      ifShareIcnVisible: '=',
      ifDownloadIcnVisible: '=',
      ifTypeVisible: '=',
      currentActiveTab: '=',
      isListView: '='
  }
};

module.exports = productMediaCatagoryComponent;
