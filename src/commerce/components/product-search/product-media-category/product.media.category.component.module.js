'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('angular-animate');

var productMediaCategoryComponent = require('./product.media.category.component');
var productMediaCategoryComponentService = require('./product.media.category.component.service');
var productMediaTileModule = require('../product-media-tile/product.media.tile.component.module');


var productMediaCategoryComponentModule = angular.module('productMediaCategoryComponentModule', [uiRouter,'ngAnimate', productMediaTileModule.name])
                  .component('productmediacategorycomponent',productMediaCategoryComponent)
                  .service('productMediaCategoryComponentService', productMediaCategoryComponentService)
                  
import Styles from './product.media.category.component.scss';                      

module.exports = productMediaCategoryComponentModule;
