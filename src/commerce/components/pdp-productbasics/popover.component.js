'use strict';
var template = require('./popover.component.jade');
var signUpModalTemplate = require('./signup.modal.jade');
var popoverComponent = {
    templateUrl: template,
    bindings: {
      ratingValue : '=',
      isQuestionVisible : '='
    },
    controller: ['$uibModal', '$scope', '$http', PopoverController],
    controllerAs: 'ctrl',
};

function PopoverController($uibModal, $scope, $http){
    this.submitButtonDisable = true;
    this.$uibModal = $uibModal;
    this.isOpen = false;
    this.isSigned = false;
    this.section = {
        sec1: true,
        sec2: true,
        sec3: false,
        sec4: false,
        sec5: false,
        sec6: false
    };

    var ele = angular.element(document.querySelector('.pdp-popover'));
    ele.on('click', function (evt) {
        evt.stopPropagation();
    });

    this.close= function(){
        this.isOpen = false;
        this.getOriginalState();
    }

    this.starOff1 = "../../../src/assets/images/css/original-star.png";
    this.starOn1 = "../../../src/assets/images/css/hover-star.png";
    this.starNumber1 = 5;
    this.rating1 = 0;
    this.ratingHolder1 = this.rating1;
    this.ratingFlag1 = true;

    this.getNumber = function() {
        var a = new Array();
        var i = 0;
        while (i < this.starNumber1) {
            a[i] = i+1;
            i++;
        }
        return a;
    }
    this.starFunction = function(index) {
        if (index <= this.rating1) {
            return this.starOn1;
        } else {
            return this.starOff1;
        }
    };
    this.rateProduct = function(productRating) {
        var getRatingData = {};
        this.ratingFlag1 = true;
        getRatingData = $scope.setRatingData;
        if (this.ratingFlag1) {
            var getRating = "rating-"+productRating;
            var getRatingVal = getRatingData[getRating];
            this.rating1 = productRating;
            this.ratingHolder1 = productRating;
            this.ratingVal = getRatingVal;
        }
        this.submitButtonDisable = false;
    };
    this.starMouseOver = function(index) {
        if (this.ratingFlag1) {
            this.rating1 = index;
        }
    };
    this.starMouseLeave = function(index) {
        if (this.ratingFlag1) {
            this.rating1 = this.ratingHolder1;
        }
    };

    $http.get('../../../../src/commerce/components/pdp-productbasics/pdp.productbasics.component.ratings.json').then(function(res){
        $scope.setRatingData = res.data;
    });

}

PopoverController.prototype.toggle = function (open) {
  if(open && !this.isQuestionVisible){
    this.getMediaState();
  }
if(!open){
  if(!this.isQuestionVisible){
    this.getMediaState();
  }else{
        this.getOriginalState();
    }
}
};

PopoverController.prototype.getMediaState = function(){

  angular.forEach(this.section, function (val, key) {
    if(key == 'sec5'){this.section[key] = true;}
    else{this.section[key] = false;}
  }, this);
}

PopoverController.prototype.getOriginalState = function(){

  angular.forEach(this.section, function (val, key) {
    if(key == 'sec1' || key == 'sec2'){this.section[key] = true;}
    else{this.section[key] = false;}
  }, this);
}

PopoverController.prototype.openSignUpModal = function () {

    var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: signUpModalTemplate,
        controller: function($scope, $uibModalInstance){
            $scope.params = {name:'', password:''};
            $scope.errorMessage = {name:'', password:''};
            $scope.ok = function () {
                $uibModalInstance.close($scope.params);
            };
            $scope.validate = function(){
    if($scope.params.name == '' || $scope.params.password == ''){
        $scope.errorMessage.name = !$scope.params.name ? 'Enter Valid Username': '';
        $scope.errorMessage.password = !$scope.params.password ? 'Enter Valid password': '';
    }
    else{
        $scope.ok();
    }
    
};
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        },
        size: 'sm'
    });

modalInstance.rendered.then(function(){
  var modalPopUp = angular.element(document.querySelector('.modal-body'));
  modalPopUp.on('click', function (evt) {
      evt.stopPropagation();
  });
});

    return modalInstance;
};

PopoverController.prototype.onRateProduct = function (current, next) {
var self = this;
this.openSignUpModal().result.then(function (data) {
    self.isOpen = true;
    self.changeSection(current, next);
});
};


PopoverController.prototype.changeSection = function (current, next) {

    this.section[current] = false;
    this.section[next] = true;

};

module.exports = popoverComponent ;
