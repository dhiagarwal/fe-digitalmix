'use strict';
var pdpRecurringOrderController = require("../../../commerce/components/pdp-recurring-order/pdp.recurring.order.component.controller");
var pdpRecurringOrderTemplate = require("../../../commerce/components/pdp-recurring-order/pdp.recurring.order.component.jade");
pdpProductBasicsComponentController.$inject=['pdpProductBasicsComponentService','$interval','$location', '$uibModal'];
function pdpProductBasicsComponentController(pdpProductBasicsComponentService, $interval ,$location,$uibModal) {

    this.productData = {};
    this.imageData = {};
    var self = this;
    this.$uibModal = $uibModal;
    var isFavorite = false;
    this.sample = "apple";
    this.quantity = 1;
    this.selRating = 1;
    this.isQtn = true;
    this.productQuantityMax =   self.productData.stockRemaining;
    this.productQuantityMin = 0;
    this.variationType = "dropDown";//dropDown or shade
    this.headingData = "Variant";
    this.showError = false;
    this.errorMessage = "Test error message, checking more than 1 line scenario.";// Usage for Page level error
    this.changeQuantity = function(temp) {
            self.productQuantityMax = self.productData.stockRemaining;
        if (self.quantity >= self.productQuantityMin && self.quantity <= self.productQuantityMax) {
            if (temp === "+") {
                self.quantity++;
            }
            if (temp === "-") {
                self.quantity--;
            }
            if (self.quantity <= self.productQuantityMin) {
                self.quantity++;
            }
            if (self.quantity >= self.productQuantityMax) {
                self.quantity--;
            }
        }
    }
    this.topRated = function(){
      var i = self.productData.topRated;
      if(i == "true"){
        return true;
      }
      else{
        return false;
      }
    };
    this.stockStatus = function(value){
      var i = self.productData.stockStatus;
      if(i == value){
        return true;
      }
      else{
        return false;
      }
    };
    this.validateItem = function(){
      var flag = false;
      if(self.quantity < 0  || self.quantity > self.productData.stockRemaining|| self.productData.stockStatus == "OS"||self.stockRemaining==0){
        flag = true;
      }
      return flag;
    }

    this.checkFavorite = function() {
        if (isFavorite) {
            return "active";
        }
    };
    this.addFavorite = function() {
        if (isFavorite) {
            isFavorite = false;
        } else {
            isFavorite = true;
        }
    };

    this.starOff = "../../../../src/assets/images/css/star-off.svg";
    this.starOn = "../../../../src/assets/images/css/star-on.svg";
    this.starNumber = 5;
    this.rating = 0;
    this.ratingHolder = this.rating;
    this.ratingFlag = true;
    this.getNumber = function() {
        var a = new Array();
        var i = 0;
        while (i < this.starNumber) {
            i++;
            a[i - 1] = i;

        }
        return a;
    }
    this.starFunction = function(index) {
        if (index <= this.rating) {
            return this.starOn;
        } else {
            return this.starOff;
        }
    };
    this.rateProduct = function(productRating) {
        this.ratingFlag = true;
        if (this.ratingFlag) {
            this.rating = productRating;
            this.ratingHolder = productRating;

        }

    };
    this.starMouseOver = function(index) {
        if (this.ratingFlag) {
            this.rating = index;
        }
    };
    this.starMouseLeave = function(index) {
        if (this.ratingFlag) {
            this.rating = this.ratingHolder;
        }
    };

    this.addRecurringOrder = function() {

        var options = {
            templateUrl: pdpRecurringOrderTemplate,
            controller: pdpRecurringOrderController,
            controllerAs: "ctrl",
            size: 'sm',
            windowClass: 'pdp-recurring-order'
        };
        var modalInstance = this.$uibModal.open(options);
        // modalInstance.result.then(function(credentials) {
        //     console.log(credentials);
        //     self.credentials = credentials;

        // });

    };

    self.slickPrimaryConfig = {
        enabled: false,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        speed: 500,
        responsive: [{
            breakpoint: 640,
            settings: {
                arrows: true
            }
        }, {
            breakpoint: 1024,
            settings: {
                arrows: true,
                draggable: false
            }
        }]
    };
    $interval(function() {
        self.slickPrimaryConfig.adaptiveHeight = true;
        self.slickPrimaryConfig.enabled = true;
    }, 600, [3]);

    var promise = pdpProductBasicsComponentService.getProductData();
    promise.then(function(data) {
        self.productData = data;
        self.imageData = data.images;
    });
    this.addProductToCart = function(){
      var getSelectedValue = "";
      var errorStatus = false;
      if(this.variationType == "dropDown"){
        getSelectedValue = angular.element("#productDropDown :selected").val();
        if(getSelectedValue == ""){
          errorStatus = true;
        }
      } else{
        if(!angular.element(".color-picker").hasClass("selected")){
          errorStatus = true;
        }
      }
      if(errorStatus){
        this.showError = true;
      }
    }
};

module.exports = pdpProductBasicsComponentController;
