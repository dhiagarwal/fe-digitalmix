'use strict';

pdpProductBasicsComponentService.$inject = ['$http','$q'];

function pdpProductBasicsComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-productbasics/pdp.productbasics.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getProductData = function() {
    return deferred.promise;
  }

};

module.exports = pdpProductBasicsComponentService;
