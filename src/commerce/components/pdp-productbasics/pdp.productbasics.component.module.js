'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-slick-carousel');
var pdpProductBasicsComponent = require('./pdp.productbasics.component');
var pdpProductBasicsComponentService = require('./pdp.productbasics.component.service');
var popoverComponent = require('./popover.component');
// var angularSlick = require('../../../../src/vendors/angular-slick/slick');

var pdpProductBasicsComponentModule = angular.module('pdpProductBasicsComponentModule', [uiRouter,'ui.bootstrap'])
                  .component('pdpproductbasicscomponent', pdpProductBasicsComponent)
                  .component('popoverComponent', popoverComponent)
                  .service('pdpProductBasicsComponentService', pdpProductBasicsComponentService)

import Styles from './pdp.productbasics.component.scss';

module.exports = pdpProductBasicsComponentModule;
