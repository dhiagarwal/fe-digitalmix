'use strict';
var template = require('./pdp.productbasics.component.jade');
var controller = require('./pdp.productbasics.component.controller');

var pdpProductBasicsComponent = {
    scope: {},
    templateUrl: template,
    controller: controller,
    controllerAs: 'productCtrl',
};

module.exports = pdpProductBasicsComponent;
