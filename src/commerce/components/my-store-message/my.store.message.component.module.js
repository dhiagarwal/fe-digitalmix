'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var myStoreMessageComponent = require('./my.store.message.component');

var myStoreMessageComponentModule = angular.module('myStoreMessageComponentModule', [uiRouter])
                  .component('mystoremessagecomponent',myStoreMessageComponent)
                
import Styles from './my.store.message.component.scss';                      

module.exports = myStoreMessageComponentModule;