'use strict';
var template = require('./my.store.message.component.jade');
var controller = require('./my.store.message.component.controller');


var myStoreMessageComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'mystoremessage',
  bindings: {
      bannerUrl: "="  
  }
};

module.exports = myStoreMessageComponent;