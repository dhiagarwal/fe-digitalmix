'use strict';

distributorAuthenticatedComponentService.$inject = ['$http',"$q"];

function distributorAuthenticatedComponentService($http,$q) {
  this.$http = $http;
  this.$q = $q;
};

distributorAuthenticatedComponentService.prototype.getquickLinks = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/commerce/components/distributor-authenticated-suggestions/quick.links.component.json')
        .success(deferred.resolve);

    return deferred.promise;
};

distributorAuthenticatedComponentService.prototype.getCardsData = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/commerce/components/distributor-authenticated-suggestions/card.component.json')
        .success(deferred.resolve);

    return deferred.promise;
};

module.exports = distributorAuthenticatedComponentService;
