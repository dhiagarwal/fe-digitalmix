'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var distributorAuthenticatedComponent =require('./distributor.authenticated.component');
var distributorAuthenticatedService =require('./distributor.authenticated.service');

var distributorAuthenticatedModule = angular.module('distributorAuthenticated', [uiRouter])
.component('distributorauthenticatedcomponent', distributorAuthenticatedComponent)
.service('distributorAuthenticatedService', distributorAuthenticatedService);

import Styles from './distributor.authenticated.scss';

module.exports= distributorAuthenticatedModule;
