var template =require('./distributor.authenticated.jade');
var controller =require('./distributor.authenticated.controller');

var distributorAuthenticatedComponent =  {
    templateUrl : template,
    controller: controller,
    controllerAs: 'ctrl',
};

module.exports = distributorAuthenticatedComponent;
