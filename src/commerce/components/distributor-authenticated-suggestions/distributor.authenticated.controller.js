'use strict';
function distributorAuthenticatedController(distributorAuthenticatedService) {
    var self = this;
    self.cardDismissShow = false;

    var promise = distributorAuthenticatedService.getCardsData();
    promise.then(function(data){
        self.toDoCards = data;
    });
    var promise1 = distributorAuthenticatedService.getquickLinks();
    promise1.then(function(data){
        self.quickLinks = data;
    });

    self.handleAccordionHeight = function() {
        angular.element('.dist-auth-cards .bg-cayman').css({'height': '', 'max-height': '', 'overflow': ''});
    };
};

module.exports = distributorAuthenticatedController;
