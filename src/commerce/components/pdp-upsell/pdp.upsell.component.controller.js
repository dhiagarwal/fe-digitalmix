'use strict';

function pdpUpsellComponentController(pdpUpsellComponentService) {

  this.upsellData ={};
  var self = this;

  var promise = pdpUpsellComponentService.getUpsellData();
  promise.then(function(data) {

    self.upsellData = data;
  });


};

module.exports = pdpUpsellComponentController;
