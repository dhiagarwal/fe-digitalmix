'use strict';
var template = require('./pdp.upsell.component.jade');
var controller = require('./pdp.upsell.component.controller');

var pdpUpsellComponent = {
  //return {
  restrict: 'E',
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
  //};
};

module.exports = pdpUpsellComponent;
