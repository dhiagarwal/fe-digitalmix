'use strict';
// var socialData = require('./pdp.social.component.json');

pdpUpsellComponentService.$inject = ['$http','$q'];

function pdpUpsellComponentService($http,$q) {
  // console.log($q);
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-upsell/pdp.upsell.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getUpsellData = function() {
    return deferred.promise;
  }

};




module.exports = pdpUpsellComponentService;
