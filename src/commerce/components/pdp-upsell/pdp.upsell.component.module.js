'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pdpUpsellComponent = require('./pdp.upsell.component');
var pdpUpsellComponentService = require('./pdp.upsell.component.service')

var pdpUpsellComponentModule = angular.module('pdpUpsellComponentModule', [uiRouter])
                  .component('pdpupsellcomponent', pdpUpsellComponent)
                  .service('pdpUpsellComponentService', pdpUpsellComponentService)
                  
         //import base from '../../../stylesheets/base.scss'
import Styles from './pdp.upsell.component.scss';                      

module.exports = pdpUpsellComponentModule;
