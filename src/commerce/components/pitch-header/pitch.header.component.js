'use strict';
var template = require('./pitch.header.jade');
var controller = require('./pitch.header.controller');

var pitchHeaderComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = pitchHeaderComponent;
