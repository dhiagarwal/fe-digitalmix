var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-ui-bootstrap');
var pitchHeaderComponent = require('./pitch.header.component');
var pitchHeaderService = require('./pitch.header.service');

var pitchHeaderModule = angular.module('pitchHeaderModule', [uiRouter, 'ui.bootstrap'])
.component('pitchheader', pitchHeaderComponent)
.service('pitchHeaderService', pitchHeaderService);

import Styles from './pitch.header.scss';

module.exports = pitchHeaderModule;
