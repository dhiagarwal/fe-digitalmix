'use strict';

pitchHeaderService.$inject = ['$http','$q'];

function pitchHeaderService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pitch-header/pitch.header.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getCategoryStandardData = function() {
    return deferred.promise;
  }

};

module.exports = pitchHeaderService;
