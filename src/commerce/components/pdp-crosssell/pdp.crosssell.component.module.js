'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var slickCarousel = require('slick-carousel');
var angularSlick =  require('angular-slick-carousel');
var pdpCrosssellComponent = require('./pdp.crosssell.component');
var pdpCrosssellComponentService = require('./pdp.crosssell.component.service');
// var angularSlick = require('../../../../src/vendors/angular-slick/slick');

var pdpCrosssellComponentModule = angular.module('pdpCrosssellComponentModule', [uiRouter,'slickCarousel'])
                  .component('pdpcrosssellcomponent', pdpCrosssellComponent)
                  .service('pdpCrosssellComponentService', pdpCrosssellComponentService)

import Styles from './pdp.crosssell.component.scss';

module.exports = pdpCrosssellComponentModule;
