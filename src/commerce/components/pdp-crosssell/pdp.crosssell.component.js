'use strict';
var template = require('./pdp.crosssell.component.jade');
var controller = require('./pdp.crosssell.component.controller');


var pdpCrosssellComponent = {
    //return {
    restrict: 'E',
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl',
    bindToController: true
        //};
};

module.exports = pdpCrosssellComponent;
