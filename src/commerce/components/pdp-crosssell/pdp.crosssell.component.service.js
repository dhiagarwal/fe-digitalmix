'use strict';

pdpCrosssellComponentService.$inject = ['$http','$q'];

function pdpCrosssellComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-crosssell/pdp.crosssell.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getCrosssellData = function() {
    return deferred.promise;
  }

};

module.exports = pdpCrosssellComponentService;
