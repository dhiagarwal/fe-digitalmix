'use strict';

function pdpCrosssellComponentController(pdpCrosssellComponentService, $timeout) {

    var self = this;
    this.crosssellData = {};
    this.apple = "dasda";
    this.slickType="type1";
    this.slickSecondaryConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        focusonselect: true,
        speed: 500,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 3,
                }
    },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    draggable: false,
                }
    }
  ]
    };
    var promise = pdpCrosssellComponentService.getCrosssellData();
    promise.then(function (data) {
        self.crosssellData = data;
    });
    this.stockStatus = function(value, temp){
      if(temp == value){
        return true;
      }
      else{
        return false;
      }
    };
};

module.exports = pdpCrosssellComponentController;
