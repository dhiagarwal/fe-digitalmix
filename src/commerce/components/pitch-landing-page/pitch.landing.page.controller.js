'use strict';

function pitchLandingPageController(pitchLandingPageService) {
  var self = this;
  this.welcomeMessage = "Welcome";
  this.welcomeUser = "Will,";
  this.welcomeMessageUser = "Here are those products we talked about. Take a look!";
  this.pitchTotal = "$3852.00";
  var getData = pitchLandingPageService.getCategoryStandardData();
  getData.then(function(data){
      self.pitchData = data;
  });
};

module.exports = pitchLandingPageController;
