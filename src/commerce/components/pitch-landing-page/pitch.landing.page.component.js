'use strict';
var template = require('./pitch.landing.page.jade');
var controller = require('./pitch.landing.page.controller');

var pitchLandingPageComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = pitchLandingPageComponent;
