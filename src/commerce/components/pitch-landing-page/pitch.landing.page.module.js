var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-ui-bootstrap');
var pitchLandingPageComponent = require('./pitch.landing.page.component');
var pitchLandingPageService = require('./pitch.landing.page.service');

var pitchLandingPageModule = angular.module('pitchLandingPageModule', [uiRouter, 'ui.bootstrap'])
.component('pitchlandingpage', pitchLandingPageComponent)
.service('pitchLandingPageService', pitchLandingPageService);

import Styles from './pitch.landing.page.scss';

module.exports = pitchLandingPageModule;
