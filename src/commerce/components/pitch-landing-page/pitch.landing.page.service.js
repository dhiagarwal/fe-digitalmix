'use strict';

pitchLandingPageService.$inject = ['$http','$q'];

function pitchLandingPageService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pitch-landing-page/pitch.landing.page.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getCategoryStandardData = function() {
    return deferred.promise;
  }

};

module.exports = pitchLandingPageService;
