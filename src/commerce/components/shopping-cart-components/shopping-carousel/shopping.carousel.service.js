'use strict';

function shoppingCarouselService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

shoppingCarouselService.prototype.getShoppingCarouselData = function(){

    var deferred = this.$q.defer();

    this.$http.get("../../../../../src/commerce/components/shopping-cart-components/shopping-carousel/shopping.carousel.json")
        .success(deferred.resolve);

    return deferred.promise;
};

module.exports = ['$http', '$q', shoppingCarouselService];
