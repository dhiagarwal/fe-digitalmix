'use strict';

function shoppingCarouselController(shoppingCarouselService) {
    this.slickType="type2";
    this.slickSecondaryConfig = {
        centerMode: true,
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        initOnload:'true',
        dots: false,
        speed: 500,
        arrows: true,
        responsive: [
            {
                breakpoint: 300,
                settings: {
                    slidesToShow: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 880,
                settings: {
                    slidesToShow: 3,
                    centerMode: true
                }
            },
            {
                breakpoint: 1130,
                settings: {
                    slidesToShow: 4,
                    draggable: false,
                    centerMode: true
                }
            }]
    };
    this.shoppingCarouselService = shoppingCarouselService;
    shoppingCarouselService.getShoppingCarouselData().then(this.setCarouselData.bind(this));

    this.stockStatus = function(value, temp){
      if(temp == value){
        return true;
      }
      else{
        return false;
      }
    };

    window.setTimeout(function(){
        angular.element(".carousel.carousel-secondary").slick('unslick').slick('reinit');
    },500);
}

shoppingCarouselController.prototype.setCarouselData = function(imagesData){
    this.images = imagesData;
};

module.exports = ['shoppingCarouselService' ,shoppingCarouselController];
