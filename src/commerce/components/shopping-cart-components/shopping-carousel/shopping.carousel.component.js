'use strict';

var template =require('./shopping.carousel.jade');
var controller =require('./shopping.carousel.controller');

var shoppingCarouselComponent =  {
    restrict: 'E',
    templateUrl : template,
    controller: controller,
    controllerAs: 'ctrl',
    bindToController: true
};

module.exports = shoppingCarouselComponent ;
