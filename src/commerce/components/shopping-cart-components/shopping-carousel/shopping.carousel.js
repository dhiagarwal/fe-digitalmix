'use strict';

require('../../../../stylesheets/base.scss');
require('./shopping-carousel.scss');

var angular=require('angular');
require('slick-carousel');
require('angular-slick-carousel');

var shoppingCarouselComponent =require('./shopping.carousel.component');
var shoppingCarouselService = require('./shopping.carousel.service');

var sharedUtils = require('../../../../__shared/shared');

var shoppingCarouselModule = angular.module('shoppingCarouselModule', ['slickCarousel', sharedUtils.name])
    .component('shoppingCarousel', shoppingCarouselComponent)
    .service('shoppingCarouselService', shoppingCarouselService);


module.exports = shoppingCarouselModule;
