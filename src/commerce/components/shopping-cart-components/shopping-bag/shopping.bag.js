'use strict';

require('../../../../stylesheets/base.scss');
require('./shopping.bag.scss');

var angular=require('angular');
require('slick-carousel');
require('angular-slick-carousel');

var shoppingBagComponent = require('./shopping.bag.component');
var orderSummaryComponent = require('./order.summary.component');
var shoppingCarouselService = require('./shopping.bag.service');
var shoppingCarouselModule = require('../shopping-carousel/shopping.carousel');

var shoppingBagModule = angular.module('shoppingBagModule', ['slickCarousel',shoppingCarouselModule.name])
    .component('shoppingBag', shoppingBagComponent)
    .component('orderSummary', orderSummaryComponent)
    .service('shoppingBagService', shoppingCarouselService);

module.exports = shoppingBagModule;