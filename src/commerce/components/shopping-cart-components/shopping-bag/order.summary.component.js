'use strict';

var template =require('./order.summary.jade');

var OrderSummaryComponent =  {
    restrict: 'E',
    templateUrl : template,
    bindings:{
        totalCost: '=',
        promoCode: '=',
        bagItems: '='
    },
    controller: OrderSummaryController,
    controllerAs: 'ctrl',
    bindToController: true
};

function OrderSummaryController(){

    this.error = {
        isError: false,
        errorMessage:'Oops, please re-enter promo code'
    };
}
OrderSummaryController.prototype.validateBag = function(){
  var error = false;
this.bagItems.forEach(function(x){
  if(x.quantity<0 || x.quantity > x.stockRemaining || x.stockStatus == "OS"||x.stockRemaining == 0 ){
    error = true;
  }
});
return error;
};
OrderSummaryController.prototype.toggle = function(open){

};

OrderSummaryController.prototype.applyCode = function(code){
    if(!code){
        this.error.isError = true;
        return;
    }
    this.error.isError = false;
};

module.exports = OrderSummaryComponent ;
