'use strict';

function shoppingBagService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

shoppingBagService.prototype.getShoppingBagData = function(){

    var deferred = this.$q.defer();

    this.$http.get("../../../../../src/commerce/components/shopping-cart-components/shopping-bag/shopping.bag.json")
        .success(deferred.resolve);

    return deferred.promise;
};

module.exports = ['$http', '$q', shoppingBagService];
