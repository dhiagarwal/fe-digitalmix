'use strict';

var template =require('./shopping.bag.jade');
var _ = require('lodash');

var ShoppingBagComponent =  {
    restrict: 'E',
    templateUrl : template,
    controller: [
        'shoppingBagService',
        shoppingBagController
    ],
    controllerAs: 'ctrl',
    bindToController: true
};

function shoppingBagController(shoppingBagService){
    this.bag = [];
    shoppingBagService.getShoppingBagData().then(this.setBagData.bind(this));
}

shoppingBagController.prototype.setBagData = function(items){
    this.bag = items;
    this.errorMessage = "Items in your cart have changed.";
    this.error =  false;
    this.estimatedTotal();
};
shoppingBagController.prototype.isNew = function(item){
  var i = item;
  if(i == "true"){
    return true;
  }
  else{
    return false;
  }
};
shoppingBagController.prototype.stockStatus = function(value, item){
  var i = item;
  if(i == value){
    return true;
  }
  else{
    return false;
  }
};


shoppingBagController.prototype.estimatedTotal = function(){

    var self = this;

    this.bag.orderSummary.totalCount = 0;

    if(this.bag.bagItems.length == 0){
        return;
    }

    this.bag.bagItems.forEach(function(x){
        self.bag.orderSummary.totalCount = self.bag.orderSummary.totalCount + (x.quantity * x.itemCost);
    });
};

shoppingBagController.prototype.changeQuantity = function(value,index,stockLeft,operator){
    if(operator == '-' && value > 1){
        this.bag.bagItems[index].quantity--;
        this.reduceCost(this.bag.bagItems[index].quantity, this.bag.bagItems[index].itemCost);
    }
    if(operator == '+' && value < stockLeft){
        console.log("here2");
        this.bag.bagItems[index].quantity++;
        this.addCost(this.bag.bagItems[index].quantity, this.bag.bagItems[index].itemCost);
    }
};

shoppingBagController.prototype.addCost = function(qty, cost){
    this.bag.orderSummary.totalCount = this.bag.orderSummary.totalCount + cost;
};

shoppingBagController.prototype.reduceCost = function(qty, cost){
    this.bag.orderSummary.totalCount = this.bag.orderSummary.totalCount - cost;
};

shoppingBagController.prototype.removeItem = function(item){
    var itemToRemove = _.find(this.bag.bagItems, {itemNo: item});
    _.pull(this.bag.bagItems, itemToRemove);
    this.estimatedTotal();
};

module.exports = ShoppingBagComponent ;
