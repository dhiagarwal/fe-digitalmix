'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var productIngredientsComponent = require('./pdp.product.ingredients.component');


var productIngredientsModule = angular.module('productIngredientsModule', [uiRouter])
                  .component('productingredientscomponent', productIngredientsComponent)
             
                  
import Styles from './pdp.product.ingredients.component.scss';                     

module.exports = productIngredientsModule;