'use strict';
var template = require('./pdp.product.ingredients.component.jade');
var controller = require('./pdp.product.ingredients.component.controller');

var productIngredientsComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'productInfoCtrl',
    bindToController: true
};

module.exports = productIngredientsComponent;