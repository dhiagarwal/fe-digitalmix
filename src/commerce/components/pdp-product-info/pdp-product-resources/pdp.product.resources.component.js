'use strict';
var template = require('./pdp.product.resources.component.jade');
var controller = require('./pdp.product.resources.component.controller');

var productResourcesComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl',
    bindToController: true
};

module.exports = productResourcesComponent;