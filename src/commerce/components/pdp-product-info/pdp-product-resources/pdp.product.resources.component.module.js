'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var productResourcesComponent = require('./pdp.product.resources.component');
var pdpProductResourcesComponentService = require('./pdp.product.resources.component.service');


var productResourcesModule = angular.module('productResourcesModule', [uiRouter])
                  .component('productresourcescomponent', productResourcesComponent)
                  .service('pdpProductResourcesComponentService', pdpProductResourcesComponentService)             
                  
import Styles from './pdp.product.resources.component.scss';                     

module.exports = productResourcesModule;
