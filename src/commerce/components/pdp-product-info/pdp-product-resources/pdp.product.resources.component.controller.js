'use strict';

function pdpProductResourcesController(pdpProductResourcesComponentService) {
	var self=this;
    var promise = pdpProductResourcesComponentService.getResourcesData();
    promise.then(function(data) {
        self.resourcesData = data;
    });

};

module.exports = pdpProductResourcesController;