'use strict';

pdpProductResourcesComponentService.$inject = ['$http','$q'];

function pdpProductResourcesComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-product-info/pdp-product-resources/pdp.product.resources.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getResourcesData = function() {
    return deferred.promise;
  }

};

module.exports = pdpProductResourcesComponentService;