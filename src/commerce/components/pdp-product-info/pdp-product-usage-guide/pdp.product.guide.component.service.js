'use strict';

pdpProductGuideComponentService.$inject = ['$http','$q'];

function pdpProductGuideComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-product-info/pdp-product-usage-guide/pdp.product.guide.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getProductVideo = function() {
    return deferred.promise;
  }

};

module.exports = pdpProductGuideComponentService;