'use strict';
var template = require('./pdp.product.guide.component.jade');
var controller = require('./pdp.product.guide.component.controller');

var productGuideComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'productGuideCtrl',
    bindToController: true
};

module.exports = productGuideComponent;