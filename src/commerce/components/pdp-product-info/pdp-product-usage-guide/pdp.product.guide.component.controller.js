'use strict';

function pdpProductGuideController(pdpProductGuideComponentService, $timeout) {
    var self = this;
    self.videoPlaying = false;
    var videoControl = angular.element.find('#video1');
    videoControl[0].controls = false;
    var promise = pdpProductGuideComponentService.getProductVideo();
    promise.then(function(data) {
        self.productData = data;
    });
	
    self.togglePlay = function() {
        var video = angular.element.find('#video1');
        self.videoDuration = video.duration;
        if (video[0].paused) {
            video[0].play();
            video[0].controls = true;
            this.videoPlaying = true;
        } else {
            $timeout(function() { video[0].pause(); }, 10)
            video[0].controls = false;
            this.videoPlaying = false;
        }
    };

    self.video = function() {
        var videoElements = angular.element.find('#video1');
        videoElements[0].currentTime = 0;
        videoElements[0].controls = false;
        self.videoPlaying = false;
    };

};

module.exports = pdpProductGuideController;
