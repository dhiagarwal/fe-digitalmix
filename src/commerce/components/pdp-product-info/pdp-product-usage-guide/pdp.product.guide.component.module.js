'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var productGuideComponent = require('./pdp.product.guide.component');
var pdpProductGuideComponentService = require('./pdp.product.guide.component.service');


var productGuideModule = angular.module('productGuideModule', [uiRouter])
                  .component('productguidecomponent', productGuideComponent)
				  .service('pdpProductGuideComponentService', pdpProductGuideComponentService);	
             
                  
import Styles from './pdp.product.guide.component.scss';                     

module.exports = productGuideModule;