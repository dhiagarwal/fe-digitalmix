'use strict';

pdpCategoryFooterService.$inject = ['$http','$q'];

function pdpCategoryFooterService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-category-footer/pdp.category.footer.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getCategoryFooterData = function() {
    return deferred.promise;
  }

};

module.exports = pdpCategoryFooterService;
