'use strict';
var template = require('./pdp.category.footer.jade');
var controller = require('./pdp.category.footer.controller');

var pdpCategoryFooterComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = pdpCategoryFooterComponent;
