'use strict';

function pdpCategoryFooterController(pdpCategoryFooterService) {
  var self = this;
  var getData = pdpCategoryFooterService.getCategoryFooterData();
  getData.then(function(data){
      self.footerOptions = data;
  });
};

module.exports = pdpCategoryFooterController;
