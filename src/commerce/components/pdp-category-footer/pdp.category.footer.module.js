var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pdpCategoryFooterComponent = require('./pdp.category.footer.component');
var pdpCategoryFooterService = require('./pdp.category.footer.service');

var pdpCategoryFooterModule = angular.module('pdpCategoryFooterModule', [uiRouter])
.component('pdpcategoryfooter', pdpCategoryFooterComponent)
.service('pdpCategoryFooterService', pdpCategoryFooterService);

import Styles from './pdp.category.footer.scss';

module.exports = pdpCategoryFooterModule;
