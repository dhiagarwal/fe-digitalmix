'use strict';

function pdpShoppingCartComponentController(pdpShoppingCartComponentService, $timeout) {

    var self = this;
    var setDuration = 3000;
    self.productBagData = {};
    self.getShoppingCartModalBox = function() {
      $("#shoppingCartModal").modal();
      $timeout(function() {
        $("#shoppingCartModal").modal("hide");
      }, setDuration);
    }
    var promise = pdpShoppingCartComponentService.getProductBagData();
    promise.then(function(data) {
        self.productBagData = data;
    });

};

module.exports = pdpShoppingCartComponentController;
