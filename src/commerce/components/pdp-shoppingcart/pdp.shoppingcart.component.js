'use strict';
var template = require('./pdp.shoppingcart.component.jade');
var controller = require('./pdp.shoppingcart.component.controller');

var pdpShoppingCartComponent = {
    //return {
    restrict: 'E',
    scope: {},
    templateUrl: template,
    controller: controller,
    controllerAs: 'productBag',
    bindToController: true,
    bindings:{
    	displayText : "=",
        hideFlag : "="
    }
};

module.exports = pdpShoppingCartComponent;
