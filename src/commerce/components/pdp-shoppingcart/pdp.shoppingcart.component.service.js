'use strict';

pdpShoppingCartComponentService.$inject = ['$http','$q'];

function pdpShoppingCartComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-shoppingcart/pdp.shoppingcart.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getProductBagData = function() {
    return deferred.promise;
  }

};

module.exports = pdpShoppingCartComponentService;
