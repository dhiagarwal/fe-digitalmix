var angular = require('angular');
var uiRouter = require('angular-ui-router');
//var pdpShoppingCart = require('./pdp.shoppingcart.component');
var pdpShoppingCartComponent = require('./pdp.shoppingcart.component');
var pdpShoppingCartComponentService = require('./pdp.shoppingcart.component.service');

var pdpShoppingCartComponentModule = angular.module('pdpShoppingCartComponentModule', [uiRouter])
.component('pdpshoppingcartcomponent', pdpShoppingCartComponent)
.service('pdpShoppingCartComponentService', pdpShoppingCartComponentService);

import Styles from './pdp.shoppingcart.component.scss';

module.exports = pdpShoppingCartComponentModule;
