'use strict';

function tenProductsController(tenProductsService) {
  var self = this;
  var getData = tenProductsService.getCategoryStandardData();
  getData.then(function(data){
      self.categoryData = data;
  });
};

module.exports = tenProductsController;
