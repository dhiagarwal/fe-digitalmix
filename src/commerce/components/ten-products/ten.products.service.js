'use strict';

tenProductsService.$inject = ['$http','$q'];

function tenProductsService($http,$q) {
  this.$http = $http;
  var deferred = $q.defer();
  var distributorInfoPayload = {};
  $http.get('../../../../src/commerce/components/ten-products/ten.products.json').then(function(response) {
      deferred.resolve(response.data);
  });

  this.getCategoryStandardData = function() {
    return deferred.promise;
  }

};

module.exports = tenProductsService;
