var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-ui-bootstrap');
var tenProductsComponent = require('./ten.products.component');
var tenProductsService = require('./ten.products.service');

var tenProductsModule = angular.module('tenProductsModule', [uiRouter, 'ui.bootstrap'])
.component('tenProductsComponent', tenProductsComponent)
.service('tenProductsService', tenProductsService);

import Styles from './ten.products.scss';

module.exports = tenProductsModule;
