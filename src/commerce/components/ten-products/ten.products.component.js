'use strict';
var template = require('./ten.products.jade');
var controller = require('./ten.products.controller');

var tenProductsComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = tenProductsComponent;
