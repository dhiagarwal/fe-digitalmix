'use strict';
var template = require('./order.detail.component.jade');
var _ = require('lodash');
var modalTemplate = require('./order.detail.modal.jade');


var orderDetailComponent = {
    templateUrl: template,
    controller: ['orderDetailService','$state','$uibModal', orderDetailController],
    controllerAs: 'ctrl'
};

function orderDetailController(orderDetailService, $state, $uibModal){

    this.checkoutData = {};
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.isChecked = false;
    orderDetailService.getOrderData().then(this.setCheckoutData.bind(this));
}



orderDetailController.prototype.setCheckoutData = function(data){
    this.checkoutData = data;
};


orderDetailController.prototype.reOrder= function() {
    var self = this;

    var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: modalTemplate,
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }],
        size: 'sm',
        windowClass: 'app-modal-window'
    });
}

module.exports = orderDetailComponent ;