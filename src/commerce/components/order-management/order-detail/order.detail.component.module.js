'use strict';

var angular = require('angular');
require('angular-ui-bootstrap');
require('./order.detail.component.scss');
var orderDetailComponent = require('./order.detail.component');
var orderDetailService = require('./order.detail.component.service');

var orderDetailComponentModule = angular.module('orderDetailComponentModule', ['ui.bootstrap'])
    .component('orderdetail', orderDetailComponent)
    .service('orderDetailService', orderDetailService);

module.exports = orderDetailComponentModule;