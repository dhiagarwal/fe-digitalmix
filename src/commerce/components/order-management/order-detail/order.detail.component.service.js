'use strict';

function orderDetailService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

orderDetailService.prototype.getOrderData = function(){
    var deferred = this.$q.defer();
    this.$http.get("../../../../../src/commerce/components/order-management/order-detail/order.detail.component.json")
        .success(deferred.resolve);
    return deferred.promise;
};

module.exports = ['$http', '$q', orderDetailService];
