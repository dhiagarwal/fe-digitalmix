'use strict';

var angular = require('angular');
require('angular-ui-bootstrap');
require('./my.order.component.scss');
var myOrderComponent = require('./my.order.component');
var myOrderService = require('./my.order.component.service');
var orderTileComponent = require('../order-tile-component/order.tile.component.module.js');
var ordersUnavailableComponent = require('../orders-unavailable-component/orders.unavailable.component.module.js');

var myOrderComponentModule = angular.module('myOrderComponentModule', ['ui.bootstrap',orderTileComponent.name,ordersUnavailableComponent.name])
    .component('myorders', myOrderComponent)
    .service('myOrderService', myOrderService);

module.exports = myOrderComponentModule;