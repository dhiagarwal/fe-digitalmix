'use strict';

function myOrderComponentController(myOrderService,$timeout,$location) {
	var self = this;
	self.active = 1;
	self.recentOrders = [];
	self.pastOrders = [];
	self.upcomingOrders = [];
	self.isDataEmpty = false;

	self.sortObjects = function(orderData,isRecentOrPastOrder){
		for (var i = 0,len = orderData.length; i<len; i++){
			var orderSummary = orderData[i]["orderSummary"],
			orderDate = new Date(orderSummary.purchaseDate),
			todaysDate = new Date(),
			timeDiff = Math.abs(todaysDate.getTime() - orderDate.getTime()),
			dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
			orderData[i]["orderSummary"]["formattedPurchaseDate"] = orderDate;

			//Formatting Shipment Dates
			for(var j=0; j < orderSummary["shipmentDetails"].length ; j++){
				var formattedShipmentDate = new Date(orderSummary["shipmentDetails"][j]["shipmentDate"]);
				orderData[i]["orderSummary"]["shipmentDetails"][j]["formattedShipmentDate"] = formattedShipmentDate;
			}
			
			if(isRecentOrPastOrder){
				//For Recent and Past Orders
				if(dayDifference <= 30){
					self.recentOrders.push(orderData[i])
				} else {
					self.pastOrders.push(orderData[i]);
				}
			} else {
				//For Upcoming Orders
				self.upcomingOrders.push(orderData[i]);
			}
		}
		//Check if data is empty of Default Tab, accordingly show/hide the filter
		self.checkIfDataEmpty('recent');
		self.orderData = orderData;
	};

	self.checkIfDataEmpty = function(currentActiveTab){
		switch(currentActiveTab){
			case 'upcoming': (self.upcomingOrders.length <= 0) ? self.isDataEmpty = true :  self.isDataEmpty = false;
					break;
			case 'past': (self.pastOrders.length <= 0) ? self.isDataEmpty = true :  self.isDataEmpty = false;
					break;
			default: (self.recentOrders.length <= 0) ? self.isDataEmpty = true :  self.isDataEmpty = false;
					break;
		}
	};


	var promise = myOrderService.getOtherOrderData();
    promise.then(function(data) {
        self.sortObjects(data,true);
    });

   	var promiseUpcomingOrders = myOrderService.getUpcomingOrderData();
   	promiseUpcomingOrders.then(function(data){
   		self.sortObjects(data,false);
   	});
}

module.exports = myOrderComponentController;