'use strict';
var template = require('./my.order.component.jade');
var controller = require('./my.order.component.controller');

var myOrderComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true
};

module.exports = myOrderComponent;