'use strict';

function myOrderService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

myOrderService.prototype.getUpcomingOrderData = function(){
    var deferred = this.$q.defer();
    this.$http.get("../../../../../src/commerce/components/order-management/my-orders/my.order.component.upcoming.json")
        .success(deferred.resolve);
    return deferred.promise;
};

myOrderService.prototype.getOtherOrderData = function(){
    var deferred = this.$q.defer();
    this.$http.get("../../../../../src/commerce/components/order-management/my-orders/my.order.component.json")
        .success(deferred.resolve);
    return deferred.promise;
};

module.exports = myOrderService;
