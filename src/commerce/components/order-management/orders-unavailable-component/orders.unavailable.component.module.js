'use strict';

var angular = require('angular');
require('angular-ui-bootstrap');
require('./orders.unavailable.component.scss');
var ordersUnavailableComponent = require('./orders.unavailable.component');

var ordersUnavailableComponentModule = angular.module('ordersUnavailableComponentModule', ['ui.bootstrap'])
    .component('ordersunavailablecomponent', ordersUnavailableComponent);

module.exports = ordersUnavailableComponentModule;