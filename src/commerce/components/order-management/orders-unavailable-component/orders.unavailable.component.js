'use strict';
var template = require('./orders.unavailable.component.jade');
var controller = require('./orders.unavailable.component.controller');

var ordersUnavailableComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      isUpcomingTab: "="
  }
};

module.exports = ordersUnavailableComponent;