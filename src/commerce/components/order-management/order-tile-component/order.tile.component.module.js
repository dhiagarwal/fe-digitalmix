'use strict';

var angular = require('angular');
require('angular-ui-bootstrap');
require('./order.tile.component.scss');
var orderTileComponent = require('./order.tile.component');

var orderTileComponentModule = angular.module('orderTileComponentModule', ['ui.bootstrap'])
    .component('ordertilecomponent', orderTileComponent);

module.exports = orderTileComponentModule;