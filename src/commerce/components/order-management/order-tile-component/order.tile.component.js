'use strict';
var template = require('./order.tile.component.jade');
var controller = require('./order.tile.component.controller');

var orderTileComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      tileData: "=",
      isUpcomingTab: "="
  }
};

module.exports = orderTileComponent;