'use strict';
var confirmModalTemplate = require('./confirmation.modal.template.jade');

function orderTileController($uibModal, $scope, $http, $location) {
    var self = this;
	self.$uibModal = $uibModal;
    self.defaultShipmentStatus = '';

    self.togglePauseResume = function(orderSummary) {
    	var confirmMsg, primaryBtn;

        if (orderSummary.isPaused) {
        	confirmMsg = 'Are you sure you want to resume this Order?';
        	primaryBtn = 'Resume Order';
        	
        	self.showConfirmationModal(confirmMsg,'YES - '+ primaryBtn,'NO - CANCEL').then(function(){
            	orderSummary.isPaused = false;
        	});
        } else {
        	confirmMsg = 'Are you sure you want to pause this Order?';
        	primaryBtn = 'Pause Order';
        	
            self.showConfirmationModal(confirmMsg,'YES - '+ primaryBtn,'NO - CANCEL').then(function(){
            	orderSummary.isPaused = true;
        	});
        }
    }

    self.redirect = function(url){
        $location.path(url);
    }
}

orderTileController.prototype.showConfirmationModal = function(Info, yesTitle, noTitle) {
    var self = this;
    
    var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: confirmModalTemplate,
        resolve: {
            modalParams: {
                description: Info,
                yesTitle: yesTitle,
                noTitle: noTitle
            }
        },
        windowClass: 'confirm-modal-class',
        controller: ['$scope', '$uibModalInstance', 'modalParams', function($scope, $uibModalInstance, modalParams) {
            $scope.ok = function() {
                $uibModalInstance.close();
            };
            $scope.modalParams = modalParams;

            $scope.cancel = function() {
                $uibModalInstance.dismiss();
            };
        }],
        size: 'sm'
    });

    //modalInstance.rendered.then(this.reposition.bind(this));

    return modalInstance.result; // making this method to return a promise object so that you can use a callback and trigger success/reject functions
};


module.exports = orderTileController;
