'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('angular-animate');

var myStoreOrderConfirmationComponent = require('./my.store.order.confirmation.component');

var myStoreOrderConfirmationComponentModule = angular.module('myStoreOrderConfirmationComponentModule', [uiRouter,'ngAnimate'])
                  .component('mystoreorderconfirmationcomponent',myStoreOrderConfirmationComponent)
                  
import Styles from './my.store.order.confirmation.component.scss';                      

module.exports = myStoreOrderConfirmationComponentModule;