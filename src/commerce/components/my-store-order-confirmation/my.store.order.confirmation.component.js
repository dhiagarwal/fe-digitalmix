'use strict';
var template = require('./my.store.order.confirmation.component.jade');
var controller = require('./my.store.order.confirmation.component.controller');

var myStoreOrderConfirmationComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'mystoreorder'
};

module.exports = myStoreOrderConfirmationComponent;