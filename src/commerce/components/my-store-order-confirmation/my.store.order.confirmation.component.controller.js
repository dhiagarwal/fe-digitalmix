'use strict';

function myStoreOrderConfirmationComponentController() {

    var self = this;
    this.showSaveCancelButtons=false;
    this.messageContentOriginal='I really hope you enjoy your new purchase! I love everything I\'ve purchased. I feel younger and healthier and happier. Be sure to let me know what you think after a few weeks!';
    this.messageContentBind='I really hope you enjoy your new purchase! I love everything I\'ve purchased. I feel younger and healthier and happier. Be sure to let me know what you think after a few weeks!';
    this.editClick=function(){
        this.showSaveCancelButtons=true;
    }
    this.saveClick=function(){
        this.messageContentOriginal=this.messageContentBind;
        this.showSaveCancelButtons = false;
    }
    this.cancelClick=function(){
        this.messageContentBind=this.messageContentOriginal;
        this.showSaveCancelButtons = false;
    }
};

module.exports = myStoreOrderConfirmationComponentController;