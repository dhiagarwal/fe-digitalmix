'use strict';
var template = require('./shipping.address.component.jade');
var controller = require('./shipping.address.component.controller');

var shippingAddressComponent = {
	restrict: 'E',
	templateUrl: template,
	controller: controller,
	controllerAs: 'ctrl',
	bindToController: true
};

module.exports = shippingAddressComponent;