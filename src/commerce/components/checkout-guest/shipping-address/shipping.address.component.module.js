'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var shippingAddressComponent = require('./shipping.address.component');
var shippingAddressComponentService = require('./shipping.address.component.service');
var formUtils = require('../../../../__shared/form-utils/form-utils'); 

var shippingAddressComponentModule = angular.module('shippingAddressComponentModule', [uiRouter, formUtils.name])
		.component('shippingaddresscomponent', shippingAddressComponent)
		.service('shippingAddressComponentService', shippingAddressComponentService)

import Styles from '../checkout.guest.component.scss';   

module.exports = shippingAddressComponentModule;