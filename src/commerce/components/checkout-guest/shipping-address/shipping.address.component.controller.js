'use strict';
var _ = require('lodash');
function shippingAddressController(shippingAddressComponentService, $state){
	this.shippingAddressData = {};
    this.$state = $state;

	var model = this.model = {};
	var self = this;

	//model.heading1 = "RONALD SISK";
	model.mainHeading = "CHECKOUT";

	model.heading2 = "SHIPPING ADDRESS";

	model.shippingAddress = {};

	var promise = shippingAddressComponentService.getFormdata();

	promise.then(function(response){
		self.model.shippingAddress.data = response.shippingAddress.data;
		self.model.shippingAddress.fields = response.shippingAddress.fields;
		self.model.shippingAddress.states = response.states;
		self.model.shippingAddress.zipcodeLookUp = response.shippingAddress.zipcodeLookUp;
	});

	self.saveAndContinue = function(isValid) {
		if (isValid) {
			window.location.href = "#/checkout/deliveryoptions";
		}
	};

	self.applyPromoCode = function(code) {
        if (!code) {
            self.model.err = true;
            return;
        }
        if (_.includes(self.model.shippingAddress.data.promotionAndRewardsApplied, code)) {
            self.model.promoCode = '';
            return;
        }
        self.model.shippingAddress.data.promotionAndRewardsApplied.push(code);
        self.model.promoCode = '';
    };

	self.gotoPage = function(pageName) {
		window.location.href = pageName;
		window.location.reload();
	};

	self.removePromoCode = function(promoCode) {
		_.pull(self.model.shippingAddress.data.promotionAndRewardsApplied, promoCode)
	};

	self.togglePanel = function() {
		if (window.innerWidth < 1024) {
			switch(self.model.isPullLabelVisible) {
				case false:
				angular.element('body').css({'background': '', 'overflow': ''});
				angular.element('input,select').removeAttr('disabled');
				angular.element('.checkout-title-container').closest('.content').css('opacity', '');
				angular.element('.accordion-panel').css({'height': '', 'min-height': '', 'overflow': ''});
				angular.element('.overlay-checkout').remove();
				angular.element('.overlay-footer').remove();
				break;
				case true:
				angular.element('body').css({'background': 'black', 'overflow': 'hidden'});
				angular.element('input,select').attr('disabled', 'true');
				angular.element('.shopping-cart-top').find('input').attr('disabled', false);
				angular.element('.checkout-title-container').closest('.content').css('opacity', '0.4');
				angular.element('.accordion-panel').css({'max-height': '27.125rem', 'min-height': '23.4375rem', 'overflow-y': 'auto', 'overflow-x': 'hidden'});
				var docHeight = angular.element(document).height();
				angular.element('.checkout-title-container').append("<div class='overlay-checkout'></div>");
				angular.element(".overlay-checkout").height(docHeight);
				angular.element('.footer-details').append("<div class='overlay-footer'></div>");
				docHeight = angular.element(".footer-details").outerHeight();
				angular.element(".overlay-footer").height(docHeight);
				break;
			}
		}
	};

	self.zipcodeLookup = function() {
		var flag = true, foundData = null;
		angular.forEach(self.model.shippingAddress.zipcodeLookUp, function(value, key) {
			if (flag && value.zipcode == self.model.shippingAddress.data.zipcode) {
				foundData = value;
				flag = false;
			}
		});
		if (foundData) {
			self.model.shippingAddress.data.zipcode = foundData.zipcode;
			self.model.shippingAddress.data.state = foundData.state;
			self.model.shippingAddress.data.city = foundData.city;
			self.model.shippingAddress.data.addressLine1 = foundData.addressLine1;
			self.model.shippingAddress.data.addressLine2 = foundData.addressLine2;
		}
	};

	self.reposition = function() {
		var modal = $(this),
		dialog = modal.find('.modal-dialog');
		modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    };

    $('.modal').on('show.bs.modal', self.reposition);
    $(window).on('resize', function() {
    	$('.modal:visible').each(self.reposition);
    });

};

shippingAddressController.prototype.toggleErr = function(){
    this.model.err = false;
};

module.exports  = shippingAddressController;
