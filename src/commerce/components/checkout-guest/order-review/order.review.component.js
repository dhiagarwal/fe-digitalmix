'use strict';
var template = require('./order.review.component.jade');
var _ = require('lodash');
var modalTemplate = require('./order.review.modal.jade');

var states = {
    isShipping: 'shippingaddress',
    isDelivery: 'deliveryoptions',
    isPayment: 'paymentmethod',
    orderPlaced: 'orderconfirmation'
};

var orderReviewComponent = {
    restrict: 'E',
    templateUrl: template,
    controller: ['guestOrderReviewService','$state','$uibModal', orderReviewController],
    controllerAs: 'ctrl',
    bindToController: true
};

var termsConditionsModal = require('../../../../../src/signup/components/terms-conditions/terms.conditions.modal.component.jade');
var skipForNowModal = require('../../../../../src/signup/components/terms-conditions/terms.conditions.skip.modal.component.jade');

function orderReviewController(guestOrderReviewService, $state, $uibModal){
var self = this;
    self.checkoutData = {};
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.showModal = false;
    this.placeOrder = function(isAccepted){
        if(isAccepted){
            this.$state.go(states['orderPlaced']);
        }
        else{
            modalPopupInstance(termsConditionsModal, this.termsConditionsController, 'app-modal-window terms-conditions-modal-body');
        }
    };
    var promise = guestOrderReviewService.getOrderData();
    promise.then(function(data){
            self.checkoutData = data; 
        }   
    );
   
    var modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };
    this.skipForNowController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };
         modalSelf.skipForNowClose=function(){
            modalSelf.ok();
            modalPopupInstance(termsConditionsModal, self.termsConditionsController, 'app-modal-window terms-conditions-modal-body');
        };
        modalSelf.remindLater=function() {
            $uibModalInstance.dismiss();
            var baseUrl=window.location.origin;
            window.location.href=baseUrl+'/#/';
        }
    };
     this.termsConditionsController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};
        modalSelf.modalData= self.checkoutData;
        modalSelf.beforeYouMayContinue= "Before you may continue with your application, please review the terms and conditions of the Distributor Agreement and Policies and Procedures (\"Policies\") located below. The Policies describe your rights and obligations as an independent Nu Skin distributor."
        modalSelf.needToAgreeText="You'll need to agree to the Distributor Agreement in order to sell Nu Skin Products and earn comissions.";
        modalSelf.definitionTitle="A. Definitions.";
        modalSelf.definitionOne="Defined terms are set forth below or may be separately defined in any of the agreements. The meaning of capitalized terms not found in this document is set forth in the Policies and Procedures.";
        modalSelf.definitionTwo="“Bonuses” means the compensation paid to Distributors based on the volume of Nu Skin Products sold by a Distributor, Downline Organization, and breakaway executives as set forth in the Sales Compensation Plan.";
        modalSelf.definitionThree="“Business Portfolio” means the non-commissionable, not-for-profit kit and is the only “Business Portfolio” means the non-commissionable, not-for-profit kit and is the only“Business Portfolio” means the non-commissionable, not-for-profit kit and is the only...";
        modalSelf.checkBoxes=false;
        modalSelf.policyCheckbox=false;
        modalSelf.arbitrationPolicyCheckbox=false;
        modalSelf.acceptButtonClicked=false;
        modalSelf.acceptTermsConditions=function(){
            modalSelf.acceptButtonClicked=true;
            if(modalSelf.policyCheckbox && modalSelf.arbitrationPolicyCheckbox){
                modalSelf.checkBoxes=false;
            }
            else{
                modalSelf.checkBoxes=true;
            }
        };
        modalSelf.checkboxShowHide=function(){
            if(modalSelf.acceptButtonClicked){
                if(modalSelf.policyCheckbox && modalSelf.arbitrationPolicyCheckbox){
                    modalSelf.checkBoxes=false;
                }
                else{
                    modalSelf.checkBoxes=true;
                }
            }
        };
        
       
        modalSelf.cancelTermsConditions=function(){
            modalSelf.cancel();
        };
        
        modalSelf.slickPrimaryConfig = {
            enabled: true,
            infinite: false,
            adaptiveHeight: false,
            mobileFirst: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            speed: 500,
            responsive: [
            {
              breakpoint: 320,
              settings: {
                slidesToShow: 1,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 1023,
              settings: {
                slidesToShow: 3,
                draggable: false,
              }
            },
            {
              breakpoint: 1440,
              settings: {
                slidesToShow: 3,
                draggable: false,
              }
            }
          ]
        };

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };
        modalSelf.skipForNowShow = function () {
            modalSelf.ok();
            modalPopupInstance(skipForNowModal, self.skipForNowController, 'app-modal-window skip-now-modal-body');
        };
    };
    
};

orderReviewController.prototype.applyCode = function(code){
    if(!code){
        this.promoError = true;
        return;
    }
    if(_.includes(this.checkoutData.orderSummary.promoCodeList, code)){
        this.checkoutData.orderSummary.promoCode = '';
        return;
    }
    this.checkoutData.orderSummary.promoCodeList.push(code);
    this.checkoutData.orderSummary.promoCode = '';
};

orderReviewController.prototype.toggleError= function(){
    this.promoError = false;
};

orderReviewController.prototype.removeItem= function(item){
    _.pull(this.checkoutData.orderSummary.promoCodeList, item)
};

orderReviewController.prototype.isAccepted = function(bool){
    this.isChecked = bool;
};


orderReviewController.prototype.onEditBag = function(){
    this.$state.go('shopping-cart');
};

orderReviewController.prototype.onEditIcon = function(stateProp){
    this.$state.go(states[stateProp]);
};

orderReviewController.prototype.navigateTo = function(view){
    this.onEditIcon(view);
};



orderReviewController.prototype.backToBag= function() {
    var self = this;

    var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: modalTemplate,
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }],
        size: 'sm'
    });

    modalInstance.result.then(function () {
        self.onEditBag();
    });
}

orderReviewController.prototype.showCustomPopup = function() {
    this.modalHeading = "Product Return Policy";
    this.modalURL = "src/assets/nuskin_refund_policy.html";
    
    this.showModal = true;
};
orderReviewController.prototype.hideCustomModal = function(){
    this.showModal = false;
};

module.exports = orderReviewComponent ;