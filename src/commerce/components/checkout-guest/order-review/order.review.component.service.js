'use strict';

function guestOrderReviewService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

guestOrderReviewService.prototype.getOrderData = function(){
    var deferred = this.$q.defer();
    this.$http.get("../../../../../src/commerce/components/checkout-guest/order-review/order.review.component.json")
        .success(deferred.resolve);
    return deferred.promise;
};

module.exports = ['$http', '$q', guestOrderReviewService];
