'use strict';

var angular = require('angular');
require('angular-ui-bootstrap');
require('./order.review.component.scss');
require('../../../../../src/signup/components/terms-conditions/terms.conditions.component.scss');
var orderReviewComponent = require('./order.review.component');
var guestOrderReviewService = require('./order.review.component.service');

var orderReviewComponentModule = angular.module('orderReviewComponentModule', ['ui.bootstrap'])
    .component('orderreview', orderReviewComponent)
    .service('guestOrderReviewService', guestOrderReviewService);

module.exports = orderReviewComponentModule;