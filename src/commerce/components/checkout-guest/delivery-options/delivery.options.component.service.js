'use strict';

deliveryOptionsComponentService.$inject = ['$http','$q'];

function deliveryOptionsComponentService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	$http.get('../../../../../src/commerce/components/checkout-guest/delivery-options/delivery.options.component.json').then(function(response) {
    	deferred.resolve(response.data);
  	});

  this.getFormdata = function() {
    return deferred.promise;
  }
};

module.exports = deliveryOptionsComponentService;