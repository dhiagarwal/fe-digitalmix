'use strict';

function deliveryOptionsController(deliveryOptionsComponentService){
	this.deliveryOptionsData = {};
	var model = this.model = {};
	var self = this;

	model.heading1 = "RONALD SISK";
	model.mainHeading = "CHECKOUT";

	model.heading2 = "Delivery Options";

	model.deliveryOptions = {};

	var promise = deliveryOptionsComponentService.getFormdata();

	promise.then(function(response){
		self.model.deliveryOptions.shippingList = response.deliveryOptions.shippingList;
		self.model.deliveryOptions.fields = response.deliveryOptions.fields;
		self.model.deliveryOptions.data = response.deliveryOptions.data;
	});

	self.gotoPage = function(pageName) {
		window.location.href = pageName;
	};

	self.togglePanel = function() {
		if (window.innerWidth < 1024) {
			switch(self.model.isPullLabelVisible) {
				case false:
				angular.element('body').css({'background': '', 'overflow': ''});
				angular.element('input,select').removeAttr('disabled');
				angular.element('.checkout-title-container').closest('.content').css('opacity', '');
				angular.element('.accordion-panel').css({'height': '', 'min-height': '', 'overflow': ''});
				angular.element('.overlay-checkout').remove();
				angular.element('.overlay-footer').remove();
				break;
				case true:
				angular.element('body').css({'background': 'black', 'overflow': 'hidden'});
				angular.element('input,select').attr('disabled', 'true');
				angular.element('.shopping-cart-top').find('input').attr('disabled', false);
				angular.element('.checkout-title-container').closest('.content').css('opacity', '0.4');
				angular.element('.accordion-panel').css({'max-height': '27.125rem', 'min-height': '23.4375rem', 'overflow-y': 'auto', 'overflow-x': 'hidden'});
				var docHeight = angular.element(document).height();
				angular.element('.checkout-title-container').append("<div class='overlay-checkout'></div>");
				angular.element(".overlay-checkout").height(docHeight);
				angular.element('.footer-details').append("<div class='overlay-footer'></div>");
				docHeight = angular.element(".footer-details").outerHeight();
				angular.element(".overlay-footer").height(docHeight);
				break;
			}
		}
	};

	self.reposition = function() {
		var modal = $(this),
		dialog = modal.find('.modal-dialog');
		modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    };

    $('.modal').on('show.bs.modal', self.reposition);
    $(window).on('resize', function() {
    	$('.modal:visible').each(self.reposition);
    });

	self.applyPromoCode = function(code) {
        if(!code){
            self.model.err = true;
            return;
        }
        if(_.includes(self.model.deliveryOptions.data.promotionAndRewardsApplied, code)){
            self.model.promoCode = '';
            return;
        }
        self.model.deliveryOptions.data.promotionAndRewardsApplied.push(code);
        self.model.promoCode = '';
	};

	self.removePromoCode = function(promoCode) {
        _.pull(self.model.deliveryOptions.data.promotionAndRewardsApplied, promoCode);
	};

	self.goBack = function() {
		window.location.href = "#/checkout/shippingaddress";
	};

	self.saveAndContinue = function(isValid) {
		if (isValid && self.model.deliveryOptions.shippingType) {}
			window.location.href = "#/checkout/paymentmethod";

	};

    self.gotoPage = function(pageName) {
        window.location.href = pageName;
        window.location.reload();
    };

};


deliveryOptionsController.prototype.toggleErr= function(){
    this.model.err = false;
};


module.exports  = deliveryOptionsController;
