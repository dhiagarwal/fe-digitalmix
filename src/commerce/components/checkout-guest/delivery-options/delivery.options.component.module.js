'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var deliveryOptionsComponent = require('./delivery.options.component');
var deliveryOptionsComponentService = require('./delivery.options.component.service');

var deliveryOptionsComponentModule = angular.module('deliveryOptionsComponentModule', [uiRouter])
		.component('deliveryoptionscomponent', deliveryOptionsComponent)
		.service('deliveryOptionsComponentService', deliveryOptionsComponentService)

module.exports = deliveryOptionsComponentModule;