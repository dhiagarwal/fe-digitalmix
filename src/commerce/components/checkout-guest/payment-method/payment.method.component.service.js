'use strict';

paymentMethodComponentService.$inject = ['$http','$q'];

function paymentMethodComponentService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	$http.get('../../../../../src/commerce/components/checkout-guest/payment-method/payment.method.component.json').then(function(response) {
    	deferred.resolve(response.data);
  	});

  this.getFormdata = function() {
    return deferred.promise;
  }
};

module.exports = paymentMethodComponentService;
