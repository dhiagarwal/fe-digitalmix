'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var paymentMethodComponent = require('./payment.method.component');
var paymentMethodComponentService = require('./payment.method.component.service');
var formUtils = require('../../../../__shared/form-utils/form-utils'); 

var paymentMethodComponentModule = angular.module('paymentMethodComponentModule', [uiRouter, formUtils.name])
		.component('paymentmethodcomponent', paymentMethodComponent)
		.service('paymentMethodComponentService', paymentMethodComponentService);

module.exports = paymentMethodComponentModule;