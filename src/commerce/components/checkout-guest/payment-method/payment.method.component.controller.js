'use strict';
var _ = require('lodash');
paymentMethodController.$inject=["paymentMethodComponentService", "$uibModal"];
function paymentMethodController(paymentMethodComponentService , $uibModal){
	this.paymentMethodData = {};
	var model = this.model = {};
	var self = this;
	model.paymentMethod = {};

	model.heading1 = "RONALD SISK";
	model.mainHeading = "CHECKOUT";

	model.heading2 = "PAYMENT METHOD";

	var promise = paymentMethodComponentService.getFormdata();

	promise.then(function(response){
		self.model.paymentMethod.data = response.paymentMethod.data;
		self.model.paymentMethod.fields = response.paymentMethod.fields;
		self.model.paymentMethod.states = response.paymentMethod.states;
		self.model.paymentMethod.zipcodeLookUp = response.paymentMethod.zipcodeLookUp;
		self.model.paymentMethod.creditCardTypes = response.paymentMethod.creditCardTypes;
	});
	self.isMobileDevice = (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || (navigator.userAgent.match('Android'))
	self.saveAndContinue = function(form) {
		this.touchAllFields(form);
		if (form.$valid) {
			window.location.href = "#/checkout/orderreview";
		}
	};

	self.touchAllFields= function(form) {
		_.forEach(form.$error.required, function(field){
			field.$setTouched();
		});
	};

	self.gotoPage = function(pageName) {
		window.location.href = pageName;
	};

	self.goBack = function() {
		window.location.href = "#/checkout/deliveryoptions";
	};
    self.gotoPage = function(pageName) {
        window.location.href = pageName;
        window.location.reload();
    };

	self.applyPromoCode = function(code) {
        if(!code){
            self.model.err = true;
            return;
        }
        if(_.includes(self.model.paymentMethod.data.promotionAndRewardsApplied, code)){
            self.model.promoCode = '';
            return;
        }
        self.model.paymentMethod.data.promotionAndRewardsApplied.push(code);
        self.model.promoCode = '';
	};

	self.removePromoCode = function(promoCode) {
        _.pull(self.model.paymentMethod.data.promotionAndRewardsApplied, promoCode)
    };

	self.togglePanel = function() {
		if (window.innerWidth < 1024) {
			switch(self.model.isPullLabelVisible) {
				case false:
				angular.element('body').css({'background': '', 'overflow': ''});
				angular.element('input,select').removeAttr('disabled');
				angular.element('.checkout-title-container').closest('.content').css('opacity', '');
				angular.element('.accordion-panel').css({'height': '', 'min-height': '', 'overflow': ''});
				angular.element('.overlay-checkout').remove();
				angular.element('.overlay-footer').remove();
				break;
				case true:
				angular.element('body').css({'background': 'black', 'overflow': 'hidden'});
				angular.element('input,select').attr('disabled', 'true');
				angular.element('.shopping-cart-top').find('input').attr('disabled', false);
				angular.element('.checkout-title-container').closest('.content').css('opacity', '0.4');
				angular.element('.accordion-panel').css({'max-height': '27.125rem', 'min-height': '23.4375rem', 'overflow-y': 'auto', 'overflow-x': 'hidden'});
				var docHeight = angular.element(document).height();
				angular.element('.checkout-title-container').append("<div class='overlay-checkout'></div>");
				angular.element(".overlay-checkout").height(docHeight);
				angular.element('.footer-details').append("<div class='overlay-footer'></div>");
				docHeight = angular.element(".footer-details").outerHeight();
				angular.element(".overlay-footer").height(docHeight);
				break;
			}
		}
	};

	self.zipcodeLookup = function() {
		var flag = true, foundData = null;
		angular.forEach(self.model.paymentMethod.zipcodeLookUp, function(value, key) {
			if (flag && value.zipcode == self.model.paymentMethod.data.zipcode) {
				foundData = value;
				flag = false;
			}
		});
		if (foundData) {
			self.model.paymentMethod.data.zipcode = foundData.zipcode;
			self.model.paymentMethod.data.state = foundData.state;
			self.model.paymentMethod.data.city = foundData.city;
			self.model.paymentMethod.data.addressLine1 = foundData.addressLine1;
			self.model.paymentMethod.data.addressLine2 = foundData.addressLine2;
		}
	};

	self.reposition = function() {
		var modal = angular.element(this),
		dialog = modal.find('.modal-dialog');
		modal.css('display', 'block');
		dialog.css("margin-top", Math.max(0, (angular.element(window).height() - dialog.height()) / 2));
	};

	angular.element('.modal').on('show.bs.modal', self.reposition);
	angular.element(window).on('resize', function() {
		angular.element('.modal:visible').each(self.reposition);
	});

	self.clearCardError = function () {
		this.invalidCardError = false;
	}

	self.togglePaymentSection = function (source, target, event) {
		angular.element(source).fadeIn(1000);
		angular.element(target).fadeOut(0);
	};

	self.getLastFourDigitsOfPaymentCard = function (input) {
		if (parseInt(input)) {
			var truncatedCardNumber = "";
			for (var i = input.length - 4; i < input.length; i++) {
				truncatedCardNumber += input[i];
			}
			return truncatedCardNumber;
		}
	};

	self.checkCCMonth = function(input, event) {
		if(input && input.indexOf('/') < 0) {
			self.model.paymentMethod.data.expiry = input.substr(0, 2) + '/' + input.substr(2, 2);
		}
        if (input) {
            var cardExpDate = self.model.paymentMethod.data.expiry.substr(0, 2) + "/" + self.model.paymentMethod.data.expiry.substr(self.model.paymentMethod.data.expiry.length - 2); // mm/yy format
            var expdatearr = cardExpDate.split("/");
            var currentYear = +(new Date().getFullYear() + "").substr(2);
            var currentMonth = +(new Date().getMonth() + 1);

            if (+expdatearr[0] > 12 || +expdatearr[0] < 1) {
                self.model.showCCExpError = true;
            } else if(+expdatearr[1] < currentYear){
                self.model.showCCExpError = true;
            } else if(+expdatearr[0] < currentMonth && +expdatearr[1] <= currentYear){
            	self.model.showCCExpError = true;
            }else {
            	self.model.showCCExpError = false;
            }
        }
		if(!self.model.showCCExpError) {
			self.checkCreditCardType(event)
		}
    };

	self.checkCreditCardType = function(event) {
		var isGetCreditCardType = self.model.paymentMethod.data.name && self.model.paymentMethod.data.expiry && self.model.paymentMethod.data.cvv && self.model.paymentMethod.data.paymentCardNumber;
		if (isGetCreditCardType && !self.model.showCCExpError) {
			switch(self.GetCardType(self.model.paymentMethod.data.paymentCardNumber)) {
				case self.model.paymentMethod.creditCardTypes.visa:
				self.model.creditCardType = self.model.paymentMethod.creditCardTypes.visa;
				self.invalidCardError = false;
				break;

				case self.model.paymentMethod.creditCardTypes.mastercard:
				self.model.creditCardType = self.model.paymentMethod.creditCardTypes.mastercard;
				self.invalidCardError = false;
				break;

				case self.model.paymentMethod.creditCardTypes.discover:
				self.model.creditCardType = self.model.paymentMethod.creditCardTypes.discover;
				self.invalidCardError = false;
				break;

				case self.model.paymentMethod.creditCardTypes.amex:
				self.model.creditCardType = self.model.paymentMethod.creditCardTypes.amex;
				self.invalidCardError = false;
				break;

				default:
				self.model.creditCardType = self.model.paymentMethod.creditCardTypes.other;
				self.invalidCardError = true;
				break;
			}
			self.togglePaymentSection('.payment-section-validated', '.payment-section',event);
		}
	};

	self.GetCardType = function(number)
	{
    // visa
    var re = new RegExp("^4");
    if (number.match(re) != null)
    	return self.model.paymentMethod.creditCardTypes.visa;

    // Mastercard
    re = new RegExp("^5[1-5]");
    if (number.match(re) != null)
    	return self.model.paymentMethod.creditCardTypes.mastercard;

    // AMEX
    re = new RegExp("^3[47]");
    if (number.match(re) != null)
    	return "AMEX";

    // Discover
    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
    if (number.match(re) != null)
    	return self.model.paymentMethod.creditCardTypes.discover;

    // Diners
    re = new RegExp("^36");
    if (number.match(re) != null)
    	return "Diners";

    // Diners - Carte Blanche
    re = new RegExp("^30[0-5]");
    if (number.match(re) != null)
    	return "Diners - Carte Blanche";

    // JCB
    re = new RegExp("^35(2[89]|[3-8][0-9])");
    if (number.match(re) != null)
    	return "JCB";

    // Visa Electron
    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
    if (number.match(re) != null)
    	return "Visa Electron";

    return null;
}
	this.isAmexCard = false;
};

paymentMethodController.prototype.toggleErr = function(){
    this.model.err = false;
};

module.exports  = paymentMethodController;
