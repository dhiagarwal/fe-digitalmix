'use strict';

orderConfirmationComponentService.$inject = ['$http','$q'];

function orderConfirmationComponentService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	$http.get('../../../../../src/commerce/components/checkout-guest/order-confirmation/order.confirmation.component.json').then(function(response) {
    	deferred.resolve(response.data);
  	});

  this.getFormdata = function() {
    return deferred.promise;
  }
};

module.exports = orderConfirmationComponentService;