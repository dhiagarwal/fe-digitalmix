'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var orderConfirmationComponent = require('./order.confirmation.component');
var orderConfirmationComponentService = require('./order.confirmation.component.service');

var orderConfirmationComponentModule = angular.module('orderConfirmationComponentModule', [uiRouter])
		.component('orderconfirmationcomponent', orderConfirmationComponent)
		.service('orderConfirmationComponentService', orderConfirmationComponentService)

import Styles from '../checkout.guest.component.scss';   

module.exports = orderConfirmationComponentModule;