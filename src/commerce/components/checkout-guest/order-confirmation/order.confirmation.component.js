'use strict';
var template = require('./order.confirmation.component.jade');
var controller = require('./order.confirmation.component.controller');

var orderConfirmationComponent = {
	restrict: 'E',
	templateUrl: template,
	controller: controller,
	controllerAs: 'ctrl',
	bindToController: true
};

module.exports = orderConfirmationComponent;