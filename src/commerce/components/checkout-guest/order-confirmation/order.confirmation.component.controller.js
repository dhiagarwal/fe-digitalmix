'use strict';

function orderConfirmationController(orderConfirmationComponentService) {
	this.orderConfirmationData = {};
	var model = this.model = {};
	var self = this;

	model.orderConfirmation = {};
	this.avatarImage = '../../../../src/assets/images/css/avatar.png';

	var promise = orderConfirmationComponentService.getFormdata();

	promise.then(function(response) {
		self.model.orderConfirmation.data = response.orderConfirmation.data;
		self.model.orderConfirmation.fields = response.orderConfirmation.fields;
	});

	self.viewOrPrint = function() {

	};

	self.createAccount = function() {

	};
}

module.exports = orderConfirmationController;
