var angular = require('angular');
var uiRouter =require('angular-ui-router');
var landingPageBanner = require('./landing.page.banner');
var landingPageBannerModule = angular.module('landingPageBannerModule', [uiRouter])
    .component('landingPageBanner', landingPageBanner)

import Styles from './landing.page.banner.scss';

module.exports = landingPageBannerModule;
