'use strict';

var template = require('./landing.page.banner.jade');
var controller = require('./landing.page.banner.controller');

var landingPageBanner =  {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = landingPageBanner;
