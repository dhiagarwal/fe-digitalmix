'use strict';

featuredCategoriesService.$inject = ['$http','$q'];

function featuredCategoriesService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/featured-categories/featured.categories.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getFeaturedCategoriesData = function() {
    return deferred.promise;
  }

};

module.exports = featuredCategoriesService;