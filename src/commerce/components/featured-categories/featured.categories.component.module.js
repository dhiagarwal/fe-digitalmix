'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var featuredCategoriesComponent = require('./featured.categories.component');
var featuredCategoriesService = require('./featured.categories.service');

var featuredCategoriesComponentModule = angular.module('featuredCategoriesComponentModule', [])
                  .component('featuredcategoriescomponent',featuredCategoriesComponent)
                  .service('featuredCategoriesService', featuredCategoriesService);
                  
import Styles from './featured.categories.component.scss';                      

module.exports = featuredCategoriesComponentModule;