'use strict';

function featuredCategoriesComponentController(featuredCategoriesService) {

    var self = this;
    this.bannerTitleValue="LONG HEADLINE CONTINUES HERE PART OF HEADLINE HERE TOO";
    this.bannerTextValue = "The main body of a book or other piece of writing, as distince from other material such as notes, appendiced, and illustrations. The pictures are clear and relate well to the text";
    var getData = featuredCategoriesService.getFeaturedCategoriesData();
    getData.then(function(data){
        self.featuredCategoriesData = data;
  });
};

module.exports = featuredCategoriesComponentController;