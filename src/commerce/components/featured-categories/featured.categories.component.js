'use strict';
var template = require('./featured.categories.component.jade');
var controller = require('./featured.categories.component.controller');

var featuredCategoriesComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'featuredcategories'
};

module.exports = featuredCategoriesComponent;