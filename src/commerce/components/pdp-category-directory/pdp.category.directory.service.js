'use strict';

pdpCategoryDirectoryService.$inject = ['$http','$q'];

function pdpCategoryDirectoryService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-category-directory/pdp.category.directory.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getCategoryDirectoryData = function() {
    return deferred.promise;
  }

};

module.exports = pdpCategoryDirectoryService;
