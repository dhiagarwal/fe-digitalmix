var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-ui-bootstrap');
var pdpCategoryDirectoryComponent = require('./pdp.category.directory.component');
var pdpCategoryDirectoryService = require('./pdp.category.directory.service');

var pdpCategoryDirectoryModule = angular.module('pdpCategoryDirectoryModule', [uiRouter, 'ui.bootstrap'])
.component('pdpcategorydirectory', pdpCategoryDirectoryComponent)
.service('pdpCategoryDirectoryService', pdpCategoryDirectoryService);

import Styles from './pdp.category.directory.scss';

module.exports = pdpCategoryDirectoryModule;
