'use strict';
var template = require('./pdp.category.directory.jade');
var controller = require('./pdp.category.directory.controller');

var pdpCategoryDirectoryComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = pdpCategoryDirectoryComponent;
