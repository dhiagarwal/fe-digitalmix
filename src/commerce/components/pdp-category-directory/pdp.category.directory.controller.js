'use strict';

function pdpCategoryDirectoryController(pdpCategoryDirectoryService,$timeout,$interval) {
  var self = this;
  this.pageHeadline = "Nature + Science";
  this.pageShortDescription = "Short description messaging about ingredients. Short description messaging about ingredients. Short description messaging about ingredients.";
  this.pageCategory = "Nutrition";
  this.pageSubCategory = "Ingredients";
  this.bannerUrl = '../../../../src/assets/images/content/landing-page-directory-banner.png';
  this.watchVideoUrl = '../../../../src/assets/images/content/watch-video-banner.png';
  this.videoURL="../../../../src/assets/videos/Testimonial-Video.mp4";
  this.videoPlaying = false;
  this.isFirstOpen = false;
  this.togglePlay = function() {
    var video = angular.element.find('#video-category-directory');
    if (video[0].paused) {
        $timeout(function(){video[0].play();},10)
        video[0].controls=true;
        this.videoPlaying = true;
        angular.element(".watch-video-panel").css("background-image","");
    } else {
        $timeout(function(){video[0].pause();},10)
        video[0].controls=false;
        this.videoPlaying = false;
        angular.element(".watch-video-panel").css("background-image","url("+this.watchVideoUrl+")");
    }
  }
  var getData = pdpCategoryDirectoryService.getCategoryDirectoryData();
  getData.then(function(data){
      self.groups = data;
  });
  this.onVideoEnd = function() {
    var video = angular.element.find('#video-category-directory');
    video[0].pause();
  };
  if(window.innerWidth < 768){
    this.isFirstOpen = true;
  }
};

module.exports = pdpCategoryDirectoryController;
