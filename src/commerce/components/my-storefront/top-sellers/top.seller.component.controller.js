'use strict';

function topSellerComponentController(topSellerComponentService) {

    this.topSellerProducts = [];
    var self = this;

    var promise = topSellerComponentService.getTopSellerProducts();
    promise.then(function (data) {
        self.topSellerProducts = data;
        angular.forEach(self.topSellerProducts,function(value,key){
        	value.productRatingsGreen = [];
        	value.productRatingsWhite = [];
	    	var nGreenStars = value.rating;
	    	var nWhiteStars = 5 - value.rating; 
	    	for(var i=0; i<nGreenStars; i++) {
	    		value.productRatingsGreen[i] = "../../../../src/assets/images/css/star.png";
	    	}
	    	for(var j=0; j<nWhiteStars; j++) {
	    		value.productRatingsWhite[j] = "../../../../src/assets/images/css/ic-star-outline.png";
	    	}

	    	self.topSellerProductsXl = angular.copy(self.topSellerProducts);
	    	self.topSellerProductsXl.splice(0,1);
	    })
	    console.log(self.topSellerProducts);
    });
    


};

module.exports = topSellerComponentController;