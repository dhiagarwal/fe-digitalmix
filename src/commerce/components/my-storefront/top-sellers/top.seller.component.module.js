'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var topSellerComponent = require('./top.seller.component');
var topSellerComponentService = require('./top.seller.component.service');

var topSellerComponentModule = angular.module('topSellerComponentModule', [uiRouter])
                  .component('topsellercomponent', topSellerComponent)
                  .service('topSellerComponentService', topSellerComponentService)

import Styles from './top.seller.component.scss';

module.exports = topSellerComponentModule;
