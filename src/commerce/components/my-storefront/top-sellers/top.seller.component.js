'use strict';
var template = require('./top.seller.component.jade');
var controller = require('./top.seller.component.controller');


var topSellerComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'vm'
};

module.exports = topSellerComponent;
