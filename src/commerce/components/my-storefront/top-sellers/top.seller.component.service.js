'use strict';

topSellerComponentService.$inject = ['$http','$q'];

function topSellerComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/my-storefront/top-sellers/top.seller.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getTopSellerProducts = function() {
    return deferred.promise;
  }

};

module.exports = topSellerComponentService;
