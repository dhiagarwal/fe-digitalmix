'use strict';
var controller = require('./page.leave.confirmation.component.controller');

var pageLeaveConfirmationComponent = {
	controller: controller,
	controllerAs: 'ctrl',
    bindings: {
        isModalCall: '@',
		callbackOnSuccess: '@',
		booleanVariableToInvokeModal: '@'
    }
};

module.exports = pageLeaveConfirmationComponent;