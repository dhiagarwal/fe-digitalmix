'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pageLeaveConfirmationComponent = require('./page.leave.confirmation.component');
var formUtils = require('../../../../../../src/__shared/form-utils/form-utils');
require('../storefront.modal.scss');

var pageLeaveConfirmationComponentModule = angular.module('pageLeaveConfirmationComponentModule', [uiRouter, formUtils.name])
		.component('pageleaveconfirmationcomponent', pageLeaveConfirmationComponent)

module.exports = pageLeaveConfirmationComponentModule;