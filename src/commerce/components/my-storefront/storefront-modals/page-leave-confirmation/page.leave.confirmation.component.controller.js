'use strict';
var modalVerificationTemplate = require('./page.leave.confirmation.component.jade');

function pageLeaveConfirmationController($scope, $uibModal) {
    this.$uibModal = $uibModal;
    var self = this;

    self.modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };

    self.pageLeaveConfirmationModalController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.closeAndReset = function () {
            var resetBooleanVariable = '$scope.$parent.' + self.booleanVariableToInvokeModal + ' = false;';
            eval(resetBooleanVariable);
            modalSelf.cancel();
        };

        modalSelf.proceedAhead = function () {
            modalSelf.closeAndReset();
            var setCallBackMethod = '$scope.$parent.' + self.callbackOnSuccess + '()';
            eval(setCallBackMethod);
        };

        modalSelf.saveProductDetails = function () {
            // AEM should write the required service here to perform data updation.
            modalSelf.closeAndReset();
        };
    };

    if (self.isModalCall) {
        self.modalPopupInstance(modalVerificationTemplate, self.pageLeaveConfirmationModalController, 'app-modal-window page-leave-confirmation-popup storefront-modal');
    }
};

module.exports = pageLeaveConfirmationController;
