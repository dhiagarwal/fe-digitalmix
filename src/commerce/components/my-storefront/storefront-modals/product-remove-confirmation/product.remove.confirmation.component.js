'use strict';
var controller = require('./product.remove.confirmation.component.controller');

var productRemoveConfirmationComponent = {
	controller: controller,
	controllerAs: 'ctrl',
    bindings: {
        isModalCall: '@',
		toBeDeletedProductData: '=',
		callbackOnSuccess: '@',
		booleanVariableToInvokeModal: '@',
        parentPageName: '=',
        scrollToId: '='
    }
};

module.exports = productRemoveConfirmationComponent;