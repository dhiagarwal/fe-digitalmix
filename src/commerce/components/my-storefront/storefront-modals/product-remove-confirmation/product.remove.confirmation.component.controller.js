'use strict';
var modalVerificationTemplate = require('./product.remove.confirmation.component.jade');

productRemoveConfirmationController.inject = ["$scope", "$uibModal", "$location", "$anchorScroll"];
function productRemoveConfirmationController($scope, $uibModal, $location, $anchorScroll) {
    this.$uibModal = $uibModal;
    var self = this;

    self.scrollToASection = function (targetId) {
        $location.hash(targetId);
        $anchorScroll();
    };

    self.modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };

    self.productRemoveConfirmationModalController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.toBeDeletedProductData = self.toBeDeletedProductData;
        modalSelf.parentPageName = self.parentPageName;
        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.closeAndReset = function () {
            var resetBooleanVariable = '$scope.$parent.' + self.booleanVariableToInvokeModal + ' = false;';
            eval(resetBooleanVariable);
            modalSelf.cancel();
            self.scrollToASection(self.scrollToId);
        };

        modalSelf.removeProduct = function () {
            modalSelf.closeAndReset();
            var setCallBackMethod = '$scope.$parent.' + self.callbackOnSuccess + '(modalSelf.toBeDeletedProductData)';
            eval(setCallBackMethod);
            self.scrollToASection(self.scrollToId);            
        };
    };

    if (self.isModalCall) {
        self.modalPopupInstance(modalVerificationTemplate, self.productRemoveConfirmationModalController, 'app-modal-window product-remove-confirmation-popup storefront-modal');
    }
};

module.exports = productRemoveConfirmationController;