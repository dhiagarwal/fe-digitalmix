'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var productRemoveConfirmationComponent = require('./product.remove.confirmation.component');
var formUtils = require('../../../../../../src/__shared/form-utils/form-utils');
require('../storefront.modal.scss');

var productRemoveConfirmationComponentModule = angular.module('productRemoveConfirmationComponentModule', [uiRouter, formUtils.name])
		.component('productremoveconfirmationcomponent', productRemoveConfirmationComponent)

module.exports = productRemoveConfirmationComponentModule;