'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var storefrontBundleComponent = require('./storefront.bundle.component');
var storefrontBundleComponentService = require('./storefront.bundle.component.service');

var storefrontBundleComponentModule = angular.module('storefrontBundleComponentModule', [uiRouter])
.component('mystorefrontbundlecomponent', storefrontBundleComponent)
.service('storefrontBundleComponentService', storefrontBundleComponentService);


import Styles from './storefront.bundle.component.scss';

module.exports = storefrontBundleComponentModule;