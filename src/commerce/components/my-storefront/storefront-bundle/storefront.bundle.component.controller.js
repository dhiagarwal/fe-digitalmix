'use strict';

function storefrontBundleController(storefrontBundleComponentService) {
    var self = this;
    var model = self.model = {};
    self.bundleData = {};

    var promise = storefrontBundleComponentService.getBundleData();
    promise.then(function(data) {
        self.bundleData = data.productList;
    });


};

module.exports = storefrontBundleController;