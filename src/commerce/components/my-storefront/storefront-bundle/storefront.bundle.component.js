'use strict';
var template = require('./storefront.bundle.component.jade');
var controller = require('./storefront.bundle.component.controller');

var storefrontBundleComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl'
};

module.exports = storefrontBundleComponent;
