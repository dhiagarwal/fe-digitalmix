'use strict';

storefrontBundleComponentService.$inject = ['$http','$q'];

function storefrontBundleComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/my-storefront/storefront-bundle/storefront.bundle.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getBundleData = function() {
    return deferred.promise;
  }

};

module.exports = storefrontBundleComponentService;