'use strict';

function storefrontOverviewController($window, $timeout) {
	var self=this;
    self.isSubDomainAvailable = false;
    self.showSubDomainMsg = false;
    
    self.checkSubDomain = function(){
        if(self.webAddress) {
            self.showSubDomainMsg = true;
            if(self.webAddress=='distributor') {
                self.isSubDomainAvailable = false;
                return false;
            }
            else {
                self.isSubDomainAvailable = true;
                $timeout(function () {
                  $window.location.href = "#/myStorefront/setup";
                }, 3000)
            }
        }
        else {
            self.isSubDomainAvailable = false;
            self.showSubDomainMsg = false;
            return false;
        }
    };
};

module.exports = storefrontOverviewController;