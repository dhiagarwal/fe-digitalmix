'use strict';
var template = require('./storefront.overview.component.jade');
var controller = require('./storefront.overview.component.controller');

var storefrontOverviewComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl'
};

module.exports = storefrontOverviewComponent;
