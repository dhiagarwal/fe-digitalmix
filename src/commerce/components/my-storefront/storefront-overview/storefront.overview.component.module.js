'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var myStorefrontOverviewComponent = require('./storefront.overview.component');

var myStorefrontOverviewComponentModule = angular.module('myStorefrontOverviewComponentModule', [uiRouter])
.component('mystorefrontoverviewcomponent', myStorefrontOverviewComponent)


import styles from './storefront.overview.component.scss';

module.exports = myStorefrontOverviewComponentModule;