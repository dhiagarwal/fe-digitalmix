'use strict';

function storefrontSetupController($window, scrollToTopWithoutClickService) {
    var self = this;
    self.currentState = 1;

    self.errorData = {};
    self.isDisplayErrorMessage = false;

    self.states = [{
        state: 1,
        name: "MESSAGES"
    }, {
        state: 2,
        name: "FEATURED PRODUCTS"
    }, {
        state: 3,
        name: "PROMOTION & BUNDLE"
    }, {
        state: 4,
        name: "REVIEW"
    }];
    self.goToState = function(state) {
        if (state > self.currentState) {
            return false;
        } else {
            self.currentState = state;
        }
    };

    self.save = function() {
        scrollToTopWithoutClickService.scrollToTop();
        if (self.currentState < 4)
            self.currentState++;
    };

    self.back = function() {
        scrollToTopWithoutClickService.scrollToTop();
        if (self.currentState > 1)
            self.currentState--;
        else {
            $window.location.href = "#/myStorefront/overview";
        }
    };

    self.executeLeavePopup = function(clickedState) {
        self.isBackLinkClicked = true;
    };

    self.showErrorSection = function(data) {
        self.errorData = data;
        self.isDisplayErrorMessage = true;
    };
};

module.exports = storefrontSetupController;
