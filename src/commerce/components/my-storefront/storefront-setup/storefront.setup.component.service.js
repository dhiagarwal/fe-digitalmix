'use strict';

storefrontSetupService.$inject = ['$http','$q'];

function storefrontSetupService($http,$q) {
  this.$http = $http;
  this.$q = $q;
};

storefrontSetupService.prototype.getData = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../../src/commerce/components/my-storefront/storefront-setup/storefront.setup.json')
        .success(deferred.resolve);

    return deferred.promise;
};


module.exports = storefrontSetupService;