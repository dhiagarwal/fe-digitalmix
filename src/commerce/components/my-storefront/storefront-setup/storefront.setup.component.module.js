'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var myStorefrontSetupComponent = require('./storefront.setup.component');
var storefrontSetupService = require('./storefront.setup.component.service');
var pageLeaveConfirmationModal = require('../../../../../src/commerce/components/my-storefront/storefront-modals/page-leave-confirmation/page.leave.confirmation.component.module');
var scrollToTopWithoutClickService = require('../../../../../src/__shared/scroll-to-top-without-click/scroll-to-top-without-click.service');


var myStorefrontSetupComponentModule = angular.module('myStorefrontSetupComponentModule', [uiRouter, pageLeaveConfirmationModal.name])
.component('mystorefrontsetupcomponent', myStorefrontSetupComponent)
.service('storefrontSetupService', storefrontSetupService)
.service('scrollToTopWithoutClickService', scrollToTopWithoutClickService);


import Styles from './storefront.setup.component.scss';

module.exports = myStorefrontSetupComponentModule;