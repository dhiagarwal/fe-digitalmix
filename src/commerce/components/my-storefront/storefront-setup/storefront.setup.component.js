'use strict';
var template = require('./storefront.setup.component.jade');
var controller = require('./storefront.setup.component.controller');

var storefrontSetupComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindings:{
        save: '&'
    }
};

module.exports = storefrontSetupComponent;
