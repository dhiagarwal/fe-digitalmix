'use strict';

mystorefrontProductService.$inject = ['$http', '$q'];

function mystorefrontProductService($http, $q) {
    this.$http = $http;
    this.addedProductList = {};
    this.bundlesModelData = {};
    this.selectedProductType = null;
    var deferred = $q.defer();
    $http.get('../../../../../src/commerce/components/my-storefront/storefront-products/storefront.products.component.json').then(function (response) {
        deferred.resolve(response.data);
    });

    this.getJsonData = function () {
        return deferred.promise;
    };

    this.getArrangedProductList = function () {
        return this.addedProductList;
    };

    this.setArrangedProductList = function (value) {
        this.addedProductList = value;
    };

    this.getBundlesModelData = function () {
        return this.bundlesModelData;
    };

    this.setBundlesModelData = function (value) {
        this.bundlesModelData = value;
    };

    this.getSelectedProductType = function () {
        return this.selectedProductType;
    };

    this.setSelectedProductType = function (value) {
        this.selectedProductType = value;
    };

    this.scrollToTop = function (selector) {
        var selector = selector ? selector : "html, body";
        angular.element(selector).animate({
            scrollTop: 0
        }, 400);
    };

    this.calculateTotal = function(targetArray) {
		var totalCost = 0;
        for (var key in targetArray) {
			totalCost += parseFloat(targetArray[key].cost);            
        }
		return totalCost;
	};
};

module.exports = mystorefrontProductService;