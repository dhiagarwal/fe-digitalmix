'use strict';

function storefrontProductsController(mystorefrontProductService, mystorefrontProductSetupService, myStorefrontEditableReviewComponentService) {
    var self = this;
    self.mainHeading = "CHOOSE PRODUCTS";
    self.subHeading = "You can either use your country’s best sellers or select your own products.";
    self.minimumProductsAllowed = 1;
    self.maximumProductsAllowed = 12;  
    self.defaultProductSelectionValue = 1;
	self.customProductSelectionValue = 2;  

    self.model = {};

    self.model.tileOne = {
        "heading": "Show Default Products",
        "subHeading": "Show the top 12 sellers of my country."
    };
    self.model.tileTwo = {
        "heading": "Select Custom Products",
        "subHeading": "Choose up to 12 products to feature on your store."
    };
    self.errorData = {
        "errorMessage": null,
        "showInfoIconOnError": true,
        "isDisplayErrorMessage": true
    };

    self.mimimumProductsSelectionError = function (errorMessage) {
        self.errorData.errorMessage = errorMessage;
        self.error({ "data": self.errorData });
        mystorefrontProductService.scrollToTop();
    };

    self.checkProductTypeSelection = function() {
        self.selectedProductType = mystorefrontProductService.getSelectedProductType();
        if(self.selectedProductType) {
            mystorefrontProductService.setSelectedProductType(self.selectedProductType);
        }  
    };

    self.checkProductTypeSelection();

    self.saveAndContinue = function () {
        self.model.selectedProductList = mystorefrontProductService.getArrangedProductList();
        self.selectedProductType = mystorefrontProductService.getSelectedProductType();
        if (self.selectedProductType == self.customProductSelectionValue && (!self.model.selectedProductList || !self.model.selectedProductList.length)) {
            self.mimimumProductsSelectionError("Sorry. You need at least one item to proceed ahead.");
        }
        else if (!self.model.selectedProductList || !self.model.selectedProductList.length) {
            self.errorData.errorMessage = "Sorry. You need to select at one product type to proceed ahead.";
            self.error({ "data": self.errorData });
            mystorefrontProductService.scrollToTop();
        }
        else if (self.model.selectedProductList && self.model.selectedProductList.length < self.minimumProductsAllowed) {
           self.mimimumProductsSelectionError("Sorry. You need at least one item to proceed ahead.");
        }
        else if (self.model.selectedProductList && self.model.selectedProductList.length > self.maximumProductsAllowed) {
            self.errorData.errorMessage = "Sorry. You cannot add more than " + self.maximumProductsAllowed + " items.";
            self.error({ "data": self.errorData });
            mystorefrontProductService.scrollToTop();
        }
        else {
            self.error({ "data": null });
            myStorefrontEditableReviewComponentService.setOriginalProductList(self.model.selectedProductList);
            self.save();
        }
    };

    self.saveAndPublish = function() {
        self.saveAndContinue();     
    };

    self.revertAndCancel = function() {
        self.cancel();
    };

    var promise = mystorefrontProductSetupService.getData();
    promise.then(function (response) {
		self.model.defaultProductsList = response.productsList.defaultProductsList;
	});
};

module.exports = storefrontProductsController;