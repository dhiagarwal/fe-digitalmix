'use strict';
var template = require('./storefront.products.component.jade');
var controller = require('./storefront.products.component.controller');

var storefrontProductsComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
   bindings:{
        save: '&',
        error: '&',
        isInlineEdit: '=',
        cancel: '&'
    }
};

module.exports = storefrontProductsComponent;
