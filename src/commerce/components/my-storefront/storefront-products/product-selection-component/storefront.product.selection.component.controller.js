'use strict';

var addProductsModalTemplate = require('./storefront.product.selection.modal.jade');

storefrontProductSelectionController.$inject = ['mystorefrontProductSetupService', 'mystorefrontProductService', '$uibModal', 'myStorefrontProductsSelectionService', '$filter', '$document', '$timeout', 'myStorefrontEditableReviewComponentService', 'globalServicesService'];

function storefrontProductSelectionController(mystorefrontProductSetupService, mystorefrontProductService, $uibModal, myStorefrontProductsSelectionService, $filter, $document, $timeout, myStorefrontEditableReviewComponentService, globalServicesService) {

	var self = this;

	self.model = {
		"bundlesSection": {
            "description": "Lorem ipsum dolor sit amet, ex eam dictas melius laboramus, id vel mazim qualisque posidonium. Pri an brute temporibus appellantur, qui error fierent et, et mei homero nostrud."
        }
	};

	self.dragControlListeners = {
        allowDuplicates: false
	};

	self.defaultProductSelectionValue = 1;
	self.customProductSelectionValue = 2;
	self.model.selectedProducts = [];
	self.parentPageName = "Product";
	self.scrollToId = "products-container";

	var promise = mystorefrontProductSetupService.getData();

	promise.then(function (response) {
		self.model.defaultProductsList = response.productsList.defaultProductsList;
		if(self.isBundlesPage && self.isInlineEdit) {
			myStorefrontEditableReviewComponentService.setBundlesPageBoolean(true);
		}
		self.isBundlesPageCall = self.isBundlesPage || myStorefrontEditableReviewComponentService.getBundlesPageBoolean();
		self.initProductSelection();
		self.setUpOnload();
	});

	var modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };

    self.addProductsController = ['$uibModalInstance', function ($uibModalInstance) {

		var _ = require('lodash');
        var modalSelf = this;
        modalSelf.model = self.model;

		modalSelf.model.searchedProductsList = modalSelf.model.customProductsList ? modalSelf.model.customProductsList : mystorefrontProductService.getArrangedProductList();
		if(!modalSelf.model.searchedProductsList) {
			modalSelf.model.searchedProductsList = new Array();
		}
		modalSelf.model.selectedProducts = modalSelf.model.searchedProductsList ? modalSelf.model.searchedProductsList : null;
		if(self.isBundlesPageCall) {
			modalSelf.model.selectedProducts = globalServicesService.shortenArray(modalSelf.model.selectedProducts, self.maximumProductsTarget);
		}

		$timeout(function() {
			var headerHeight = document.getElementById('searchcontainer').innerHeight;
			angular.element('#modal-page-container').css({'top': headerHeight/16 + 'rem'});
		}, 0);
		
        modalSelf.isError = false;
		modalSelf.errorData = {
			"errorMessage": null,
			"showInfoIconOnError": true,
			"isDisplayErrorMessage": true
		};
        modalSelf.parent = self;
        modalSelf.errorMessage = "Sorry. Cannot add anymore products as you exceeded the limit of " + modalSelf.parent.maximumProductsTarget + ". Please remove products to add new.";

        var promise = myStorefrontProductsSelectionService.getTypeAheadKeys();

        promise.then(function (data) {

			modalSelf.model.keys = data;

			//code for typeahead
			modalSelf.selectedNumber = null;

			// instantiate the bloodhound suggestion engine
			modalSelf.numbers = new Bloodhound({
				datumTokenizer: function (d) {
					return Bloodhound.tokenizers.whitespace(d.Key);
				},
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: modalSelf.model.keys
			});

			modalSelf.numbers.initialize();

			// initialize the bloodhound suggestion engine
			modalSelf.numbersDataset = {
				displayKey: 'Key',
				source: modalSelf.numbers.ttAdapter()
			};

			modalSelf.exampleOptions = {
				highlight: true,
				limit: 5,
				minLength: 3
			};

		});

		modalSelf.clearValue = function () {
			modalSelf.selectedNumber = null;
		};

		modalSelf.loadSearchedProducts = function () {

			if (angular.element('.tt-open')) {
				angular.element('.tt-menu').prop('style', 'position: absolute; top: 100%; left: 0px; z-index: 100; display: none;');
			}

			angular.element('#searchbox').blur();

			var promiseSearch = mystorefrontProductSetupService.getData();

			promiseSearch.then(function (response) {
				// when actually integrated then search results will be coming from back end 
				if (modalSelf.selectedNumber[0] == '1') { // here it is assumed that id starts from 1
					modalSelf.model.searchedProductsList = $filter('filter')(response.productsList.defaultProductsList, { id: modalSelf.selectedNumber });
				} else {
					modalSelf.model.searchedProductsList = $filter('filter')(response.productsList.defaultProductsList, { name: modalSelf.selectedNumber });
				}
				modalSelf.errorDisplaySetUp();
			});
		}

        modalSelf.ok = function () {
			self.model.productList = modalSelf.model.customProductsList = modalSelf.model.selectedProducts;
			if(!self.model.productList || (self.model.productList && self.model.productList.length >= self.minimumProductsTarget)) {
				self.model.productListCostTotal = mystorefrontProductService.calculateTotal(self.model.productList);				
				self.getProductsList = true;
				if (self.isBundlesPageCall) {
					mystorefrontProductService.setBundlesModelData(self.model.bundlesSection);
				}
				mystorefrontProductService.setArrangedProductList(self.model.productList);
            	$uibModalInstance.close();
			}
			else {
				modalSelf.isError = true;
				modalSelf.errorData.errorMessage = "Sorry. You need at least " + self.minimumProductsTarget + " item(s) to proceed ahead.";
				mystorefrontProductService.scrollToTop('.app-modal-window.products-popup');
				modalSelf.errorDisplaySetUp();				
			}
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

		modalSelf.errorDisplaySetUp = function() {
			self.headerHeight = angular.element('#searchcontainer').height();
			angular.element('#modal-page-container').css({'top': (self.headerHeight/16) + 'rem'});
			self.errorHeight = angular.element('#modal-page-container').height();
			angular.element('.products-container').css({'padding-top': (self.errorHeight/16) + 'rem'});			
			$timeout(function () {
				modalSelf.isError = false;
				angular.element('#modal-page-container').css({'top': 0});
				angular.element('.products-container').css({'padding-top': 0});						
			}, 5000);
		};

        modalSelf.addProductInSelection = function (id) {
			if (modalSelf.model.selectedProducts.length < self.maximumProductsTarget) {
				modalSelf.model.currentProduct = modalSelf.findItemById(id, modalSelf.model.searchedProductsList);
				modalSelf.model.selectedProducts.push(modalSelf.model.currentProduct);
			}
			else {
				modalSelf.isError = true;
				modalSelf.errorData.errorMessage = modalSelf.errorMessage;
				mystorefrontProductService.scrollToTop('.app-modal-window.products-popup');
				modalSelf.errorDisplaySetUp();				
			}
        };

        modalSelf.removeProductFromSelection = function (id) {
			var index = modalSelf.findItemById(id, modalSelf.model.selectedProducts, "I");
			modalSelf.model.selectedProducts.splice(index, 1);
			modalSelf.model.currentProduct = {};
			if (modalSelf.model.selectedProducts.length < self.maximumProductsTarget) {
				modalSelf.isError = false;
			}
        }

        modalSelf.checkActiveClass = function (id) {
			var isFound = $filter('filter')(modalSelf.model.selectedProducts, {id: id})[0];
			if(isFound) {
				return 'active-product';
			}
			return '';
        }

        modalSelf.findItemById = function (id, arrayToBeProcessed, flag) {
			var returnItem;
			angular.forEach(arrayToBeProcessed, function (value, key) {
				if (value.id == id) {
					if (flag == "I") {
						returnItem = key;
					} else if (flag == "ID") {
						returnItem = value.id;
					}
					else {
						returnItem = value;
					}
				}
			});
			return returnItem;
		}
    }];


	self.addProduct = function () {
		modalPopupInstance(addProductsModalTemplate, self.addProductsController, 'app-modal-window products-popup');
	};

	self.initProductSelection = function() {
		self.model.productList = !self.isBundlesPageCall ? myStorefrontEditableReviewComponentService.getOriginalProductList() : myStorefrontEditableReviewComponentService.getOriginalBundlesList();
		self.model.selectedProductList = self.model.productList;
		if(self.model.selectedProductList) {
        	self.activeProduct = mystorefrontProductService.getSelectedProductType();
		}

	};

	self.productSelectionChoice = function (targetValue, isOnLoad) {
		self.activeProduct = targetValue;
		switch (targetValue) {
			case self.defaultProductSelectionValue:
				self.model.productList = self.model.defaultProductsList;
				myStorefrontEditableReviewComponentService.setProductsListTouchedStatus(self.defaultProductSelectionValue, true, self.isBundlesPageCall);				
				break;
			case self.customProductSelectionValue:
				self.model.productList = null;
				if(self.isBundlesPageCall) {
					self.model.productList = myStorefrontEditableReviewComponentService.getOriginalBundlesList();
				}
				else {
					self.model.productList = myStorefrontEditableReviewComponentService.getOriginalProductList();
				}
				myStorefrontEditableReviewComponentService.setProductsListTouchedStatus(self.customProductSelectionValue, true, self.isBundlesPageCall);								
				break;
		}
		mystorefrontProductService.setSelectedProductType(targetValue);
		self.model.productListOriginal = angular.copy(self.model.productList);
		mystorefrontProductService.setArrangedProductList(self.model.productListOriginal);
		return self.activeProduct;
	};

	self.setUpOnload = function() {
		var choice = myStorefrontEditableReviewComponentService.getProductsListTouchedStatus(self.isBundlesPageCall);		
		if(self.isInlineEdit && choice) {
			self.productSelectionChoice(choice.id, true);
		}
		else if(self.isInlineEdit) {
			self.productSelectionChoice(self.defaultProductSelectionValue, true);
		}
	};

	self.saveShortDesc = function() {
		myStorefrontEditableReviewComponentService.setShortDescOfBundles(self.model.bundlesSection.description);
	};

	self.deleteProduct = function (product) {
		self.model.productList.splice(self.model.productList.indexOf(product), 1);
		self.model.productListOriginal = angular.copy(self.model.productList);
		mystorefrontProductService.setArrangedProductList(self.model.productListOriginal);
		if(self.isBundlesPageCall) {
			mystorefrontProductService.setArrangedProductList(globalServicesService.shortenArray(self.model.productListOriginal, self.maximumProductsTarget));
			self.model.productListCostTotal = mystorefrontProductService.calculateTotal(mystorefrontProductService.getArrangedProductList());									
		}
		myStorefrontEditableReviewComponentService.setOriginalProductList(self.model.productListOriginal);
	};

	self.executeDeletePopup = function(product) {
		self.productToBeDeleted = product;
		self.isRemoveProductClicked = true;
	};

};

module.exports = storefrontProductSelectionController;
