'use strict';
var template = require('./storefront.product.selection.component.jade');
var controller = require('./storefront.product.selection.component.controller');

var storefrontProductsSelectionComponent = {
    templateUrl: template,
    bindings: {
        tileOneHeading: '=',
        tileOneSubheading: '=',
        tileTwoHeading: '=',
        tileTwoSubheading: '=',
        isBundlesPage: '=',
        minimumProductsTarget: '=',
        maximumProductsTarget: '=',
        isInlineEdit: '='
    },
    controller: controller,
    controllerAs: 'productSelectionCtrl'
};

module.exports = storefrontProductsSelectionComponent;
