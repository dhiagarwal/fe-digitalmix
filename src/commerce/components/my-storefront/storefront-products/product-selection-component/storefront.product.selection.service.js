'use strict';

mystorefrontProductSelectionService.$inject = ['$http','$q'];

function mystorefrontProductSelectionService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	$http.get('../../../../../src/commerce/components/my-storefront/storefront-products/product-selection-component/storefront.product.selection.quicksearch.json').then(function(response) {
    	deferred.resolve(response.data);
  	});

  this.getTypeAheadKeys = function() {
    return deferred.promise;
  };
  
};

module.exports = mystorefrontProductSelectionService;