'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('../../../../../../src/commerce/components/my-storefront/storefront-products/storefront.products.component.scss');
require('../../../../../../src/vendors/ng-sortable/ng-sortable.min.css');

var myStorefrontProductSelectionComponent = require('./storefront.product.selection.component');
var productRemoveConfirmationModule = require('../../../../../../src/commerce/components/my-storefront/storefront-modals/product-remove-confirmation/product.remove.confirmation.component.module');
var myStorefrontEditableReviewComponentService = require('../../../../../../src/commerce/components/my-storefront/storefront-editable-review/storefront.editable.review.component.service');
var globalServicesService = require('../../../../../../src/__shared/services/global-services/global.services.service');

var myStorefrontProductSelectionComponentModule = angular.module('myStorefrontProductSelectionComponentModule', [uiRouter, productRemoveConfirmationModule.name,  'as.sortable'])
.component('mystorefrontproductselectioncomponent', myStorefrontProductSelectionComponent)
.service('myStorefrontEditableReviewComponentService',myStorefrontEditableReviewComponentService)
.service('globalServicesService', globalServicesService);


module.exports = myStorefrontProductSelectionComponentModule;