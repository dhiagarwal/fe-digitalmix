'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('../../../../../../src/commerce/components/my-storefront/storefront-products/storefront.products.component.scss');

var myStorefrontHeadingComponent = require('./storefront.heading.component');

var myStorefrontHeadingComponentModule = angular.module('myStorefrontHeadingComponentModule', [uiRouter])
.component('mystorefrontheadingcomponent', myStorefrontHeadingComponent)

module.exports = myStorefrontHeadingComponentModule;