'use strict';
var template = require('./storefront.heading.component.jade');
var controller = require('./storefront.heading.component.controller');

var storefrontProductsHeadingComponent = {
    templateUrl: template,
    bindings: {
        mainHeading: '=',
        subHeading: '='
    },
    controller: controller,
    controllerAs: 'headingCtrl'
};

module.exports = storefrontProductsHeadingComponent;
