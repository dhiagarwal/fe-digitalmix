'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('./storefront.products.component.scss');

var myStorefrontProductsComponent = require('./storefront.products.component');

var myStorefrontHeadingComponentModule = require('../../../../../src/commerce/components/my-storefront/storefront-products/heading-component/storefront.heading.component.module');
var myStorefrontProductsSelectionComponentModule = require('../../../../../src/commerce/components/my-storefront/storefront-products/product-selection-component/storefront.product.selection.component.module');
var mystorefrontProductService = require('./storefront.products.component.service');
var mystorefrontProductSetupService = require('../../../../../src/commerce/components/my-storefront/storefront-setup/storefront.setup.component.service');
var myStorefrontProductsSelectionService = require('../../../../../src/commerce/components/my-storefront/storefront-products/product-selection-component/storefront.product.selection.service');
var myStorefrontEditableReviewComponentService = require('../../../../../src/commerce/components/my-storefront/storefront-editable-review/storefront.editable.review.component.service');

var myStorefrontProductsComponentModule = angular.module('myStorefrontProductsComponentModule', [uiRouter, myStorefrontHeadingComponentModule.name, myStorefrontProductsSelectionComponentModule.name])
.component('mystorefrontproductscomponent', myStorefrontProductsComponent)
.service('mystorefrontProductService', mystorefrontProductService)
.service('mystorefrontProductSetupService', mystorefrontProductSetupService)
.service('myStorefrontProductsSelectionService',myStorefrontProductsSelectionService)
.service('myStorefrontEditableReviewComponentService',myStorefrontEditableReviewComponentService)


module.exports = myStorefrontProductsComponentModule;