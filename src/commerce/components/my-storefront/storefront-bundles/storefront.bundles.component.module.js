'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('../../../../../src/commerce/components/my-storefront/storefront-products/storefront.products.component.scss');

var myStorefrontBundlesComponent = require('./storefront.bundles.component');
var myStorefrontHeadingComponentModule = require('../../../../../src/commerce/components/my-storefront/storefront-products/heading-component/storefront.heading.component.module');
var myStorefrontProductsSelectionComponentModule = require('../../../../../src/commerce/components/my-storefront/storefront-products/product-selection-component/storefront.product.selection.component.module');
var mystorefrontProductService = require('../../../../../src/commerce/components/my-storefront/storefront-products/storefront.products.component.service');
var formUtils = require('../../../../../src/__shared/form-utils/form-utils');
var myStorefrontEditableReviewComponentService = require('../../../../../src/commerce/components/my-storefront/storefront-editable-review/storefront.editable.review.component.service');
var globalServicesService = require('../../../../../src/__shared/services/global-services/global.services.service');

var myStorefrontBundlesComponentModule = angular.module('myStorefrontBundlesComponentModule', [uiRouter, myStorefrontHeadingComponentModule.name, myStorefrontProductsSelectionComponentModule.name, formUtils.name])
.component('mystorefrontbundlescomponent', myStorefrontBundlesComponent)
.service('mystorefrontProductService', mystorefrontProductService)
.service('myStorefrontEditableReviewComponentService', myStorefrontEditableReviewComponentService)
.service('globalServicesService', globalServicesService)


module.exports = myStorefrontBundlesComponentModule;