'use strict';

function storefrontBundlesController(mystorefrontProductService, myStorefrontEditableReviewComponentService, globalServicesService) {
    var self = this;

    self.mainHeading = "SHOW PROMOTION & BUNDLE";
    self.subHeading = "<p class=\"m-b-0\">Some products go better together.</p> <p>Make your own bundle of products or show your country’s current promotions.</p>";
    self.minimumProductsAllowed = 2;
    self.maximumProductsAllowed = 5;
    self.defaultProductSelectionValue = 1;
	self.customProductSelectionValue = 2;  

    self.model = {
        "promotionAndBundlesSwitch": false,
        "selectedProductList": null
    };

    self.manageSwitchControl = function(){
        if(!self.model.promotionAndBundlesSwitch)
            self.model.selectedProductList = null;
    }

    var isBundlesListAvailable = myStorefrontEditableReviewComponentService.getOriginalBundlesList() && myStorefrontEditableReviewComponentService.getOriginalBundlesList().length && myStorefrontEditableReviewComponentService.getOriginalBundlesList().length > 0 ? myStorefrontEditableReviewComponentService.getOriginalBundlesList() : null;

    if(self.isInlineEdit && isBundlesListAvailable) {
        self.model.promotionAndBundlesSwitch = true;
    }

    mystorefrontProductService.setArrangedProductList(self.model.selectedProductList);

    self.model.tileOne = {
        "heading": "Show Default Bundles",
        "subHeading": "Show my country’s bundles and promotions"
    };
    self.model.tileTwo = {
        "heading": "Create Custom Bundle",
        "subHeading": "Choose between 2 and 5 products to create your own bundle"
    };
    self.errorData = {
        "errorMessage": null,
        "showInfoIconOnError": true,
        "isDisplayErrorMessage": true
    };

    mystorefrontProductService.setSelectedProductType(null);

    self.mimimumProductsSelectionError = function (errorMessage) {
        self.errorData.errorMessage = errorMessage;
        self.error({ "data": self.errorData });
        mystorefrontProductService.scrollToTop();
    };

    self.saveAndContinue = function () {
        if (self.model.promotionAndBundlesSwitch) {
            self.model.selectedProductList = mystorefrontProductService.getArrangedProductList();
            self.model.selectedProductList = globalServicesService.shortenArray(self.model.selectedProductList, self.maximumProductsAllowed);
            self.selectedProductType = mystorefrontProductService.getSelectedProductType();
            if (self.selectedProductType == self.defaultProductSelectionValue && (!self.model.selectedProductList)) {
                self.model.selectedProductList = self.model.selectedProductList.slice(self.maximumProductsAllowed, self.model.selectedProductList.length);
            }
            if (self.selectedProductType == self.customProductSelectionValue && (!self.model.selectedProductList || !self.model.selectedProductList.length)) {
                self.mimimumProductsSelectionError("Sorry. You need at least two items to proceed ahead.");
            }
            else if (!self.model.selectedProductList || !self.model.selectedProductList.length) {
                self.errorData.errorMessage = "Sorry. You need to select at least one product type to proceed ahead.";
                self.error({ "data": self.errorData });
                mystorefrontProductService.scrollToTop();
            }
            else if (self.model.selectedProductList && self.model.selectedProductList.length < self.minimumProductsAllowed) {
                self.mimimumProductsSelectionError("Sorry. You need at least two items to proceed ahead.");                
            }
            else if (self.model.selectedProductList && self.model.selectedProductList.length > self.maximumProductsAllowed) {
                self.errorData.errorMessage = "Sorry. You cannot add more than " + self.maximumProductsAllowed + " items.";
                self.error({ "data": self.errorData });
                mystorefrontProductService.scrollToTop();
            }
            else {
                self.error({ "data": null });
                myStorefrontEditableReviewComponentService.setOriginalBundlesList(self.model.selectedProductList);
                self.save();
            }
        }
        else {
            self.model.selectedProductList = null;
            myStorefrontEditableReviewComponentService.setOriginalBundlesList(null);            
            self.save();
        }
    };

    self.saveAndPublish = function() {
        self.saveAndContinue();  
    };

    self.revertAndCancel = function() {
        self.cancel();
    };
};

module.exports = storefrontBundlesController;