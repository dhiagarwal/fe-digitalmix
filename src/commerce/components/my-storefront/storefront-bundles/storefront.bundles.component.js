'use strict';
var template = require('./storefront.bundles.component.jade');
var controller = require('./storefront.bundles.component.controller');

var storefrontBundlesComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'bundlesCtrl',
   bindings:{
        save: '&',
        error: '&',
        isInlineEdit: '=',
        cancel: '&',
    }
};

module.exports = storefrontBundlesComponent;
