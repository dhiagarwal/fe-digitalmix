'use strict'

function storefrontIntroController(storefrontSetupService, myStorefrontEditableReviewComponentService){
    var self=this;
        
    self.saveIntroMessage = function(isValid) {
        self.isSubmitClicked = true;
        // Save data and then call below function
        if(isValid) {
            self.save();
        }
    };
    
    var promise = storefrontSetupService.getData();
    promise.then(function(data) {
        self.welcomeMessage = data.welcomeMessage; 
        self.checkoutMessage = data.checkoutMessage; 
        if(self.isInlineEdit) {
            self.welcomeMessage = myStorefrontEditableReviewComponentService.getWelcomeMessage();
            self.checkoutMessage = myStorefrontEditableReviewComponentService.getCheckoutMessage();
        }  
    });

    self.cancelStep = function() {
        self.isPublishClicked = false;        
        self.cancel();
    };

    self.savePublish = function(isValid) {
        self.isPublishClicked = true;
        if(isValid) {
            myStorefrontEditableReviewComponentService.setWelcomeMessage(self.welcomeMessage);
            myStorefrontEditableReviewComponentService.setCheckoutMessage(self.checkoutMessage);
            self.save();
        }
    };
};

module.exports = storefrontIntroController;