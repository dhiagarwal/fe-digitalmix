var template =require('./storefront.intro.component.jade');
var controller =require('./storefront.intro.component.controller');

var storefrontIntroComponent =  {
    templateUrl: template,
    controller: controller,
    controllerAs: 'sfIntro',
    bindings:{
        save: '&',
        isInlineEdit: '=',
        cancel: '&'
    }
};

module.exports= storefrontIntroComponent;