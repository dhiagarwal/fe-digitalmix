var angular =require('angular');
var storefrontIntroComponent =require('./storefront.intro.component');
var myStorefrontEditableReviewComponentService = require('../../../../../src/commerce/components/my-storefront/storefront-editable-review/storefront.editable.review.component.service');

var storefrontIntroModule = angular.module('storefrontIntro', [])
.component('storefrontIntro', storefrontIntroComponent)
.service('myStorefrontEditableReviewComponentService', myStorefrontEditableReviewComponentService);

import Styles from './storefront.intro.component.scss'; 

module.exports= storefrontIntroModule;
