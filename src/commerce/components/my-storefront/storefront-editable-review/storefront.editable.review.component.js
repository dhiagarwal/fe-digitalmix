'use strict';
var template = require('./storefront.editable.review.component.jade');
var controller = require('./storefront.editable.review.component.controller');

var storefrontEditableReviewComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindings: {
  	changeState: "&",
  	isGuidedExperience: "=",
  	isReviewMode: "=",
    showError: "&"
  }
};

module.exports = storefrontEditableReviewComponent;
