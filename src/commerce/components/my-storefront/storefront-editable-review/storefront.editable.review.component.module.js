'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var slickCarousel = require('slick-carousel');
var angularSlick =  require('angular-slick-carousel');
var myStorefrontEditableReviewComponent = require('./storefront.editable.review.component');
var myStorefrontEditableReviewComponentService = require('./storefront.editable.review.component.service');
var storefrontProductsComponentModule = require('../../../../../src/commerce/components/my-storefront/storefront-products/storefront.products.component.module');
var storefrontSetupService = require('../../../../../src/commerce/components/my-storefront/storefront-setup/storefront.setup.component.service');
var globalServicesService = require('../../../../../src/__shared/services/global-services/global.services.service');

var myStorefrontEditableReviewComponentModule = angular.module('myStorefrontEditableReviewComponentModule', [uiRouter,'slickCarousel', storefrontProductsComponentModule.name])
.component('mystorefronteditablereviewcomponent', myStorefrontEditableReviewComponent)
.service('myStorefrontEditableReviewComponentService', myStorefrontEditableReviewComponentService)
.service('storefrontSetupService', storefrontSetupService)
.service('globalServicesService', globalServicesService);

import Styles from './storefront.editable.review.component.scss';

module.exports = myStorefrontEditableReviewComponentModule;