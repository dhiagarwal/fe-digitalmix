'use strict';

storefrontEditableReviewController.inject = ["myStorefrontEditableReviewComponentService", "mystorefrontProductService", "storefrontSetupService", "globalServicesService", "$location", "$anchorScroll", "$timeout"];
function storefrontEditableReviewController(myStorefrontEditableReviewComponentService, mystorefrontProductService, storefrontSetupService, globalServicesService, $location, $anchorScroll, $timeout) {
    var self = this;
    self.slickSecondaryConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        focusonselect: true,
        responsive: [{
            breakpoint: 640,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 1023,
            settings: {
                slidesToShow: 3,
                draggable: false
            }
        }, {
            breakpoint: 1432,
            settings: {
                slidesToShow: 3,
                draggable: false
            }
        }]
    };

    self.errorData = {
		"errorMessage": null,
		"showInfoIconOnError": true,
		"isDisplayErrorMessage": true
	};
    self.viewData = {
        "bundlesList": []
    };

    self.maximumBundelsList = 5;
    self.bundlesTotalCost = 0;

    self.inlineStepList = [{
        "id": 0,
        "status": false,
        "isFirstLoad": true,
    },
    {
        "id": 1,
        "status": false,
        "isFirstLoad": true,
    }, {
        "id": 2,
        "status": false,
        "isFirstLoad": true,
    }, {
        "id": 3,
        "status": false,
        "isFirstLoad": true,
    }, {
        "id": 4,
        "status": false,
        "isFirstLoad": true,
    }];

    self.edit = function(state) {
        self.changeState(state);
    };

    self.resetInlineEdit = function(target) {
        if(target) {
            angular.forEach(self.inlineStepList, function(value, key) {
                if(value == target) {
                    value.status = false;
                }
            });
        }
        else {
            angular.forEach(self.inlineStepList, function(value, key) {
                value.status = false;
            });
        }
    }

    self.executeInlineStep = function(stepNo) {
        self.resetInlineEdit(); 
        self.inlineStepList[(stepNo-1)].status = true;            
        if((stepNo - 1) >= 0) {
            switch(stepNo) {
                case 1:
                    myStorefrontEditableReviewComponentService.setWelcomeMessage(self.viewData.welcomeMessage);
                    myStorefrontEditableReviewComponentService.setCheckoutMessage(self.viewData.checkoutMessage);
                    break;
                case 2: 
                    if(self.inlineStepList[(stepNo-1)].isFirstLoad) {
                        myStorefrontEditableReviewComponentService.setOriginalProductList(self.viewData.productsList);
                        self.inlineStepList[(stepNo-1)].isFirstLoad = false;  
                    }                  
                    break;
                case 3: 
                    if(self.inlineStepList[(stepNo-1)].isFirstLoad) {
                        if(self.viewData.bundlesList) {
                            myStorefrontEditableReviewComponentService.setOriginalBundlesList(self.viewData.bundlesList);                        
                        }
                        self.inlineStepList[(stepNo-1)].isFirstLoad = false;  
                    }
                    self.viewData.bundlesShortDesc = myStorefrontEditableReviewComponentService.getShortDescOfBundles();                    
                    break;
                case 4: 
                    break;
            }
        }
    };

    self.cancelInlineStep = function(stepNo) {
        self.resetInlineEdit();
        self.inlineStepList[stepNo - 1].status = false;
        $location.hash('content-' + stepNo);
        $anchorScroll();   
        if((stepNo - 1) >= 0) {
            switch(stepNo) {
                case 1: 
                    break;
                case 2: 
                    self.viewData.productsList = myStorefrontEditableReviewComponentService.getOriginalProductList();
                    break;
                case 3: 
                    self.viewData.bundlesList = myStorefrontEditableReviewComponentService.getOriginalBundlesList();
                    break;
                case 4: 
                    break;
            }
        }
    };

    self.saveInlineStep = function(stepNo) {
        self.resetInlineEdit();  
        self.inlineStepList[stepNo - 1].status = false;   
        $location.hash('content-' + stepNo);
        $anchorScroll();        
        if((stepNo - 1) >= 0) {
            switch(stepNo) {
                case 1: 
                    self.viewData.welcomeMessage =  myStorefrontEditableReviewComponentService.getWelcomeMessage();
                    self.viewData.checkoutMessage =  myStorefrontEditableReviewComponentService.getCheckoutMessage();
                    break;
                case 2: 
                    self.viewData.productsList = mystorefrontProductService.getArrangedProductList();
                    break;
                case 3: 
                    self.viewData.bundlesList = myStorefrontEditableReviewComponentService.getOriginalBundlesList();
                    self.bundlesTotalCost = mystorefrontProductService.calculateTotal(self.viewData.bundlesList);
                    self.viewData.bundlesShortDesc = myStorefrontEditableReviewComponentService.getShortDescOfBundles();
                    break;
                case 4: 
                    break;
            }
        }
    };

    self.showErrorSection = function(data) {
        self.errorData = data;
        self.showError({ "data": self.errorData });
    };

    self.getBundlesTotalCost = function(targetArray) {
        var totalCost = 0;
        angular.forEach(targetArray, function(value, key) {
            totalCost += parseFloat(value.cost);
        });
        return totalCost;
    };


    /* -- END -- */

    var promise = storefrontSetupService.getData();
    promise.then(function(response) {
        self.viewData.productsList = response.productsList.defaultProductsList;
        self.viewData.bundlesList = globalServicesService.shortenArray(response.productsList.defaultProductsList, self.maximumBundelsList);
        self.bundlesTotalCost = mystorefrontProductService.calculateTotal(self.viewData.bundlesList);
        self.viewData.bundlesShortDesc = response.bundlesShortDesc;
        self.viewData.welcomeMessage = response.welcomeMessage;
        self.viewData.checkoutMessage = response.checkoutMessage;
        self.viewData.webAddress = response.webAddress;
        myStorefrontEditableReviewComponentService.setWebAddress(self.viewData.webAddress);
    });
};

module.exports = storefrontEditableReviewController;
