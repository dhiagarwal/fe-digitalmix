'use strict';

myStorefrontEditableReviewComponentService.$inject = ['$http','$q', '$filter'];

function myStorefrontEditableReviewComponentService($http,$q, $filter) {
  
  	this.$http = $http;
  	this.$q = $q;
	this.$filter = $filter;
	this.productsListTouched = [{
        "id": 1,
        "status": false,
        "label": "default"
    }, {
        "id": 2,
        "status": false,
        "label": "custom"
    }]; 
	this.productsListTouchedForBundles = [{
        "id": 1,
        "status": false,
        "label": "default"
    }, {
        "id": 2,
        "status": false,
        "label": "custom"
    }]; 
	this.originalProductList = [];
	this.originalBundlesList = [];
	this.welcomeMessage = null;
	this.checkoutMessage = null;
	this.shortDescOfBundles = null; 
	this.isBundlesPage = null;
	this.webAddress = null;
};

myStorefrontEditableReviewComponentService.prototype.getStorefrontReviewData = function(Url){
	var deferred = this.$q.defer();
	
	this.$http.get(Url)
		.success(deferred.resolve);

		return deferred.promise;
};

myStorefrontEditableReviewComponentService.prototype.getProductsListTouchedStatus = function(isBundlesPage) {
	if(isBundlesPage) {
		var target = this.$filter('filter')(this.productsListTouchedForBundles, {status: true})[0];
		return target;
	}
	else {
		var target = this.$filter('filter')(this.productsListTouched, {status: true})[0];
		return target;
	}
};

myStorefrontEditableReviewComponentService.prototype.setProductsListTouchedStatus = function (targetId, targetValue, isBundlesPage) {
	if(!isBundlesPage) {
		angular.forEach(this.productsListTouched, function(value, key) {
			value.status = false;
		}); 
		angular.forEach(this.productsListTouched, function(value, key) {
			if(value.id == targetId) {
				value.status = targetValue;
			}
		});   
	}
	else {
		angular.forEach(this.productsListTouchedForBundles, function(value, key) {
			value.status = false;
		}); 
		angular.forEach(this.productsListTouchedForBundles, function(value, key) {
			if(value.id == targetId) {
				value.status = targetValue;
			}
		});
	}
};

myStorefrontEditableReviewComponentService.prototype.getOriginalProductList = function () {
    return this.originalProductList;
};

myStorefrontEditableReviewComponentService.prototype.setOriginalProductList = function (value) {
    this.originalProductList = value;
};

myStorefrontEditableReviewComponentService.prototype.getOriginalBundlesList = function () {
    return this.originalBundlesList;
};

myStorefrontEditableReviewComponentService.prototype.setOriginalBundlesList = function (value) {
    this.originalBundlesList = value;
};

myStorefrontEditableReviewComponentService.prototype.getWelcomeMessage = function () {
    return this.welcomeMessage;
};

myStorefrontEditableReviewComponentService.prototype.setWelcomeMessage = function (value) {
    this.welcomeMessage = value;
};

myStorefrontEditableReviewComponentService.prototype.getCheckoutMessage = function () {
    return this.checkoutMessage;
};

myStorefrontEditableReviewComponentService.prototype.setCheckoutMessage = function (value) {
    this.checkoutMessage = value;
};

myStorefrontEditableReviewComponentService.prototype.getShortDescOfBundles = function () {
    return this.shortDescOfBundles;
};

myStorefrontEditableReviewComponentService.prototype.setShortDescOfBundles = function (value) {
    this.shortDescOfBundles = value;
};

myStorefrontEditableReviewComponentService.prototype.getBundlesPageBoolean = function () {
    return this.isBundlesPage;
};

myStorefrontEditableReviewComponentService.prototype.setBundlesPageBoolean = function (value) {
    this.isBundlesPage = value;
};

myStorefrontEditableReviewComponentService.prototype.getWebAddress = function () {
    return this.webAddress;
};

myStorefrontEditableReviewComponentService.prototype.setWebAddress = function (value) {
    this.webAddress = value;
};

module.exports = myStorefrontEditableReviewComponentService;