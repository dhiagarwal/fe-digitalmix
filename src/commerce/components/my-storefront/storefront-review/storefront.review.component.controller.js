'use strict';

function storefrontReviewController($location) {
	var self = this;	
    self.errorData = {};

    self.edit = function(state){
    	self.goToState({"state":state});
    }
    self.redirect = function(url){
        $location.path(url);
    }
    self.showErrorSection = function(data) {
        self.errorData = data;
    };
};

module.exports = storefrontReviewController;
