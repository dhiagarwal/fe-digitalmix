'use strict';
var template = require('./storefront.review.component.jade');
var controller = require('./storefront.review.component.controller');

var storefrontReviewComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindings:{
  	goToState: "&",
  	showReviewFields: "=",
  }
};

module.exports = storefrontReviewComponent;
