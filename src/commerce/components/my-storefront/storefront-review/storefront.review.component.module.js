'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var slickCarousel = require('slick-carousel');
var angularSlick =  require('angular-slick-carousel');
var myStorefrontReviewComponent = require('./storefront.review.component');
var myStorefrontEditableReviewComponentModule = require('../storefront-editable-review/storefront.editable.review.component.module');

var myStorefrontReviewComponentModule = angular.module('myStorefrontReviewComponentModule', [uiRouter,'slickCarousel',myStorefrontEditableReviewComponentModule.name])
.component('mystorefrontreviewcomponent',myStorefrontReviewComponent)

import Styles from './storefront.review.component.scss';

module.exports = myStorefrontReviewComponentModule;