'use strict';
var template = require('./featured.video.component.jade');
var controller = require('./featured.video.component.controller');

var featuredVideoComponent  = {
  //return {
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
  //};
};

module.exports = featuredVideoComponent ;
