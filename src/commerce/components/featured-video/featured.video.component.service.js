'use strict';

featuredVideoComponentService.$inject = ['$http','$q'];

function featuredVideoComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/featured-video/featured.video.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getFeaturedVideo = function() {
    return deferred.promise;
  }

};

module.exports = featuredVideoComponentService;
