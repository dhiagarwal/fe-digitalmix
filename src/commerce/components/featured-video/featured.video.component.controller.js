'use strict';

function featuredVideoComponentController(featuredVideoComponentService,$timeout) {
  this.watchVideoUrl = '../../../../src/assets/images/content/video-still.jpg';
  this.videoURL="../../../../src/assets/videos/Testimonial-Video.mp4";
  this.videoPlaying = false;
  this.togglePlay = function() {
		this.videoPlaying = true;
		var video = document.querySelector('video');
		if (video.paused) {
			$timeout(function(){video.play();},10)
			video.controls=true;
			this.videoPlaying = true;
			angular.element(".watch-video-panel").css("background-image","");
		} else {
			$timeout(function(){video.pause();},10)
			video.controls=false;
			this.videoPlaying = false;
			angular.element(".watch-video-panel").css("background-image","url("+this.watchVideoUrl+")");
		}
  };

    this.onVideoEnd = function() {
    	var video = document.querySelector('video');

    	if (video.ended) {
    		video.controls=false;
            this.videoPlaying = false;
            angular.element(".watch-video-panel").css("background-image","url("+this.watchVideoUrl+")");
    	}
    };
};

module.exports = featuredVideoComponentController;
