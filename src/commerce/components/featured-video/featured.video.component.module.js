'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-slick-carousel');
var featuredVideoComponent = require('./featured.video.component');
var featuredVideoComponentService = require('./featured.video.component.service')
var featuredVideoComponentModule = angular.module('featuredVideoComponentModule', [uiRouter])
                  .component('featuredvideocomponent', featuredVideoComponent)
                  .service('featuredVideoComponentService', featuredVideoComponentService)

import Styles from './featured.video.component.scss';

module.exports = featuredVideoComponentModule;
