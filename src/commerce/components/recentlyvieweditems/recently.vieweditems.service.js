'use strict';

recentlyVieweditemsComponentService.$inject = ['$http',"$q"];

function recentlyVieweditemsComponentService($http,$q) {
  this.$http = $http;
  var deferred = $q.defer();
  var distributorInfoPayload = {};
  $http.get('../../../../src/commerce/components/recentlyvieweditems/recentlyvieweditems.component.json').then(function(response) {
      deferred.resolve(response.data);
  });

  this.getCategoryStandardData = function() {
    return deferred.promise;
  }

};
module.exports = recentlyVieweditemsComponentService;
