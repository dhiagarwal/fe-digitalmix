var angular =require('angular');
var uiRouter =require('angular-ui-router');
var recentlyVieweditemsComponent =require('./recently.vieweditems.component');
var recentlyVieweditemsService =require('./recently.vieweditems.service');

var recentlyVieweditemsModule = angular.module('recentlyVieweditems', [
  uiRouter
])
.component('recentlyvieweditemscomponent', recentlyVieweditemsComponent)
.service('recentlyVieweditemsService', recentlyVieweditemsService);
var base =require('../../../stylesheets/base.scss');
var Styles =require('./recently.vieweditems.scss');

module.exports= recentlyVieweditemsModule;
