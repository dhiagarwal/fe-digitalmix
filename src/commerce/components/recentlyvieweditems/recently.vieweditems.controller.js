'use strict';
function recentlyVieweditemsController(recentlyVieweditemsService) {
var self = this;
this.slickType="type1";
this.slickSecondaryConfig = {
    enabled: true,
    infinite: true,
    adaptiveHeight: false,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    focusonselect: true,
    speed: 500,
    responsive: [
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                draggable: false,
            }
        }
    ]
};
var getData = recentlyVieweditemsService.getCategoryStandardData();
  getData.then(function(data){
      self.categoryData = data;
  });
};
module.exports = recentlyVieweditemsController;
