var template =require('./recently.vieweditems.jade');
var controller =require('./recently.vieweditems.controller');

var recentlyVieweditemsComponent =  {
    templateUrl : template,
    controller: controller,
    controllerAs: 'recentctrl',
};

module.exports = recentlyVieweditemsComponent;
