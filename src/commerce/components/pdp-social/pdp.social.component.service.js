'use strict';

pdpSocialComponentService.$inject = ['$http','$q'];

function pdpSocialComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-social/pdp.social.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getSocialIconData = function() {
    return deferred.promise;
  }

};

module.exports = pdpSocialComponentService;
