'use strict';
var template = require('./pdp.social.component.jade');
var controller = require('./pdp.social.component.controller');

var pdpSocialComponent = {
  //return {
  restrict: 'E',
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
  //};
};

module.exports = pdpSocialComponent;
