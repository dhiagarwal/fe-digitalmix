'use strict';

function pdpSocialComponentController(pdpSocialComponentService) {

  this.socialIcons ={};
  var self = this;

  var promise = pdpSocialComponentService.getSocialIconData();
  promise.then(function(data) {
    self.socialIcons = data;
    console.log(self.socialIcons);
  });

};

module.exports = pdpSocialComponentController;
