'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pdpSocialComponent = require('./pdp.social.component');
var pdpSocialComponentService = require('./pdp.social.component.service')

var pdpSocialComponentModule = angular.module('pdpSocialComponentModule', [uiRouter])
                  .component('pdpsocialcomponent', pdpSocialComponent)
                  .service('pdpSocialComponentService', pdpSocialComponentService)
                  
import Styles from './pdp.social.component.scss';

module.exports = pdpSocialComponentModule;
