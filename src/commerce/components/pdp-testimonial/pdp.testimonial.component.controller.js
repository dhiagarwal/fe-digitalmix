'use strict';

function pdpTestimonialComponentController(pdpTestimonialComponentService, $timeout) {

    this.testimonials = {};
    var self = this;
    this.videoPlaying = false;

    this.slickVideoConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: false,
        arrows: false,
        dots: true,
        speed:500,
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
                var video = angular.element.find('video');
                angular.forEach(video, function(value, key){
                    if(video[key]) {
                        video[key].pause();
                        video[key].controls=false;
                        self.videoPlaying = false;
                    }
                });
            }
        }
    };
    this.togglePlay = function(index) {
        var video = angular.element.find('#video-'+index);

        if (video[0].paused) {
            video[0].play();
            video[0].controls=true;
            this.videoPlaying = true;
        } else {
            $timeout(function(){video[0].pause();},10)
            video[0].controls=false;
            this.videoPlaying = false;
        }
    };
    
    self.video = function(index) {
        var videoElements = angular.element.find('#video-'+index);
        videoElements[0].currentTime = 0;
        videoElements[0].controls = false;
        self.videoPlaying = false;
    };

    var promise = pdpTestimonialComponentService.getTestimonialIconData();
    promise.then(function(data) {
        self.testimonials = data;
    });

};

module.exports = pdpTestimonialComponentController;
