'use strict';

pdpTestimonialComponentService.$inject = ['$http','$q'];

function pdpTestimonialComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-testimonial/pdp.testimonial.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getTestimonialIconData = function() {
    return deferred.promise;
  }

};

module.exports = pdpTestimonialComponentService;
