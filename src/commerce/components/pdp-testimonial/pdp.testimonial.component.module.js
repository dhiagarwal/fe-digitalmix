'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-slick-carousel');
var pdpTestimonialComponent = require('./pdp.testimonial.component');
var pdpTestimonialComponentService = require('./pdp.testimonial.component.service')

var pdpTestimonialComponentModule = angular.module('pdpTestimonialComponentModule', [uiRouter])
                  .component('pdptestimonialcomponent', pdpTestimonialComponent)
                  .service('pdpTestimonialComponentService', pdpTestimonialComponentService)
                  
import Styles from './pdp.testimonial.component.scss';

module.exports = pdpTestimonialComponentModule;
