'use strict';
var template = require('./pdp.testimonial.component.jade');
var controller = require('./pdp.testimonial.component.controller');

var pdpTestimonialComponent = {
  //return {
  restrict: 'E',
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
  //};
};

module.exports = pdpTestimonialComponent;
