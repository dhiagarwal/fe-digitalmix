'use strict';

function pdpUpsellKitComponentController(pdpUpsellKitComponentService) {

  this.upsellKitData ={};
  var self = this;

  var promise = pdpUpsellKitComponentService.getUpsellKitData();
  promise.then(function(data) {

    self.upsellKitData = data;
  });


};

module.exports = pdpUpsellKitComponentController;
