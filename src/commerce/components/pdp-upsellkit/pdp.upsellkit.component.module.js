'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pdpUpsellKitComponent = require('./pdp.upsellkit.component');
var pdpUpsellKitComponentService = require('./pdp.upsellkit.component.service')

var pdpUpsellKitComponentModule = angular.module('pdpUpsellKitComponentModule', [uiRouter])
                  .component('pdpupsellkitcomponent', pdpUpsellKitComponent)
                  .service('pdpUpsellKitComponentService', pdpUpsellKitComponentService)
                  
         //import base from '../../../stylesheets/base.scss'
import Styles from './pdp.upsellkit.component.scss';                      

module.exports = pdpUpsellKitComponentModule;
