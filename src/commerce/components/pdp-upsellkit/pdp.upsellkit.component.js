'use strict';
var template = require('./pdp.upsellkit.component.jade');
var controller = require('./pdp.upsellkit.component.controller');

var pdpUpsellKitComponent = {
  //return {
  restrict: 'E',
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
  //};
};

module.exports = pdpUpsellKitComponent;
