'use strict';
// var socialData = require('./pdp.social.component.json');

pdpUpsellKitComponentService.$inject = ['$http','$q'];

function pdpUpsellKitComponentService($http,$q) {
  // console.log($q);
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/pdp-upsellkit/pdp.upsellkit.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getUpsellKitData = function() {
    return deferred.promise;
  }

};




module.exports = pdpUpsellKitComponentService;
