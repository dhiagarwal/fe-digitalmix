'use strict';

welcomeMessageComponentService.$inject = ['$http','$q'];

function welcomeMessageComponentService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/commerce/components/welcome-message/welcome.message.component.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getDistributorData = function() {
    return deferred.promise;
  }

};

module.exports = welcomeMessageComponentService;
