'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var welcomeMessageComponent = require('./welcome.message.component');
var welcomeMessageComponentService = require('./welcome.message.component.service')

var welcomeMessageComponentModule = angular.module('welcomeMessageComponentModule', [uiRouter])
                  .component('welcomemessagecomponent', welcomeMessageComponent)
                  .service('welcomeMessageComponentService', welcomeMessageComponentService)

import Styles from './welcome.message.component.scss';

module.exports = welcomeMessageComponentModule;
