'use strict';

function welcomeMessageComponentController(welcomeMessageComponentService) {
    var promise = welcomeMessageComponentService.getDistributorData();
    var self = this;
    self.distributor={};
    promise.then(function(data) {
        self.distributor = data;
    });
};

module.exports = welcomeMessageComponentController;
