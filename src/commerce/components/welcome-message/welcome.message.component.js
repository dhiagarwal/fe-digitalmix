'use strict';
var template = require('./welcome.message.component.jade');
var controller = require('./welcome.message.component.controller');

var welcomeMessageComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl'
};

module.exports = welcomeMessageComponent;
