var angular = require('angular');
var uiRouter =require('angular-ui-router');
var landingPageBannerFull = require('./landing.page.banner.full');
var landingPageBannerFullModule = angular.module('landingPageBannerFullModule', [uiRouter])
    .component('landingPageBannerFull', landingPageBannerFull)

import Styles from './landing.page.banner.full.scss';

module.exports = landingPageBannerFullModule;
