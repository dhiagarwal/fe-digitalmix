'use strict';

var template = require('./landing.page.banner.full.jade');
var controller = require('./landing.page.banner.full.controller');

var landingPageBannerFull =  {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = landingPageBannerFull;
