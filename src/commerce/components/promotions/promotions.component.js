var template = require('./promotions.jade');
var controller = require('./promotions.controller');

var promotionsComponent = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'promoctrl',
};

module.exports = promotionsComponent;