'use strict';

function promotionsController(promotionsService, $location) {
    var self = this;
    self.tileData = {};

    self.tile1Layout = false;
    self.tile3Layout = false;
    self.tile7Layout = true;


    var promise = promotionsService.getTileData();
    promise.then(function(data) {
        self.tileData = data;

    });


    self.redirect = function(){
    	$location.path("/pdpProductBasics");
    }
};

module.exports = promotionsController;