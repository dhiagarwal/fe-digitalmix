var angular =require('angular');
var uiRouter =require('angular-ui-router');
var promotionsComponent =require('./promotions.component');
var promotionsService =require('./promotions.service');

var promotionsModule = angular.module('tenProducts', [
  uiRouter
])
.component('promotionscomponent', promotionsComponent)
.service('promotionsService', promotionsService);
var Styles =require('./promotions.scss');

module.exports = promotionsModule;
