'use strict';

promotionsComponentService.$inject = ['$http', "$q"];

function promotionsComponentService($http, $q) {
    this.$http = $http;
    var deferred = $q.defer();

    promotionsComponentService.prototype.getTileData = function() {

        this.$http.get('../../../../src/commerce/components/promotions/promotions.component.json')
            .success(deferred.resolve);

        return deferred.promise;
    }

};
module.exports = promotionsComponentService;