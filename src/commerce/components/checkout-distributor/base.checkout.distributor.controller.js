'use strict';
var confirmModalTemplate = require('./confirmation.modal.template.jade');

function BaseCheckoutController(){

}

BaseCheckoutController.prototype.togglePanel = function(){
    if (window.innerWidth < 1024) {
        switch(this.model.isPullLabelVisible) {
            case false:
                // angular.element('body').css({'background': '', 'overflow': ''});
                // angular.element('input,select').removeAttr('disabled');
                angular.element('#myModalBackDrop').modal('hide');
                // angular.element('.accordion-panel').css({'height': '', 'min-height': '', 'overflow': ''});
                break;
            case true:
                // angular.element('body').css({'background': 'black', 'overflow': 'hidden'});
                // angular.element('input,select').attr('disabled', 'true');
                angular.element('#myModalBackDrop').modal();
                // angular.element('.shopping-cart-top').find('input').attr('disabled', false);
                // angular.element('.accordion-panel').css({'max-height': '27.125rem', 'min-height': '23.4375rem', 'overflow-y': 'auto', 'overflow-x': 'hidden'});
                break;
        }
    }
};

BaseCheckoutController.prototype.showConfirmationModal= function(Info, yesTitle, noTitle){
    var self = this;
    var modalInstance = this.$uibModal.open({
        animation: true,
        backdrop: 'static',
        templateUrl: confirmModalTemplate,
        resolve:{
          modalParams:{
              description: Info,
              yesTitle: yesTitle,
              noTitle: noTitle
          }
        },
        windowClass: 'app-modal-window confirm-modal-class',
        controller: ['$scope', '$uibModalInstance','modalParams', function ($scope, $uibModalInstance, modalParams) {
            $scope.ok = function () {
                $uibModalInstance.close();
            };
            $scope.modalParams = modalParams;

            $scope.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }],
        size: 'sm'
    });

    modalInstance.rendered.then(this.reposition.bind(this));

    return modalInstance.result; // making this method to return a promise object so that you can use a callback and trigger success/reject functions
};

module.exports = BaseCheckoutController;