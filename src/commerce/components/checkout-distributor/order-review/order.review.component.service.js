'use strict';

function orderReviewService($http, $q){
    this.$http = $http;
    this.$q = $q;
}

orderReviewService.prototype.getOrderData = function(){
    var deferred = this.$q.defer();
    this.$http.get("../../../../../src/commerce/components/checkout-distributor/order-review/order.review.component.json")
        .success(deferred.resolve);
    return deferred.promise;
};

module.exports = ['$http', '$q', orderReviewService];