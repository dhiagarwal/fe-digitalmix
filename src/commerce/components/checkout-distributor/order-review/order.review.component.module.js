'use strict';

var angular = require('angular');
require('angular-messages');
require('angular-ui-bootstrap');
require('./order.review.component.scss');
var deliveryOrderReview = require('./order.review.component');
var orderReviewService = require('./order.review.component.service');
var formUtils = require('../../../../../src/__shared/form-utils/form-utils'); 

var orderReviewCompModule = angular.module('orderReviewCompModule ', ['ui.bootstrap','ngMessages', formUtils.name])
    .component('deliveryorderreview', deliveryOrderReview)
    .service('orderReviewService', orderReviewService);

module.exports = orderReviewCompModule ;