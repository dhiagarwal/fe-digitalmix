'use strict';
var template = require('./order.review.component.jade');
var _ = require('lodash');
var modalTemplate = require('./order.review.modal.jade');
var baseCheckoutController = require('../base.checkout.distributor.controller');

var states = {
    isShipping: 'checkoutDeliveryShipAddress',
    isDelivery: 'checkoutDeliveryOptions',
    isPayment: 'paymentMethodDistributor',
    orderPlaced: 'orderconfirmation'
};

var orderReviewComp = {
    restrict: 'E',
    templateUrl: template,
    controller: ['orderReviewService','$state','$uibModal', orderReviewController],
    controllerAs: 'ctrl',
    bindToController: true
};
orderReviewController.prototype = Object.create(baseCheckoutController.prototype);
function orderReviewController(orderReviewService, $state, $uibModal){
    baseCheckoutController.call(this);
    this.checkoutData = {};
    this.$state = $state;
    this.$uibModal = $uibModal;
    this.isChecked = false;
    this.errorMessage = "Out of stock items have been removed from your order.";
    this.error =  true;
    this.isDisplayErrorMessage = false;
    this.isShowInfoIconOnError = false;
    this.termsConditionsErr = false;
    this.showModal=false;
    this.isNumber= function (n) {
     return !isNaN(parseFloat(n)) && isFinite(n);
   }
    self.isMobileDevice = (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || (navigator.userAgent.match('Android'));
    orderReviewService.getOrderData().then(this.setCheckoutData.bind(this));
}

orderReviewController.prototype.isNew = function(item){
  var temp = item;
  if(temp == "true"){
    return true;
  }
  else{
    return false;
  }
};
orderReviewController.prototype.stockStatus = function(value, item){
  var temp = item;
  if(temp == value){
    return true;
  }
  else{
    return false;
  }
};

orderReviewController.prototype.applyCode = function(code){
    if(!code){
        this.promoError = true;
        return;
    }
    if(this.checkoutData.orderSummary.promoCodeList.length >= 1){
        this.checkoutData.orderSummary.promoCode = '';
        this.promoLengthError = true;
        return;
    }
    this.checkoutData.orderSummary.promoCodeList.push(code);
    this.checkoutData.orderSummary.promoCode = '';
};

orderReviewController.prototype.toggleError= function(){
    this.promoError = false;
    this.promoLengthError = false;
};

orderReviewController.prototype.removeItem= function(item){
    _.pull(this.checkoutData.orderSummary.promoCodeList, item)
};

orderReviewController.prototype.isAccepted = function(bool){
    this.isChecked = bool;
};

orderReviewController.prototype.setCheckoutData = function(data){
    this.checkoutData = data;
};

orderReviewController.prototype.onEditBag = function(){
    this.$state.go('shopping-cart');
};

orderReviewController.prototype.onEditIcon = function(stateProp){
    this.$state.go(states[stateProp]);
};

orderReviewController.prototype.navigateTo = function(view){
    this.onEditIcon(view);
};

orderReviewController.prototype.placeOrder = function(){
    if(this.isChecked){
        this.termsConditionsErr = false;
        this.$state.go(states['orderPlaced']);
    } else {
        this.showError = true;
        this.termsConditionsErr = true;
        return;
    }
};

orderReviewController.prototype.backToBag= function() {
    var self = this;

    this.showConfirmationModal('Are you Sure?','GO BACK TO SHOPPING BAG','STAY IN CHECKOUT').then(function () {
        self.onEditBag();
    });

};

orderReviewController.prototype.showCustomPopUp = function(linkType){
    if(linkType == 'termsConditions'){
        this.modalHeading = "Terms & Conditions";
        this.modalURL = "src/assets/distearnings.html";
    } else {
        this.modalHeading = "Product Return Policy";
        this.modalURL = "src/assets/nuskin_refund_policy.html";
    }
    this.showModal = true;
};

orderReviewController.prototype.hideCustomModal = function(){
    this.showModal = false;
};

module.exports = orderReviewComp ;
