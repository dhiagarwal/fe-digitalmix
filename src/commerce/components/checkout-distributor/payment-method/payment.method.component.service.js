'use strict';

paymentMethodService.$inject = ['$http','$q'];

function paymentMethodService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	$http.get('../../../../../src/commerce/components/checkout-distributor/payment-method/payment.method.component.json').then(function(response) {
    	deferred.resolve(response.data);
  	});

  this.getFormdata = function() {
    return deferred.promise;
  }
};

module.exports = paymentMethodService;
