'use strict';
var template = require('./payment.method.component.jade');
var controller = require('./payment.method.component.controller');

var paymentMethodComponent = {
	templateUrl: template,
	controller: controller,
	controllerAs: 'ctrl',
	bindToController: true
};

module.exports = paymentMethodComponent;
