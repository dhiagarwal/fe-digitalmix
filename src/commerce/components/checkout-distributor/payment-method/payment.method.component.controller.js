'use strict';
var _ = require('lodash');
var deleteModalTemplate = require('./payment.method.delete.modal.jade');
var cancelModalTemplate = require('./payment.method.cancel.modal.jade');
var baseCheckoutController = require('../base.checkout.distributor.controller');

payMethodController.prototype = Object.create(baseCheckoutController.prototype);
payMethodController.$inject=["paymentMethodComponentService", "$uibModal"];
function payMethodController(paymentMethodComponentService, $uibModal) {

    var model = this.model = {};
    var self = this;
    baseCheckoutController.call(this);
    this.paymentMethodData = {};
    this.$uibModal = $uibModal;
    self.model.isPullLabelVisible = true;
    model.paymentMethod = {};
    self.model.paymentMethod.handleCreditCardNumber = new Array(4);
    self.model.paymentMethod.myCards = [];
    model.heading1 = "RONALD SISK";
    model.mainHeading = "CHECKOUT";
    this.showLess();
    model.heading2 = "PAYMENT METHOD";
    self.model.promoLengthErr = false;
    self.model.isShowSaveCard = true;
    this.invalidCardError = false;
    self.model.showCCExpError = false;
    self.model.isEditReturn = false;
    this.isAmexCard = true;
    var promise = paymentMethodComponentService.getFormdata();

    promise.then(function(response) {
        self.model.paymentMethod.data = response.paymentMethod.data;
        self.model.paymentMethod.fields = response.paymentMethod.fields;
        self.model.paymentMethod.states = response.paymentMethod.states;
        self.model.paymentMethod.zipcodeLookUp = response.paymentMethod.zipcodeLookUp;
        self.model.paymentMethod.creditCardTypes = response.paymentMethod.creditCardTypes;
        self.model.paymentMethod.psvData = response.paymentMethod.psvData;
        self.model.paymentMethod.myCards = response.paymentMethod.myCards;
        self.selectPrimary();

    });
    self.isMobileDevice = (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || (navigator.userAgent.match('Android'));
    self.saveAndContinue = function(isValid) {
        self.model.paymentMethod.cardNumber = self.model.paymentMethod.handleCreditCardNumber.join('-');
        window.location.href = "#/checkout-delivery/order-review";

    };

    self.gotoPage = function(pageName) {
        window.location.href = pageName;
    };

    self.goBack = function() {
        window.location.href = "#/checkout/deliveryoptions";
    };

    self.gotoPage = function(pageName) {
        window.location.href = pageName;
        window.location.reload();
    };

    self.applyPromoCode = function(code) {
        if (!code) {
            self.model.err = true;
            return;
        }
        if (self.model.paymentMethod.data.promotionAndRewardsApplied.length >= 1) {
            self.model.promoCode = '';
            self.model.promoLengthErr = true;
            return;
        }
        self.model.paymentMethod.data.promotionAndRewardsApplied.push(code);
        self.model.promoCode = '';
    };

    self.removePromoCode = function(promoCode) {
        _.pull(self.model.paymentMethod.data.promotionAndRewardsApplied, promoCode)
    };

    self.zipcodeLookup = function() {
        var flag = true,
            foundData = null;
        angular.forEach(self.model.paymentMethod.zipcodeLookUp, function(value, key) {
            if (flag && value.zipcode == self.model.paymentMethod.data.zipcode) {
                foundData = value;
                flag = false;
            }
        });
        if (foundData) {
            self.model.paymentMethod.data.zipcode = foundData.zipcode;
            self.model.paymentMethod.data.state = foundData.state;
            self.model.paymentMethod.data.city = foundData.city;
            self.model.paymentMethod.data.addressLine1 = foundData.addressLine1;
            self.model.paymentMethod.data.addressLine2 = foundData.addressLine2;
        }
    };

    self.reposition = function() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    };

    $('.modal').on('show.bs.modal', self.reposition);
    $(window).on('resize', function() {
        $('.modal:visible').each(self.reposition);
    });

    self.isCreditCardEmpty = function(index) {
        if (index != 0 && !self.model.paymentMethod.handleCreditCardNumber[0]) {
            document.getElementById('credit-card-number-0').focus();
        }
    };

    self.togglePaymentSection = function(source, target) {
        $(source).fadeIn(1000);
        $(target).fadeOut(0);
    };

    self.checkCCMonth = function() {
        if (self.model.paymentMethod.data.expiry) {
            var cardExpDate = self.model.paymentMethod.data.expiry.substr(0, 2) + "/" + self.model.paymentMethod.data.expiry.substr(self.model.paymentMethod.data.expiry.length - 2); // mm/yy format
            var expdatearr = cardExpDate.split("/");
            var currentYear = +(new Date().getFullYear() + "").substr(2);
            var currentMonth = +(new Date().getMonth() + 1);

            if (+expdatearr[0] > 12 || +expdatearr[0] < 1) {
                self.model.showCCExpError = true;
            } else if(+expdatearr[1] < currentYear){
                self.model.showCCExpError = true;
            } else if(+expdatearr[0] < currentMonth && +expdatearr[1] <= currentYear){
            	self.model.showCCExpError = true;
            }else {
            	self.model.showCCExpError = false;
            }
        }
    };

    self.checkCreditCardType = function() {
        var cardNumber = self.model.paymentMethod.data.cardNumber;
        var isGetCreditCardType = cardNumber && self.model.paymentMethod.data.name && self.model.paymentMethod.data.expiry && self.model.paymentMethod.data.cvv;
        if (isGetCreditCardType) {
            self.model.paymentMethod.cardNumber = cardNumber;
            self.model.test = self.GetCardType(self.model.paymentMethod.cardNumber);
            switch (self.GetCardType(self.model.paymentMethod.cardNumber)) {
                case self.model.paymentMethod.creditCardTypes.visa:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.visa;
                    self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.visa;
                    break;

                case self.model.paymentMethod.creditCardTypes.mastercard:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.mastercard;
                    self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.mastercard;
                    break;

                case self.model.paymentMethod.creditCardTypes.discover:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.discover;
                    self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.discover;

                    break;

                case self.model.paymentMethod.creditCardTypes.amex:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.amex;
                    self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.amex;
                    break;

                default:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.other;
                    self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.other;

                    break;
            }
            self.togglePaymentSection('.payment-section-validated', '.payment-section');
        }
    };

    self.GetCardType = function(number) {
        // visa
        var re = new RegExp("^4");
        if (re.test(number))
            return self.model.paymentMethod.creditCardTypes.visa;

        // Mastercard
        re = new RegExp("^5[1-5]");
        if (re.test(number))
            return self.model.paymentMethod.creditCardTypes.mastercard;

        // AMEX
        re = new RegExp("^3[47]");
        if (re.test(number))
            return "AMEX";

        // Discover
        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        if (re.test(number))
            return self.model.paymentMethod.creditCardTypes.discover;

        // Diners
        re = new RegExp("^36");
        if (number.match(re) != null)
            return "Diners";

        // Diners - Carte Blanche
        re = new RegExp("^30[0-5]");
        if (number.match(re) != null)
            return "Diners - Carte Blanche";

        // JCB
        re = new RegExp("^35(2[89]|[3-8][0-9])");
        if (number.match(re) != null)
            return "JCB";

        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null)
            return "Visa Electron";

        return null;
    }

};

payMethodController.prototype.toggleErr = function() {
    this.model.err = false;
    this.model.promoLengthErr = false;
};

payMethodController.prototype.cardSelected = function(card) {
    if (!self.isCardEditPressed) {
        this.model.selectedCard = card;
        this.model.isChecked = card;
        this.selectRadioForCreditCard(card);
    }
};

payMethodController.prototype.selectRadioForCreditCard = function(card) {
    this.isPrimary = null;
    this.cardNumber =null;
    this.cardNumber =card.cardNumber;
    this.isPrimary = card.isPrimary;
};

payMethodController.prototype.onCardEdit = function(card, event) {
    self.isCardEditPressed = true;
    this.editCard();
    this.model.paymentMethod.data.name = card.name;
    this.model.paymentMethod.data.cardNumber = card.cardNumber;

    this.model.paymentMethod.data.expiry = card.expiry;
    this.model.paymentMethod.data.cvv = card.cvv;
    this.model.paymentMethod.data.zip = card.zip;

    angular.extend(this.model.paymentMethod.data, card);
    this.model.isAdd = true;
    this.model.isEdit = true;
    this.model.isEditReturn = false;
};

payMethodController.prototype.showMore = function() {
    this.model.showMore = false;
};

payMethodController.prototype.showLess = function() {
    this.model.showMore = true;
};

payMethodController.prototype.showDeleteModal = function(item) {
    var self = this;
    this.showConfirmationModal('Are you Sure you want to permanently remove this card from your list?', 'YES - DELETE CARD', 'NO - CANCEL').then(function() {
        self.deleteCard(item);
    });
};
payMethodController.prototype.mockedCardNumber = function(cardNumber) {
    if (cardNumber) return cardNumber.toString().match(/.{1,4}/g);
};

payMethodController.prototype.isItemPrimary = function(item) {
    return item.isPrimary;
};

payMethodController.prototype.deleteCard = function(item) {
    if (this.isItemPrimary(item)) {
        _.pull(this.model.paymentMethod.myCards, item);
        if (_.find(this.model.paymentMethod.myCards, { isPrimary: true })) {
            this.selectPrimary();
        } else {
            this.model.paymentMethod.myCards[0].isPrimary = true;
            this.selectPrimary();
        }
        return;
    }
    _.pull(this.model.paymentMethod.myCards, item);

};

payMethodController.prototype.onCardDelete = function(item, event) {
    event.stopPropagation();
    this.showDeleteModal(item);
};
payMethodController.prototype.onAddNew = function() {
    this.model.isAdd = true;
    this.model.isEditReturn = false;
    this.resetParams();
    this.model.isShowSaveCard = true;
};

payMethodController.prototype.clearFormErr = function() {
    angular.forEach(this.formPayment.$error.required, function(input) {
        input.$setUntouched();
    });
};

payMethodController.prototype.setFieldsTouched = function(form) {
    angular.forEach(form.$error.required, function(input) {
        input.$setTouched();
    });
};

payMethodController.prototype.editCard = function(form) {
    this.togglePaymentSection('.payment-section', '.payment-section-validated');
    this.model.isShowSaveCard = true;
}

payMethodController.prototype.saveCard = function(form) {
	this.checkCCMonth();
	if(this.model.showCCExpError){
		return;
	}
    if (form.$valid) {
        this.checkCreditCardType();
        if(this.model.showCCExpError){
        	return;
        }

        if (!this.model.test) {
            this.invalidCardError = true;
        } else {
            this.invalidCardError = false;
            this.model.isShowSaveCard = false;
            this.togglePaymentSection('.payment-section-validated', '.payment-section');
        }
    } else {
        return this.setFieldsTouched(form);

    }
    self.isCardEditPressed = false;
}

payMethodController.prototype.clearCardError = function() {
    this.invalidCardError = false;
}

payMethodController.prototype.checkForPrimary = function(formData) {
    if (formData.isPrimary) {
        var existingPrimary;
        existingPrimary = _.find(this.model.paymentMethod.myCards, { isPrimary: true });
        existingPrimary.isPrimary = false;
    }
}

payMethodController.prototype.saveCreditCard = function(form, data, isEdit) {
    this.isSubmitted = true;
    var copy;

    this.checkForPrimary(data);

    if (form.$valid) {

        if (isEdit) {
            angular.extend(_.find(this.model.paymentMethod.myCards, { cardNumber: data.cardNumber }), data);
            this.model.isEdit = false;
            this.model.isAdd = false;
        } else {
            copy = angular.copy(data);
            copy.expiry = form.cardExpiry.$viewValue;
            this.model.paymentMethod.myCards.push(copy);
            this.model.isAdd = false;
            this.resetParams.bind(this);
        }
    }
    this.sortPrimary();
    this.selectPrimary();
};

payMethodController.prototype.onCancel = function() {
    var self = this;
    self.isCardEditPressed = false;
    this.showConfirmationModal('Are you Sure you want to go back and not save any of the information?', 'YES - GO BACK TO PAYMENT METHOD', 'NO - CANCEL').then(function() {
        self.model.isAdd = false;
        self.model.isEdit = false;
        self.model.showCCExpError = false;
        self.invalidCardError = false;
        self.model.isEditReturn = true;
        angular.element('.payment-section-validated').show();
    });
};

payMethodController.prototype.resetParams = function() {
    this.model.paymentMethod.data.name = "";
    this.model.paymentMethod.data.cardNumber = "";
    this.model.paymentMethod.handleCreditCardNumber = ["", "", "", ""];
    this.model.paymentMethod.data.expiry = "";
    this.model.paymentMethod.data.cvv = "";
    this.model.paymentMethod.data.zip = "";
};

payMethodController.prototype.selectPrimary = function() {
    this.cardSelected(_.find(this.model.paymentMethod.myCards, { isPrimary: true }));
};

payMethodController.prototype.sortPrimary = function() {
    this.model.paymentMethod.myCards = _.sortBy(this.model.paymentMethod.myCards, function(o) {
        return !o.isPrimary; });
};

module.exports = ['paymentMethodComponentService', '$uibModal', payMethodController];
