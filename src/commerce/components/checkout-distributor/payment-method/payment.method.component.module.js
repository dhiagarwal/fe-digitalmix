'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('../checkout.distributor.component.scss');

var payMethodComponent = require('./payment.method.component');
var paymentMethodService = require('./payment.method.component.service');
var formUtils = require('../../../../__shared/form-utils/form-utils'); 

var paymentMethodModule = angular.module('paymentMethodModule', [uiRouter, formUtils.name])
		.component('distributorpaymentmethod', payMethodComponent)
		.service('paymentMethodComponentService', paymentMethodService);

module.exports = paymentMethodModule;
