'use strict';
var _ = require('lodash');
var baseCheckoutController = require('../base.checkout.distributor.controller');

shippingAddressCtrl.prototype = Object.create(baseCheckoutController.prototype);
function shippingAddressCtrl(shippingAddressComponentService, $state, $uibModal){
	this.shippingAddressData = {};
    this.$state = $state;
	this.$uibModal = $uibModal;

	var model = this.model = {};
	var self = this;
	baseCheckoutController.call(this);
	self.model.isPullLabelVisible = true;
	self.model.isChecked = false;
	model.heading1 = "RONALD SISK";
	model.mainHeading = "CHECKOUT";

	model.heading2 = "SHIPPING ADDRESS";

	this.emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	model.shippingAddress = {};
	model.myAddresses = [];
	this.showLess();
	model.formData = {};
	model.sortedAddresses = [];
	self.model.promoLengthErr = false;
	var promise = shippingAddressComponentService.getFormdata();

	promise.then(function(response){
		self.model.shippingAddress.data = response.shippingAddress.data;
		self.model.shippingAddress.fields = response.shippingAddress.fields;
		self.model.shippingAddress.states = response.states;
		self.model.shippingAddress.zipcodeLookUp = response.shippingAddress.zipcodeLookUp;
		self.model.shippingAddress.psvData = response.psvData;

		self.model.myAddresses = response.myAddresses;
		self.checkDefault();
		self.model.sortedAddresses = _.sortBy(self.model.myAddresses, function(o){ return o.isPrimary === 'NO';});
	});


	self.saveAndContinue = function(isValid) {
		if (isValid) {
			window.location.href = "#/checkout/deliveryoptions";
		}
	};

	self.applyPromoCode = function(code) {

        if (!code) {
            self.model.err = true;
            return;
        }
        if (self.model.shippingAddress.data.promotionAndRewardsApplied.length >= 1) {
            self.model.promoCode = '';
			self.model.promoLengthErr = true;
            return;
        }
        self.model.shippingAddress.data.promotionAndRewardsApplied.push(code);
        self.model.promoCode = '';
    };

	self.gotoPage = function(pageName) {
		window.location.href = pageName;
		window.location.reload();
	};

	self.removePromoCode = function(promoCode) {
		_.pull(self.model.shippingAddress.data.promotionAndRewardsApplied, promoCode);
	};

	self.zipcodeLookup = function() {
		var flag = true, foundData = null;
		angular.forEach(self.model.shippingAddress.zipcodeLookUp, function(value, key) {
			if (flag && value.zipcode == self.model.shippingAddress.data.zipcode) {
				foundData = value;
				flag = false;
			}
		});
		if (foundData) {
			self.model.shippingAddress.data.zipcode = foundData.zipcode;
			self.model.shippingAddress.data.state = foundData.state;
			self.model.shippingAddress.data.city = foundData.city;
			self.model.shippingAddress.data.addressLine1 = foundData.addressLine1;
			self.model.shippingAddress.data.addressLine2 = foundData.addressLine2;
		}
	};

    self.reposition = function() {
		var modal = $(this),
		dialog = modal.find('.modal-dialog');
		modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    };

    $('.modal').on('show.bs.modal', self.reposition);
    $(window).on('resize', function() {
    	$('.modal:visible').each(self.reposition);
    });

};

shippingAddressCtrl.prototype.modalReSize = function(){
	var ele = angular.element(document).find('.modal-sm');
};

shippingAddressCtrl.prototype.toggleErr = function(){
    this.model.err = false;
    this.model.promoLengthErr = false;
};

shippingAddressCtrl.prototype.onAddressDelete = function(item){
	this.showDeleteModal(item);
};

shippingAddressCtrl.prototype.showMore= function(){
	this.model.showMore = false;
};


shippingAddressCtrl.prototype.showLess= function(){
	this.model.showMore = true;
};

shippingAddressCtrl.prototype.showDeleteModal = function(item){
	var self = this;

	this.showConfirmationModal('Are you Sure you want to permanently remove this address from your account?', 'YES - DELETE ADDRESS', 'NO - CANCEL').then(function(){
		self.deleteAddress(item);
	});
};

shippingAddressCtrl.prototype.goBack = function(){
	var self = this;

	this.showConfirmationModal('Are you Sure you want to go back and not save any of the information?', 'YES - GO BACK TO SHIPPING ADDRESS', 'NO - CANCEL').then(function(){
		self.model.isAdd = false;
		self.model.isEdit = false;
	});
};

shippingAddressCtrl.prototype.onAddressEdit = function(item){
	this.model.isEdit = true;
	angular.extend(this.model.formData, item);
};

shippingAddressCtrl.prototype.addNewAddress = function(){
	this.model.isAdd = true;
	this.model.formData = {isPrimary: 'NO', blindKey: Math.floor(Math.random()*100000+1)};
};

shippingAddressCtrl.prototype.navigateToDelivery = function(){
	window.location.href='#/checkout-delivery/delivery-options';
};

shippingAddressCtrl.prototype.updateAddress = function(formData, addNew, form){
	if(addNew){
		formData.phone = form.phone.$viewValue;
		this.model.myAddresses.push(formData);
	}
	else{
		angular.extend(_.find(this.model.myAddresses, {blindKey: formData.blindKey}), formData);
		//this.isDefault(_.find(this.model.myAddresses, {blindKey: formData.blindKey}));
	}
	this.sortPrimaryAddress();
	this.checkDefault();
	this.model.isAdd = false;
	this.model.isEdit = false;
};

shippingAddressCtrl.prototype.deleteAddress = function(item){
	_.pull(this.model.myAddresses, item);
	this.sortPrimaryAddress();
};

shippingAddressCtrl.prototype.sortPrimaryAddress = function(){
	this.model.sortedAddresses = _.sortBy(this.model.myAddresses, function(o){ return o.isPrimary === 'NO';});
};

shippingAddressCtrl.prototype.toggleDefaultCheck = function(item){
	_.forEach(this.model.myAddresses, function(address){
		address.isPrimary = "NO";
	}, this);
	item.isPrimary = "YES";
};

shippingAddressCtrl.prototype.isDefault = function(item){
	this.toggleDefaultCheck(item);
};

shippingAddressCtrl.prototype.navigateToDelivary = function(){
	window.location.href = '#/checkout-delivery/delivery-options';
};

shippingAddressCtrl.prototype.checkDefault = function(){
	this.isChecked = _.find(this.model.myAddresses, {isPrimary: 'YES'});
};

module.exports  = shippingAddressCtrl;