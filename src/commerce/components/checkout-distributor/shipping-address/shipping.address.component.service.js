'use strict';

shippingAddressComponentService.$inject = ['$http','$q'];

function shippingAddressComponentService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	$http.get('../../../../../src/commerce/components/checkout-distributor/shipping-address/shipping.address.component.json').then(function(response) {
    	deferred.resolve(response.data);
  	});

  this.getFormdata = function() {
    return deferred.promise;
  }
};

module.exports = shippingAddressComponentService;