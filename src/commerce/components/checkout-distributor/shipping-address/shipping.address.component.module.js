'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('../checkout.distributor.component.scss');
var distributorShippingAddress = require('./shipping.address.component');
var shippingAddressComponentService = require('./shipping.address.component.service');
var formUtils = require('../../../../__shared/form-utils/form-utils'); 

var shipAddressModule = angular.module('shipAddressModule  ', [uiRouter, formUtils.name])
		.component('distributorshippingaddress', distributorShippingAddress )
		.service('shippingAddressComponentService', shippingAddressComponentService);

module.exports = shipAddressModule ;