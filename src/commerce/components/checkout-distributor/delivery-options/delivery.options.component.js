'use strict';
var template = require('./delivery.options.component.jade');
var controller = require('./delivery.options.component.controller');

var deliveryOptionsComponent = {
	templateUrl: template,
	controller: controller,
	controllerAs: 'ctrl',
	bindToController: true
};

module.exports = deliveryOptionsComponent;