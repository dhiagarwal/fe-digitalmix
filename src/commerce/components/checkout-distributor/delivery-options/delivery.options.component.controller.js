'use strict';

var baseCheckoutController = require('../base.checkout.distributor.controller');

deliveryOptionsController.prototype = Object.create(baseCheckoutController.prototype);
function deliveryOptionsController(deliveryOptionsComponentService){
	var model = this.model = {};
	var self = this;
	baseCheckoutController.call(this);

	this.deliveryOptionsData = {};

	this.model.isPullLabelVisible = true;
	model.heading1 = "";
	model.mainHeading = "CHECKOUT";

	model.heading2 = "Delivery Options";
	self.model.promoLengthErr = false;
	model.deliveryOptions = {};

	var promise = deliveryOptionsComponentService.getFormdata();

	promise.then(function(response){
		self.model.heading1 = response.deliveryOptions.user;
		self.model.deliveryOptions.shippingList = response.deliveryOptions.shippingList;
		self.model.deliveryOptions.fields = response.deliveryOptions.fields;
		self.model.deliveryOptions.data = response.deliveryOptions.data;
		self.model.deliveryOptions.psvData = response.deliveryOptions.psvData;
	});

	self.gotoPage = function(pageName) {
		window.location.href = pageName;
	};

	self.reposition = function() {
		var modal = $(this),
		dialog = modal.find('.modal-dialog');
		modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    };

    $('.modal').on('show.bs.modal', self.reposition);
    $(window).on('resize', function() {
    	$('.modal:visible').each(self.reposition);
    });

	self.applyPromoCode = function(code) {
        if(!code){
            self.model.err = true;
            return;
        }
        if(self.model.deliveryOptions.data.promotionAndRewardsApplied.length >= 1){
            self.model.promoCode = '';
            self.model.promoLengthErr = true;
            return;
        }
        self.model.deliveryOptions.data.promotionAndRewardsApplied.push(code);
        self.model.promoCode = '';
	};

	self.removePromoCode = function(promoCode) {
        _.pull(self.model.deliveryOptions.data.promotionAndRewardsApplied, promoCode);
	};

	self.goBack = function() {
		window.location.href = "#/checkout-delivery/ship-address";
	};

	self.saveAndContinue = function(isValid) {
		if (isValid && self.model.deliveryOptions.shippingType) {}
			window.location.href = "#/checkout-delivery/payment-method";

	};

    self.gotoPage = function(pageName) {
        window.location.href = pageName;
        window.location.reload();
    };

};


deliveryOptionsController.prototype.toggleErr= function(){
    this.model.err = false;
    this.model.promoLengthErr = false;
};


module.exports  = deliveryOptionsController;