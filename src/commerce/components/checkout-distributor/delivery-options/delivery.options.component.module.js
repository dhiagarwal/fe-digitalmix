'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var checkoutdeliveryoptions = require('./delivery.options.component');
var deliveryOptionsComponentService = require('./delivery.options.component.service');

var deliveryOptionsModule = angular.module('deliveryOptionsModule ', [uiRouter])
		.component('checkoutdeliveryoptions', checkoutdeliveryoptions)
		.service('deliveryOptionsComponentService', deliveryOptionsComponentService)

module.exports = deliveryOptionsModule ;