var angular = require('angular');
var uiRouter =require('angular-ui-router');
require('angular-slick-carousel');
var pdpVariationsComponent = require('./pdp.variations.component');
var pdpVariationsComponentService = require('./pdp.variations.component.service');
var pdpVariationsComponentModule = angular.module('pdpVariationsComponentModule', [uiRouter])
    .component('pdpVariationsComponent', pdpVariationsComponent)
    .service('pdpVariationsComponentService', pdpVariationsComponentService);

import Styles from './pdp.variations.component.scss';

module.exports = pdpVariationsComponentModule;
