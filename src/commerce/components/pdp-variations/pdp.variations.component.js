'use strict';

var template = require('./pdp.variations.component.jade');
var controller = require('./pdp.variations.component.controller');

var pdpVariationsComponent =  {
    bindings: {
      variationType : '=',
      productData : '=',
      headingData : '=',
      showError: '=',
      slickData: "="
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'variations'
};

module.exports = pdpVariationsComponent;
