'use strict';

pdpVariationsComponentService.$inject = ['$http','$q'];

function pdpVariationsComponentService($http,$q) {
  this.$http = $http;
  var deferred = $q.defer();

  this.getProductData = function(variationType) {
    var url = "";
    deferred = $q.defer();

    if(variationType == "shade"){
      var shadeUrl = '../../../../src/commerce/components/pdp-variations/pdp.variations.shades.json';
    } else if(variationType == "dropDown"){
      var shadeUrl = '../../../../src/commerce/components/pdp-variations/pdp.variations.component.json';
    }
    url = shadeUrl;
    $http.get(url).then(function(response) {
      deferred.resolve(response.data);
    });
  }

  this.setProductData = function() {
    return deferred.promise;
  }

};

module.exports = pdpVariationsComponentService;
