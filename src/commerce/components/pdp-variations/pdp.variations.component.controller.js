'use strict';

function pdpVariationsComponentController(pdpVariationsComponentService, $interval){

  var self = this;

  this.getData = [];
  this.cosmetics = {};

  this.showShadesDiv = false;
  this.showDropDownDiv = false;
  this.variationTypeHeading = "";
  this.showError = this.showError || false;
  this.selectedVariant = "";

  this.variationTypeHeading = this.headingData;

  if(this.variationType == "shade"){
    this.showShadesDiv = true;
  } else if(this.variationType == "dropDown"){
    this.showDropDownDiv = true;
  }
  pdpVariationsComponentService.getProductData(this.variationType);
  var promise = pdpVariationsComponentService.setProductData();
  promise.then(function(data) {
    self.cosmetics = data.cosmetics;
  });

  this.loadProduct = function(index, event){
    this.selectedVariant = this.cosmetics[index].color;
    this.getData = [];
    if(this.cosmetics[index].length !== 0){
      var colorPicker = angular.element('.color-picker');
      colorPicker.removeClass('selected');
      $(event.target).addClass('selected');
      this.getData = this.cosmetics[index].colorImg;
      this.productData = [];
      angular.element("#slickProduct").slick('slickRemove');
      angular.element("#slickProduct").slick('slickAdd');
      this.productData = this.getData;
      angular.element("#slickProduct").not('.slick-initialized').slick();
      this.showError = false;
      self.slickData.enabled= false;
      $interval(function() {
          self.slickData.adaptiveHeight = true;
          self.slickData.enabled = true;
      }, 200, [1]);
    }
  };

  this.loadProductOnChange = function(selectedItem){
    this.showError = false;
    var index = angular.element("#productDropDown").val();
    self.productData = [];
    angular.element("#slickProduct").slick('slickRemove');
    angular.element("#slickProduct").slick('slickAdd');
    self.productData = selectedItem.colorImg;
    angular.element("#slickProduct").not('.slick-initialized').slick();
    self.slickData.enabled = false;
    $interval(function () {
      self.slickData.adaptiveHeight = true;
      self.slickData.enabled = true;
    }, 200, [1]);
  };

}

module.exports= pdpVariationsComponentController;
