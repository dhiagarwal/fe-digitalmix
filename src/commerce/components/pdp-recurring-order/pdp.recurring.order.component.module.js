var angular =require('angular');
var pdpRecurringOrderComponent =require('./pdp.recurring.order.component');
var pdpRecurringOrderService = require('./pdp.recurring.order.component.service');

var pdpRecurringOrderModule = angular.module('pdpRecurringOrder', [
])
.component('pdpRecurringOrder', pdpRecurringOrderComponent)
.service('pdpRecurringOrderService', pdpRecurringOrderService);

import Styles from './pdp.recurring.order.component.scss';

module.exports= pdpRecurringOrderModule;