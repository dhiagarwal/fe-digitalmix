'use strict'
pdpRecurringOrderController.$inject=["pdpRecurringOrderService","$scope", "$uibModalInstance", "$timeout"];
function pdpRecurringOrderController(pdpRecurringOrderService, $scope, $uibModalInstance, $timeout){
  this.result = {};
  var model = this.model = {};
  model.createNewOrder = false;
  model.newRecurringOrderCreated = true;
  model.addtoExistingOrder = false;
  model.hasexistingRecurringAcc = true;
  model.currentOrderSelected = true;  
  this.$uibModalInstance = $uibModalInstance;
  this.$timeout = $timeout;
  this.pdpRecurringOrderService = pdpRecurringOrderService;
  var setExistingRecurringOrder = pdpRecurringOrderService.getExistingRecurringOrder();
  setExistingRecurringOrder.then(function(data) {
    model.result = data;
    model.existingRecurringOrder = model.result.recurringOrder;
    model.shoppingCartDetail = model.result.shoppingCart;
  });
  this.newOrder = function() {
      model.createNewOrder = true;
      model.addtoExistingOrder =false;
  };
  this.addToExisting = function(recurringData){
    model.recurringItem = recurringData;
    model.currentOrderSelected = false;
    model.createNewOrder = false;
    model.addtoExistingOrder =true;
  }
  this.addtoCart = function() {
    var self = this;
    model.newOrderShoppingCart = true;
    this.$timeout(function(){
      self.$uibModalInstance.close();
    }, 3000);

  };
  this.addtoRecurringOrder = function() {
    model.existingRecurringShoppingCart = true;
  };
  this.seeAllItems = function(){
    if(model.seeAllitems == true){
      model.seeAllitems = false;
    }
    else
      model.seeAllitems = true;
  };
  this.addtoCurrentOrder = function() {
    model.currentOrderSelected = true;
    model.addtoExistingOrder =false;
  };
  this.cancel = function() {
    this.$uibModalInstance.dismiss();
  };
};

module.exports = pdpRecurringOrderController;