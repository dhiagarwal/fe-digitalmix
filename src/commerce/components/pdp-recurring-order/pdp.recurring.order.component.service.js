'use strict';

pdpRecurringOrderService.$inject = ['$http','$q'];
function pdpRecurringOrderService($http,$q) {
    var deferred = $q.defer();
    $http.get('../../../../src/commerce/components/pdp-recurring-order/pdp.recurring.order.component.json').then(function(response) {
    deferred.resolve(response.data);
    });

    this.getExistingRecurringOrder = function() {
        return deferred.promise;
    }
  
}
module.exports= pdpRecurringOrderService;