var template =require('./pdp.recurring.order.component.jade');
var controller =require('./pdp.recurring.order.component.controller.js');

var pdpRecurringOrderComponent =  {
	transclude: true,
    bindings:{
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'  
};

module.exports= pdpRecurringOrderComponent;