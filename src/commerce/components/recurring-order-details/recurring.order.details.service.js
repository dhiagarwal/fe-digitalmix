'use strict';

recurringOrderDetailsService.$inject = ['$http','$q'];

function recurringOrderDetailsService($http,$q) {
  this.$http = $http;

  var deferred = $q.defer();
  var deferredForm = $q.defer();
  var deferredCard = $q.defer();

  $http.get('../../../../../src/commerce/components/recurring-order-details/recurring.order.details.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getOrderData = function() {
    return deferred.promise;
  }

  $http.get('../../../../../src/commerce/components/recurring-order-details/shipping.address.component.json').then(function(response) {
    	deferredForm.resolve(response.data);
	});

  this.getFormData = function() {
    return deferredForm.promise;
  }

  $http.get('../../../../../src/commerce/components/recurring-order-details/payment.method.component.json').then(function(response) {
    	deferredCard.resolve(response.data);
  	});

  this.getCCdata = function() {
    return deferredCard.promise;
  }

};

module.exports = recurringOrderDetailsService;
