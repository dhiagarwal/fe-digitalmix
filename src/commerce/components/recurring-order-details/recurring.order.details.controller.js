'use strict';
var _ = require('lodash');
var baseCheckoutController = require('../../../../src/commerce/components/checkout-distributor/base.checkout.distributor.controller');
recurringOrderDetailsController.prototype = Object.create(baseCheckoutController.prototype);

function recurringOrderDetailsController(recurringOrderDetailsService, $state, $uibModal) {

  var self = this;
  this.$uibModal = $uibModal;

  this.$state = $state;
  var model = this.model = {};
  model.sortedAddresses = [];
  model.shippingAddress = {};
  model.paymentMethod = {};
  model.formData = {};
  this.editShippingAddress = false;
  this.editPaymentOrder = false;
  this.editOrderWeek = false;
  this.isCheckedOrder = "2 Weeks";
  this.isCheckedOrderDisplay = this.isCheckedOrder;
  this.isCheckedOrderSM = "3 Weeks";
  this.isCheckedOrderSMDisplay = this.isCheckedOrderSM;
  this.productQuantityMax = 50;
  this.productQuantityMin = 0;
  this.recurringOrderPause = true;
  this.recurringOrderPauseSM = true;

  this.emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  var states = {
      isShipping: 'shippingaddress',
      isDelivery: 'deliveryoptions',
      isPayment: 'paymentmethod',
      orderPlaced: 'orderconfirmation'
  };

  this.checkoutData = {};
  self.videoPlaying = false;
  self.videoBannerShow = true;
  self.cardDismissShow = false;
  var promise = recurringOrderDetailsService.getOrderData();
  promise.then(function(data) {
      self.checkoutData = data;
  });

  var promise1 = recurringOrderDetailsService.getFormData();
	promise1.then(function(response){
    self.model.shippingAddress.data = response.shippingAddress.data;
		self.model.shippingAddress.fields = response.shippingAddress.fields;
		self.model.shippingAddress.states = response.states;
		self.model.shippingAddress.zipcodeLookUp = response.shippingAddress.zipcodeLookUp;
		self.model.shippingAddress.psvData = response.psvData;

    self.model.myAddresses = response.myAddresses;
		self.checkDefault();
		self.model.sortedAddresses = _.sortBy(self.model.myAddresses, function(o){ return o.isPrimary === 'NO';});
	});

  var promise2 = recurringOrderDetailsService.getCCdata();
  promise2.then(function(response){
    self.model.paymentMethod.data = response.paymentMethod.data;
		self.model.paymentMethod.fields = response.paymentMethod.fields;
		self.model.paymentMethod.states = response.paymentMethod.states;
		self.model.paymentMethod.zipcodeLookUp = response.paymentMethod.zipcodeLookUp;
		self.model.paymentMethod.creditCardTypes = response.paymentMethod.creditCardTypes;
		self.model.paymentMethod.psvData = response.paymentMethod.psvData;
		self.model.paymentMethod.myCards = response.paymentMethod.myCards;
		self.selectPrimary();
  });

  this.reposition = function() {
  var modal = $(this),
  dialog = modal.find('.modal-dialog');
  modal.css('display', 'block');
      dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
  };

  $('.modal').on('show.bs.modal', self.reposition);
  $(window).on('resize', function() {
    $('.modal:visible').each(self.reposition);
  });

  this.checkDefault = function(){
  	this.isChecked = _.find(this.model.myAddresses, {isPrimary: 'YES'});
  };

  this.addNewAddress = function(){
  	this.model.isAdd = true;
  	this.model.formData = {isPrimary: 'NO', blindKey: Math.floor(Math.random()*100000+1)};
  };

  this.goBack = function(){
    var self = this;
  	this.showConfirmationModal('Are you Sure you want to go back and not save any of the information?', 'YES - GO BACK TO SHIPPING ADDRESS', 'NO - CANCEL').then(function(){
  		self.model.isAdd = false;
  		self.model.isEdit = false;
  	});
  };

  this.onAddressEdit = function(item){
  	this.model.isEdit = true;
  	angular.extend(this.model.formData, item);
  };

  this.onEditIcon = function(stateProp){
    if(stateProp == "isShipping"){
        this.editShippingAddress = true;
    } else if(stateProp == "isPayment"){
      this.editPaymentOrder = true;
    }
  };

  this.showMore= function(){
  	this.model.showMore = false;
  };

  this.showLess= function(){
  	this.model.showMore = true;
  };

  this.selectPrimary= function(){
  	this.cardSelected(_.find(this.model.paymentMethod.myCards, {isPrimary: true}));
  };

  this.cardSelected = function(card){
  	this.model.selectedCard = card;
  	this.model.isCheckedCard = card;
  };

  this.updateAddress = function(formData, addNew, form){
    if(formData.isPrimary == "YES"){
      this.clearAllPrimary();
    }
  	if(addNew){
  		formData.phone = form.phone.$viewValue;
  		this.model.myAddresses.push(formData);
  	}
  	else{
      formData.phone = form.phone.$viewValue;
  		angular.extend(_.find(this.model.myAddresses, {blindKey: formData.blindKey}), formData);
  	}
  	this.sortPrimaryAddress();
  	this.checkDefault();
  	this.model.isAdd = false;
  	this.model.isEdit = false;
    this.checkoutData.shippingAddress = _.find(this.model.myAddresses, {isPrimary: "YES"});
  };

  this.clearAllPrimary = function(){
    this.model.myAddresses.forEach(function(x){
  	   x.isPrimary = "NO";
     });
  };

  this.sortPrimaryAddress = function(){
  	this.model.sortedAddresses = _.sortBy(this.model.myAddresses, function(o){ return o.isPrimary === 'NO';});
  };

  this.deleteAddress = function(item){
  	_.pull(this.model.myAddresses, item);
  	this.sortPrimaryAddress();
  };

  this.saveAddressChanges = function(){
    this.editShippingAddress = false;
    this.checkoutData.shippingAddress = this.isChecked;
  };

  this.saveCreditCardChanges = function(isValid) {
    this.editPaymentOrder = false;
  };

  this.showOrderWeek = function(callFrom){
    if(callFrom == "sm"){
      this.isCheckedOrderSM = this.isCheckedOrderSMDisplay;
    } else{
      this.isCheckedOrder = this.isCheckedOrderDisplay;
    }
    this.editOrderWeek = true;
  };

  this.cancelOrderWeek = function(){
    this.editOrderWeek = false;
  };

  this.saveOrderWeek = function(callFrom){
    if(callFrom == "sm"){
      this.isCheckedOrderSMDisplay = this.isCheckedOrderSM;
    } else{
      this.isCheckedOrderDisplay = this.isCheckedOrder;
    }
    this.editOrderWeek = false;
  };

  this.changeQuantity = function(value,index,operator){
      if(operator == '-' && value > 1){
          this.checkoutData.bagItems[index].quantity--;
          this.reduceCost(this.checkoutData.bagItems[index].quantity, this.checkoutData.bagItems[index].itemCost);
      }
      if(operator == '+'){
          this.checkoutData.bagItems[index].quantity++;
          this.addCost(this.checkoutData.bagItems[index].quantity, this.checkoutData.bagItems[index].itemCost);
      }
  };

  this.addCost = function(qty, cost){
      console.log(this.checkoutData.orderSummary.estimatedTotal);
      console.log(cost);
      this.checkoutData.orderSummary.estimatedTotal = this.checkoutData.orderSummary.estimatedTotal + cost;
  };

  this.reduceCost = function(qty, cost){
      this.checkoutData.orderSummary.estimatedTotal = this.checkoutData.orderSummary.estimatedTotal - cost;
  };

  this.showLess();
  this.showCardLess();
};

recurringOrderDetailsController.prototype.onAddressDelete = function(item){
	this.showDeleteModal(item);
};

recurringOrderDetailsController.prototype.showDeleteModal = function(item){
	var self = this;
	this.showConfirmationModal('Are you Sure you want to permanently remove this address from your account?', 'YES - DELETE ADDRESS', 'NO - CANCEL').then(function(){
		self.deleteAddress(item);
	});
};

recurringOrderDetailsController.prototype.onCardEdit = function(card, event){
	event.stopPropagation();
	this.model.paymentMethod.data.name = card.name;
	this.model.paymentMethod.data.cardNumber = card.cardNumber;
	this.model.paymentMethod.handleCreditCardNumber = card.cardNumber;
	this.model.paymentMethod.data.expiry = card.expiry;
	this.model.paymentMethod.data.cvv = card.cvv;
	//this.model.paymentMethod.data.zip = card.zip;

	angular.extend(this.model.paymentMethod.data, card);
	this.model.isCardAdd = true;
	this.model.isCardEdit = true;
};

recurringOrderDetailsController.prototype.saveCreditCard = function(formValid, data, isEdit){
	var copy;
	if(formValid){
    if(data.isPrimary){
      this.clearAllPrimaryCards();
    }
		if(isEdit){
			angular.extend(_.find(this.model.paymentMethod.myCards, {cardNumber: data.cardNumber}), data);
			this.model.isCardEdit = false;
			this.model.isCardAdd = false;
		} else{
      this.checkCreditCardType()
			data.cardNumber = this.model.paymentMethod.handleCreditCardNumber;
			copy = angular.copy(data);
			this.model.paymentMethod.myCards.push(copy);
      this.model.isCardEdit = false;
			this.model.isCardAdd = false;
			this.resetParams.bind(this);
		}
	}
	this.sortPrimary();
	this.selectPrimary();
	this.togglePaymentSection('.payment-section', '.payment-section-validated');

};

recurringOrderDetailsController.prototype.clearAllPrimaryCards = function(){
  this.model.paymentMethod.myCards.forEach(function(x){
	   x.isPrimary = false;
   });
};

recurringOrderDetailsController.prototype.onCancel = function(){
	var self = this;
	this.showConfirmationModal('Are you Sure you want to go back and not save any of the information?','YES - GO BACK TO PAYMENT METHOD','NO - CANCEL').then(function () {
		self.model.isCardAdd = false;
		self.model.isCardEdit = false;
	});
};

recurringOrderDetailsController.prototype.sortPrimary = function(){
	this.model.paymentMethod.myCards = _.sortBy(this.model.paymentMethod.myCards, function(o){ return !o.isPrimary;});
};

recurringOrderDetailsController.prototype.togglePaymentSection = function(source, target){
	$(source).fadeIn(1000);
	$(target).fadeOut(0);
};

recurringOrderDetailsController.prototype.onCardDelete = function(item, event){
	event.stopPropagation();
	this.showCardDeleteModal(item);
};

recurringOrderDetailsController.prototype.showCardDeleteModal = function(item){
	var self = this;
	this.showConfirmationModal('Are you Sure you want to permanently remove this card from your list?','YES - DELETE CARD','NO - CANCEL').then(function () {
		self.deleteCard(item);
	});
};

recurringOrderDetailsController.prototype.isItemPrimary= function(item){
	return item.isPrimary;
};

recurringOrderDetailsController.prototype.deleteCard= function(item){
	if(this.isItemPrimary(item)){
		_.pull(this.model.paymentMethod.myCards, item);
		if(_.find(this.model.paymentMethod.myCards, {isPrimary: true})){
			this.selectPrimary();
		}
		else{
			this.model.paymentMethod.myCards[0].isPrimary = true;
			this.selectPrimary();
		}
		return;
	}
	_.pull(this.model.paymentMethod.myCards, item);

};

recurringOrderDetailsController.prototype.onAddNew = function(){
	this.model.isCardAdd = true;
	this.resetParams();
};

recurringOrderDetailsController.prototype.resetParams = function(){
	this.model.paymentMethod.data.name="";
	this.model.paymentMethod.data.cardNumber="";
	this.model.paymentMethod.handleCreditCardNumber = "";
	this.model.paymentMethod.data.expiry="";
	this.model.paymentMethod.data.cvv="";
	//this.model.paymentMethod.data.zip="";
  this.formPayment.$setPristine();
  this.formPayment.$setUntouched();
};

recurringOrderDetailsController.prototype.showCardMore = function(){
  this.model.showCardMore = false;
};

recurringOrderDetailsController.prototype.showCardLess = function(){
  this.model.showCardMore = true;
};

recurringOrderDetailsController.prototype.removeItem = function(item){
    var itemToRemove = _.find(this.checkoutData.bagItems, {itemNo: item});
    _.pull(this.checkoutData.bagItems, itemToRemove);
    this.estimatedTotal();
};

recurringOrderDetailsController.prototype.estimatedTotal = function(){

    var self = this;

    this.checkoutData.orderSummary.estimatedTotal = 0;

    if(this.checkoutData.bagItems.length == 0){
        return;
    }

    this.checkoutData.bagItems.forEach(function(x){
        self.checkoutData.orderSummary.estimatedTotal = self.checkoutData.orderSummary.estimatedTotal + (x.quantity * x.itemCost);
    });
};

recurringOrderDetailsController.prototype.pauseRecurringOrderPause = function(orderCall){
  var self = this;
  this.showConfirmationModal('Are you Sure you want to pause this Order?','YES - Pause Order','NO - CANCEL').then(function () {
    if(orderCall == 'sm'){
      self.recurringOrderPauseSM = false;
    } else{
      self.recurringOrderPause = false;
    }
	});
};

recurringOrderDetailsController.prototype.pauseRecurringOrderResume = function(orderCall){
  var self = this;
  this.showConfirmationModal('Are you Sure you want to resume this Order?','YES - Resume Order','NO - CANCEL').then(function () {
    if(orderCall == 'sm'){
      self.recurringOrderPauseSM = true;
    } else{
      self.recurringOrderPause = true;
    }
	});
};

recurringOrderDetailsController.prototype.cancelRecurringOrder = function(orderCall){
  this.showConfirmationModal('Are you Sure you want to Cancel this Order?','YES - Cancel Order','NO - Discard').then(function () {
	});
};

recurringOrderDetailsController.prototype.checkCreditCardType = function() {
  var self = this;
  var cardNumber = self.model.paymentMethod.handleCreditCardNumber ? self.model.paymentMethod.handleCreditCardNumber : null;
  var isGetCreditCardType = cardNumber && (cardNumber.length == 19) && self.model.paymentMethod.data.name && self.model.paymentMethod.data.expiry && self.model.paymentMethod.data.cvv;
  self.model.paymentMethod.cardNumber = self.model.paymentMethod.handleCreditCardNumber;
  if (isGetCreditCardType) {
    self.model.paymentMethod.cardNumber = cardNumber;
    self.model.test = self.GetCardType(self.model.paymentMethod.cardNumber);
    switch(self.GetCardType(self.model.paymentMethod.cardNumber)) {
      case self.model.paymentMethod.creditCardTypes.visa:
      self.model.creditCardType = self.model.paymentMethod.creditCardTypes.visa;
        self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.visa;
      break;

      case self.model.paymentMethod.creditCardTypes.mastercard:
      self.model.creditCardType = self.model.paymentMethod.creditCardTypes.mastercard;
        self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.mastercard;
        break;

      case self.model.paymentMethod.creditCardTypes.discover:
      self.model.creditCardType = self.model.paymentMethod.creditCardTypes.discover;
        self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.discover;

      break;

      case self.model.paymentMethod.creditCardTypes.amex:
      self.model.creditCardType = self.model.paymentMethod.creditCardTypes.amex;
        self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.amex;
      break;

      default:
      self.model.creditCardType = self.model.paymentMethod.creditCardTypes.other;
        self.model.paymentMethod.data.cardType = self.model.paymentMethod.creditCardTypes.other;

        break;
    }
  }
};

recurringOrderDetailsController.prototype.GetCardType = function(number)
{
  var self = this;
  // visa
  var re = new RegExp("^4");
  if (number.match(re) != null)
    return self.model.paymentMethod.creditCardTypes.visa;

  // Mastercard
  re = new RegExp("^5[1-5]");
  if (number.match(re) != null)
    return self.model.paymentMethod.creditCardTypes.mastercard;

  // AMEX
  re = new RegExp("^3[47]");
  if (number.match(re) != null)
    return "AMEX";

  // Discover
  re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
  if (number.match(re) != null)
    return self.model.paymentMethod.creditCardTypes.discover;

  // Diners
  re = new RegExp("^36");
  if (number.match(re) != null)
    return "Diners";

  // Diners - Carte Blanche
  re = new RegExp("^30[0-5]");
  if (number.match(re) != null)
    return "Diners - Carte Blanche";

  // JCB
  re = new RegExp("^35(2[89]|[3-8][0-9])");
  if (number.match(re) != null)
    return "JCB";

  // Visa Electron
  re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
  if (number.match(re) != null)
    return "Visa Electron";

  return null;
};

recurringOrderDetailsController.prototype.getLastFourDigitsOfPaymentCard = function (input) {
    if (parseInt(input)) {
        var truncatedCardNumber = "";
        for (var i = input.length - 4; i < input.length; i++) {
            truncatedCardNumber += input[i];
        }
        return truncatedCardNumber;
    }
};


module.exports = recurringOrderDetailsController;
