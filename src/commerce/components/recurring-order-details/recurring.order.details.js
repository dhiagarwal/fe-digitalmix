'use strict';
var template = require('./recurring.order.details.jade');
var templateShipping = require('./shipping.address.edit.order.details.jade');
var templatePayment = require('./payment.edit.order.details.jade');

var controller = require('./recurring.order.details.controller');

var recurringOrderDetails = {
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl'
};

module.exports = recurringOrderDetails;
