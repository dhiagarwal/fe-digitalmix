var angular = require('angular');
var uiRouter = require('angular-ui-router');

var recurringOrderDetails = require('./recurring.order.details');
var recurringOrderDetailsService = require('./recurring.order.details.service');

var recurringOrderDetailsModule = angular.module('recurringOrderDetailsModule', [uiRouter])
.component('recurringOrderComponent', recurringOrderDetails)
.service('recurringOrderDetailsService', recurringOrderDetailsService);

import Styles from './recurring.order.details.scss';

module.exports = recurringOrderDetailsModule;
