var angular =require('angular');
var uiRouter =require('angular-ui-router');
var myStoreComponent =require('./my.store.component');
var myStoreService =require('./my.store.service');
var myStoreModule = angular.module('myStore', [uiRouter])
  .component('myStoreComponent', myStoreComponent)
  .service('myStoreService', myStoreService)

var Styles =require('./my.store.scss');

module.exports = myStoreModule;
