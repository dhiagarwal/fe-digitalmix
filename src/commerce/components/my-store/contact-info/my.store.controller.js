'use strict';

function myStoreController(myStoreService) {

  var self = this;
  var model = this.model = {};
  this.required = false;
  this.credentials = {};
  this.clickedSearch = false;
  this.credentials.userEmailField = "";
  model.phoneInfo = { editStatus: false, footerStatus: false, value: "8861138782" };
  model.emailInfo = { editStatus: false, footerStatus: true, value: "justindib19@gmail.com" };
  model.facebookInfo = { editStatus: false, footerStatus: true, value: "justindib19" };
  model.twitterInfo = { editStatus: false, footerStatus: false, value: "" };
  model.instagramInfo = { editStatus: false, footerStatus: false, value: "" };
  this.credentials.userEmailField = model.emailInfo.value;
  this.credentials.userPhoneField = model.phoneInfo.value;
  model.facebookInfo.text = model.facebookInfo.value;
  model.twitterInfo.text = model.twitterInfo.value;
  model.instagramInfo.text = model.instagramInfo.value;
  this.pageHeadline = "Contact Info";
  this.pageShortDescription = "Choose which methods customers may use to connect with you.";
  this.pageCategory = "Personal Care";
  this.pageSubCategory = "Face Care";
  this.bannerUrl = '../../../../src/assets/images/content/age-loc-me-27-img.png';

  this.editContactInfo = function(editType){
    if(editType == "email"){
      model.emailInfo.editStatus = true;
    } else if(editType == "phone"){
      model.phoneInfo.editStatus = true;
    } else if(editType == "facebook"){
      model.facebookInfo.editStatus = true;
    } else if(editType == "twitter"){
      model.twitterInfo.editStatus = true;
    } else if(editType == "instagram"){
      model.instagramInfo.editStatus = true;
    }
    this.clickedSearch = false;
    this.resetContactInfo(editType);
  }

  this.cancelContactInfo = function(editType){
    if(editType == "email"){
      this.credentials.userEmailField = model.emailInfo.value;
      model.emailInfo.editStatus = false;
    } else if(editType == "phone"){
      this.credentials.userPhoneField = model.phoneInfo.value;
      model.phoneInfo.editStatus = false;
    } else if(editType == "facebook"){
      model.facebookInfo.value = model.facebookInfo.text;
      this.facebookId = model.facebookInfo.value;
      model.facebookInfo.editStatus = false;
    } else if(editType == "twitter"){
      model.twitterInfo.value = model.twitterInfo.text;
      this.twitterId = model.twitterInfo.value;
      model.twitterInfo.editStatus = false;
    } else if(editType == "instagram"){
      model.instagramInfo.value = model.instagramInfo.text;
      this.instagramId = model.instagramInfo.value;
      model.instagramInfo.editStatus = false;
    }
    this.resetContactInfo(editType);
  }

  this.saveContactInfo = function(formvalid, editType) {
    var searchText = "";
    this.clickedSearch = true;
    if(formvalid){
      if(editType == "email"){
        searchText = this.credentials.userEmailField;
        model.emailInfo.value = searchText;
        model.emailInfo.editStatus = false;
      } else if(editType == "phone"){
        searchText = this.credentials.userPhoneField;
        model.phoneInfo.value = searchText;
        model.phoneInfo.editStatus = false;
        if (searchText !== "") {
            model.phoneInfoDisplay = this.convertPhoneText(searchText);
        } else{
          model.phoneInfoDisplay = "";
        }
      } else if(editType == "facebook"){
        searchText = this.model.facebookInfo.value;
        model.facebookInfo.text = searchText;
        model.facebookInfo.editStatus = false;
      } else if(editType == "twitter"){
        searchText = this.model.twitterInfo.value;
        model.twitterInfo.text = searchText;
        model.twitterInfo.editStatus = false;
      } else if(editType == "instagram"){
        searchText = this.model.instagramInfo.value;
        model.instagramInfo.text = searchText;
        model.instagramInfo.editStatus = false;
      }
    }
  }

  this.resetContactInfo = function(editType) {
    if(editType == "email"){
      this.credentials.isSubmitted = false;
      this.emailForm.$setPristine();
      this.emailForm.$setUntouched();
    } else if(editType == "phone"){
      this.credentials.isSubmitted = false;
      this.phoneForm.$setPristine();
      this.phoneForm.$setUntouched();
    } else if(editType == "facebook"){
      this.facebookForm.$setPristine();
      this.facebookForm.$setUntouched();
    } else if(editType == "twitter"){
      this.twitterForm.$setPristine();
      this.twitterForm.$setUntouched();
    } else if(editType == "instagram"){
      this.instagramForm.$setPristine();
      this.instagramForm.$setUntouched();
    }
  }

  this.convertPhoneText = function(convertText) {
    return "(" + convertText.substr(0, 3) + ") " + convertText.substr(3, 3) + "-" + convertText.substr(6);
  }

  if(model.phoneInfo.value){
    model.phoneInfoDisplay = this.convertPhoneText(model.phoneInfo.value);
  }
}
module.exports = myStoreController;
