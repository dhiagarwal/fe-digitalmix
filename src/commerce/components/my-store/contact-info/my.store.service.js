'use strict';

myStoreService.$inject = ['$http','$q'];

function myStoreService($http, $q) {
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/digital-toolkit/components/my-store/my.store.json').then(function(response) {
    deferred.resolve(response.data);
  });

  this.getSearchData = function() {
    return deferred.promise;
  }
}



module.exports = myStoreService;
