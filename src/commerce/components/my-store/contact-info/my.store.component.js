var template =require('./my.store.jade');
var controller =require('./my.store.controller');

var myStoreComponent =  {
    templateUrl : template,
    controller,
    controllerAs: 'ctrl'
};

module.exports = myStoreComponent;
