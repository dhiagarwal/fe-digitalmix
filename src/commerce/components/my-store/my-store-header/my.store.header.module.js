'use strict';
var angular = require('angular');

var myStoreHeaderComponent = require('./my.store.header');

var myStoreHeaderComponentModule = angular.module('myStoreHeaderModule', [])
                  .component('myStoreHeader',myStoreHeaderComponent);

import Styles from './my.store.header.scss';

module.exports = myStoreHeaderComponentModule;
