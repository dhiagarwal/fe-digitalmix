'use strict';
var template = require('./my.store.header.jade');
var controller = require('./my.store.header.controller');

var myStoreHeader = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl'
};

module.exports = myStoreHeader;
