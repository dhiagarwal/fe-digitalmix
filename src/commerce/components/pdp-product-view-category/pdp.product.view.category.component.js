'use strict';
var template = require('./pdp.product.view.category.jade');
var controller = require('./pdp.product.view.category.controller');

var pdpProductViewCategoryComponent = {
    bindings: {
      productData : '=',
      show : '<'
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'productCtrl'
};

module.exports = pdpProductViewCategoryComponent;
