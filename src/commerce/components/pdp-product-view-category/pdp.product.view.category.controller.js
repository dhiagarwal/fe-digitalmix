'use strict';

function pdpProductViewCategoryController($window,$location) {

  var self = this;
  this.navigatetopdpProductBasics = function(){
      $window.location.href = '#/pdpProductBasics';
  }
  this.isNew = function(){
    var temp = self.productData.isNew;
    if(temp == "true"){
      return true;
    }
    else{
      return false;
    }
  };
  this.topRated = function(){
    var temp = self.productData.topRated;
    if(temp == "true"){
      return true;
    }
    else{
      return false;
    }
  };
  this.redirect =function(product){
    $location.path('/pdpProductBasics');
  }
  this.priceChanged = function(){
    var temp = self.productData.priceChanged;
    if(temp == "true"){
      return true;
    }
    else{
      return false;
    }
  };
  this.stockStatus = function(value){
    var temp = self.productData.stockStatus;
    if(temp == value){
      return true;
    }
    else{
      return false;
    }
  };

  this.maxRating = 5;
  this.show = this.show || false;
	this.toggle = function () {
    this.isShown = !this.isShown
	}

};

module.exports = pdpProductViewCategoryController;
