var angular = require('angular');
var uiRouter = require('angular-ui-router');
var pdpProductViewCategoryComponent = require('./pdp.product.view.category.component');
require('angular-animate');

var pdpProductViewCategoryModule = angular.module('pdpProductViewCategoryModule', [uiRouter,'ngAnimate'])
.component('pdpproductviewcategory', pdpProductViewCategoryComponent)

import Styles from './pdp.product.view.category.scss';

module.exports = pdpProductViewCategoryModule;
