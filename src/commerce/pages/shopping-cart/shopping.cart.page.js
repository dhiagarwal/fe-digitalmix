'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');

var shoppingCartPageComponent = require('./shopping.cart.page.component');
var shoppingCarouselComponent = require('../../components/shopping-cart-components/shopping-carousel/shopping.carousel');
var shoppingBagComponent = require('../../components/shopping-cart-components/shopping-bag/shopping.bag');

var shoppingCartPage = angular.module('shoppingCartPage', [
    uiRouter,
    shoppingCarouselComponent.name,
    shoppingBagComponent.name
])
    .config(['$stateProvider', function($stateProvider){
        $stateProvider
            .state('shopping-cart', {
                url: '/shopping-cart',
                template: '<shopping-cart></shopping-cart>'
            });
    }])
    .component('shoppingCart', shoppingCartPageComponent);


module.exports= shoppingCartPage;
