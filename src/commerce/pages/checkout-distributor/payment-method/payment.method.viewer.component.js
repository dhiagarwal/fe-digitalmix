'use strict';
var template = require('./payment.method.viewer.jade');

var payViewComp = {
	templateUrl: template
};

module.exports = payViewComp;
