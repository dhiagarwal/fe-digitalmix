'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var paymentMethodViewComp = require('./payment.method.viewer.component');
var distributorPaymentComponent = require('../../../components/checkout-distributor/payment-method/payment.method.component.module');

var paymentViewModule = angular.module('paymentViewModule', [
	uiRouter,
	distributorPaymentComponent.name
])

		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state('paymentMethodDistributor', {
					url: '/checkout-delivery/payment-method',
					template: '<deliverypaymentmethod></deliverypaymentmethod>'
				});
		}])
.component('deliverypaymentmethod', paymentMethodViewComp);

module.exports = paymentViewModule;
