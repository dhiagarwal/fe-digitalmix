'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var shipAddressView = require('./shipping.address.viewer.component');
var shippingAddressComp = require('../../../components/checkout-distributor/shipping-address/shipping.address.component.module');

var shipAddressViewModule = angular.module('shipAddressViewModule', [
	uiRouter,
    shippingAddressComp.name
])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('checkoutDeliveryShipAddress', {
                url: '/checkout-delivery/ship-address',
                template: '<shipaddressview></shipaddressview>'
            });
    }])
    .component('shipaddressview', shipAddressView );

module.exports = shipAddressViewModule ;