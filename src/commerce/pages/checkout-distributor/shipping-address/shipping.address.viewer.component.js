'use strict';
var template = require('./shipping.address.viewer.jade');
var shipAddressViewComponent = {
	templateUrl: template
};

module.exports = shipAddressViewComponent ;