'use strict';
var template = require('./order.review.viewer.jade');

var orderViewComponent = {
    templateUrl: template
};

module.exports = orderViewComponent ;