'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');

var orderViewComponent = require('./order.review.viewer.component');
var orderReviewComponent = require('../../../components/checkout-distributor/order-review/order.review.component.module');

var orderReviewModule = angular.module('orderReviewModule', [
    uiRouter,
    orderReviewComponent.name
])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('deliveryOrderReview', {
                url: '/checkout-delivery/order-review',
                template: '<orderview></orderview>'
            });
    }])
    .component('orderview', orderViewComponent);

module.exports = orderReviewModule ;