var angular = require('angular');
var uiRouter = require('angular-ui-router');
var deliveryOptionsViewComponent = require('./delivery.options.viewer.component');
var deliveryOptionsComponentModule = require('../../../components/checkout-distributor/delivery-options/delivery.options.component.module');

var deliveryOptionsViewModule = angular.module('deliveryOptionsViewModule', [
	uiRouter,
	deliveryOptionsComponentModule.name
])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('checkoutDeliveryOptions', {
                url: '/checkout-delivery/delivery-options',
                template: '<deliveryoptionsview></deliveryoptionsview>'
            });
    }])
.component('deliveryoptionsview', deliveryOptionsViewComponent);

module.exports = deliveryOptionsViewModule;