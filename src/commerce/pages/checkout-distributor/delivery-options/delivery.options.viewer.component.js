'use strict';
var template = require('./delivery.options.viewer.jade');
var deliveryOptionsViewComponent = {
	templateUrl: template
};

module.exports = deliveryOptionsViewComponent;