var angular=require('angular');
var uiRouter =require('angular-ui-router');
var recurringOrderDetailsViewerComponent = require('./recurring.order.details.viewer.component');
var recurringOrderDetailsComponentModule = require('../../components/recurring-order-details/recurring.order.details.module');
console.log(recurringOrderDetailsComponentModule.name);
var recurringOrderDetailsViewerModule = angular.module('recurringOrderDetailsViewerModule', [
  uiRouter,
  recurringOrderDetailsComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('recurringOrderDetails', {
      url: '/recurringOrderDetails',
      template: '<recurringorderdetailsviewer></recurringorderdetailsviewer>'
    });
})
.component('recurringorderdetailsviewer',recurringOrderDetailsViewerComponent);

module.exports= recurringOrderDetailsViewerModule;
