'use strict';
var template = require('./recurring.order.details.viewer.jade');
var onBoardingViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = onBoardingViewerComponent;
