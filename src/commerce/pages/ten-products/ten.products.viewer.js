var angular=require('angular');
var uiRouter =require('angular-ui-router');

var tenProductsComponent = require('./ten.products');
var tenProductsModule = require('../../components/ten-products/ten.products.module');
var featuredVideoComponentModule = require('../../components/featured-video/featured.video.component.module');

var tenProductsViewerModule = angular.module('tenProductsViewerModule', [
  uiRouter,
  tenProductsModule.name,
  featuredVideoComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('tenProducts', {
      url: '/tenProducts',
      template: '<tenproductsviewer></tenproductsviewer>'
    });
})
.component('tenproductsviewer',tenProductsComponent);

module.exports = tenProductsViewerModule;
