'use strict';
var template = require('./pdp.category.directory.viewer.jade');
var pdpCategoryViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpCategoryViewerComponent;
