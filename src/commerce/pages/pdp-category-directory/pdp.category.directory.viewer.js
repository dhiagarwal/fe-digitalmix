var angular=require('angular');
var uiRouter =require('angular-ui-router');

var pdpCategoryDirectoryComponent = require('./pdp.category.directory');
var pdpCategoryDirectoryModule = require('../../components/pdp-category-directory/pdp.category.directory.module');
var pdpCategoryFooterModule = require('../../components/pdp-category-footer/pdp.category.footer.module');

var pdpCategoryDirectoryViewerModule = angular.module('pdpCategoryDirectoryViewerModule', [
  uiRouter,
  pdpCategoryDirectoryModule.name,
  pdpCategoryFooterModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpcategorydirectory', {
      url: '/pdpCategoryDirectory',
      template: '<pdpcategorydirectoryviewer></pdpcategorydirectoryviewer>'
    });
})
.component('pdpcategorydirectoryviewer',pdpCategoryDirectoryComponent);

module.exports= pdpCategoryDirectoryViewerModule;
