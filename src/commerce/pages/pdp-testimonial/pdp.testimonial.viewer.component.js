'use strict';
var template = require('./pdp.testimonial.viewer.jade');
var pdpTestimonialViewerComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpTestimonialViewerComponent;