var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpTestimonialViewerComponent = require('./pdp.testimonial.viewer.component');
var pdpTestimonialComponentModule = require('../../components/pdp-testimonial/pdp.testimonial.component.module');

var pdpTestimonialViewerModule = angular.module('pdpTestimonialViewerModule', [
  uiRouter,
  pdpTestimonialComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpTestimonial', {
      url: '/pdpTestimonial',
      template: '<pdptestimonialviewer></pdptestimonialviewer>'
    });
})
.component('pdptestimonialviewer',pdpTestimonialViewerComponent);

module.exports= pdpTestimonialViewerModule;