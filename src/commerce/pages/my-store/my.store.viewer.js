var angular=require('angular');
var uiRouter =require('angular-ui-router');
var myStoreComponent = require('./my.store.component');

var myStoreComponentModule = require('../../components/my-store/contact-info/my.store');
var myStoreHeaderComponentModule = require('../../components/my-store/my-store-header/my.store.header.module');

var myStoreViewerModule = angular.module('myStoreComponentModule', [
  uiRouter,
  myStoreComponentModule.name,
  myStoreHeaderComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('myStore', {
      url: '/myStore',
      template: '<mystoreviewer></mystoreviewer>'
    })
    .state('myStore.contactInfo', {
      url: '/contactInfo',
      templateUrl: 'src/digital-toolkit/pages/my-store/my.store.contact.info.html'
    });
})
.component('mystoreviewer', myStoreComponent);

module.exports = myStoreViewerModule;
