'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var promotionsViewerComponent = require('./promotions.component');
var promotions = require('../../components/promotions/promotions');
var promotionsModule = angular.module('promotionsModule', [
  uiRouter,
  promotions.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('promotions', {
      url: '/promotions',
      template: '<promotions></promotions>'
    });
}])
.component('promotions', promotionsViewerComponent);


module.exports= promotionsModule;
