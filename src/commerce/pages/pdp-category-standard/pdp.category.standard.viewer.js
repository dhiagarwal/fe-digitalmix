var angular=require('angular');
var uiRouter =require('angular-ui-router');

var pdpCategoryStandardComponent = require('./pdp.category.standard');
var pdpCategoryStandardModule = require('../../components/pdp-category-standard/pdp.category.standard.module');
var pdpCategoryFooterModule = require('../../components/pdp-category-footer/pdp.category.footer.module');
var pdpProductViewCategoryModule = require('../../components/pdp-product-view-category/pdp.product.view.category.module');

var pdpCategoryStandardViewerModule = angular.module('pdpCategoryStandardViewerModule', [
  uiRouter,
  pdpCategoryStandardModule.name,
  pdpCategoryFooterModule.name,
  pdpProductViewCategoryModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpcategorystandard', {
      url: '/pdpCategoryStandard',
      template: '<pdpcategorystandardviewer></pdpcategorystandardviewer>'
    });
})
.component('pdpcategorystandardviewer',pdpCategoryStandardComponent);

module.exports= pdpCategoryStandardViewerModule;
