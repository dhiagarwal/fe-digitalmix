'use strict';
var template = require('./pdp.category.standard.viewer.jade');
var pdpCategoryStandardViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpCategoryStandardViewerComponent;
