'use strict';
var template = require('./pdp.upsellkit.viewer.jade');
var pdpUpsellKitViewerComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpUpsellKitViewerComponent;