var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpUpsellKitViewerComponent = require('./pdp.upsellkit.viewer.component');
var pdpUpsellKitComponentModule = require('../../components/pdp-upsellkit/pdp.upsellkit.component.module');

var pdpUpsellKitViewerModule = angular.module('pdpUpsellKitViewerModule', [
  uiRouter,
  pdpUpsellKitComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpUpsellKit', {
      url: '/pdpUpsellKit',
      template: '<pdpupsellkitviewer></pdpupsellkitviewer>'
    });
})
.component('pdpupsellkitviewer',pdpUpsellKitViewerComponent);

module.exports= pdpUpsellKitViewerModule;