'use strict';
var template = require('./shipping.address.viewer.jade');
var shippingAddressViewerComponent = {
	restrict: 'E',
	templateUrl: template
};

module.exports = shippingAddressViewerComponent;