var angular = require('angular');
var uiRouter = require('angular-ui-router');
var shippingAddressViewerComponent = require('./shipping.address.viewer.component');
var shippingAddressComponentModule = require('../../../components/checkout-guest/shipping-address/shipping.address.component.module');
var shippingAddressViewerModule = angular.module('shippingAddressViewerModule', [
	uiRouter,
	shippingAddressComponentModule.name
])
.config(($stateProvider) => {
	$stateProvider
		.state('shippingaddress', {
			url: '/checkout/shippingaddress',
			template: '<shippingaddressviewer></shippingaddressviewer>'
	});
})
.component('shippingaddressviewer', shippingAddressViewerComponent);

module.exports = shippingAddressViewerModule;