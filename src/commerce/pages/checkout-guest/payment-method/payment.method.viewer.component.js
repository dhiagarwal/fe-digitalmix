'use strict';
var template = require('./payment.method.viewer.jade');

var paymentMethodViewerComponent = {
	restrict: 'E',
	templateUrl: template
};

module.exports = paymentMethodViewerComponent;
