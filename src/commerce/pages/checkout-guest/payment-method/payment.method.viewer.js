'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var paymentMethodViewerComponent = require('./payment.method.viewer.component');
var paymentMethodComponentModule = require('../../../components/checkout-guest/payment-method/payment.method.component.module');
var paymentMethodViewerModule = angular.module('paymentMethodViewerModule', [
	uiRouter,
	paymentMethodComponentModule.name
])
.config(($stateProvider) => {
	$stateProvider
		.state('paymentmethod', {
			url: '/checkout/paymentmethod',
			template: '<paymentmethodviewer></paymentmethodviewer>'
	});
})
.component('paymentmethodviewer', paymentMethodViewerComponent);

module.exports = paymentMethodViewerModule;
