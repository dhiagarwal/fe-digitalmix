'use strict';
var template = require('./order.confirmation.viewer.jade');
var orderConfirmationViewerComponent = {
	restrict: 'E',
	templateUrl: template
};

module.exports = orderConfirmationViewerComponent;