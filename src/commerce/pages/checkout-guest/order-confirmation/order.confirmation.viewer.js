var angular = require('angular');
var uiRouter = require('angular-ui-router');
var orderConfirmationViewerComponent = require('./order.confirmation.viewer.component');
var orderConfirmationComponentModule = require('../../../components/checkout-guest/order-confirmation/order.confirmation.component.module');
var orderConfirmationViewerModule = angular.module('orderConfirmationViewerModule', [
	uiRouter,
	orderConfirmationComponentModule.name
])
.config(($stateProvider) => {
	$stateProvider
		.state('orderconfirmation', {
			url: '/checkout/orderconfirmation',
			template: '<orderconfirmationviewer></orderconfirmationviewer>'
	});
})
.component('orderconfirmationviewer', orderConfirmationViewerComponent)
// .component('helpsectioncomponent', helpSectionComponent)
;

module.exports = orderConfirmationViewerModule;