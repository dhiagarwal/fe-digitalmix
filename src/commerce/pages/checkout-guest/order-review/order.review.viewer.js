'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');

var orderViewerComponent = require('./order.review.viewer.component');
var orderReviewComponentModule = require('../../../components/checkout-guest/order-review/order.review.component.module');

var orderReviewViewerModule = angular.module('orderReviewViewerModule', [
    uiRouter,
    orderReviewComponentModule .name
])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('orderreview', {
                url: '/checkout/orderreview',
                template: '<orderviewer></orderviewer>'
            });
    }])
    .component('orderviewer', orderViewerComponent);

module.exports = orderReviewViewerModule ;