'use strict';
var template = require('./order.review.viewer.jade');

var orderViewerComponent = {
    restrict: 'E',
    templateUrl: template
};

module.exports = orderViewerComponent ;