var angular = require('angular');
var uiRouter = require('angular-ui-router');
var deliveryOptionsViewerComponent = require('./delivery.options.viewer.component');
var deliveryOptionsComponentModule = require('../../../components/checkout-guest/delivery-options/delivery.options.component.module');
var deliveryOptionsViewerModule = angular.module('deliveryOptionsViewerModule', [
	uiRouter,
	deliveryOptionsComponentModule.name
])
.config(($stateProvider) => {
	$stateProvider
		.state('deliveryoptions', {
			url: '/checkout/deliveryoptions',
			template: '<deliveryoptionsviewer></deliveryoptionsviewer>'
	});
})
.component('deliveryoptionsviewer', deliveryOptionsViewerComponent);

module.exports = deliveryOptionsViewerModule;