'use strict';
var template = require('./delivery.options.viewer.jade');
var deliveryOptionsViewerComponent = {
	restrict: 'E',
	templateUrl: template
};

module.exports = deliveryOptionsViewerComponent;