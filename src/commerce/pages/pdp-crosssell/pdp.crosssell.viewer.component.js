'use strict';
var template = require('./pdp.crosssell.viewer.jade');
var pdpCrosssellViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpCrosssellViewerComponent;
