var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpCrosssellViewerComponent = require('./pdp.crosssell.viewer.component');
var pdpCrosssellComponentModule = require('../../components/pdp-crosssell/pdp.crosssell.component.module');
var pdpCrosssellViewerModule = angular.module('pdpCrosssellViewerModule', [
  uiRouter,
  pdpCrosssellComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpCrosssell', {
      url: '/pdpCrosssell',
      template: '<pdpcrosssellviewer></pdpcrosssellviewer>'
    });
})
.component('pdpcrosssellviewer',pdpCrosssellViewerComponent);

module.exports= pdpCrosssellViewerModule;
