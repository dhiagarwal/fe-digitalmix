var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpProductInfoViewerComponent = require('./pdp.productinfo.viewer.component');
var pdpProductInfoComponentModule = require('../../components/pdp-product-info/pdp-product-ingredients/pdp.product.ingredients.component.module');
// var pdpProductInfoComponentModule = require('../../components/pdp-product-info/pdp-product-usage-guide/pdp.product.guide.component.module');
// var pdpProductInfoComponentModule = require('../../components/pdp-product-info/pdp-product-resources/pdp.product.resources.component.module');


var pdpProductInfoViewerModule = angular.module('pdpProductInfoViewerModule', [
  uiRouter,
  pdpProductInfoComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpProductInfoIngredient', {
      url: '/pdpProductInfoIngredient',
      template: '<pdpproductinfoviewer></pdpproductinfoviewer>'
    });
})
.component('pdpproductinfoviewer',pdpProductInfoViewerComponent);

module.exports= pdpProductInfoViewerModule;