'use strict';
var template = require('./pdp.productinfo.viewer.jade');
var pdpProductInfoViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpProductInfoViewerComponent;
