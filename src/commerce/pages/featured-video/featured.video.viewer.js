var angular=require('angular');
var uiRouter =require('angular-ui-router');
var featuredVideoViewerComponent  = require('./featured.video.viewer.component');
var featuredVideoComponentModule = require('../../components/featured-video/featured.video.component.module');

var featuredVideoViewerModule = angular.module('featuredVideoViewerModule', [
  uiRouter,
  featuredVideoComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('featuredVideo', {
      url: '/featuredVideo',
      template: '<featuredvideoviewer></featuredvideoviewer>'
    });
})
.component('featuredvideoviewer',featuredVideoViewerComponent );

module.exports= featuredVideoViewerModule;
