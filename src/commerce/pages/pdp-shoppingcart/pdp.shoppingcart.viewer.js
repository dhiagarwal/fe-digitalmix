var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpShoppingCartViewerComponent = require('./pdp.shoppingcart.viewer.component');
var pdpShoppingCartComponentModule = require('../../components/pdp-shoppingcart/pdp.shoppingcart.component.module');
var landingPageBannerModule = require('../../components/landing-page-banner/landing.page.banner.module');
var landingPageBannerFullModule = require('../../components/landing-page-banner-full/landing.page.banner.full.module');
var tenProductsModule = require('../../components/ten-products/ten.products.module');
var pdpShoppingCartViewerModule = angular.module('pdpShoppingCartViewerModule', [
  uiRouter,
  pdpShoppingCartComponentModule.name,
  landingPageBannerModule.name,
  landingPageBannerFullModule.name,
  tenProductsModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpShoppingCart', {
      url: '/pdpShoppingCart',
      template: '<pdpshoppingcartviewer></pdpshoppingcartviewer>'
    });
})
.component('pdpshoppingcartviewer',pdpShoppingCartViewerComponent);

module.exports= pdpShoppingCartViewerModule;
