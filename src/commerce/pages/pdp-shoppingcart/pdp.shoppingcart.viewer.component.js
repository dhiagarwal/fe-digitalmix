'use strict';
var template = require('./pdp.shoppingcart.viewer.jade');
var pdpShoppingCartViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpShoppingCartViewerComponent;
