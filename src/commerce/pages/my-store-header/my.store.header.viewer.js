var angular=require('angular');
var uiRouter =require('angular-ui-router');
var myStoreHeaderViewerComponent = require('./my.store.header.viewer.component');
var myStoreMessageComponentModule = require('../../components/my-store-message/my.store.message.component.module');
var myStoreOrderConfirmationComponentModule = require('../../components/my-store-order-confirmation/my.store.order.confirmation.component.module');
var myStoreHeaderViewerModule = angular.module('myStoreHeaderViewerModule', [
  uiRouter,
  myStoreMessageComponentModule.name,
  myStoreOrderConfirmationComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('myStoreHeader', {
      url: '/myStoreHeader',
      template: '<mystoreheaderviewer></mystoreheaderviewer>'
    });
})
.component('mystoreheaderviewer',myStoreHeaderViewerComponent);

module.exports= myStoreHeaderViewerModule;