'use strict';
var template = require('./my.storefront.viewer.jade');
var myStorefrontOverview = require('./my.storefront.overview.jade');
var myStorefrontSetup = require('./my.storefront.setup.jade');
var myStorefrontReview = require('./my.storefront.review.jade');
var myStorefrontProducts = require('./my.storefront.products.jade');
var myStorefrontBundles = require('./my.storefront.bundles.jade');
var myStorefrontBundle = require('./my.storefront.bundle.jade');
var myStorefrontTopSeller = require('./my.storefront.top.seller.jade');

var myStorefrontViewerComponent = {
    restrict: 'E',
    templateUrl: template
};

module.exports = myStorefrontViewerComponent;
