var angular = require('angular');
var uiRouter = require('angular-ui-router');

var myStorefrontViewerComponent = require('./my.storefront.viewer.component');

var myStorefrontOverviewViewerComponentModule = require('../../components/my-storefront/storefront-overview/storefront.overview.component.module');

var myStorefrontSetupViewerComponentModule = require('../../components/my-storefront/storefront-setup/storefront.setup.component.module');
var myStorefrontProductsViewerComponentModule = require('../../components/my-storefront/storefront-products/storefront.products.component.module');
var myStorefrontBundlesViewerComponentModule = require('../../components/my-storefront/storefront-bundles/storefront.bundles.component.module');

var myStorefrontReviewViewerComponentModule = require('../../components/my-storefront/storefront-review/storefront.review.component.module');

var myStorefrontBundleViewerComponentModule = require('../../components/my-storefront/storefront-bundle/storefront.bundle.component.module');

var myStorefrontIntroViewerComponentModule = require('../../components/my-storefront/storefront-intro/storefront.intro.component.module');

var myStorefrontTopSellerViewerComponentModule = require('../../components/my-storefront/top-sellers/top.seller.component.module')

var myStorefrontViewerModule = angular.module('myStorefrontViewerModule', [
  uiRouter,
  myStorefrontOverviewViewerComponentModule.name,
  myStorefrontSetupViewerComponentModule.name,
  myStorefrontReviewViewerComponentModule.name,
  myStorefrontIntroViewerComponentModule.name,
  myStorefrontBundleViewerComponentModule.name,
  myStorefrontProductsViewerComponentModule.name,
  myStorefrontBundlesViewerComponentModule.name,
  myStorefrontTopSellerViewerComponentModule.name
])
    .config(($stateProvider) => {
        $stateProvider
            .state('myStorefront', {
                url: '/myStorefront',
                template: '<mystorefrontviewer></mystorefrontviewer>'
            })
            .state('myStorefront.overview', {
                url: '/overview',
                templateUrl: 'src/commerce/pages/my-storefront/my.storefront.overview.html'
            })
            .state('myStorefront.setup', {
                url: '/setup',
                templateUrl: 'src/commerce/pages/my-storefront/my.storefront.setup.html'
            })
            .state('myStorefront.products', {
                url: '/products',
                templateUrl: 'src/commerce/pages/my-storefront/my.storefront.products.html'
            })
            .state('myStorefront.bundles', {
                url: '/bundles',
                templateUrl: 'src/commerce/pages/my-storefront/my.storefront.bundles.html'
            })
             .state('myStorefront.bundle', {
                url: '/bundle',
                templateUrl: 'src/commerce/pages/my-storefront/my.storefront.bundle.html'
            })
            .state('myStorefront.review', {
                url: '/review',
                templateUrl: 'src/commerce/pages/my-storefront/my.storefront.review.html'
            })
            .state('myStorefront.topseller', {
                url: '/topseller',
                templateUrl: 'src/commerce/pages/my-storefront/my.storefront.top.seller.html'
            })
    })

.component('mystorefrontviewer',myStorefrontViewerComponent);

module.exports = myStorefrontViewerModule;
