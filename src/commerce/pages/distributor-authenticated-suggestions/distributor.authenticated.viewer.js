var angular = require('angular');
var uiRouter = require('angular-ui-router');

var distributorAuthenticatedViewerComponent = require('./distributor.authenticated.viewer.component');
var distributorauthenticated = require('../../components/distributor-authenticated-suggestions/distributor.authenticated.module');

var distributorAuthenticatedModule = angular.module('distributorAuthenticatedModule', [
  uiRouter,
  distributorauthenticated.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('distributorAuthenticatedSuggestions', {
      url: '/distributorAuthenticatedSuggestions',
      template: '<distributorauthenticated></distributorauthenticated>'
    });
}])
.component('distributorauthenticated', distributorAuthenticatedViewerComponent);


module.exports= distributorAuthenticatedModule;
