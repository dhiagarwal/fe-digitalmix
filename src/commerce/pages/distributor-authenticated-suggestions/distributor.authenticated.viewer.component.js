'use strict';
var template = require('./distributor.authenticated.viewer.jade');
var distributorAuthenticatedComponent = {
  templateUrl: template
};

module.exports = distributorAuthenticatedComponent;
