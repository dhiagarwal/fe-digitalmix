'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var recentlyVieweditemsViewerComponent = require('./recently.vieweditems.component');
var recentlyvieweditems = require('../../components/recentlyvieweditems/recently.vieweditems');
var recentlyVieweditemsModule = angular.module('recentlyVieweditemsModule', [
  uiRouter,
  recentlyvieweditems.name
])

.config(['$stateProvider', function($stateProvider){
  $stateProvider
    .state('recentlyViewedItems', {
      url: '/recentlyViewedItems',
      template: '<recentlyvieweditems></recentlyvieweditems>'
    });
}])
.component('recentlyvieweditems', recentlyVieweditemsViewerComponent);


module.exports= recentlyVieweditemsModule;
