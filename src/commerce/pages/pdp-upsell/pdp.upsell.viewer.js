var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpUpsellViewerComponent = require('./pdp.upsell.viewer.component');
var pdpUpsellComponentModule = require('../../components/pdp-upsell/pdp.upsell.component.module');

var pdpUpsellViewerModule = angular.module('pdpUpsellViewerModule', [
  uiRouter,
  pdpUpsellComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpUpsell', {
      url: '/pdpUpsell',
      template: '<pdpupsellviewer></pdpupsellviewer>'
    });
})
.component('pdpupsellviewer',pdpUpsellViewerComponent);

module.exports= pdpUpsellViewerModule;