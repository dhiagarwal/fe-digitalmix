'use strict';
var template = require('./pdp.upsell.viewer.jade');
var pdpUpsellViewerComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpUpsellViewerComponent;