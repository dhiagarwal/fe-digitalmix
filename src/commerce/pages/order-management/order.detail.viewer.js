'use strict';

var angular = require('angular');
var uiRouter = require('angular-ui-router');

var orderDetailViewerComponent = require('./order.detail.viewer.component');
var orderDetailComponentModule = require('../../components/order-management/order-detail/order.detail.component.module');
var myOrderViewerComponent = require('./order.detail.viewer.component');
var myOrderComponentModule = require('../../components/order-management/my-orders/my.order.component.module');


var orderDetailViewerModule = angular.module('orderDetailViewerModule', [
    uiRouter,
    orderDetailComponentModule.name,
    myOrderComponentModule.name    
])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('orderdetail', {
                url: '/orderdetail',
                template: '<orderdetailviewer></orderdetailviewer>'
            })
            .state('myorders', {
                url: '/myorders',
                templateUrl: 'src/commerce/pages/order-management/my.order.html'
            });
    }])
    .component('orderdetailviewer', orderDetailViewerComponent);

module.exports = orderDetailViewerModule ;