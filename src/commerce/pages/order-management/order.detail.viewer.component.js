'use strict';
var template = require('./order.detail.viewer.jade');
var myOrdertemplate = require('./my.order.jade');

var orderDetailViewerComponent = {
    restrict: 'E',
    templateUrl: template
};

module.exports = orderDetailViewerComponent ;