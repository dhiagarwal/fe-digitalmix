var angular=require('angular');
var uiRouter =require('angular-ui-router');
var welcomeMessageViewerComponent = require('./welcome.message.viewer.component');
var welcomeMessageComponentModule = require('../../components/welcome-message/welcome.message.component.module');

var welcomeMessageViewerModule = angular.module('welcomeMessageViewerModule', [
  uiRouter,
  welcomeMessageComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('welcomeMessage', {
      url: '/welcomeMessage',
      template: '<welcomemessageviewer></welcomemessageviewer>'
    });
})
.component('welcomemessageviewer',welcomeMessageViewerComponent);

module.exports= welcomeMessageViewerModule;
