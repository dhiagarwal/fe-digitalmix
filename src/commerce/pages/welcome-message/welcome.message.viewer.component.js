'use strict';
var template = require('./welcome.message.viewer.jade');
var welcomeMessageViewerComponent = {
  templateUrl: template
};

module.exports = welcomeMessageViewerComponent;
