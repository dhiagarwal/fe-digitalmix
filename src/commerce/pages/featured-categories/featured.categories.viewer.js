var angular=require('angular');
var uiRouter =require('angular-ui-router');
var featuredCategoriesViewerComponent = require('./featured.categories.viewer.component');
var featuredCategoriesComponentModule = require('../../components/featured-categories/featured.categories.component.module');
var featuredCategoriesViewerModule = angular.module('featuredCategoriesViewerModule', [
  uiRouter,
  featuredCategoriesComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('featuredCategories', {
      url: '/featuredCategories',
      template: '<featuredcategoriesviewer></featuredcategoriesviewer>'
    });
})
.component('featuredcategoriesviewer',featuredCategoriesViewerComponent);

module.exports= featuredCategoriesViewerModule;