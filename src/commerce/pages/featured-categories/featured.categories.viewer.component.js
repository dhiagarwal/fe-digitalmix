'use strict';
var template = require('./featured.categories.viewer.jade');
var featuredCategoriesViewerComponent = {
  templateUrl: template
};

module.exports = featuredCategoriesViewerComponent;