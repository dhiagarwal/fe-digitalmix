var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpVariationsComponent = require('../../components/pdp-variations/pdp.variations.component.module');
var pdpProductBasicsViewerComponent = require('./pdp.productbasics.viewer.component');
var pdpProductBasicsComponentModule = require('../../components/pdp-productbasics/pdp.productbasics.component.module');
var pdpProductInfoIngredientsModule = require('../../components/pdp-product-info/pdp-product-ingredients/pdp.product.ingredients.component.module');
var pdpProductGuideComponentModule = require('../../components/pdp-product-info/pdp-product-usage-guide/pdp.product.guide.component.module');
var pdpProductResourcesComponentModule = require('../../components/pdp-product-info/pdp-product-resources/pdp.product.resources.component.module');
var pdpRecurringOrderComponentModule = require('../../components/pdp-recurring-order/pdp.recurring.order.component.module');
var pdpProductBasicsViewerModule = angular.module('pdpProductBasicsViewerModule', [
  uiRouter,
  pdpProductBasicsComponentModule.name,
  pdpVariationsComponent.name,
  pdpProductInfoIngredientsModule.name,
  pdpProductGuideComponentModule.name,
  pdpProductResourcesComponentModule.name,
  pdpRecurringOrderComponentModule.name

])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpProductBasics', {
      url: '/pdpProductBasics',
      template: '<pdpproductbasicsviewer></pdpproductbasicsviewer>'
    });
})
.component('pdpproductbasicsviewer',pdpProductBasicsViewerComponent);

module.exports= pdpProductBasicsViewerModule;
