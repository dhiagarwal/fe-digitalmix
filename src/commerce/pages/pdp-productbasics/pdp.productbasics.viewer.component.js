'use strict';
var template = require('./pdp.productbasics.viewer.jade');
var pdpProductBasicsViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpProductBasicsViewerComponent;
