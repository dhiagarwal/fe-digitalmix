'use strict';
var template = require('./pitch.landing.page.viewer.jade');
var pitchLandingPageViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = pitchLandingPageViewerComponent;
