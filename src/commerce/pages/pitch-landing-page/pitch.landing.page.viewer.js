var angular=require('angular');
var uiRouter =require('angular-ui-router');

var pitchLandingPageComponent = require('./pitch.landing.page');
var pitchLandingPageModule = require('../../components/pitch-landing-page/pitch.landing.page.module');
var pitchHeaderModule = require('../../components/pitch-header/pitch.header.module');

var pitchLandingPageViewerModule = angular.module('pitchLandingPageViewerModule', [
  uiRouter,
  pitchLandingPageModule.name,
  pitchHeaderModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pitchLandingPage', {
      url: '/pitchLandingPage',
      template: '<pitchlandingpageviewer></pitchlandingpageviewer>'
    });
})
.component('pitchlandingpageviewer',pitchLandingPageComponent);

module.exports= pitchLandingPageViewerModule;
