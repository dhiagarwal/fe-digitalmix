var angular=require('angular');
var uiRouter =require('angular-ui-router');
var pdpSocialViewerComponent = require('./pdp.social.viewer.component');
var pdpSocialComponentModule = require('../../components/pdp-social/pdp.social.component.module');

var pdpSocialViewerModule = angular.module('pdpSocialViewerModule', [
  uiRouter,
  pdpSocialComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('pdpSocial', {
      url: '/pdpSocial',
      template: '<pdpsocialviewer></pdpsocialviewer>'
    });
})
.component('pdpsocialviewer',pdpSocialViewerComponent);

module.exports= pdpSocialViewerModule;