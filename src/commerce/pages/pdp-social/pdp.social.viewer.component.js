'use strict';
var template = require('./pdp.social.viewer.jade');
var pdpSocialViewerComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = pdpSocialViewerComponent;