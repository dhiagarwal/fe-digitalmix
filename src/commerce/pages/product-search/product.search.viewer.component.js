'use strict';
var template = require('./product.search.viewer.jade');

var productSearchViewerComponent = {  
  restrict: 'E',
  templateUrl: template
};

module.exports = productSearchViewerComponent;