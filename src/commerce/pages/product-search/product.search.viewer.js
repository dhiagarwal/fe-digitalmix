var angular=require('angular');
var uiRouter =require('angular-ui-router');

var productSearchViewerComponent = require('./product.search.viewer.component');
var productSearchBannerComponentModule = require('../../components/product-search/product-search-banner/product.search.banner.component.module');
var productSearchListComponentModule = require('../../components/product-search/product-search-list/product.search.list.component.module');
var productTileComponentModule = require('../../components/product-search/product-tile/product.tile.component.module');
var recentlyViewedCarouselModule = require('../../components/product-search/recently-viewed-carousel/recently.viewed.carousel.module');

var productSearchViewerModule = angular.module('productSearchViewerModule', [
  uiRouter,
  productSearchBannerComponentModule.name,
  productSearchListComponentModule.name,
  productTileComponentModule.name,
  recentlyViewedCarouselModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('productSearch', {
      url: '/productSearch',
      template: '<productsearchviewer></productsearchviewer>'
    });
    
})
.component('productsearchviewer',productSearchViewerComponent);

module.exports= productSearchViewerModule;
