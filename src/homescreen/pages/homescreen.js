var angular=require('angular');
var uiRouter =require('angular-ui-router');
var homescreenComponent =require('./homescreen.component.js');
var homepage =require('../components/homepage/homepage');

var homescreenModule = angular.module('app.homescreen', [
  uiRouter,
  homepage.name
])

.config(($stateProvider) => {
  $stateProvider
    .state('homescreen', {
      url: '/',
      template: '<homescreen></homescreen>'
    });
})
.component('homescreen', homescreenComponent);


module.exports= homescreenModule;
