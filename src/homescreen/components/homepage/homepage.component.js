var template =require('./homepage.jade');
var controller =require('./homepage.controller');

var homepageComponent =  {
  //return {
    restrict: 'E',
    scope: {},
    templateUrl : template,
    controller,
    controllerAs: 'ctrl',
    bindToController: true
  //};
};

module.exports = homepageComponent;
