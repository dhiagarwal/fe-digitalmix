function homepageController(homepageService) {
  this.result = {};
    var model = this.model = {};
    var self = this;
  
    this.service = homepageService;
    this.name='homepage';
    var promise = homepageService.getModules();
    
    promise.then(function(data) {
      self.result =data;
      model.signup = self.result.signUp;
      model.quiz = self.result.digitalToolKit;
      model.commerce = self.result.commerce;
    });
}
module.exports = homepageController;
