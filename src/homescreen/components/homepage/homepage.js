var angular =require('angular');
var uiRouter =require('angular-ui-router');
var homepageComponent =require('./homepage.component');
var homepageService =require('./homepage.service');

var homepagemodule = angular.module('homepage', [
  uiRouter
])
.component('homepage', homepageComponent)
.service('homepageService', homepageService);
var base =require('../../../stylesheets/base.scss');
var Styles =require('./homepage.scss'); 

module.exports= homepagemodule;
