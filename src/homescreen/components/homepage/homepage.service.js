'use strict';


homepageService.$inject = ['$http','$q'];
function homepageService($http, $q) {
  console.log($q);
  
  this.$http = $http;

  var deferred = $q.defer();
  $http.get('../../../../src/homescreen/components/homepage/homepage.content.json').then(function(response) {
    console.log(response.data);
    deferred.resolve(response.data);
  });

  this.getModules = function() {
    return deferred.promise;
  }

};
module.exports= homepageService;
