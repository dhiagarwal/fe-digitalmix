'use strict';
var template = require('./prospecting.basics.component.jade');
var controller = require('./prospecting.basics.component.controller');

var prospectingBasicsComponent  = {
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
};

module.exports = prospectingBasicsComponent ;
