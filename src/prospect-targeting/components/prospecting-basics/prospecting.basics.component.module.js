'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-slick-carousel');
var prospectingBasicsComponent = require('./prospecting.basics.component');
var prospectingBasicsComponentModule = angular.module('prospectingBasicsComponentModule', [uiRouter])
                  .component('prospectingbasicscomponent', prospectingBasicsComponent);

import Styles from './prospecting.basics.component.scss';

module.exports = prospectingBasicsComponentModule;
