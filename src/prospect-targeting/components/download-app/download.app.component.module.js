'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-slick-carousel');
var downloadAppComponent = require('./download.app.component');
var downloadAppComponentModule = angular.module('downloadAppComponentModule', [uiRouter])
                  .component('downloadappcomponent', downloadAppComponent);

import Styles from './download.app.component.scss';

module.exports = downloadAppComponentModule;
