'use strict';
var template = require('./download.app.component.jade');
var controller = require('./download.app.component.controller');

var downloadAppComponent  = {
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
};

module.exports = downloadAppComponent ;
