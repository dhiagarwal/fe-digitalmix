'use strict';

function downloadAppComponentController($window) {
  var self = this;
  self.redirectToAppStore = function(){
         $window.location.href = "https://itunes.apple.com/in/genre/ios/id36?mt=8";
   };
   self.redirectToPlayStore = function(){
          $window.location.href = "https://play.google.com/store?hl=en";
    };

};

module.exports = downloadAppComponentController;
