'use strict';

function prospectingCoursesComponentController(prospectingCoursesComponentService,$interval) {
    var self = this;
    self.slickSecondaryConfig = {
            enabled: true,
            infinite: false,
            adaptiveHeight: false,
            mobileFirst: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            speed: 500,
            responsive: [
              {
                breakpoint: 359,
                settings: {
                  slidesToShow: 2,
                }
              },
              {
                breakpoint: 767,
                settings: {
                  slidesToShow: 4,
                }
              },
              {
                breakpoint: 1023,
                settings: {
                  slidesToShow: 4,
                  draggable: false,
                }
              },
              {
                breakpoint: 1439,
                settings: {
                  slidesToShow: 5,
                  draggable: false,
                }
              }
            ]
          };
          $interval(function() {
              self.slickSecondaryConfig.adaptiveHeight = true;
              self.slickSecondaryConfig.enabled = true;
          }, 600, [3]);
    var promise = prospectingCoursesComponentService.getProspectingData();
    promise.then(function (data) {
        self.courseData = data;
    });
}

module.exports = prospectingCoursesComponentController;
