'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-slick-carousel');
var prospectingCoursesComponent = require('./prospecting.courses.component');
var prospectingCoursesComponentService = require('./prospecting.courses.component.service')
var prospectingCoursesComponentModule = angular.module('prospectingCoursesComponentModule', [uiRouter])
                  .component('prospectingcoursescomponent', prospectingCoursesComponent)
                  .service('prospectingCoursesComponentService', prospectingCoursesComponentService);


import Styles from './prospecting.courses.component.scss';

module.exports = prospectingCoursesComponentModule;
