'use strict';

prospectingCoursesComponentService.$inject = ['$http', '$q'];

function prospectingCoursesComponentService($http, $q) {
    this.$http = $http;

    var deferred = $q.defer();
    $http.get('../../../../src/prospect-targeting/components/prospecting-courses/prospecting.courses.component.json').then(function (response) {
        deferred.resolve(response.data);
    });

    this.getProspectingData = function () {
        return deferred.promise;
    }
};

module.exports = prospectingCoursesComponentService;