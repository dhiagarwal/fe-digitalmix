'use strict';
var template = require('./prospecting.courses.component.jade');
var controller = require('./prospecting.courses.component.controller');

var prospectingCoursesComponent  = {
  
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
};

module.exports = prospectingCoursesComponent ;
