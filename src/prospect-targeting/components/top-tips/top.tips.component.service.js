'use strict';

topTipsComponentService.$inject = ['$http', '$q'];

function topTipsComponentService($http, $q) {
    this.$http = $http;

    var deferred = $q.defer();
    $http.get('../../../../src/prospect-targeting/components/top-tips/top.tips.component.json').then(function (response) {
        deferred.resolve(response.data);
    });

    this.getTipsData = function () {
        return deferred.promise;
    }
};

module.exports = topTipsComponentService;