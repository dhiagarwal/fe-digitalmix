'use strict';
var template = require('./top.tips.component.jade');
var controller = require('./top.tips.component.controller');

var toptipsComponent = {
    scope: {},
    templateUrl: template,
    controller: controller,
    controllerAs: 'ctrl',
    bindToController: true
        
};

module.exports = toptipsComponent;