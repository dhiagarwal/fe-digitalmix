'use strict';

function toptipsComponentController(topTipsComponentService) {
    var self = this;
    self.profilePic ="../../../../src/assets/images/content/top-seller.png";
    var promise = topTipsComponentService.getTipsData();
    promise.then(function (data) {
        self.topTips = data;
    });
    self.isLast = function (index) {
        var total = self.topTips.length - 1;
        if (index == total) {

            return true;
        }
        return false;
    };

}

module.exports = toptipsComponentController;
