'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-slick-carousel');
var toptipsComponent = require('./top.tips.component');
var topTipsComponentService = require('./top.tips.component.service')
var toptipsComponentModule = angular.module('toptipsComponentModule', [uiRouter])
    .component('toptipscomponent', toptipsComponent)
    .service('topTipsComponentService', topTipsComponentService)


import Styles from './top.tips.component.scss';

module.exports = toptipsComponentModule;