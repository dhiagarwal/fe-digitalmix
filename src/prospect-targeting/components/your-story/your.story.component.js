'use strict';
var template = require('./your.story.component.jade');
var controller = require('./your.story.component.controller');

var yourStoryComponent  = {
  scope: {},
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
};

module.exports = yourStoryComponent ;
