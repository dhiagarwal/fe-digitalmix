'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
require('angular-slick-carousel');
var yourStoryComponent = require('./your.story.component');
var yourStoryComponentService = require('./your.story.component.service')
var yourStoryComponentModule = angular.module('yourStoryComponentModule', [uiRouter])
                  .component('yourstorycomponent', yourStoryComponent)
                  .service('yourStoryComponentService', yourStoryComponentService);
import Styles from './your.story.component.scss';

module.exports = yourStoryComponentModule;
