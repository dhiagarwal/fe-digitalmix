'use strict';

yourStoryComponentService.$inject = ['$http', '$q'];

function yourStoryComponentService($http, $q) {
    this.$http = $http;

    var deferred = $q.defer();
    $http.get('../../../../src/prospect-targeting/components/your-story/your.story.component.json').then(function (response) {
        deferred.resolve(response.data);
    });

    this.getData = function () {
        return deferred.promise;
    }
};

module.exports = yourStoryComponentService;
