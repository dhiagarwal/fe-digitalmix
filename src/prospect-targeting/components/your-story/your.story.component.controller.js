'use strict';

function yourStoryComponentController($interval,yourStoryComponentService) {
    var self = this;
    self.storyList="";
    var promise = yourStoryComponentService.getData();
    promise.then(function (data) {
        self.storyList = data;
    });

    self.slickVideoConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: true,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        speed: 500,
        responsive: [{
            breakpoint: 767,
            settings: {
                arrows: true
            }
        }]
    };
     $interval(function() {
        self.slickVideoConfig.adaptiveHeight = true;
        self.slickVideoConfig.enabled = true;
    }, 600, [3]);

}

module.exports = yourStoryComponentController;
