var angular=require('angular');
var uiRouter =require('angular-ui-router');
var prospectingBasicsViewerComponent  = require('./prospecting.basics.viewer.component');
var prospectingBasicsComponentModule = require('../../components/prospecting-basics/prospecting.basics.component.module');

var prospectingBasicsViewerModule = angular.module('prospectingBasicsViewerModule', [
  uiRouter,
  prospectingBasicsComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('prospectinBasics', {
      url: '/prospectingBasics',
      template: '<prospectingbasics></prospectingbasics>'
    });
})
.component('prospectingbasics',prospectingBasicsViewerComponent );

module.exports= prospectingBasicsViewerModule;
