var angular=require('angular');
var uiRouter =require('angular-ui-router');
var prospectingCoursesViewerComponent  = require('./prospecting.courses.viewer.component');
var prospectingCoursesComponentModule = require('../../components/prospecting-courses/prospecting.courses.component.module');

var prospectingCoursesComponentViewerModule = angular.module('prospectingCoursesComponentViewerModule', [
  uiRouter,
  prospectingCoursesComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('prospectingCourses', {
      url: '/prospectingCourses',
      template: '<prospectingcoursesviewer></prospectingcoursesviewer>'
    });
})
.component('prospectingcoursesviewer',prospectingCoursesViewerComponent);

module.exports= prospectingCoursesComponentViewerModule;
