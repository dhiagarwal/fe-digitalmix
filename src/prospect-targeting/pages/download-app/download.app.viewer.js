var angular=require('angular');
var uiRouter =require('angular-ui-router');
var downloadAppViewerComponent  = require('./download.app.viewer.component');
var downloadAppComponentModule = require('../../components/download-app/download.app.component.module');
var downloadAppViewerModule = angular.module('downloadAppViewerModule', [
  uiRouter,
  downloadAppComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('downloadappviewer', {
      url: '/downloadApp',
      template: '<downloadappviewer></downloadappviewer>'
    });
})
.component('downloadappviewer',downloadAppViewerComponent );
module.exports= downloadAppViewerModule;
