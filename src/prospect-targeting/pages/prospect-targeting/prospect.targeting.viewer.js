var angular=require('angular');
var uiRouter =require('angular-ui-router');
var prospectTargetingViewerComponent  = require('./prospect.targeting.viewer.component');

var prospectTargetingViewerModule = angular.module('prospectTargetingViewerModule', [
  uiRouter
])
.config(($stateProvider) => {
  $stateProvider
    .state('prospectTargeting', {
      url: '/prospectTargeting',
      template: '<prospecttargeting></prospecttargeting>'
    });
})
.component('prospecttargeting',prospectTargetingViewerComponent );
module.exports= prospectTargetingViewerModule;
