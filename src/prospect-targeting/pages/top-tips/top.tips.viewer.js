var angular=require('angular');
var uiRouter =require('angular-ui-router');
var topTipsViewerComponent  = require('./top.tips.viewer.component');
var topTipsComponentModule = require('../../components/top-tips/top.tips.component.module');

var topTipsComponentViewerModule = angular.module('topTipsComponentViewerModule', [
  uiRouter,
  topTipsComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('topTips', {
      url: '/topTips',
      template: '<topTips></toptips>'
    });
})
.component('toptips',topTipsViewerComponent );

module.exports= topTipsComponentViewerModule;
