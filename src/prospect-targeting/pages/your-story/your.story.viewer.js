var angular=require('angular');
var uiRouter =require('angular-ui-router');
var yourStoryViewerComponent  = require('./your.story.viewer.component');
var yourStoryComponentModule = require('../../components/your-story/your.story.component.module');

var yourStoryComponentViewerModule = angular.module('yourStoryComponentViewerModule', [
  uiRouter,
  yourStoryComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('yourStory', {
      url: '/yourStory',
      template: '<yourstory></yourstory>'
    });
})
.component('yourstory',yourStoryViewerComponent );

module.exports= yourStoryComponentViewerModule;
