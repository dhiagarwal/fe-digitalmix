var angular = require('angular');
var prospectTragetingViewer = require('./pages/prospect-targeting/prospect.targeting.viewer');
var downloadAppViewer = require('./pages/download-app/download.app.viewer');
var prospectingBasicsViewer = require('./pages/prospecting-basics/prospecting.basics.viewer');
var yourStoryViewer = require('./pages/your-story/your.story.viewer');
var topTipsViewer = require('./pages/top-tips/top.tips.viewer');
var prospectingCoursesViewer = require('./pages/prospecting-courses/prospecting.courses.viewer');
var prospectTargetingModule = angular.module('app.prospect.targeting', [
    downloadAppViewer.name,
    prospectTragetingViewer.name,
    prospectingBasicsViewer.name,
    yourStoryViewer.name,
    topTipsViewer.name,
    prospectingCoursesViewer.name
]);

module.exports = prospectTargetingModule;
