'use strict';
var template = require('./quiz.intro.viewer.jade');
var quizIntroViewerComponent = {  
  templateUrl: template
};

module.exports = quizIntroViewerComponent;