var angular=require('angular');
var uiRouter =require('angular-ui-router');
var quizIntroViewerComponent = require('./quiz.intro.viewer.component');
var quizIntroComponentModule = require('../../components/quiz-intro/quiz.intro.component.module');
var feedLoaderComponentModule = require('../../components/feed-loader/feed.loader.component.module');

var quizIntroViewerModule = angular.module('quizIntroViewerModule', [
  uiRouter,
  quizIntroComponentModule.name,
  feedLoaderComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('quizIntro', {
      url: '/quizIntro',
      template: '<quizintroviewer></quizintroviewer>'
    });
})
.component('quizintroviewer',quizIntroViewerComponent);

module.exports= quizIntroViewerModule;
