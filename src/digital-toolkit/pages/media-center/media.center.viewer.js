var angular=require('angular');
var uiRouter =require('angular-ui-router');

var mediaCenterViewerComponent = require('./media.center.viewer.component');
//var mediaCenterComponentModule = require('../../components/media-center/media.center.component.module');

var mediaHeaderComponentModule = require('../../components/media-center/media-center-header/media.header.component.module');
var mediaCarouselComponentModule = require('../../components/media-center/media-carousel/media.carousel.component.module');
var mediaCategoryBannerComponentModule = require('../../components/media-center/media-category-banner/media.category.banner.component.module');
var mediaBannerComponentModule = require('../../components/media-center/media-center-banner/media.banner.component.module');
var mediaFavoriteComponentModule = require('../../components/media-center/media-favorite/media.favorite.component.module');
var mediaTileComponentModule = require('../../components/media-center/media-tile/media.tile.component.module');
var mediaCategoryComponentModule = require('../../components/media-center/media-category-list/media.category.component.module');
var mediaRecentlyViewedModule = require('../../components/media-center/media-recently-viewed/media.recently.viewed.component.module');
var mediaNoresultModule = require('../../components/media-center/media-noresult-found/media.noresult.component.module');
var mediaItemDetailComponentModule = require('../../components/media-center/media-item-detail/media.item.detail.component.module');

var mediaFileUploadComponentModule = require('../../components/media-center/media-file-upload/media.fileUpload.component.module');

var mediaCardsComponentModule = require('../../components/media-center/media-cards/media.cards.component.module');

var regionSelectionComponentModule = require('../../../__shared/region-selection/component/region.component.module');
var storageIndicatorComponentModule = require('../../components/media-center/storage-indicator/storage.indicator.component.module');

var mediaCenterViewerModule = angular.module('mediaCenterViewerModule', [
  uiRouter,
  // mediaCenterComponentModule.name,
  mediaCarouselComponentModule.name,
  mediaCategoryBannerComponentModule.name,
  mediaBannerComponentModule.name,
  mediaFavoriteComponentModule.name,
  mediaTileComponentModule.name,
  mediaHeaderComponentModule.name,
  mediaCategoryComponentModule.name,
  mediaRecentlyViewedModule.name,
  mediaNoresultModule.name,
  mediaItemDetailComponentModule.name,
  mediaFileUploadComponentModule.name,
  mediaCardsComponentModule.name,
  regionSelectionComponentModule.name,
  storageIndicatorComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('mediaCenter', {
      url: '/mediaCenter',
      template: '<mediacenterviewer></mediacenterviewer>'
    })
      .state('mediaCenter.list', {
        url: '/list',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.list.html'
      })
      .state('mediaCenter.region', {
        url: '/region',
        templateUrl: 'src/digital-toolkit/pages/media-center/region.selection.html'
      })
      .state('mediaCenter.products', {
        url: '/category1',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.products.html'
      })
      .state('mediaCenter.lifestyle', {
        url: '/category2',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.lifestyle.html'
      })
      .state('mediaCenter.mycontent', {
        url: '/mycontent?:state',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.mycontent.html'
      })
      .state('mediaCenter.opportunity', {
        url: '/category3',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.opportunity.html'
      })
      .state('mediaCenter.searchresult', {
        url: '/searchresult?:state',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.searchresult.html'
      })
      .state('mediaCenter.noresult', {
        url: '/noresult',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.noresult.html'
      })
      .state('mediaCenter.itemDetail', {
        url: '/itemDetail?:fileType',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.itemDetail.html'
      })
      .state('mediaCenter.myfavorites', {
        url: '/myfavorites?:state',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.myfavorites.html'
      })
      .state('mediaCenter.fileupload', {
        url: '/fileupload',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.fileUpload.html'
      })
      .state('mediaCenter.loader', {
        url: '/loader',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.loader.html'
      })
      .state('mediaCenter.lazyLoader', {
        url: '/lazyloader',
        templateUrl: 'src/digital-toolkit/pages/media-center/media.center.lazy.loader.html'
      });

})
.component('mediacenterviewer',mediaCenterViewerComponent);

module.exports= mediaCenterViewerModule;
