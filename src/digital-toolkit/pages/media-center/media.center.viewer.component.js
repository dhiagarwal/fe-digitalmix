'use strict';
var template = require('./media.center.viewer.jade');
var listTemplate = require('./media.center.list.jade');
var productsTemplate = require('./media.center.products.jade');
var lifestyleTemplate = require('./media.center.lifestyle.jade');
var opportunityTemplate = require('./media.center.opportunity.jade');
var mycontentTemplate = require('./media.center.mycontent.jade');
var myfavoriteTemplate = require('./media.center.myfavorites.jade');
var mysearchResultTemplate = require('./media.center.searchresult.jade');
var noresultTemplate = require('./media.center.noresult.jade');
var itemDetailTemplate = require('./media.center.itemDetail.jade');
var fileUploadTemplate = require('./media.center.fileupload.jade');
var regionSelectionTemplate = require('./region.selection.jade');
var mediaCenterLoader = require('./media.center.loader.jade');
var mediaCenterLazyLoader = require('./media.center.lazy.loader.jade');

var mediaCenterViewerComponent = {
  restrict: 'E',
  templateUrl: template
};

module.exports = mediaCenterViewerComponent;
