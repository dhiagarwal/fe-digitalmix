var angular=require('angular');
var uiRouter =require('angular-ui-router');
var genericTemplateViewerComponent = require('./generic.template.viewer.component');
var genericTemplateComponentModule = require('../../components/generic-template/generic.template.component.module');
var genericTemplateViewerModule = angular.module('genericTemplateViewerModule', [
  uiRouter,
  genericTemplateComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('generictemplate', {
      url: '/generictemplate',
      template: '<generictemplateviewer></generictemplateviewer>'
    });
})
.component('generictemplateviewer',genericTemplateViewerComponent);

module.exports= genericTemplateViewerModule;