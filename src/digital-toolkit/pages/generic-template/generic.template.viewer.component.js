'use strict';
var template = require('./generic.template.viewer.jade');
var genericTemplateViewerComponent = {  
  templateUrl: template
};

module.exports = genericTemplateViewerComponent;