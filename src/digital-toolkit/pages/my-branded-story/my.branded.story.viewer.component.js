'use strict';
var template = require('./my.branded.story.viewer.jade');
var myBrandedStoryOverview = require('./my.branded.story.overview.jade');
var myBrandedStoryBuild = require('./my.branded.story.build.jade');
var mbsStoryScript = require('./my.branded.story.story.script.jade');
var mbsStoryInstruction = require('./my.branded.story.instruction.jade');

var myBrandedStoryViewerComponent = {
    restrict: 'E',
    templateUrl: template
};

module.exports = myBrandedStoryViewerComponent;
