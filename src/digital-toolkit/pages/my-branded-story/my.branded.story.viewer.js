var angular=require('angular');
var uiRouter =require('angular-ui-router');

var myBrandedStoryViewerComponent = require('./my.branded.story.viewer.component');

var myBrandedStoryOverviewViewerComponentModule = require('../../components/my-branded-story/mbs-overview/my.branded.story.component.module');

var myBrandedStoryBuildViewerComponentModule = require('../../components/my-branded-story/mbs-build/my.branded.story.build.component.module');
var mbsStoryScriptViewerComponentModule = require('../../components/my-branded-story/mbs-story-script/mbs.story.script.component.module');
var myBrandedStoryInstructionViewerComponentModule = require('../../components/my-branded-story/mbs-instructional-pages/my.branded.story.instructional.page.component.module');

var myBrandedStoryViewerModule = angular.module('myBrandedStoryViewerModule', [
  uiRouter,
  myBrandedStoryOverviewViewerComponentModule.name,
  myBrandedStoryBuildViewerComponentModule.name,
  mbsStoryScriptViewerComponentModule.name,
  myBrandedStoryInstructionViewerComponentModule.name
])
.config(($stateProvider) => {
  $stateProvider
    .state('myBrandedStory', {
        url: '/myBrandedStory',
        template: '<mybrandedstoryviewer></mybrandedstoryviewer>'
    })
    .state('myBrandedStory.overview', {
        url: '/overview',
        templateUrl: 'src/digital-toolkit/pages/my-branded-story/my.branded.story.overview.html'
    })
    .state('myBrandedStory.build', {
        url: '/build',
        templateUrl: 'src/digital-toolkit/pages/my-branded-story/my.branded.story.build.html'
    })
    .state('myBrandedStory.storyScript', {
        url: '/storyScript',
        templateUrl: 'src/digital-toolkit/pages/my-branded-story/my.branded.story.story.script.html'
    })
    .state('myBrandedStory.instruction', {
        url: '/instruction?:fileType',
        templateUrl: 'src/digital-toolkit/pages/my-branded-story/my.branded.story.instruction.html'
    })
})
.component('mybrandedstoryviewer',myBrandedStoryViewerComponent);

module.exports= myBrandedStoryViewerModule;
