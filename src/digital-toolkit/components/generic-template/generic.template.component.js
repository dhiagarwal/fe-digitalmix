'use strict';
var template = require('./generic.template.component.jade');
var controller = require('./generic.template.component.controller');

var genericTemplateComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'generictemplate',
  bindings: {
      bannerUrl: "="  
  }
};

module.exports = genericTemplateComponent;