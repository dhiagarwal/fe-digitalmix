'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var genericTemplateComponent = require('./generic.template.component');

var genericTemplateComponentModule = angular.module('genericTemplateComponentModule', [uiRouter])
                  .component('generictemplatecomponent', genericTemplateComponent)
                  
import Styles from './generic.template.component.scss';                      

module.exports = genericTemplateComponentModule;