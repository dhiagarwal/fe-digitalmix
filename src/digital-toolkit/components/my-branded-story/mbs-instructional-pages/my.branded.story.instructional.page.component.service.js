'use strict';

myBrandedStoryInstructionComponentService.$inject = ['$http', '$q'];

function myBrandedStoryInstructionComponentService($http, $q) {
    this.$http = $http;
    this.$q = $q;
    var deferred = $q.defer();
    $http.get('../../../../src/digital-toolkit/components/my-branded-story/mbs-instructional-pages/my.branded.story.instructional.download.component.json').then(function(response) {
        deferred.resolve(response.data);
    });

    this.getProductData = function() {
      return deferred.promise;
    }
};

myBrandedStoryInstructionComponentService.prototype.getCarouselData = function() {
    var deferred = this.$q.defer();
    this.$http.get('../../../../src/digital-toolkit/components/my-branded-story/mbs-instructional-pages/my.branded.story.instructional.page.component.json')
        .success(deferred.resolve);

    return deferred.promise;
};

myBrandedStoryInstructionComponentService.prototype.getItemData = function(sourceUrl) {
    var deferred = this.$q.defer();
    this.$http.get(sourceUrl)
        .success(deferred.resolve);

    return deferred.promise;
};



module.exports = myBrandedStoryInstructionComponentService;
