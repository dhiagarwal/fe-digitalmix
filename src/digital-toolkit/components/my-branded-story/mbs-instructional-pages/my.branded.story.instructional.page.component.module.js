'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var myBrandedStoryInstructionComponent = require('./my.branded.story.instructional.page.component');
var myBrandedStoryInstructionComponentService = require('./my.branded.story.instructional.page.component.service');

var myBrandedStoryInstructionComponentModule = angular.module('myBrandedStoryInstructionComponentModule', [uiRouter])
.component('mybrandedstoryinstructioncomponent', myBrandedStoryInstructionComponent)
.service('myBrandedStoryInstructionComponentService', myBrandedStoryInstructionComponentService);


import Styles from './my.branded.story.instructional.page.component.scss';

module.exports = myBrandedStoryInstructionComponentModule;
