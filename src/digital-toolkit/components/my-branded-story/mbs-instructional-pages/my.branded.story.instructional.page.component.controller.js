'use strict';

function myBrandedStoryInstructionController(myBrandedStoryInstructionComponentService, $timeout, $stateParams, $window, $location) {
    var self = this;
    self.videoPlaying = false;
    self.sourceUrl = '';
    self.imageMaxHeight = $window.innerHeight; 
    self.contentType = "Presentation";
    self.readMoreContent = {}; 


    self.init = function() {
        //URL to check different options - /itemDetail?fileType=Video
        if ($stateParams.fileType == 'social')
            self.sourceUrl = '/src/digital-toolkit/components/my-branded-story/mbs-instructional-pages/my.branded.story.instructional.socialDetail.component.json';
        else if ($stateParams.fileType == 'video')
            self.sourceUrl = '/src/digital-toolkit/components/my-branded-story/mbs-instructional-pages/my.branded.story.instructional.videoDetail.component.json'
        else {
            self.sourceUrl = '/src/digital-toolkit/components/my-branded-story/mbs-instructional-pages/my.branded.story.instructional.presentationDetail.component.json'
        }
        var promise = myBrandedStoryInstructionComponentService.getItemData(self.sourceUrl);

        promise.then(function(data) {
            self.itemData = data;

        });

        var promise = myBrandedStoryInstructionComponentService.getItemData(self.sourceUrl);
    };

    self.slickVideoConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: false,
        arrows: true,
        dots: true,
        speed: 500,
        event: {
            beforeChange: function(event, slick, currentSlide, nextSlide) {
            },
            afterChange: function(event, slick, currentSlide, nextSlide) {
                var video = angular.element.find('video');
                angular.forEach(video, function(value, key){
                    if(video[key]) {
                        video[key].pause();
                        video[key].controls=false;
                        self.videoPlaying = false;
                    }
                });
            }
        }
    };

    self.togglePlay = function(index) {
        var video = angular.element.find('#video-' + index);

        if (video[0].paused) {
            video[0].play();
            video[0].controls = true;
            self.videoPlaying = true;
            video[0].onended = function() {
                $timeout(function() { video[0].pause(); }, 10)
                video[0].controls = false;
                self.videoPlaying = false;
            };
        } else {
            $timeout(function() { video[0].pause(); }, 10)
            video[0].controls = false;
            self.videoPlaying = false;
        }
    };

    self.video = function(index) {
        var videoElements = angular.element.find('#video-'+index);
        videoElements[0].currentTime = 0;
        videoElements[0].controls = false;
        self.videoPlaying = false;
    };

    self.downloadAsPdf = function(id) {
        var pdf = new jsPDF('p', 'pt', 'letter');
        var source = $('#' + id).html();
        source = source.split('\n');
        var newPdfDiv = document.createElement('div');
        newPdfDiv.className = "pdf";

        for (var s in source) {
            var newElement = document.createElement('p');
            newElement.innerHTML = source[s];
            newPdfDiv.appendChild(newElement);
        }

        var specialElementHandlers = {
            '#editor': function(element, renderer) {
                return true
            }
        }
        var margins = {
            top: 0,
            left: 10,
            width: 600
        };
        pdf.fromHTML(newPdfDiv, margins.left, margins.top, {
            'width': margins.width,
            'elementHandlers': specialElementHandlers
        }, function(dispose) {
            pdf.save('instruction-list.pdf');
        });
    };
    self.gotoPage = function(pageURL) {
        $location.path(pageURL);
    };

    self.readMore = function(type, content) {    
        self.readMoreContent.type = type;    
        self.readMoreContent.content = content;    
        angular.element("#carouselModal").modal();  
    }; 


    var promise = myBrandedStoryInstructionComponentService.getCarouselData();
    promise.then(function(data) {

        self.carouselExamples = data;
    });
    var downloadContentData = myBrandedStoryInstructionComponentService.getProductData();
    downloadContentData.then(function (data) {
        self.productData = data;
    });
}
module.exports = myBrandedStoryInstructionController;
