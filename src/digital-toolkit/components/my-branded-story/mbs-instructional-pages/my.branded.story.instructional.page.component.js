'use strict';
var template = require('./my.branded.story.instructional.page.component.jade');
var controller = require('./my.branded.story.instructional.page.component.controller');

var myBrandedStoryInstructionComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl'
};

module.exports = myBrandedStoryInstructionComponent;
