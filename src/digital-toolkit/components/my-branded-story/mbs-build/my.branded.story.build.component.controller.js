'use strict';

function myBrandedStoryBuildController(myBrandedStoryBuildComponentService, $timeout, $window, $location, $scope) {
    var self = this;
    this.videoPlaying = false;
    this.currentState = 'PERSONAL MOMENTS';
    this.layout = ($window.innerWidth<1024)?"sm-md":"lg-xl";
    this.readMoreContent ={};
    this.skipMBS = false;
    this.imageMaxHeight = $window.innerHeight;

    self.slickVideoConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: false,
        arrows: true,
        dots: true,
        speed:500,
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
                var video = angular.element.find('video');
                angular.forEach(video, function(value, key){
                    if(video[key]) {
                        video[key].pause();
                        video[key].controls=false;
                        self.videoPlaying = false;
                    }
                });
            }
        }
    };

    self.togglePlay = function(index) {
        var video = angular.element.find('#video-'+index);

        if (video[0].paused) {
            video[0].play();
            video[0].controls=true;
            self.videoPlaying = true;
            video[0].onended = function() {
                $timeout(function(){video[0].pause();},10)
                video[0].controls=false;
                self.videoPlaying = false;
            };
        } else {
            $timeout(function(){video[0].pause();},10)
            video[0].controls=false;
            self.videoPlaying = false;
        }
    };
    
    self.video = function(index) {
        var videoElements = angular.element.find('#video-'+index);
        videoElements[0].currentTime = 0;
        videoElements[0].controls = false;
        self.videoPlaying = false;
    };
	
    self.readMore = function(type, content){
        self.readMoreContent.type = type;
        self.readMoreContent.content = content;
        $("#carouselModal").modal();
    };

    self.textAlert = function(){
        if(self.currentState=='PERSONAL MOMENTS') {
            if(self.personalMomentScript && self.personalMomentScript.length>=1000)
                return true;
            else
                return false;
        }
        else if(self.currentState=='CREATE A CONNECTION') {
            if(self.createConnectionScript && self.createConnectionScript.length>=1000)
                return true;
            else
                return false;
        }
        else if(self.currentState=='THE RESOLUTION') {
            if(self.resolutionScript && self.resolutionScript.length>=1000)
                return true;
            else
                return false;
        }
    };

    self.saveScript = function(nav){
        if(nav=='forward') {
            if(self.currentState=='PERSONAL MOMENTS') {
                //save script and navigate
                self.currentState='CREATE A CONNECTION';
            }
            else if(self.currentState=='CREATE A CONNECTION') {
                //save script and navigate
                self.currentState='THE RESOLUTION';
            }
            else if(self.currentState=='THE RESOLUTION') {
                //save script and go to Script review screen
                $window.location.href="#/myBrandedStory/storyScript";
            }
        }
        else if(nav=='back') {
            $("#saveUnfinishedWork").modal('hide');
            if(self.skipMBS) {
                // save script and navigate
                // Navigate to appropriate page when skipped
                $window.location.href="#/myBrandedStory/storyScript";
                $window.location.reload();
            }
            else if(self.currentState=='PERSONAL MOMENTS') {
                // save script and navigate
                //go back to MBS Overview screen
                $window.location.href="#/myBrandedStory/overview";
                $window.location.reload();
            }
            else if(self.currentState=='CREATE A CONNECTION') {
                // save script and navigate
                self.currentState='PERSONAL MOMENTS';
            }
            else if(self.currentState=='THE RESOLUTION') {
                // save script and navigate
                self.currentState='CREATE A CONNECTION';
            }
        }
        $window.scrollTo(0,0);
    };

    self.init = function () {
        var isGoToStep = $location.search() ? $location.search().goToUrl : false;
        if (isGoToStep) {
            switch (parseInt(isGoToStep)) {
                case 3:
                    self.currentState = 'THE RESOLUTION';
                    break;
                default:
                    self.currentState = 'PERSONAL MOMENTS';
                    break;
            }
        } else {
            self.currentState = 'PERSONAL MOMENTS';
        }
    };

    self.goBack = function(){
        if(self.currentState=='PERSONAL MOMENTS') {
            if(self.personalMomentScript && self.personalMomentScript!='') {
                $("#saveUnfinishedWork").modal();
            }
            else {
                if(self.skipMBS) {
                    // Navigate to appropriate page when skipped
                    $window.location.href="#/myBrandedStory/storyScript";
                    $window.location.reload();
                }
                else {
                    //go back to MBS Overview screen
                    $window.location.href="#/myBrandedStory/overview";
                    $window.location.reload();
                }
            }
        }
        else if(self.currentState=='CREATE A CONNECTION') {
            if(self.createConnectionScript && self.createConnectionScript!='') {
                $("#saveUnfinishedWork").modal();
            }
            else {
                if(self.skipMBS) {
                    // Navigate to appropriate page when skipped
                    $window.location.href="#/myBrandedStory/storyScript";
                    $window.location.reload();
                }
                else {
                    $window.scrollTo(0,0);
                    self.currentState='PERSONAL MOMENTS';
                }
            }
        }
        else if(self.currentState=='THE RESOLUTION') {
            if(self.resolutionScript && self.resolutionScript!='') {
                $("#saveUnfinishedWork").modal();
            }
            else {
                if(self.skipMBS) {
                    // Navigate to appropriate page when skipped
                    $window.location.href="#/myBrandedStory/storyScript";
                    $window.location.reload();
                }
                else {
                    $window.scrollTo(0,0);
                    self.currentState='CREATE A CONNECTION';
                }
            }
        }
    };

    self.dontSave = function(){
        $("#saveUnfinishedWork").modal('hide');
        if(self.skipMBS) {
            // Navigate to appropriate page when skipped
            $window.location.href="#/myBrandedStory/storyScript";
            $window.location.reload();
        }
        else if(self.currentState=='PERSONAL MOMENTS') {
            self.personalMomentScript='';
            //go back to MBS Overview screen
            $window.location.href="#/myBrandedStory/overview";
            $window.location.reload();
        }
        else if(self.currentState=='CREATE A CONNECTION') {
            self.createConnectionScript='';
            self.currentState='PERSONAL MOMENTS';
            $window.scrollTo(0,0);
        }
        else if(self.currentState=='THE RESOLUTION') {
            self.resolutionScript='';
            self.currentState='CREATE A CONNECTION';
            $window.scrollTo(0,0);
        }
    };

    self.skipMBSfn = function(){
        self.skipMBS= true;
        self.goBack();
    };

    angular.element(window).on('resize', function() {
        $scope.$apply(function(){
            self.layout = ($window.innerWidth<1024)?"sm-md":"lg-xl";
        });
    });

    var promise = myBrandedStoryBuildComponentService.getCarouselData();
    promise.then(function(data) {
        self.carouselExamples = data;
    });

    var promise1 = myBrandedStoryBuildComponentService.getBuildTypeData();
    promise1.then(function(data) {
        self.storyData = data;
    });
}
module.exports = myBrandedStoryBuildController;
