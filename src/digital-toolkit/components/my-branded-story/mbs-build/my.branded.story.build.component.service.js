'use strict';

myBrandedStoryBuildComponentService.$inject = ['$http','$q'];

function myBrandedStoryBuildComponentService($http,$q) {
  this.$http = $http;
  this.$q = $q;
};

myBrandedStoryBuildComponentService.prototype.getBuildTypeData = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/digital-toolkit/components/my-branded-story/mbs-build/my.branded.story.build.component.json')
        .success(deferred.resolve);

    return deferred.promise;
};

myBrandedStoryBuildComponentService.prototype.getCarouselData = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/digital-toolkit/components/my-branded-story/mbs-build/my.branded.story.build.examples.json')
        .success(deferred.resolve);

    return deferred.promise;
};


module.exports = myBrandedStoryBuildComponentService;