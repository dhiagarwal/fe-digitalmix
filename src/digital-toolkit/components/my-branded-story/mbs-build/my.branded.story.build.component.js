'use strict';
var template = require('./my.branded.story.build.component.jade');
var controller = require('./my.branded.story.build.component.controller');

var myBranedStoryBuildComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl'
};

module.exports = myBranedStoryBuildComponent;
