'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var myBrandedStoryBuildComponent = require('./my.branded.story.build.component');
var myBrandedStoryBuildComponentService = require('./my.branded.story.build.component.service');

var myBrandedStoryBuildComponentModule = angular.module('myBrandedStoryBuildComponentModule', [uiRouter])
.component('mybrandedstorybuildcomponent', myBrandedStoryBuildComponent)
.service('myBrandedStoryBuildComponentService', myBrandedStoryBuildComponentService);


import Styles from './my.branded.story.build.component.scss';

module.exports = myBrandedStoryBuildComponentModule;
