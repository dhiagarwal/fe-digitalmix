'use strict';

mbsStoryScriptComponentService.$inject = ['$http','$q'];

function mbsStoryScriptComponentService($http, $q) {
	this.$http = $http;
	var deferred = $q.defer();
	$http.get('../../../../../src/digital-toolkit/components/my-branded-story/mbs-story-script/mbs.story.script.component.json').then(function(response) {
		deferred.resolve(response.data);
	});

	this.getData = function() {
		return deferred.promise;
	}
};

module.exports = mbsStoryScriptComponentService;