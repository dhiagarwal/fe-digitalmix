'use strict';

require('./mbs.story.script.component.scss');

var angular = require('angular');
var uiRouter = require('angular-ui-router');
var mbsStoryScriptComponent = require('./mbs.story.script.component');
var mbsStoryScriptComponentService = require('./mbs.story.script.component.service');
var formUtils = require('../../../../__shared/form-utils/form-utils');
var pageLevelErrorComponent = require('../../../../__shared/page-level-error/page.level.error.module');
var scrollToTopWithoutClickComponent = require('../../../../__shared/scroll-to-top-without-click/scroll-to-top-without-click.module');

var mbsStoryScriptComponentModule = angular.module('mbsStoryScriptComponentModule', [uiRouter, formUtils.name, pageLevelErrorComponent.name, scrollToTopWithoutClickComponent.name])
		.component('mbsstoryscriptcomponent', mbsStoryScriptComponent)
		.service('mbsStoryScriptComponentService', mbsStoryScriptComponentService)

module.exports = mbsStoryScriptComponentModule;