'use strict';
var template = require('./mbs.story.script.component.jade');
var controller = require('./mbs.story.script.component.controller');

var mbsStoryScriptComponent = {
	templateUrl: template,
	controller: controller,
	controllerAs: 'ctrl',
	bindings: {
		backgroundUrl:"="
	}
};

module.exports = mbsStoryScriptComponent;
