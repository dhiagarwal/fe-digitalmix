'use strict';

function myBrandedStoryController(myBrandedStoryOverviewComponentService, $window) {
    var self = this;
    this.redirectGetStarted = function () {
        $window.location.href = "/#/GetStarted";
    }
    this.redirectSkip = function () {
        $window.location.href = "/#/StoryScript";
    }

    var promise = myBrandedStoryOverviewComponentService.getImgPlaceholderData();
    promise.then(function(data) {
        self.mbsPlaceholderImages = data;
    });
}
module.exports = myBrandedStoryController;
