'use strict';
var template = require('./my.branded.story.component.jade');
var controller = require('./my.branded.story.component.controller');

var quizIntroComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'myBrandedStoryCtrl'
};

module.exports = quizIntroComponent;
