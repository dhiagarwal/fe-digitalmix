'use strict';

myBrandedStoryOverviewComponentService.$inject = ['$http','$q'];

function myBrandedStoryOverviewComponentService($http,$q) {
  this.$http = $http;
  this.$q = $q;
};

myBrandedStoryOverviewComponentService.prototype.getImgPlaceholderData = function(){
	var deferred = this.$q.defer();
	this.$http.get('../../../../src/digital-toolkit/components/my-branded-story/mbs-overview/my.branded.story.component.json')
        .success(deferred.resolve);

    return deferred.promise;
};

module.exports = myBrandedStoryOverviewComponentService;
