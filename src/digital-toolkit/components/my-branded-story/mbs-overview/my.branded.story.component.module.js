'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var myBrandedStoryComponent = require('./my.branded.story.component');
var myBrandedStoryOverviewComponentService = require('./my.branded.story.component.service');


var myBrandedStoryComponentModule = angular.module('myBrandedStoryComponentModule', [uiRouter])
                  .component('mybrandedstorycomponent', myBrandedStoryComponent)
                  .service('myBrandedStoryOverviewComponentService', myBrandedStoryOverviewComponentService);


import Styles from './my.branded.story.component.scss';

module.exports = myBrandedStoryComponentModule;
