'use strict';

var feedLoadingController = require("../../../digital-toolkit/components/feed-loader/feed.loader.component.controller");
var feedLoadingTemplate = require("../../../digital-toolkit/components/feed-loader/feed.loader.component.jade");

quizIntroController.$inject = ['$location', '$uibModal'];

function quizIntroController($location, $uibModal) {
    var self = this;
    this.textname="TWENTYFIVECH ARACTERSRIGHTER"
// pass quiz answers in the credentials object
    this.credentials = {

    };

    this.openResultsPage = function() {

    	// options object to pass parameters to the modal

        var options = {
            templateUrl: feedLoadingTemplate,
            controller: feedLoadingController,
            controllerAs: "quizIntroCtrl",
            windowClass: 'feed-loader',
            backdropClass: 'modal-backdrop',
            resolve: {
                credentials: function() {
                    return self.credentials;
                }
            }
        };
        var modalInstance = $uibModal.open(options);
        modalInstance.result.then(function(credentials) {
            console.log(credentials);
            self.credentials = credentials;

        });
    };
    // modal has no cancel button

};

module.exports = quizIntroController;
