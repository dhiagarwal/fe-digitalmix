'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var quizIntroComponent = require('./quiz.intro.component');

var quizIntroComponentModule = angular.module('quizIntroComponentModule', [uiRouter])
                  .component('quizintrocomponent', quizIntroComponent)
 
                  
import Styles from './quiz.intro.component.scss';                      

module.exports = quizIntroComponentModule;
