'use strict';
var template = require('./quiz.intro.component.jade');
var controller = require('./quiz.intro.component.controller');

var quizIntroComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'quizIntroCtrl'
};

module.exports = quizIntroComponent;
