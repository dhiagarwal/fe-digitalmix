'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var mediaItemDetailComponent = require('./media.item.detail.component');
var mediaItemDetailComponentService = require('./media.item.detail.component.service');


var mediaItemDetailComponentModule = angular.module('mediaItemDetailComponentModule', [uiRouter])
                  .component('mediaitemdetailcomponent',mediaItemDetailComponent)
                  .service('mediaItemDetailComponentService', mediaItemDetailComponentService)
                  
import Styles from './media.item.detail.component.scss';                      

module.exports = mediaItemDetailComponentModule;