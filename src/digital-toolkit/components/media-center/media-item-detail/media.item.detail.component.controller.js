'use strict';

function mediaItemDetailComponentController(mediaItemDetailComponentService, $timeout, $stateParams, mediaCenterFactory) {
	
	this.itemData ={};
	var self = this;
	this.videoPlaying = false;
    this.sourceUrl = '';
    this.factory = mediaCenterFactory;


    if ( mediaCenterFactory.getSuccessData() ){
        var err= "Success. Your file is uploaded.";
        angular.element('.error-message-id').html("<img src='../../../../../src/assets/images/css/icon-completed.svg' class='infoIcon'>" +err);
        angular.element('.error-message-id').css('display','block');
    }

    angular.element('body').on('click', function(e){
        mediaCenterFactory.setSuccessData(false);
    });

    this.init = function(){
        //URL to check different options - /itemDetail?fileType=Video
        if($stateParams.fileType =='Audio')
            this.sourceUrl = '/src/digital-toolkit/components/media-center/media.center.component.itemDetailAudio.json';
        else if($stateParams.fileType =='Video')
            this.sourceUrl = '/src/digital-toolkit/components/media-center/media.center.component.itemDetailVideo.json';
        else if($stateParams.fileType =='PDF')
            this.sourceUrl = '/src/digital-toolkit/components/media-center/media.center.component.itemDetailPDF.json';
        else if($stateParams.fileType =='Document')
            this.sourceUrl = '/src/digital-toolkit/components/media-center/media.center.component.itemDetailDocument.json';
        else if($stateParams.fileType =='Presentation')
            this.sourceUrl = '/src/digital-toolkit/components/media-center/media.center.component.itemDetailPresentation.json';
        else
            this.sourceUrl = '/src/digital-toolkit/components/media-center/media.center.component.itemDetailImage.json';
        
        var promise = mediaItemDetailComponentService.getItemData(this.sourceUrl);

        promise.then(function(data) {
           self.itemData = data;
           self.rating = data.rating;
        });
    };
    
    this.editItem =function(){
        console.log("Edit Item");  
    };
    
	this.starOff = "../../../../src/assets/images/content/pdp-productbasics/star-off.svg";
	this.starOn = "../../../../src/assets/images/content/pdp-productbasics/star-on.svg";
	this.starNumber = 5;
	this.rating = 0;
	this.ratingHolder = this.rating;
	this.ratingFlag = true;
    
    this.toggleFavorite = function () {
        self.itemData.isFavorite = !self.itemData.isFavorite;
	};
    
	this.getNumber = function() {
	    var a = new Array();
	    var i = 0;
	    while (i < this.starNumber) {
	        a[i] = i;
	        i++;
	    }
	    return a;
	};

	this.starFunction = function(index) {
	    if (index <= this.rating) {
	        return this.starOn;
	    } else {
	        return this.starOff;
	    }
	};

	this.rateProduct = function(productRating) {
        console.log("Clicked Ratings");
	};

	this.togglePlay = function() {
        var video = angular.element.find('#media-video');
        
        if (video[0].paused) {
            video[0].play();
            video[0].controls=true;
            this.videoPlaying = true;
        } else {
            $timeout(function(){video[0].pause();},10) 
            video[0].controls=false;
            this.videoPlaying = false;
        }
    };

    this.videoEnd = function() {
    	var video = angular.element.find('#media-video');

    	if (video[0].ended) {
    		video[0].controls=false;
        	this.videoPlaying = false;
    	}
    };

    this.downloadOptions = function(content) {
    	if(content == 'Image' || content == 'Video') {
    		$("#mediaSizes").modal();
    	}
    	else {
    		this.download(content);
    	}
    };

	this.download = function(content) {
        if(content == 'Image' || content == 'Video')
            console.log(self.downloadCategory);
        else
            console.log(content);
	};
};


module.exports = mediaItemDetailComponentController;