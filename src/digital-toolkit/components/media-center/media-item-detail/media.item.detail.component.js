'use strict';
var template = require('./media.item.detail.component.jade');
var controller = require('./media.item.detail.component.controller');

var mediaItemDetailComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
};

module.exports = mediaItemDetailComponent;
