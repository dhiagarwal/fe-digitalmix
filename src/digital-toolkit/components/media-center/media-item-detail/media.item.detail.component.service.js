'use strict';

mediaItemDetailComponentService.$inject = ['$http','$q'];

function mediaItemDetailComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

mediaItemDetailComponentService.prototype.getItemData = function(Url){
	var deferred = this.$q.defer();
	this.$http.get(Url)
		.success(deferred.resolve);

		return deferred.promise;
};


module.exports = mediaItemDetailComponentService;