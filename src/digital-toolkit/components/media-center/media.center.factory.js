"use strict";
mediaCenterFactory.$inject=[];
function mediaCenterFactory(){
	var factoryObj = {
		isErrorRequired:"",
		isSuccess:"",
		getData: function(){
			return this.isErrorRequired;

		},
		setData: function(isErrorRequired){
			this.isErrorRequired = isErrorRequired; 
		},
		getSuccessData: function(){
			return this.isSuccess;

		},
		setSuccessData: function(isSuccess){
			this.isSuccess = isSuccess; 
		}
	};
	return factoryObj;
}
module.exports = mediaCenterFactory;