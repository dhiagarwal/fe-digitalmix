'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var mediaTileComponent = require('./media.tile.component');


var mediaTileComponentModule = angular.module('mediaTileComponentModule', [uiRouter])
                  .component('mediatilecomponent',mediaTileComponent)
                 
                  
import Styles from './media.tile.component.scss';                      

module.exports = mediaTileComponentModule;
