'use strict';
var modalTemplate = require('./media.tile.share.modal.jade');

function mediaTileComponentController($uibModal) {

	this.$uibModal = $uibModal;
	var self = this;
	self.isDeleted = false;
	self.isFavorited = false;
	this.selRating = 1;
	
	this.toggleFavorite = function () {
        self.isFavorited = !self.isFavorited;
        self.listItem.isFavorite = !self.listItem.isFavorite;
	};

	this.toggleDel = function(){
		self.isDeleted = !self.isDeleted;
	};
    
	this.shareContent = function(){
		
    	var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: modalTemplate,
        controller: ['$uibModalInstance', function ($uibModalInstance) {
            this.ok = function () {
                $uibModalInstance.close();
            };

            this.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }],
        size: 'sm',
        controllerAs: 'modalCtrl',
        windowClass: 'app-modal-window'
	});
   };

};

module.exports = mediaTileComponentController;
