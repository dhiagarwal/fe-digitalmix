'use strict';
var template = require('./media.tile.component.jade');
var controller = require('./media.tile.component.controller');

var mediaTileComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      listItem: "=",
      extraClass: "@",
      ifDeleteVisible: "=",
      ifFavUndoVisible: "=",
      ifShareVisible: "=",
      ifFavoriteVisible: "="
  }
};

module.exports = mediaTileComponent;
