'use strict';
var modalTemplate = require('./media.banner.component.modal.jade');

function mediaBannerComponentController($uibModal) {

  	this.$uibModal = $uibModal;
	var self = this;
    this.successMessage= "Success! Your feed is updated";
	
	self.quizVisible = true;
	
	this.toggleQuiz = function () {
	      self.quizVisible = !self.quizVisible;
	}

	this.getConfirmation = function(){
		
    	var modalInstance = this.$uibModal.open({
        animation: true,
        templateUrl: modalTemplate,
        controller: ['$uibModalInstance', function ($uibModalInstance) {
            this.ok = function () {
                $uibModalInstance.close();
            };

            this.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }],
        size: 'sm',
        backdrop: 'static',
        controllerAs: 'modalCtrl',
        windowClass: 'app-modal-window dashboard-reset-modal'
	});

};
}

//mediaCenterComponentController.$inject = ['mediaCentercomponentService'];

module.exports = mediaBannerComponentController;