'use strict';
var template = require('./media.banner.component.jade');
var controller = require('./media.banner.component.controller');

var mediaBannerComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
  	backgroundUrl:"="
  }
};

module.exports = mediaBannerComponent;
