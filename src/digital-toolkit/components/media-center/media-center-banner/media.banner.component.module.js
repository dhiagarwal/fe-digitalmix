'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var mediaBannerComponent = require('./media.banner.component');


var mediaBannerComponentModule = angular.module('mediaBannerComponentModule', [uiRouter])
                  .component('mediabannercomponent',mediaBannerComponent)
                  
                  
import Styles from './media.banner.component.scss';                      

module.exports = mediaBannerComponentModule;
