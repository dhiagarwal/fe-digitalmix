'use strict';
function storageIndicatorComponentController() {
  this.progressIndicator = false;
  this.storageText = false;

  if (this.myCurrentStorage >= 4.7) {
    this.progressIndicator = true;
    this.storageText = true;
  }

  if (this.myCurrentStorage >= 5) {
    this.myCurrentStorage = 5;
  }

  angular.element("#completeBar").css("width", this.myCurrentStorage / this.myTotalStorage * 100 + "%");

}
module.exports = storageIndicatorComponentController;
