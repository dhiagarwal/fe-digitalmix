'use strict';

var template =require('./storage.indicator.jade');
var controller =require('./storage.indicator.component.controller');

var storageIndicatorComponent =  {
    templateUrl : template,
    controller: controller,
    controllerAs: 'ctrl',
    bindToController: true,
    bindings:{
      myCurrentStorage: "<",
      myTotalStorage: "<",
      valueType: "<"
    }
};

module.exports = storageIndicatorComponent;
