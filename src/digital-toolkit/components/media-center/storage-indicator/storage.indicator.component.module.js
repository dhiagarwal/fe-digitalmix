var angular =require('angular');
var uiRouter =require('angular-ui-router');
var storageIndicatorComponent =require('./storage.indicator.component');

var storageIndicatorComponentModule = angular.module('storageIndicatorComponentModule', [uiRouter])
  .component('storageIndicatorComponent', storageIndicatorComponent)
var base =require('../../../../stylesheets/base.scss');
var Styles =require('./storage.indicator.scss');

module.exports= storageIndicatorComponentModule;
