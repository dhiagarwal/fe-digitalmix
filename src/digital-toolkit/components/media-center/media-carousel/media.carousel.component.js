'use strict';
var template = require('./media.carousel.component.jade');
var controller = require('./media.carousel.component.controller');

var mediaCarouselComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindings:{
    sourceUrl:'='
  }
};

// import Styles from './media.carousel.component.scss';   

module.exports = mediaCarouselComponent;
