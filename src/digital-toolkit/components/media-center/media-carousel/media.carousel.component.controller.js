'use strict';

var _ = require('lodash');

function mediaCarouselComponentController(mediaCarouselComponentService) {
  
  this.tileData ={};
  var self = this;

  this.slickSecondaryConfig = {
          enabled: true,
          infinite: false,
          adaptiveHeight: false,
          mobileFirst: true,
          slidesToShow: 4,
          slidesToScroll: 1,
          dots: false,
          speed: 500,
          responsive: [
            {
              breakpoint: 320,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 4,
              }
            },
            {
              breakpoint: 1023,
              settings: {
                slidesToShow: 4,
                draggable: false,
              }
            },
            {
              breakpoint: 1425,
              settings: {
                slidesToShow: 5,
                draggable: false,
              }
            }
          ]
        };

var promise = mediaCarouselComponentService.getTileData(this.sourceUrl);
  
  promise.then(function(data) {

    self.title = Object.keys(data)[0];
    self.filteredData = _.take(data.data, 10);
    self.tileData = data;
    
  });
  


};


module.exports = mediaCarouselComponentController;
