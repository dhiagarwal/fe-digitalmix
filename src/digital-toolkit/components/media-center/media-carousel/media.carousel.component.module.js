'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('slick-carousel');
require('angular-slick-carousel');
require('angular-animate');

var mediaCarouselComponent = require('./media.carousel.component');
var mediaCarouselComponentService = require('./media.carousel.component.service');


var mediaCarouselComponentModule = angular.module('mediaCarouselComponentModule', [uiRouter,'ngAnimate','slickCarousel'])
                  .component('mediacarouselcomponent',mediaCarouselComponent)
                  .service('mediaCarouselComponentService', mediaCarouselComponentService)

                  
import Styles from './media.carousel.component.scss';                      

module.exports = mediaCarouselComponentModule;
