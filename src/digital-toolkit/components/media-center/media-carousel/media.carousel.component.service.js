'use strict';

mediaCarouselComponentService.$inject = ['$http','$q'];

function mediaCarouselComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

mediaCarouselComponentService.prototype.getTileData = function(Url){
	console.log('URL :' + Url);
	var deferred = this.$q.defer();
	this.$http.get(Url)
		.success(deferred.resolve);

		return deferred.promise;
};

//   var deferred = $q.defer();
//   $http.get('../../../../../src/digital-toolkit/components/media-center/media.center.component.product.json').then(function(response) {
//     deferred.resolve(response.data);
//   });

//   this.getTileData = function(sourceUrl) {

//     return deferred.promise;
//   }

// };

module.exports = mediaCarouselComponentService;