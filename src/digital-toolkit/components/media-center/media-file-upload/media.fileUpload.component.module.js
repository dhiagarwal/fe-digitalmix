'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var ngFileUpload = require('../../../../../src/vendors/angular-file-upload/angular-file-upload.js');
var ngAwsS3 = require('../../../../../src/vendors/angular-file-upload/aws-sdk-2.1.48.js');
var circleCss = require('../../../../../src/vendors/angular-file-upload/circle.css');
var mediaCenterFactory = require("../media.center.factory");

/*require('slick-carousel');
require('angular-slick-carousel');
require('angular-animate');*/

var mediafileUploadComponent = require('./media.fileUpload.component');
var mediafileUploadComponentService = require('./media.fileUpload.component.service');



var mediafileUploadComponentModule = angular.module('mediafileUploadComponentModule', [uiRouter, 'ngFileUpload'])
                  .component('mediafileuploadcomponent',mediafileUploadComponent)
                  .factory('mediaCenterFactory', mediaCenterFactory)
                  .service('mediafileUploadComponentService', mediafileUploadComponentService)
                 
                  
mediafileUploadComponentModule.run();

import Styles from './media.fileUpload.component.scss';                      

module.exports = mediafileUploadComponentModule;
