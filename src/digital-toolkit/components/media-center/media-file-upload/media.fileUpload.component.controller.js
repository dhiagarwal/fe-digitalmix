'use strict';

mediafileUploadComponentController.$inject = ['$scope', '$location', 'mediafileUploadComponentService', 'mediaCenterFactory']

function mediafileUploadComponentController($scope, $location, mediafileUploadComponentService, mediaCenterFactory) {
    this.factory = mediaCenterFactory;
    this.contentTitleInput = 'Image.png';
    this.showUploadStatus = false;
    this.showDragDrop = false;

    var self = this;
    self.showPreview = false;
    self.fileNameType = 'Image';
    self.objFile = '';
    self.tagsArray = [];

    this.enterPressed = function(keyEvent) {
    if (keyEvent.which === 13){
      self.addTag();
      return ;
    }
  }
    var promise = mediafileUploadComponentService.getFormdata();

    promise.then(function(response) {
        self.language = response.language;
    });


    $scope.uploadFiles = function(files) {

        if(files == undefined){
            return;
        }

        var arrayOfFiles;
        var err;

        if (files instanceof Array) {
            $scope.Files = files;
            arrayOfFiles = files;

        } else {
            arrayOfFiles = new Array();
            arrayOfFiles.push(files);
            $scope.Files = arrayOfFiles;
        }

        if (arrayOfFiles && arrayOfFiles.length > 0) {

            if (arrayOfFiles.length > 1) {
                err = "Please select only one file to upload."
                angular.element('.upload-content-banner .errorMessage').html("<img src='../../../../../src/assets/images/css/ic-info.png' class='infoIcon'>" + err);
                angular.element('.upload-content-banner .errorMessage').css('display', 'block');
                self.showUploadStatus = false;
                return;

            } else {

                angular.forEach($scope.Files, function(file, key) {

                    self.objFile = file; // this si required to refer this file object globally [AEM will need this].
                    var sFileName = file.name;


                    var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
                    var iFileSize = file.size;
                    var _url = '';

                    if (sFileExtension === "png" || sFileExtension === "jpg" || sFileExtension === "gif" || sFileExtension === "tif") {

                        self.fileNameType = "Image";
                        _url = angular.element(".showType").attr('ng-src');
                        _url = _url + 'ic-asset-image@3x.png';
                        angular.element(".showType").attr('src', _url);

                    } else if (sFileExtension === "mp4" || sFileExtension === "mov" || sFileExtension === "qt" || sFileExtension === "flv" || sFileExtension === "wmv" || sFileExtension === "mpg" || sFileExtension === "avi") {

                        self.fileNameType = "Video";
                        _url = angular.element(".showType").attr('ng-src');
                        _url = _url + 'ic-asset-video@3x.png';
                        angular.element(".showType").attr('src', _url);

                    } else if (sFileExtension === "aac" || sFileExtension === "mp3" || sFileExtension === "wav") {

                        self.fileNameType = "Audio";
                        _url = angular.element(".showType").attr('ng-src');
                        _url = _url + 'ic-audio-asset-copy-10@3x.png';
                        angular.element(".showType").attr('src', _url);

                    } else if (sFileExtension === "doc" || sFileExtension === "docx" || sFileExtension === "txt" || sFileExtension === "xls" || sFileExtension === "xlsx") {

                        self.fileNameType = "Document";
                        _url = angular.element(".showType").attr('ng-src');
                        _url = _url + 'ic-document-asset-copy-18@3x.png';
                        angular.element(".showType").attr('src', _url);

                    } else if (sFileExtension === "ppt" || sFileExtension === "pptx") {

                        self.fileNameType = "PPT";
                        _url = angular.element(".showType").attr('ng-src');
                        _url = _url + 'ic-presentation-asset-copy-18@3x.png';
                        angular.element(".showType").attr('src', _url);

                    } else if (sFileExtension === "pdf") {

                        self.fileNameType = "PDF";
                        _url = angular.element(".showType").attr('ng-src');
                        _url = _url + 'ic-pdf-asset-copy-19@3x.png';
                        angular.element(".showType").attr('src', _url);

                    }else{
                        err = "Your file type is not supported. Please check for the accepted formats below"
                        angular.element('.upload-content-banner .errorMessage').html("<img src='../../../../../src/assets/images/css/ic-info.png' class='infoIcon'>" + err);
                        angular.element('.upload-content-banner .errorMessage').css('display', 'block');
                        self.showUploadStatus = false;
                        return;
                    }


                    if ((!(sFileExtension === "png" || sFileExtension === "jpg" || sFileExtension === "gif" || sFileExtension === "tif") || iFileSize > (2 * 1024 * 1024)) &&

                        (!(sFileExtension === "mp4" || sFileExtension === "mov" || sFileExtension === "qt" || sFileExtension === "flv" || sFileExtension === "wmv" || sFileExtension === "mpg" || sFileExtension === "avi") || iFileSize > (100 * 1024 * 1024)) &&

                        (!(sFileExtension === "aac" || sFileExtension === "mp3" || sFileExtension === "wav") || iFileSize > (20 * 1024 * 1024)) &&

                        (!(sFileExtension === "doc" || sFileExtension === "docx" || sFileExtension === "txt" || sFileExtension === "xls" || sFileExtension === "xlsx") || iFileSize > (12 * 1024 * 1024)) &&

                        (!(sFileExtension === "ppt" || sFileExtension === "pptx") || iFileSize > (1 * 1024 * 1024)) &&

                        (!(sFileExtension === "pdf") || iFileSize > (1 * 1024 * 1024))

                    ) {
                        err = "Your file size exceeds the maximum file size allowed. Please see size limitation below."
                        angular.element('.upload-content-banner .errorMessage').html("<img src='../../../../../src/assets/images/css/ic-info.png' class='infoIcon'>" + err);
                        angular.element('.upload-content-banner .errorMessage').css('display', 'block');
                        self.showUploadStatus = false;
                        return;

                    } else {
                        self.showUploadStatus = true;
                        angular.element('body').css('overflow-y', 'hidden');
                    }


                    mediafileUploadComponentService.Upload(file).then(function(result) {
                        // Mark as success
                        file.Success = true;
                    }, function(error) {
                        // Mark the error
                        $scope.Error = error;
                    }, function(progress) {
                        // Write the progress as a percentage
                        file.Progress = (progress.loaded / progress.total) * 100;
                        console.log("file.Progress =" + file.Progress);

                        if (file.Progress == "100") {
                            self.showPreview = true;
                            angular.element('body').css('overflow-y', 'auto');
                            angular.element('body mediacategorybannercomponent').hide();
                            var fetchSrc;

                            if (self.fileNameType == "Image") {
                                fetchSrc = 'https://s3-us-west-2.amazonaws.com/nuskinupload/' + sFileName;
                            } else if (self.fileNameType == "Video") {
                                fetchSrc = '../../../../../src/assets/images/css/ic-video.svg';
                            } else if (self.fileNameType == "Audio") {
                                fetchSrc = '../../../../../src/assets/images/css/ic-audio.svg';
                            } else if (self.fileNameType == "Document") {
                                fetchSrc = '../../../../../src/assets/images/css/ic-document.svg';
                            } else if (self.fileNameType == "PPT") {
                                fetchSrc = '../../../../../src/assets/images/css/ic-presentation.svg';
                            } else if (self.fileNameType == "PDF") {
                                fetchSrc = '../../../../../src/assets/images/css/ic-pdf.svg';
                            }

                            angular.element('.prevImg').attr('src', fetchSrc);
                            angular.element('.prevImg').css('content', 'url(' + fetchSrc + ')');

                        }
                    });
                });

            }
        }
    };

    self.addTag = function() {
        if (!self.tags || _.includes(self.tagsArray, self.tags)) {
            return;
        } else {
            self.tagsArray.push(self.tags);
            self.tags = '';
        }
    };

    self.removeTag = function(tag) {
        self.tagsArray.splice(self.tagsArray.indexOf(tag), 1);
    };

    self.gotoMyContent = function() {
        $location.path("/mediaCenter/mycontent");
        mediaCenterFactory.setData(true);

    }

    self.sendContentInformation = function() {

        $location.path("/mediaCenter/itemDetail");
        mediaCenterFactory.setSuccessData(true);

        console.log("Inside  sendContentInformation");
        console.log("Content Title = " + self.contentTitleInput);
        console.log("Content Description = " + self.contentDescInput);
        console.log("Language = " + self.selectedlanguage);
        console.log("Is Social share checked  = " + self.socialShare);
        console.log("Tags  = " + self.tags);
    }


    angular.element('body').on('dragover', function(e) {
        self.showDragDrop = true;
    });

    angular.element('body').on('dragleave', function(e) {
        self.showDragDrop = false;
    });

    angular.element('body').on('drop', function(e) {
        var err;
        e.stopPropagation();
        e.preventDefault();
        self.showDragDrop = false;
        self.showUploadStatus = true;
        if(e.originalEvent.dataTransfer){
          if(e.originalEvent.dataTransfer.files.length == 1){
            $scope.$watch('files', function() {
                $scope.uploadFiles($scope.files);
            });
          } else{
            err = "Please select only one file to upload.";
            angular.element('.upload-content-banner .errorMessage').html("<img src='../../../../../src/assets/images/css/ic-info.png' class='infoIcon'>" + err);
            angular.element('.upload-content-banner .errorMessage').css('display', 'block');
            self.showUploadStatus = false;
          }
        }
    });

    angular.element('.uploadBtn').on('dragover', function(e) {
        e.preventDefault();
    });

    angular.element('.category-name').css({'display':'none'});
    angular.element('.refine').css('display', 'none');

};


module.exports = mediafileUploadComponentController;
