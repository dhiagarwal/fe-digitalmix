'use strict';
var template = require('./media.fileUpload.component.jade');
var controller = require('./media.fileUpload.component.controller');

var mediafileUploadComponent = {
  restrict: 'E',
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true,
  replace: true
};

module.exports = mediafileUploadComponent;
