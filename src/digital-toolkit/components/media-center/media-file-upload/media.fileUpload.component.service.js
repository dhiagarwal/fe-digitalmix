'use strict';

mediafileUploadComponentService.$inject = ['$http','$q'];

function mediafileUploadComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

  
  // Us standard region
    AWS.config.region = 'us-west-2';
    AWS.config.update({ accessKeyId: 'AKIAJNTLKZMHQMVQDTOA', secretAccessKey: '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' });

    var bucket = new AWS.S3({ params: { Bucket: 'nuskinupload', maxRetries: 10 }, httpOptions: { timeout: 360000 } });
    this.Progress = 0;


    // File upload service ptototype
    mediafileUploadComponentService.prototype.Upload = function (file) {
        
        var deferred = $q.defer();
        var params = { Bucket: 'nuskinupload', Key: file.name, ContentType: file.type, Body: file };
        var options = {
            // Part Size of 10mb
            partSize: 10 * 1024 * 1024,
            queueSize: 1,
            // Give the owner of the bucket full control
            ACL: 'bucket-owner-full-control'
        };
        var uploader = bucket.upload(params, options, function (err, data) {
            if (err) {
                deferred.reject(err);
            }
            deferred.resolve();
        });
        uploader.on('httpUploadProgress', function (event) {
            deferred.notify(event);
        });

        return deferred.promise;
    };

    // the below deferred Object is for Language JSon for Preview page
    var deferredJSON = $q.defer();

    $http.get('../../../../../src/digital-toolkit/components/media-center/media.center.component.language.json').then(function(response) {
        deferredJSON.resolve(response.data);
    });

    // Simple custom function for to promise the JSON call value.
    this.getFormdata = function() {
        return deferredJSON.promise;
    }

};

module.exports = mediafileUploadComponentService;
