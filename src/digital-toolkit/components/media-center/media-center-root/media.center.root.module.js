'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('slick-carousel');
require('angular-slick-carousel');
require('angular-animate');


var mediaFavoriteComponent = require('./media.favorite.component');


var mediaFavoriteComponentModule = angular.module('mediaFavoriteComponentModule', [uiRouter,'ngAnimate','slickCarousel'])
                  .component('mediafavoritecomponent',mediaFavoriteComponent)
                
                  
import Styles from './media.favorite.component.scss';                      

module.exports = mediaFavoriteComponentModule;
