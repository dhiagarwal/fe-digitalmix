'use strict';
var template = require('./media.favorite.component.jade');
var controller = require('./media.favorite.component.controller');

var mediaFavoriteComponent = {
  restrict: 'E',
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  bindToController: true
};

module.exports = mediaFavoriteComponent;
