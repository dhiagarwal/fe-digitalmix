'use strict';
var modalTemplate = require('../media-tile/media.tile.share.modal.jade');
function mediaRecentlyViewedComponentController($uibModal) {
	var self = this;
	self.del = false;
	self.fav = false;
	this.$uibModal = $uibModal;

	this.toggleFavorite = function () {
        self.fav = !self.fav;
        self.listItem.isFavorite = !self.listItem.isFavorite;
	};

	this.toggleDel = function(){
		self.del = !self.del;
	};
	this.shareContent = function(){

			var modalInstance = this.$uibModal.open({
				animation: true,
				templateUrl: modalTemplate,
				controller: ['$uibModalInstance', function ($uibModalInstance) {
						this.ok = function () {
								$uibModalInstance.close();
						};

						this.cancel = function () {
								$uibModalInstance.dismiss();
						};
				}],
				size: 'sm',
				controllerAs: 'modalCtrl',
				windowClass: 'app-modal-window'
	});
};

};

module.exports = mediaRecentlyViewedComponentController;
