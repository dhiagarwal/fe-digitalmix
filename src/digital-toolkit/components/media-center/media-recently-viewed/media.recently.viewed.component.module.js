'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('slick-carousel');
require('angular-slick-carousel');
require('angular-animate');

var mediaRecentlyViewedComponent = require('./media.recently.viewed.component');


var mediaRecentlyViewedComponentModule = angular.module('mediaRecentlyViewedComponentModule', [uiRouter,'ngAnimate','slickCarousel'])
                  .component('mediarecentlyviewedcomponent',mediaRecentlyViewedComponent)
                 
                  
import Styles from './media.recently.viewed.component.scss';                      

module.exports = mediaRecentlyViewedComponentModule;
