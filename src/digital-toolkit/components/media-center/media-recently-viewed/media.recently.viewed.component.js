'use strict';
var template = require('./media.recently.viewed.component.jade');
var controller = require('./media.recently.viewed.component.controller');

var mediaRecentlyViewedComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      listItem: "=",
      extraClass: "@",
      ifDeleteVisible: "=",
      ifFavUndoVisible: "="
  }
};

module.exports = mediaRecentlyViewedComponent;
