'use strict';
var template = require('./media.category.component.jade');
var controller = require('./media.category.component.controller');

var mediaCatagoryComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      sourceUrl:'=',
      ifDeleteVisible:'=',
      ifFavUndoVisible:'='
  }
};

module.exports = mediaCatagoryComponent;
