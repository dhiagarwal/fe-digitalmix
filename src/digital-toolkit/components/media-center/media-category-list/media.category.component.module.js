'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('angular-animate');

var mediaCategoryComponent = require('./media.category.component');
var mediaCategoryComponentService = require('./media.category.component.service');


var mediaCategoryComponentModule = angular.module('mediaCategoryComponentModule', [uiRouter,'ngAnimate'])
                  .component('mediacategorycomponent',mediaCategoryComponent)
                  .service('mediaCategoryComponentService', mediaCategoryComponentService)
                  
import Styles from './media.category.component.scss';                      

module.exports = mediaCategoryComponentModule;
