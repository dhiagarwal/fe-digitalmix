'use strict';

mediaCategoryComponentService.$inject = ['$http','$q'];

function mediaCategoryComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

mediaCategoryComponentService.prototype.getTileData = function(Url){
	
	var deferred = this.$q.defer();
	
	this.$http.get(Url)
		.success(deferred.resolve);

		return deferred.promise;
};
 mediaCategoryComponentService.prototype.getFilterData = function(){
 		var deferred = this.$q.defer();

 		this.$http.get('../../../../src/digital-toolkit/components/media-center/media-category-list/media.category.filter.component.json')
 			.success(deferred.resolve);

 			return deferred.promise;
 }

module.exports = mediaCategoryComponentService;