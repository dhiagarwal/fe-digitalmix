var angular = require('angular');

var mediaCardsComponent = require('./media.cards.component');
var mediaCardsComponentService = require('./media.cards.component.service');

var mediaCardsComponentModule = angular.module('mediaCardsComponentModule', [])
                  .component('mediacardscomponent',mediaCardsComponent)
                  .service('mediaCardsComponentService', mediaCardsComponentService)


import Styles from './media.cards.component.scss';

module.exports = mediaCardsComponentModule;
