'use strict';

mediaCardsComponentService.$inject = ['$http','$q'];

function mediaCardsComponentService($http,$q) {

  this.$http = $http;

  var cardComponents = $q.defer();

  $http.get('../../../../src/digital-toolkit/components/media-center/media.center.component.cards.json').then(function(response) {
    cardComponents.resolve(response.data);
  });

  this.getCardComponent = function() {
    return cardComponents.promise;
  }

};

module.exports = mediaCardsComponentService;
