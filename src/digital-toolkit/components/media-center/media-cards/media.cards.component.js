'use strict';
var template = require('./media.cards.component.jade');
var controller = require('./media.cards.component.controller');

var mediaCardsComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl'
};

import Styles from './media.cards.component.scss';

module.exports = mediaCardsComponent;
