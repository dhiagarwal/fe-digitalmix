'use strict';

function mediaCardsComponentController(mediaCardsComponentService) {
  var self = this;
  this.cardDismissShow = false;

  this.cardsStartUpMessage = "Media Center Basics";
  var getCardVal = mediaCardsComponentService.getCardComponent();

  getCardVal.then(function(data) {
      self.toDoCards = data;
  });

};


module.exports = mediaCardsComponentController;
