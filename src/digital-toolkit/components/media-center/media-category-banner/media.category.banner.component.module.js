'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');


var mediaCategoryBannerComponent = require('./media.category.banner.component');
var mediaCategoryComponent = require('../media-category-list/media.category.component');


var mediaCategoryBannerComponentModule = angular.module('mediaCategoryBannerComponentModule', [uiRouter])
                  .component('mediacategorybannercomponent',mediaCategoryBannerComponent)
                  
                  
                  
import Styles from './media.category.banner.component.scss';                      

module.exports = mediaCategoryBannerComponentModule;
