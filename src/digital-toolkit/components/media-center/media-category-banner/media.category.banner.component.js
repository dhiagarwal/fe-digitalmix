'use strict';
var template = require('./media.category.banner.component.jade');
var controller = require('./media.category.banner.component.controller');


var mediaCategoryBannerComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
      categoryName: "@",
      uploadIcon: "=",
      bannerUrl: "=",
      content: "@"
  }
};

module.exports = mediaCategoryBannerComponent;
