'use strict';

function mediaCategoryBannerComponentController($stateParams,$location,mediaCenterFactory, $rootScope) {

    this.onlyBanner='';
    this.bannerClass = '';
    this.btnName='';
    this.factory = mediaCenterFactory;
    this.myTotalStorage = 5;
    this.myCurrentStorage = 4.7;
    this.valueType = "GB";

    var self = this;

    this.refineClicked = function(){
        // this.parent.callFilter();
        $rootScope.$broadcast('showrefine');
    }

    self.init= function(){
        if($stateParams.state)
            self.onlyBanner=true;
        else
            self.onlyBanner=false;

        if(self.categoryName=='My Favorites')
        {
            self.bannerClass = 'myFavBanner';
            self.btnName = 'START EXPLORING';
        }
        else if(self.categoryName=='My Content')
        {
            self.bannerClass = 'myContentBanner';
            self.btnName = 'UPLOAD';
        }
        else if(self.categoryName=='Search Result'){
            self.bannerClass = 'myContentBanner';
            self.btnName = 'SEARCH AGAIN';
        }
        else if(self.categoryName=='category1')
            self.bannerClass = 'productsBanner';
        else if(self.categoryName=='category1')
            self.bannerClass = 'lifestyleBanner';
        else if(self.categoryName=='category1')
            self.bannerClass = 'opportunityBanner';
    };

    self.showFileUpload = function(){
        $location.path("/mediaCenter/fileupload");
    };

    if ( mediaCenterFactory.getData() ){
        //console.log("ISERRORREQUIRED IS = " + mediaCenterFactory.getData() );
        angular.element(".errorMessage").css('display','block');
    }
};


module.exports = mediaCategoryBannerComponentController;
