'use strict';
var template = require('./media.noresult.component.jade');
var controller = require('./media.noresult.component.controller');

var mediaNoresultComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
    bannerUrl: "="
  }
};

module.exports = mediaNoresultComponent;
