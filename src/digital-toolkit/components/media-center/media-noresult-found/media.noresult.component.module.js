'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

var mediaNoresultComponent = require('./media.noresult.component');



var mediaNoresultComponentModule = angular.module('mediaNoresultComponentModule', [uiRouter])
                  .component('medianoresultcomponent',mediaNoresultComponent)
                  
                  
import Styles from './media.noresult.component.scss';                      

module.exports = mediaNoresultComponentModule;
