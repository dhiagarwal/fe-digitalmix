'use strict';

mediaHeaderComponentService.$inject = ['$http','$q'];

function mediaHeaderComponentService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

mediaHeaderComponentService.prototype.getRecentlyViewedData = function(){
	var deferred = this.$q.defer();
	this.$http.get('/src/digital-toolkit/components/media-center/media.center.component.recentlyviewed.json')
		.success(deferred.resolve);

		return deferred.promise;
};

mediaHeaderComponentService.prototype.getSearchedHistoryData = function(){
	var deferred = this.$q.defer();
	this.$http.get('/src/digital-toolkit/components/media-center/media.center.component.searchedhistory.json')
		.success(deferred.resolve);

		return deferred.promise;
};

mediaHeaderComponentService.prototype.getQuickSearchData = function(){
	var deferred = this.$q.defer();
	this.$http.get('/src/digital-toolkit/components/media-center/media.center.component.quicksearch.json')
		.success(deferred.resolve);

		return deferred.promise;
};


module.exports = mediaHeaderComponentService;