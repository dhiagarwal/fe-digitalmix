'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');

require('angular-animate');
require('../../../../../src/vendors/typeahead/typeahead.js');
require('../../../../../src/vendors/typeahead/angular-typeahead.js');

var mediaHeaderComponent = require('./media.header.component');
var mediaHeaderComponentService = require('./media.header.component.service');

var mediaHeaderComponentModule = angular.module('mediaHeaderComponentModule', [uiRouter,'ngAnimate','nuskin.typeahead'])
                  .component('mediaheadercomponent',mediaHeaderComponent)
                  .service('mediaHeaderComponentService', mediaHeaderComponentService)	
                  
import Styles from './media.header.component.scss';                      

module.exports = mediaHeaderComponentModule;
