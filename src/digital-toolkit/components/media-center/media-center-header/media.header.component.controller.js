'use strict';

var _ = require('lodash');

function mediaHeaderComponentController(mediaHeaderComponentService, $scope, $window, mediaCenterFactory,$timeout) {

    this.dropdown = false;
    this.showSearch = false;
    this.searchOverlayHeight = $window.innerHeight;
    var self = this;
    this.dropdown = false;
    this.toggledd = function() {

        if (this.dropdown) {
            this.dropdown = false;
        } else {
            this.dropdown = true;
        }
    }

    this.hitSearch = function() {

        angular.element('#searchModal').on('hidden.bs.modal', function() {  
            $window.location.href = '#/mediaCenter/searchresult';
        }).modal('hide');

    }

    this.factory = mediaCenterFactory;

    if (mediaCenterFactory.getData()) {
        var err = "Sorry. Unknown error occured.Your file was not uploaded. Please try again.";
        angular.element('.errorMessage').html("<img src='../../../../../src/assets/images/css/ic-info.png' class='infoIcon'>" + err);
        angular.element(".errorMessage").css('display', 'block');
    }

    angular.element('body').on('click', function(e) {
        mediaCenterFactory.setData(false);
    });


    var promise = mediaHeaderComponentService.getRecentlyViewedData();

    promise.then(function(data) {

        self.recentlyViewedData = _.take(data, 6);

    });

    promise = mediaHeaderComponentService.getSearchedHistoryData();

    promise.then(function(data) {

        self.searchedHistoryData = _.take(data, 5);

        // self.searchedHistoryData = data;

    });

    promise = mediaHeaderComponentService.getQuickSearchData();

    promise.then(function(data) {

        self.quickSearchData = data;

        //code for typeahead
        self.selectedNumber = null;

        // instantiate the bloodhound suggestion engine
        self.numbers = new Bloodhound({
            datumTokenizer: function(d) {
                return Bloodhound.tokenizers.whitespace(d.Key); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: self.quickSearchData
        });

        self.numbers.initialize();

        // initialize the bloodhound suggestion engine
        self.numbersDataset = {
            displayKey: 'Key',
            source: self.numbers.ttAdapter()
        };

        self.exampleOptions = {
            highlight: true,
            limit: 5,
            minLength: 3
        };

        self.clearValue = function() {
            self.selectedNumber = null;
        };

    });


    this.callSearch = function() {
        self.searchOverlayHeight = $window.innerHeight;
        self.showSearch = true;
        $timeout(function(){
            document.getElementById('input-focus').focus();
        },1000); 
    }

};

//mediaCenterComponentController.$inject = ['mediaCentercomponentService'];

module.exports = mediaHeaderComponentController;