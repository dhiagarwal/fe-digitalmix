'use strict';
var template = require('./media.header.component.jade');
var controller = require('./media.header.component.controller');

var mediaHeaderComponent = {
  templateUrl: template,
  controller: controller,
  controllerAs: 'ctrl',
  replace: true,
  bindings: {
				listItem: "=",
				extraClass: "@"
			}
};

module.exports = mediaHeaderComponent;
