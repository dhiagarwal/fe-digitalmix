'use strict';

function mediaFavoriteComponentController() {

	var self = this;

	self.isShown = false;
	
	this.toggle = function () {
	      self.isShown = !self.isShown
	}

};

//mediaCenterComponentController.$inject = ['mediaCentercomponentService'];

module.exports = mediaFavoriteComponentController;
