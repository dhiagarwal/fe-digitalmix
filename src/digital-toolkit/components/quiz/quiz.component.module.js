'use strict';
var angular = require('angular');
var uiRouter = require('angular-ui-router');
var quizComponent = require('./quiz.component');
var quizComponentService = require('./quiz.component.service');
var scrollToTopWithoutClick = require('../../../../src/__shared/scroll-to-top-without-click/scroll-to-top-without-click.module');

var quizComponentModule = angular.module('quizComponentModule', [uiRouter, scrollToTopWithoutClick.name])
                  .component('quizcomponent', quizComponent)
                  .service('quizComponentService', quizComponentService)
                  
import Styles from './quiz.component.scss';                      

module.exports = quizComponentModule;
