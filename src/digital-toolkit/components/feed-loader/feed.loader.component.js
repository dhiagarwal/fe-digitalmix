var template =require('./feed.loader.component.jade');
var controller =require('./feed.loader.component.controller');

var feedLoaderComponent =  {
	transclude: true,
    bindings:{
    },
    templateUrl: template,
    controller: controller,
    controllerAs: 'quizIntroCtrl'  
};

module.exports= feedLoaderComponent;
