'use strict';

feedLoaderService.$inject = ['$http','$q'];

function feedLoaderService($http,$q) {
  
  this.$http = $http;
  this.$q = $q;

};

// feedLoaderService.prototype.verifyCredentials = function(data){
// 	var deferred = this.$q.defer();
// 	this.$http.post("", data)
// 		.success(deferred.resolve(data))
// 		.error(deferred.reject(data));

// 		return deferred.promise;
// };

module.exports = feedLoaderService;