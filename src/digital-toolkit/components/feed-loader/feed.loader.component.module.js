var angular =require('angular');
var feedLoaderComponent =require('./feed.loader.component');
var feedLoaderService = require('./feed.loader.component.service');


var feedLoaderModule = angular.module('feedLoaderModule', [
])
.component('feedLoaderComponent', feedLoaderComponent)
.service('feedLoaderService', feedLoaderService);

import Styles from './feed.loader.component.scss';

module.exports= feedLoaderModule;