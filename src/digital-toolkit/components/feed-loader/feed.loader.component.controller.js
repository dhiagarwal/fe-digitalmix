'use strict';

feedLoaderController.$inject = ["feedLoaderService" , "$scope", "$uibModalInstance", "credentials"];

function feedLoaderController(feedLoaderService, $scope, $uibModalInstance, credentials) {
    var self = this;
    this.imageUrl = "../../../../src/assets/images/css/quiz_calculation_animation2.gif";
    this.textname="ADAAANAAAAN AQUAAAAAAAAAB";
    this.$uibModalInstance = $uibModalInstance;
    this.cancel = function() {
        this.$uibModalInstance.dismiss();
    };
};

module.exports = feedLoaderController;
