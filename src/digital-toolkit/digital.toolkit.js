var angular = require('angular');
var QuizViewer = require('./pages/quiz/quiz.viewer');
var MediaCenterViewer = require('./pages/media-center/media.center.viewer');
var QuizIntroViewer = require('./pages/quiz-intro/quiz.intro.viewer');
var BrandedStory = require('./pages/my-branded-story/my.branded.story.viewer');
var GenericTemplateViewer = require('./pages/generic-template/generic.template.viewer');
// var BrandedStoryInstructionViewer = require('./pages/my-branded-story/my.branded.story.instruction');

var digitalToolkitModule = angular.module('app.digital.toolkit', [
  QuizViewer.name,
  MediaCenterViewer.name,
  QuizIntroViewer.name,
  BrandedStory.name,
  GenericTemplateViewer.name
  // BrandedStoryInstructionViewer.name
]);

module.exports = digitalToolkitModule;
